## Esercizi Input/Output

### Fonte: Esercizi Svolti In Aula

**Esercizio 5:**

Un processo richiede l'utilizzo della CPU ogni $0.5 sec$. Si supponga che la CPU risponda
immediatamente alla richiesta e impieghi $100 msec$ per processarla. Inoltre, sia $P W$ il consumo elettrico della CPU e si supponga che il consumo elettrico della CPU in stato idle sia nullo (cioè, il consumo elettrico statico è zero).

Rispondere alle seguenti domande:

1. Se il voltaggio $V$ della CPU è ridotto a $V/n$, qual è il valore ottimale di $n$?

2. Qual è il risparmio energetico che si ottiene in $1 sec$, riducendo il voltaggio della CPU come al punto (1) rispetto a non ridurlo? Si assuma che il processo invii la prima richiesta al tempo 0

**Risposta:**

_Punto 1:_

Quale valore di $n$ tale che la CPU riesca a finire di eseguire il processo in $0.5sec$, ovvero  $500msec$, evitando l'idle di $400msec$ (ottenuto facendo $500msec-100msec$)?

Dobbiamo allungare il tempo totale a $500msec$, togliendo quell'idle di 400msec.

$$
n_{sol} = floor(\frac{T_{tot}}{T_{exec}}) = floor(\frac{500}{100}) = 5
$$

Ricordati che $n_{sol}$ deve essere intero.

_Punto 2:_

Supponendo che il processo invii la prima richiesta al tempo 0, in un secondo la CPU riceve 3 richieste (la prima al tempo 0, la seconda al tempo 0.5 e la terza al tempo 1); la terza richiesta però non è da considerare ai fini dell'esercizio perché verrebbe processata oltre il tempo di riferimento di 1 sec definito nel punto (2). Quindi, dato che in 1 sec la CPU processa 2 richieste inviate dal processo, occorre moltiplicare per 2 il consumo elettrico.

Il consumo energetico $E1$ con la CPU a pieno regime in $1sec$ è:

$$
E1 = (P*0.1+0*(0.1+0.4))*2=0.2*PJ
$$

Dove $(P*0.1+0*(0.1+0.4))$ è il consumo della CPU per processare la prima richiesta e per l'attesa in stato idle della seconda. Viene moltiplicato per $2$ perché questo avviene per entrambe le richieste. 

Analizzando la formula possiamo vedere:

- il consumo energetico dinamico dovuto al processamento: consumo elettrico
  dinamico * tempo processamento = P * 0.1;

- il consumo energetico statico (che sarà uguale a 0 in quanto l'esercizio assume un consumo elettrico statico pari a 0): consumo elettrico statico * (tempo processamento+tempo idle) = 0*(0.1+0.4).

### Fonte: Esercizi Extra

**Esercizio 1.8:**

Si supponga di effettuare le seguenti operazioni in sequenza su un sistema RAID livello 5 costituito da 5 dischi identici (inizialmente vuoti) e con blocchi (strip) da 1 byte:

1. Scrittura della sequenza di byte:
   01000101, 00000110, 10110100, 11101101, 11000111, 10000101,
   01110111, 01010101

2. Lettura secondo e settimo byte di dati

3. Modifica del terzo byte di dati da 10110100 a 01001011

Per ogni punto, s’illustrino le operazioni compiute dal sistema, evidenziando quante READ e quante WRscrittura sui dischi è da sinisITE vengono effettuate, e quante di queste sono fatte in parallelo.

NOTA:

- Per ognuno dei suddetti punti, il controller RAID riceve i comandi di scrittura/lettura di byte come un'unica richiesta.

- L'ordine di scrittura sui dischi è da sinistra verso destra, dall'alto verso il basso.

- Racchiudere ogni blocco di parità tra parentesi tonde.

- Il sistema RAID non è dotato di dischi hot-spare.

**Risposta:**

**Punto 1**

Calcolo della strip di parità per la prima scrittura.

$$
01000101\oplus \\ 00000110\oplus \\ 10110100\oplus\\ 11101101\oplus\\ ------\\00011010
$$

| Disco 0  | Disco 1  | Disco 2  | Disco 3  | Disco 4    |
|:--------:|:--------:|:--------:|:--------:|:----------:|
| 01000101 | 00000110 | 10110100 | 11101101 | (00011010) |

Per svolgere questa operazione sono servite 5 WRITE, tutte in parallelo.

Calcolo della seconda strip di parità per la seconda scrittura.

$$
11000111\oplus\\ 10000101\oplus\\01110111\oplus\\ 01010101\oplus\\ ------\\01100000
$$

| Disco 0  | Disco 1  | Disco 2  | Disco 3    | Disco 4    |
|:--------:|:--------:|:--------:|:----------:|:----------:|
| 01000101 | 00000110 | 10110100 | 11101101   | (00011010) |
| 11000111 | 10000101 | 01110111 | (01100000) | 01010101   |

Per svolgere questa operazione sono servite 5 WRITE, tutte in parallelo.

Complessivamente sono servite 10 WRITE, di cui in parallelo in gruppi da 5.

**Punto 2:**

La lettura del secondo e del settimo byte di dati può avvenire, perché le strip necessarie si trovano su dischi diversi.

In particolare si trovano sul disco 2 e sul disco 4 .

| Disco 0  | Disco 1  | Disco 2                                               | Disco 3    | Disco 4                                               |
|:--------:|:--------:|:-----------------------------------------------------:|:----------:|:-----------------------------------------------------:|
| 01000101 | 00000110 | <mark style="background-color: green">10110100</mark> | 11101101   | (00011010)                                            |
| 11000111 | 10000101 | 01110111                                              | (01100000) | <mark style="background-color: green">01010101</mark> |

Sono necessarie 2 READ in parallelo.

**Punto 3:**

Dobbiamo decidere se utilizzare una additive o subtractive parity. In questo caso conviene utilizzare una subtractive parity, dato richiede 2 READ (vecchia strip di parità e il vecchio contenuto della strip che vogliamo modificare) e 2 WRITE (nuova strip di parità e la strip modificata).

La additive parity avrebbe richieste 3 READ (la strip 0, 1 e 3, ovvero tutte le strip escluse quella da modificare) e 2 WRITE (nuova strip di parità  e la strip modificata).

Applicazione della subtractive parity:

$$
10110100 \oplus\\ 01001011 \oplus\\ 00011010 \oplus\\ ------\\11100101
$$

Come già spiegato prima questa operazione necessità di 2 READ e 2 WRITE.

Il contenuto dei dischi è il seguente:

| Disco 0  | Disco 1  | Disco 2               | Disco 3    | Disco 4                 |
|:--------:|:--------:|:---------------------:|:----------:|:-----------------------:|
| 01000101 | 00000110 | <mark>01001011</mark> | 11101101   | <mark>(11100101)</mark> |
| 11000111 | 10000101 | 01110111              | (01100000) | 01010101                |

**Esercizio 1.11:**

Al driver del disco arrivano, nell'ordine specificato, le richieste di cilindri 11, 23, 21, 3, 41, 7, e 39.
Ogni operazione di seek tra due cilindri consecutivi (track-to-track seek time) impiega 6msec.
Specificare l'ordine di visita dei vari cilindri e il tempo totale di seek che si ottengono utilizzando i seguenti algoritmi:

1. First-Come, First-Served (FCFS)

2. Shortest-Seek First (SSF)

3. LOOK

4. C-LOOK

In tutti i casi, il braccio del disco è inizialmente posizionato sul cilindro 31.
Per gli algoritmi LOOK e varianti, la direzione iniziale è DOWN.

**Risposta:**

FCFS:

Ordine di visita: 11, 23, 21, 3, 41, 7, 39.

Numero totale di cilindri: $|31-11|+|11-23|+|23-21|+|21-3|+|3-41|+|41-7|+|7-39| = 156$

Tempo totale di seek: $156 \times 6msec = 936msec$ 

SSF:

Ordine di visita: 39, 41, 23, 21, 11, 7, 3.

*Anche 23, 21, 11, 7, 3, 39, 41 andava bene, entrambi inizialmente hanno una differenza di 8 cilindri.*

Numero totale di cilindri: $|31-39|+|39-41|+|41-23|+|23-21|+|21-11|+|11-7|+|7-3| = 48$

Tempo totale di seek: $48 \times 6msec = 288msec$

LOOK (down):

Ordine di visita: 23, 21, 11, 7, 3, 39, 41.

Numero totale di cilindri: $|31-23|+|23-21|+|21-11|+|11-7|+|7-3|+|3-39|+|39-41| = 66$

Tempo totale di seek: $66\times6 = 396msec$

C-LOOK (down):

Ordine di visita: 23, 21, 11, 7, 3, 41, 39.

Numero totale di cilindri: $|31-23|+|23-21|+|21-11|+|11-7|+|7-3|+|3-41|+|41-39|= 68$

Tempo totale di seek: $68\times6 = 408$

**Esercizio 1.14:**

Si supponga di voler attuare una politica di risparmio del consumo energetico basata sull’utilizzo del disco tale per cui in base a una stima $T_{EST}$ della durata di utilizzo del disco si decide se lasciare i suoi piatti in movimento (stato “active”) o arrestarne la rotazione (stato “sleep”).

In particolare, si consideri un disco con le seguenti caratteristiche:

- consumo elettrico nello stato “active” $P_w=6 Watt$,

- consumo elettrico nello stato “sleep” $P_s=2 Watt$,

- tempo per passare da “active” a “sleep” $T_{sd}=10 sec$,

- energia elettrica consumata nel passaggio da “active” a “sleep” $E_{sd}=26 Joule$,

- tempo per passare da “sleep” a “active” $T_{wu}=5 sec$,

- energia elettrica consumata nel passaggio da “sleep” a “active” $E_{wu}=60 Joule$.

Calcolare il valore Td tale per cui per $T_{EST} > T_d$ diventi vantaggioso porre il disco da “active” a “sleep”.

**Risposta:**

Sapendo che:

$$
E_{sd} + P_s \times(T_d-T_{sd}-T_{wu})+E_{wu} = P_w \times T_{d}
$$

E sapendo che $T_{EST} > T_d$, possiamo utilizzare la formula del break-even point (che non è altro che una formula inversa) ed ottenere:

$$
T_d = \frac{(E_{sd}+E_{wu}-P_s\times(T_{sd}+T_{wu}))}{P_w-P_s}
$$

Sostituendo con i valori corretti otteniamo:

$$
T_d = \frac{26+60-2\times(10+5)}{6-2} = 14sec
$$

Quindi se il $T_{EST}$, ovvero una stima della durata di utilizzo del disco è:

- $T_{EST} > 14sec \rightarrow$ conviene passare da active a idle;

- $T_{EST} < 14sec \rightarrow$ non conviene passare da active a idle;

- $T_{EST} = 14sec \rightarrow$ irrilevante.

### Fonte: Esercizi Slide:

**Slide 179 (Input/Output):**

Avendo un disco con i seguenti valori:

$N_C=65535$, $N_H=16$, $N_S=63$.

Convertire da CHS ad LBA il seguente indirizzo.

$c = 32$, $h = 0$, $s=1$.

Usando la seguente formula per la conversione:

$$
LBA= (c\times N_H + h) \times N_S + (s -1)
$$

Sostituendo i valori otteniamo:

$$
LBA = (32 \times 16+0)\times 63 + (1-1) = 32256
$$

Ora proviamo la conversione a ritroso, quindi da LBA a CHS.

$$
c = LBA \text{ div }(N_H\times N_S)\\
h = (LBA \text{ div }N_S)\bmod N_H\\
s = (LBA \bmod N_S)+1
$$

Sostituiamo usando l'indirizzo LBA 32256 ed otteniamo:

$$
c = 32256 \text{ div }(16\times 63) = 32\\
h = (32256 \text{ div }63)\bmod 16 = 0\\
s = (32256 \bmod 63)+1 = 1
$$

**Slide 190 (Input/Output):**

Rotational speed $R: 7200 RPM$;
Avg seek time $T_{S,avg}: 9 msec$;
Avg sectors/track $N_{S,avg}: 400$.

Determine:

- Avg rotational latency $T_{R,avg}$ (in msec)?

- Avg transfer time $T_{T,avg}$ (in msec)?

- Avg access time $T_{A,avg}$ (in msec)?

$$
T_{R,max} = \frac{1}{\text{rotational speed}}\\
T_{R,avg} = \frac{T_{R,max}}{2}\\

$$

Sostiuendo i valori ed unendo le formule otteniamo:

$$
T_{R,avg} =\frac{1}{2} \times \frac{60sec}{7200RPM} \times 1000msec = 4.17msec 
$$

Per il $T_{T,avg}$ abbiamo:

$$
T_{T,avg} = \frac{1}{R} \times \frac{1}{N_{S_,avg}} \times 60sec \times 1000msec/sec
$$

$$
T_{T,avg} = \frac{60sec}{7200RPM} \times \frac{1}{400} \times 1000msec \approx 0.02
$$

Per il $T_{T,avg}$ abbiamo:

$$
T_{A,avg} = T_{S,avg} + T_{R,avg} + T_{T,avg}
$$

$$
T_{A,avg} = 9msec+4.17msec+0.02msec = 13.19msec
$$
