# Formulario Sistemi Operativi 2

## LBA e CHS:

> Sempre arrotondare per difetto.

### Da LBA a CHS:

1. Elevare in potenze di due i bit forniti, $2^{\text{Bit Forniti}}$;

2. Calcolare C, H ed S con le seguenti formule:
   
   $C = LBA/(\text{Num Settori} * \text{Num Testine})$
   
   $H = (LBA/\text{Num Settori})\text{ mod Num Testine}$
   
   $S = (LBA \text{ mod Num Settori}) +1$

### Da CHS a LBA:

1. Elevare in potenze di due i bit forniti, $2^{\text{Bit Forniti}}$;

2. Calcolare LBA con la seguente formula:
   $LBA = (C* \text{Num Testine +} H) * \text{Num Settori} +(S-1)$;

---

## Cylinder Skew:

> Sempre arrontondamento per eccesso.

1. Calcolare latenza rotazionale max [secondi]: $60/RPM$;

2. Calcolare tempo su singolo settore [secondi]: $\text{Latenza Rotazionale Max} * \text{Numero Settori per Traccia}$

3. Calcolare il Cylinder Skew [settori]: $\text{Tempo Seek} / \text{Tempo singolo settore}$

---

## Capacità del disco:

Capacità del disco [bytes]: 

$\text{N Testine} * \text{N Cilindri} * \text{N Settori} * \text{Dim Settore}$

---

## Sector Interleaving:

> Sempre arrontondamento per eccesso.

1. Calcolare la latenza rotazionale max [secondi]: $60/RPM$

2. Calcolare il tempo sul singolo settore [secondi]: $\text{Lat Rot Max} / \text{Num Settori per traccia}$;

3. Calcolare il sector interleaving [settori]: 
   
   $\text{Tempo Transferimento dati} / \text{Tempo singolo settore}$ 

---

### Tasso Trasferimento:

Tasso Trasferimento [Byte/secondo]: 

$\text{Num Settore per traccia} * \text{Dimensione Settore in Byte} * (\text{RPM}/\text{60 Secondi})$

---

## Ordine Visita Cilindri:

Ricordati che il LOOK fa _due passate_, invece C-LOOK torna immediatamente all'inizio/fine senza visitare i cilindri.

Numero totale di cilindri [cilindri]: $|\text{Cil}_1 - \text{Cil}_2|+|\text{Cil}_2 - \text{Cil}_3| ...
$

Seek Time [secondi]: $\text{Num tot cilindri} * \text{Tempo operazione di seek}$;

---

## RAID:

### RAID 3:

Due tipe di parità possibile:

- Even Parity: il numero di bit ad 1 deve essere pari;

- Odd Parity: il numero di bit ad 1 deve essere dispari.

### RAID 4 e 5:

**Dimostrazione  recupero Strip:**
Devi dimostrare come recuperari i dati in caso di disco rotto.
Ti viene dato una cosa del genere:

$S[3] = S[0] \oplus S[1] \oplus S[2] \oplus S[4] \oplus S[5] \oplus S[6] \oplus P$

Te devi partire da questo:
$P = \oplus \text{ tra tutte le strip presenti sopra senza Parity}$

Poi aggiungi a sinistra e destra la strip che devi ricavare, in questo caso $S[3]$, quindi ottieni:

$S[3] \oplus P = S[0] \oplus S[1] \oplus S[2] \oplus S[3] \oplus S[4] \oplus S[5] \oplus S[6] \oplus S[3]$

Ora semplifica i due $S[3]$ di destra perché lo XOR tra due strip uguali fa zero.

Fai lo stesso passaggio ma con P, semplifichi le due P a sinistra ed ottieni la tesi

**Additive Parity (RAID 4 e 5):**

Additive Parity: $\text{Old Strip}_0 \oplus \text{Old Strip}_1 \oplus \text{New Strip}_2 \oplus ...$

Fai XOR tra tutte le strip non modificate + XOR con strip nuova, il risultato è la parity strip.

**Subtrative Parity (RAID 4 e 5):**

Subtractive Parity: $\text{Old Strip}_0 \oplus \text{New Strip}_0 \oplus \text{Old Parity Strip}$

Fai XOR dei valori vecchi delle strip che stanno per essere modificate con i valori nuovi delle strip, in più ci aggiungi la vecchia parity, il risultato è la nuova parity strip.

**Trovare il punto di pareggio (RAID 4 e 5):**

Sappiamo che:

- $\text{Parità Additiva} = (n-1)-B$, dove $B$ sono le strip modificate;

- $\text{Parità Sottrattiva} = B +1$

Il Crossover point è dato da:

$\text{Parità Additiva} = \text{Parita Sottrattiva}$

$(n-1)-B = B + 1$

$2B = n-2$, che diventa $B = (n-2)/2$.

Se le strip modificate sono $> (n-2)/2$ allora conviene usare la parità additiva, se invece è $<$ allora conviene la sottrattiva. Se sono uguali è indifferente.

In entrambi i casi il numero di WRITE rimane lo stesso, cambia soltanto il numero di READ.

**Ricavare una strip danneggiata:**

Usi la additive parity con anche il valore della parity strip, quello che ottiene è il valore della strip danneggiata.

### RAID 6:

Usa due dischi per la parità, il resto è identico.

---

## Risparmio Energetico:

### Hard Disk:

1. Calcolare $T_d$ [secondi]: 
   $T_d = (E_{sd} + E_{wu}-P_s*(T_{wu}+T_{sd}))/(P_w-P_s)$

2. Controllare se:
   
   1. $T_u < T_d$ allora conviene passare dallo stato ASLEEP allo stato AWAKE;
   
   2. $T_u > T_d$ allora conviene passare dallo stato AWAKE lo stato ASLEEP;
   
   3. se uguali indifferente.

### CPU:

$T_r$ è ogni quanto tempo un processo richiede la CPU.

$T_p$ è quanto tempo ci mette la CPU a processarla.

$P_{dyn}$ è il consumo elettrico instantaneo della CPU.

$P_{static}$ è il consumo elettrico della CPU in stato idle.

1. Calcolare $n$:
   
   $n = T_r / T_p$

2. Dato $nreq$, con $nreq \not= n$ bisogna calcolare:
   $E_{nocut} = ((P_{dyn} * T_p)+(P_{static} * T_r))*nreq$
   
   $E_{cut} = nreq*((P_{dyn}/(n^2)) * T_p*n+(P_{static} * T_r))$
   
   $\Delta E = E_{nocut} - E_{cut}$
   
   $\Delta E \text{ percentuale} = (\Delta E / E_{nocut}) * 100$

---

## File Systems:

### Lista Concatenata:

1. Converti tutti i dati in potenze di due, ricordandoti di trasformare eventuali bit in byte;

2. Calcolare il numero di puntatori in un blocco [bytes]: $\text{Dim Partizione} / \text{Dim Blocco}$;

3. Calcolare lo spazio effettivo [bytes]: $\text{Dim Partizione} - \text{Numero puntatori}$.
   
   Se viene richiesto Overhead e Wasted Space continua sotto.

4. Calcolare Dimensione dati dentro blocco [bytes]: $\text{Dim blocchi}- \text{Dim Puntatore}$;

5. Calcolare Numero di nodi necessari per salvare il file F [blocchi]: $F/\text{Dim Dati dentro blocco}$;

6. Calcolare Wasted Space [bytes]: $(\text{Dim Dati dentro blocco} * \text{Numero nodi necessari})-\text{File }F$;

7. Calcolare Wasted Space [percentuale]:
   
   $(\text{Wasted Space in Bytes}/(\text{Dim Dati dentro blocco * numero nodi necessari}))*100$

8. Calcolare l'overhead [percentuale]: $(\text{Dim Puntatori}/(\text{Dim Blocco}*\text{Num Blocchi} )) * 100$;

### FAT:

1. Converti tutti i dati in potenze di due, ricordandoti di trasformare eventuali bit in byte;

2. Calcolare il numero di entries FAT [bytes]: $\text{Dim Partizione} / \text{Dim Blocco}$;

3. Calcolare la dimensione della FAT [bytes]: $\text{Numero di Entries Fat} * \text{Dim Puntatore}$;

4. Calcolare la dimensione massima di un file [bytes]: $\text{Dim Partizione} - \text{Dim Fat}$,
   se l'esercizio richiede la dimensione di un file in blocchi allora fai:
   $\frac{\text{Dim Partizione}}{\text{Dim Blocco}}-\frac{\text{Dim Fat}}{\text{Dim Blocco}}$
   
   Se viene richiesto Overhead e Wasted Space continua sotto.

5. Wasted Space [bytes]: $\text{Dim Fat}-\text{Dim File F}$

6. Wasted Space [percentuale]: $(\text{Wasted Space in Bytes}/\text{Dim Partizione}) * 100$

### I-Node:

1. Converti tutti i dati in potenze di due, ricordandoti di trasformare eventuali bit in byte;

2. Calcolare il numero di puntatori I-Node in un blocco [bytes]: $\text{Dim Blocco} / \text{Dim Puntatore}$;

3. Calcolare la Dim File Possibile [bytes]:
   $(\text{Num Puntatori Diretti} + \text{Num Puntatori in un blocco} + \text{Num Puntatori in un blocco}^2 ...) * \text{Dim Blocco}$
   La formula cambia in base a quanti puntatori indirtti ci sono, per esempio se c'è solo un puntatore indiretto singolo ti fermi a $\text{Num Puntatori in un blocco}$, sennò vai avanti, se è triplo aggiungi $+ \text{Num Puntatori in un blocco}^3$.
   
   Se l'esercizio richiede la dimensione in blocchi allora fai il risultato del punto 3 diviso $\text{Dim Blocco}$.

---

## FSCK:

### Consistenza blocchi:

> 2 tabelle, in uso e liberi

- Entrambi 0 = missing block, FSCK lo assegna alla struttura dati dei blocchi liberi e aggiorna il contatore

- Entrambi 1 = blocco sia in uso che libero, FSCK può scegliere se rimuoverlo dai blocchi allocati o dai blocchi liberi

- Valore > 1 nei blocchi liberi = Blocco libero duplicato, FSCK ricostruisce la struttura dati dei blocchi liberi e aggiorna il contatore

- Valore > 1 nei blocchi in uso = Blocco in uso duplicato, è l’inconsistenza peggiore, FSCK alloca n-1 blocchi (da quelli liberi), ci copia i dati del blocco inconsistente e fa puntare n-1 file ai nuovi blocchi allocati, poi aggiorna le tabelle.

### Consistenza su file:

> Solo una tabella del file in uso, ammessi valori > 1

- Se il Link counter > Counter file in uso, ci può essere spreco di spazio se tutti gli utenti eliminano quel file perché il sistema operativo non libera il blocco, FSCK mette il link counter = al Counter dei file in uso

- Se il Link counter < counter file in uso, ci può essere un problema di sicurezza, quando quell’i-node verrà rimosso una directory gli punterà ancora e quindi quando verrà riallocato quella directory avrà accesso a dei dati che magari non dovevano essere condivisi, FSCK setta il link counter = al counter della tabella dei file in uso.

---

## Dump Unix:

Formato da 4 fasi:

1. Prima fase:
   
   Visita dell’albero (partendo dalla radice) per contrassegnare i file modificati e tutte le directory (anche quelle che non contengono file modificati) che s’incontrano lungo l’attraversamento.

2. Seconda fase:
   
   Visita dell’albero per lasciare contrassegnate solo quelle directory che contengono file modificati (o che portano a file modificati) e i file modificati.

3. Terza fase:
   
   Si effettua il dump delle directory che sono rimaste contrassegnate, e si deselezionano le directory di cui si è fatto il dump.
   Di fatto restano selezionati solo i file modificati.

4. Quarta fase:
   
   si effettua il dump dei file modificati (cioè, quelli contrassegnati come 3, 6, 9, 12) e si deselezionano.
   Alla fine di questa operazione la tabella di dump è completamente vuota.

---

## Backup:

Quando nella domanda dice "Elencare i backup usati per il ripristino di file fino al giorno $x$ (incluso)" vuole sapere quali backup hai usato per creare il backup del giorno $x + 1$.

### Backup Differenziale:

- Quando Backup Full: prendi File Modificati di quel giorno + Backup differenziale del giorno precedente + Backup Full precedente;

- Quando Backup Differenziale: prendi File Modificati di quel giorno + Backup differenziale del giorno precedente; Non prendi MAI un Backup Full.

Se si danneggia un backup è comunque possibile recuperarlo usando il backup del giorno successivo a quello del danneggiamento + Backup Full più recente.

### Backup Incrementale:

- Quando Backup Full: prendi i file modificati di quel giorno + prendi tutti i backup incrementali precedenti + Backup Full precedente;

- Quando Backup Incrementale: prendi File Modificati di quel giorno.

Se si danneggia un backup è recuperabile fino al giorno precedente al guasto, tutti i giorni successivi sono persi (a meno che non ci sia un backup full).

---

## Links:

I File hanno sempre N/D per link valido.
Se viene spostato il link non cambia nulla per entrmabi i casi.
Ogni volta che viene cancellato un file il suo link count viene decrementato di 1.

### Hard Links:

Il Link ha lo stesso proprietario e i-node del file.

Quando si crea un Hard-Link si incrementa di uno il valore di Link Count sia per il file che per il link (hanno sempre link count uguali il file ed il link).

Se il file viene spostato l'Hard-Link rimane valido.
Un Hard-Link non diventa mai non valido.

### Soft Links:

Il numero di I-Node deve essere scelto tra gli I-Node disponibili.
Il Link ha come proprietario chi crea il link.
Appena viene creato un Soft Link ha come Link Count 1, il Link Count del file alla quale punta rimane invariato

Se viene spostato o eliminato il file il Soft-Link non è più valido.
