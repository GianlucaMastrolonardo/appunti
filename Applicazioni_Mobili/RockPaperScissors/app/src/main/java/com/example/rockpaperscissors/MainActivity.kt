package com.example.rockpaperscissors

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {
    private fun parseInput(inputText: String): Int {
        if (inputText.lowercase().contains("paper")) return 0;
        if (inputText.lowercase().contains("rock")) return 1;
        if (inputText.lowercase().contains("scissor")) return 2;
        throw IllegalArgumentException("Invalid input!")
    }

    private fun setChooseText(appInput: Int): String {
        if (appInput == 0) return "Paper"
        if (appInput == 1) return "Rock"
        if (appInput == 2) return "Scissors"
        throw IllegalArgumentException("Invalid input!")
    }

    private fun chooseWinner(userInput: Int, appInput: Int): Int {
        //0: Draw, 1: UserWin, 2: AppWin
        if (userInput == appInput) return 0;
        return if ((userInput == 0 && appInput == 2) || (userInput == 1 && appInput == 0) || (userInput == 2 && appInput == 1)) 2;
        else 1;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        var resultText: TextView = findViewById(R.id.textResult);
        resultText.text = ""

        var guessText: TextView = findViewById(R.id.textGuess);
        guessText.text = ""


        val radioGroup: RadioGroup = findViewById(R.id.radioGroupGame)
        var checkedButton: RadioButton? = null

        var userInput: Int = -1

        radioGroup.setOnCheckedChangeListener() { _: RadioGroup, id: Int ->
            checkedButton = findViewById(id)
            Log.i(
                "SelectedRadioButton",
                checkedButton?.text.toString()
            )
            userInput = parseInput(checkedButton?.text.toString())
            Log.i("SelectedRadioButtonValue", userInput.toString())
        }

        val playBtn: Button = findViewById(R.id.btnPlay)

        playBtn.setOnClickListener() {
            //Generate Rock, Paper or Scissors random
            val appInput = (0..2).random()
            Log.i("AppInput", appInput.toString())

            val gameResult: Int = chooseWinner(userInput, appInput)
            guessText.text = setChooseText(appInput)

            if (gameResult == 0) {
                resultText.text = getString(R.string.outputTextDraw)
            } else if (gameResult == 1) {
                resultText.text = getString(R.string.outputTextWinner)
            } else if (gameResult == 2) {
                resultText.text = getString(R.string.outputTextLoser)
            }
        }

        Toast.makeText(this, "Ciao", Toast.LENGTH_SHORT).show()
    }


}