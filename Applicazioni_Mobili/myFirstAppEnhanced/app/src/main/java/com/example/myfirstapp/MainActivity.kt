package com.example.myfirstapp

import android.graphics.Color
import android.os.Bundle
import android.widget.TextView
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet.Constraint
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import java.util.Date


class MainActivity : AppCompatActivity() {
    fun showTime(){
        val timeMsg = findViewById<TextView>(R.id.timestampMsg);
        timeMsg.text = Date().toString()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val helloMsg = findViewById<TextView>(R.id.welcomeMsg)
        helloMsg.setTextColor(Color.parseColor("#FF00FF"))

        val rootView = findViewById<ConstraintLayout>(R.id.main)
        rootView.setOnClickListener({showTime()})
    }
}