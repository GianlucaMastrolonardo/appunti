package com.example.intentfilter

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val startBrowserView = findViewById<Button>(R.id.startBrowserView)
        startBrowserView.setOnClickListener {
            val intentStartBrowserView =
                Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://uniupo.it"))
            startActivity(intentStartBrowserView)
        }

        val startBrowserLaunch = findViewById<Button>(R.id.startBrowserLaunch)
        startBrowserLaunch.setOnClickListener {
            val intentStartBrowserLaunch =
                Intent("com.example.intentdemo.LAUNCH", Uri.parse("http://uniupo.it"))
            startActivity(intentStartBrowserLaunch)
        }

        val startException = findViewById<Button>(R.id.startException)
        startException.setOnClickListener {
            val intentException =
                Intent("com.example.intentdemo.LAUNCH", Uri.parse("https://uniupo.it"))
            startActivity(intentException)
        }
    }
}