package com.example.intentfilter

import android.app.Activity
import android.os.Bundle
import android.widget.TextView

class CustomActivity : Activity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.second_view)
        val label = findViewById(R.id.value) as TextView

        val url = intent.data
        label.text = url?.toString()


    }
}