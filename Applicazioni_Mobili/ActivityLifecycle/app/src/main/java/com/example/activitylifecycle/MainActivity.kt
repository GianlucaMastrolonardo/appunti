package com.example.activitylifecycle

import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {
    lateinit var msgText: TextView;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        msgText = findViewById<TextView>(R.id.msg);
        Toast.makeText(this, "OnCreate Called", Toast.LENGTH_LONG).show();
        msgText.text = "Hello"
    }

    override fun onStart() {
        super.onStart()
        Toast.makeText(this, "OnStart Called", Toast.LENGTH_LONG).show();
    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "OnResume Called", Toast.LENGTH_LONG).show();
    }

    override fun onRestart() {
        super.onRestart()
        Toast.makeText(this, "OnRestart Called", Toast.LENGTH_LONG).show();
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, "OnDestroy Called", Toast.LENGTH_LONG).show();
    }

    override fun onPause() {
        super.onPause()
        msgText.text = "I was PAUSED!!!!"
    }

    override fun onStop() {
        super.onStop()
        msgText.text = "I was STOPPED!!!!"
    }
}