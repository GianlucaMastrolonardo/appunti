package com.example.widgetsexamples

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {
    private fun startBrowser() {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://uniupo.it"))
        try {
            startActivity(browserIntent)
        } catch (e: ActivityNotFoundException) {
            Log.e("Browser", "Can't Start Browser")
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val selection: TextView = findViewById(R.id.SelectedStringValue)
        val items: Array<String> = arrayOf(
            "Ganimenede",
            "Europa",
            "Io",
            "Callisto",
            "Terra",
            "Saturno",
            "Nettuno",
            "Urano",
            "Giove",
            "Venere",
            "Marte"
        )

        val listView: ListView = findViewById(R.id.ItemsListView)
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, items)
        listView.adapter = arrayAdapter

        listView.setOnItemClickListener { parent, _, position, _ -> selection.setText(items[position]) }
    }
}