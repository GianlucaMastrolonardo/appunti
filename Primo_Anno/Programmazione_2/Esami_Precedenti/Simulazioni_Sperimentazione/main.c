#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// struct dati_prodotto
typedef struct dati_prodotto
{
    int codice_prodotto;
    char azienda_produttrice[40];
    char nome_prodotto[40];
    float prezzo_listino;
    int quantita;
} dati_prodotto;

typedef dati_prodotto DATA;

typedef struct linked_list
{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

LINK crealista(FILE *fPtr)
{
    DATA x;
    int isRead = fscanf(fPtr, "%d %s %s %f %d\n", &x.codice_prodotto, x.azienda_produttrice, x.nome_prodotto, &x.prezzo_listino, &x.quantita);
    if (x.codice_prodotto < 0 || isRead == EOF)
    {
        return NULL;
    }
    else
    {
        LINK head = newnode();
        head->d = x;
        head->next = NULL;
        LINK tail = head;
        LINK p;
        int isEnd;
        // printf("%s\n", head->d.azienda_produttrice);

        fscanf(fPtr, "%d %s %s %f %d\n", &x.codice_prodotto, x.azienda_produttrice, x.nome_prodotto, &x.prezzo_listino, &x.quantita);
        while (x.codice_prodotto >= 0 && isEnd != EOF)
        {
            p = newnode();
            p->d = x;
            p->next = NULL;
            tail->next = p;
            tail = p;
            isEnd = fscanf(fPtr, "%d %s %s %f %d\n", &x.codice_prodotto, x.azienda_produttrice, x.nome_prodotto, &x.prezzo_listino, &x.quantita);
        }
        return head;
    }
}

/*
Codice prodotto: [CODICE PRODOTTO]
Nome prodotto: [NOME_PRODOTTO]
Prodotto da: [AZIENDA_PRODUTTRICE]
Quantità: [QUANTITA’]
Prezzo: [PREZZO_LISTINO]
*/

void stampalista_ric(LINK head, FILE *fPtr)
{
    if (head != NULL)
    {
        fprintf(fPtr, "Codice prodotto: %d\n", head->d.codice_prodotto);
        fprintf(fPtr, "Nome prodotto: %s\n", head->d.nome_prodotto);
        fprintf(fPtr, "Prodotto da: %s\n", head->d.azienda_produttrice);
        fprintf(fPtr, "Quantità: %d\n", head->d.quantita);
        fprintf(fPtr, "Prezzo: %0.2f\n", head->d.prezzo_listino);
        fprintf(fPtr, "-------------\n");
        stampalista_ric(head->next, fPtr);
    }
}

void stampalista(LINK head)
{
    
    char outputFilename[30];
    printf("Inserire nome del file di output: ");
    scanf("%s", outputFilename);

    FILE *fPtr = fopen(outputFilename, "w");
    if (fPtr != NULL)
    {
        stampalista_ric(head, fPtr);
    }
    else
    {
        printf("Errore Creazione File Output! Chiusura programma!");
        exit(1);
    }
}

void debug_printList(LINK list)
{
    while (list != NULL)
    {
        //printf("-------------\n"); per maggiore leggibilità rimuovere commento
        printf("Codice prodotto: %d\n", list->d.codice_prodotto);
        printf("Nome prodotto: %s\n", list->d.nome_prodotto);
        printf("Prodotto da: %s\n", list->d.azienda_produttrice);
        printf("Quantità: %d\n", list->d.quantita);
        printf("Prezzo: %0.2f\n", list->d.prezzo_listino);

        list = list->next;
    }
}

float calcolaval(LINK l1)
{
    float cnt = 0;
    while (l1 != NULL)
    {
        cnt = cnt + (l1->d.prezzo_listino * l1->d.quantita);
        l1= l1 -> next;
    }
    return cnt;
}

/*
Creare una funzione ricorsiva che genera una nuova lista contenente tutti i prodotti di un
certo fornitore, inserito dall’utente da tastiera (la lista creata ha nodi nuovi, copia dei
corrispondenti nodi nella lista di origine che non deve essere modificata).
LINK listaforni(LINK L1, char *fornitore)
*/

LINK listaforni (LINK l1, char *fornitore){
    LINK p;
    if(l1 == NULL){
        return NULL;
    }
    if(strcmp(l1->d.azienda_produttrice, fornitore) == 0){
        p = newnode();
        p->d = l1->d;
        p -> next = listaforni(l1->next, fornitore);
        return p;
    }else{
        return listaforni(l1->next, fornitore);
    }
}

int main()
{

    FILE *fPtr = fopen("fileProdotti.txt", "r");
    if (fPtr == NULL)
    {
        printf("Errore nell'apertura del File, chiusura programma\n");
        exit(1);
    }

    LINK myList = crealista(fPtr);
    fclose(fPtr);

    //debug_printList(myList);

    stampalista(myList);

    printf("\nValore: %f\n", calcolaval(myList));


    char nomericerca[50];
    printf("Inserire nome dell'azienda produttrice: ");
    scanf("%s", nomericerca);
    LINK forniList = listaforni(myList, nomericerca);
    //debug_printList(forniList);
    stampalista(forniList);


    return 0;
}