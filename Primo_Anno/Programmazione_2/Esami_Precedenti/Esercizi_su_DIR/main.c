// Compito 24 luglio 2020

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef int DATA;

typedef struct linked_list
{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

int randomNumber()
{
    return rand() % 10 - 2;
}

LINK debug_BuildList()
{
    LINK head = NULL;
    LINK current = NULL;

    // Aggiungi gli elementi alla lista
    int elements[] = {5, 5, 2, 1, 7, 1, 2, 2, 2, 1, 2, 1, 1, 1, 1, 8};
    int numElements = sizeof(elements) / sizeof(int);

    for (int i = 0; i < numElements; i++)
    {
        LINK newNode = newnode();
        newNode->d = elements[i];
        newNode->next = NULL;

        if (head == NULL)
        {
            head = newNode;
            current = newNode;
        }
        else
        {
            current->next = newNode;
            current = newNode;
        }
    }

    return head;
}

LINK buildList()
{
    int x = randomNumber();
    if (x >= 0)
    {
        LINK head = newnode();
        head->d = x;
        head->next = buildList();
        return head;
    }
    else
    {
        return NULL;
    }
}

void printList(LINK list)
{
    if (list != NULL)
    {
        printf("%d -> ", list->d);
        printList(list->next);
    }
    else
    {
        printf("NULL\n");
    }
}

// Es1
int biggestSequence(LINK list)
{
    if (list == NULL)
    {
        return 0;
    }
    int cnt = 1;
    int maxCnt = 1;
    int prec = list->d;
    list = list->next;

    while (list != NULL)
    {
        if (prec <= (list->d))
        {
            cnt++;

            if (maxCnt < cnt)
            {
                maxCnt = cnt;
            }
        }
        else
        {
            cnt = 1;
        }
        prec = list->d;
        list = list->next;
    }

    return maxCnt;
}

void deletePositionNode(int pos, int *currentpos, LINK *list)
{
    if (list != NULL)
    {
        if (pos == *currentpos)
        {
            if ((*list)->next == NULL)
            {
                LINK tmp = *list;
                *list = NULL;
                free(tmp);
            }
            else
            {
                LINK tmp = *list;
                *list = (*list)->next;
                free(tmp);
            }
        }
        deletePositionNode(pos, currentpos + 1, &(*list)->next);
    }
}

void delfirsts_rt(int x, LINK *lis)
{
    LINK p;
    if (*lis != NULL)
        if ((*lis)->d == x)
        {
            p = *lis;
            *lis = (*lis)->next;
            delfirsts_rt(x, lis);
            free(p);
        }
}

void delfirstElement_rt(LINK *list)
{
    LINK p;
    if (*list != NULL)
    {
        p = *list;
        *list = (*list)->next;
        free(p);
    }
}

int main(void)
{
    srand(time(NULL));

    // LINK myList = NULL;
    LINK myList = debug_BuildList();

    printList(myList);
    printf("Sequenza più grande: %d\n", biggestSequence(myList));

    // deletePositionNode(2, &currentPos, &myList);
    delfirstElement_rt(&myList);
    printList(myList);

    return 0;
}
