/*
Scrivere un programma che riceve in input il nome di un file di testo; il file di testo ricevuto contiene
una serie di numeri interi, uno per riga. Il programma deve leggere i dati nel file, salvarli in una lista,
visualizzarla, indicare i nodi presenti nella lista, calcolare la media dei valori contenuti nei nodi e
aggiungere in coda alla lista il valore zero.
Scrivere quindi le seguenti funzioni:
1) int lista_leggi_file: dato un puntatore al file e il riferimento a una lista, crea una lista con i dati
contenuti nel file e restituisce il numero di nodi creati; la funzione deve essere iterativa;
2) void lista_visualizza: visualizza la lista; la funzione deve essere ricorsiva;
3) void lista_media: data una lista, restituisce la media dei suoi elementi; la funzione deve essere
ricorsiva;
Suggerimento (1): il calcolo della media deve essere effettuato solo nel caso base (a fine ricorsione).
Suggerimento (2): si consiglia di creare una funzione come la seguente:

void lista_media(Node * lista, int * somma, float * media)
4) void lista_inserisci: data una lista per riferimento, aggiunge in coda il numero 0;
5) int main: per richiamare le funzioni di cui sopra e mostrare a video i risultati.
*/

#include <stdio.h>
#include <stdlib.h>

typedef int DATA;

typedef struct linked_list{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT * LINK;

LINK newnode(){
    return malloc(sizeof(ELEMENT));
}


int lista_leggi_file(FILE *fPtr, LINK *list){
    int nodeCounter = 0;
    LINK head, tail, p;
    if(feof(fPtr)){
        *list = NULL;
        return -1;
    }else{
        head = newnode();
        fscanf(fPtr, "%d\n", &(head->d));
        head -> next = NULL;
        tail = head;
        nodeCounter++;
        while(!feof(fPtr)){
            p = newnode();
            fscanf(fPtr, "%d\n", &p->d);
            p->next = NULL;
            tail -> next = p;
            tail = p;
            nodeCounter++;
        }
    }
    *list = head;
    return nodeCounter;
}

void visualizza_lista(LINK list){
    if (list != NULL){
        printf("%d\n", list ->d);
        visualizza_lista(list -> next);
    }
}

void lista_media(LINK list, int *sum, float *avg, int nodes){
    if(list == NULL){
        *avg = *sum / nodes;
    }else{
        *sum = *sum + list->d;
        lista_media(list->next, sum, avg, nodes);
    }
}

void aggiungi_zero(LINK *lis){
    if(*lis == NULL || (*lis)-> next == NULL){
        LINK p = newnode();
        p -> d = 0;
        p -> next = NULL;
        (*lis) -> next = p;
    }else{
        aggiungi_zero(&(*lis)->next);
    }
}

int main(){
    printf("Esame 8 Febbraio 2023\n");

    FILE *fPtr = fopen("file.txt", "r");
    if(fPtr == NULL){
        printf("Esplosione tra 3, 2, 1...");
        exit(1);
    }
    LINK myList = NULL;

    int listDimension = lista_leggi_file(fPtr, &myList);
    
    fclose(fPtr);
    printf("Stampa Lista: \n");
    visualizza_lista(myList);
    printf(">>> Dimensione Lista: %d\n", listDimension);


    int sum = 0;
    float avg = 0;
    
    lista_media(myList, &sum, &avg, listDimension);
    printf(">>> Media: %f\n", avg);

    
    aggiungi_zero(&myList);
    printf("Stampa Lista: \n");
    visualizza_lista(myList);
    
    return 0;
}