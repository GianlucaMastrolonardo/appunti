/*
//TODO(
Scrivere un programma che legge un file di testo contenente una serie di caratteri, uno per riga. Il
programma deve leggere i dati nel file, salvarli in una lista, visualizzarla, indicarne la lunghezza,
mostrare il numero di vocali e consonanti presenti nella lista e infine cancellare dalla lista i nodi
contenenti vocali.
)

Scrivere quindi le seguenti funzioni:
1) int lista_leggi_file: dato un puntatore al file e il riferimento a una lista, crea una lista con i dati
contenuti nel file e restituisce il numero di nodi creati; la funzione deve essere iterativa;
2) void lista_visualizza: visualizza la lista; la funzione deve essere ricorsiva;
3) void lista_conta: data la testa della lista, conta il numero di vocali e consonanti presenti nella lista;
la funzione deve essere ricorsiva;
Suggerimento: void lista_conta(Node * lista, int * n_vocali, int * n_consonanti);
4) void lista_cancella_vocali: data la testa della lista, cancella i nodi di tipo vocale; la funzione deve
essere iterativa.
5) int main: per richiamare le funzioni di cui sopra e mostrare a video i risultati.
*/

#include <stdio.h>
#include <stdlib.h>

typedef char DATA;

typedef struct linked_list{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode(){
    return malloc(sizeof(ELEMENT));
}

/*
int lista_leggi_file: dato un puntatore al file e il riferimento a una lista, crea una lista con i dati
contenuti nel file e restituisce il numero di nodi creati; la funzione deve essere iterativa;
*/

int lista_leggi_file(FILE *fPtr, LINK *list){
    int cnt = 1;
    char valore;
    if(fscanf(fPtr, "%c\n", &valore) == EOF){
        printf("Il file è vuoto\n");
        *list = NULL;
    }else{
        LINK head, tail, p;
        head = newnode();
        cnt++;
        head -> d = valore;
        head -> next = NULL;
        tail = head;
        while(!feof(fPtr)){
            fscanf(fPtr, "%c\n", &valore);
            cnt++;
            p = newnode();
            p -> d = valore;
            p -> next = NULL;
            tail -> next = p;
            tail = p;
        }
        *list = head;
    }
    return cnt;
}

/*
void lista_visualizza: visualizza la lista; la funzione deve essere ricorsiva;
*/

void lista_visualizza(LINK list){
    if(list != NULL){
        printf("%c -> ", list -> d);
        lista_visualizza(list -> next);
    }else{
        printf("NULL\n");
    }
}

/*
void lista_conta: data la testa della lista, conta il numero di vocali e consonanti presenti nella lista;
la funzione deve essere ricorsiva;
*/

void lista_conta(LINK *list, int *n_vocali, int *n_consonanti){
    if(*list != NULL){
        if((*list) -> d == 'a' || (*list) -> d == 'e' || (*list) -> d == 'i' || (*list) -> d == 'o' || (*list) -> d == 'u'
         || (*list) -> d == 'A' || (*list) -> d == 'E' || (*list) -> d == 'I' || (*list) -> d == 'O' || (*list) -> d == 'U'){
            ++(*n_vocali);
        }else{
            ++(*n_consonanti);
        }
        lista_conta(&(*list)->next, n_vocali, n_consonanti);
    }
}

/*
void lista_cancella_vocali: data la testa della lista, cancella i nodi di tipo vocale; la funzione deve
essere iterativa.
*/

void lista_cancella_vocali(LINK *head) {
    LINK current = *head;
    LINK previous = NULL;

    while (current != NULL) {
        if (current->d == 'a' || current->d == 'e' || current->d == 'i' || current->d == 'o' || current->d == 'u' ||
            current->d == 'A' || current->d == 'E' || current->d == 'I' || current->d == 'O' || current->d == 'U') {
            if (previous == NULL) {
                // Caso primo nodo ha una vocale
                *head = current->next;
                free(current);
                current = *head;
            } else {
                // Caso nodo centrale o ultimo ha una vocale
                previous->next = current->next;
                free(current);
                current = previous->next;
            }
        } else {
            previous = current;
            current = current->next;
        }
    }
}

int main(){
    FILE *fPtr = fopen("file.txt", "r");
    if (fPtr == NULL){
        printf("Errore Apertura File\n");
        exit(1);
    }else{
        LINK myList;
        printf(">>> Numero Nodi: %d\n", lista_leggi_file(fPtr, &myList));
        lista_visualizza(myList);

        int n_vocali = 0;
        int n_consonanti = 0;
        lista_conta(&myList, &n_vocali, &n_consonanti);
        
        printf(">>> Vocali: %d | Consonanti: %d\n", n_vocali, n_consonanti);

        lista_cancella_vocali(&myList);
        lista_visualizza(myList);
    }

    return 0;
}