/*Ricorsivo:
Date in input tre liste dare in output una quarta lista contente i maggiori elementi presenti (confrontando elemento per elemento)
La funzione termina quando la lista più corta è NULL
La funzione non deve creare nuovi nodi ma solo spostare i puntatori

Esempio:
L1: 3->5->9->10
L2: 1->8->7->12->8
L3: 5->2->5->4

L4: 5->8->9->12

*/

#include <stdio.h>
#include <stdlib.h>

typedef int DATA;
typedef struct linked_list
{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

LINK debug_BuildList()
{
    LINK p = newnode();
    int x;
    printf("Inserire elemento: ");
    scanf("%d", &x);
    if (x < 0)
    {
        return NULL;
    }
    else
    {
        p->d = x;
        p->next = debug_BuildList();
        return p;
    }
}

void debug_PrintList(LINK list)
{
    if (list != NULL)
    {
        printf("%d -> ", list->d);
        debug_PrintList(list->next);
    }
    else
    {
        printf("NULL\n");
    }
}

LINK onlyMax(LINK l1, LINK l2, LINK l3)
{
    LINK l4;
    if (l1 == NULL || l2 == NULL || l3 == NULL)
    {
        return NULL;
    }
    else
    {
        if ((l1->d >= l2->d) && (l1->d >= l3->d))
        {
            l4 = l1;
            l4->next = onlyMax(l1->next, l2->next, l3->next);
            return l4;
        }
        else if ((l2->d >= l1->d) && (l2->d >= l3->d))
        {
            l4 = l2;
            l4->next = onlyMax(l1->next, l2->next, l3->next);
            return l4;
        }
        else
        {
            l4 = l3;
            l4->next = onlyMax(l1->next, l2->next, l3->next);
            return l4;
        }
    }
}

int main()
{

    printf("Lista 1:\n");
    LINK l1 = debug_BuildList();

    printf("Lista 2:\n");
    LINK l2 = debug_BuildList();

    printf("Lista 3:\n");
    LINK l3 = debug_BuildList();

    printf("Lista 1:\n");
    debug_PrintList(l1);

    printf("Lista 2:\n");
    debug_PrintList(l2);

    printf("Lista 3:\n");
    debug_PrintList(l3);

    debug_PrintList(onlyMax(l1, l2, l3));

    return 0;
}
