#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
Esercizio 1
Scrivere una funzione iterativa che data una lista in input e un intero k, calcoli la media dei primi k nodi nella
lista. Si analizzi e motivi la complessità in spazio e in tempo.
Es: 5->3->7->1->2->NULL k = 3 Risultato 5
*/

typedef int DATA;

typedef struct linked_list{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT * LINK;

LINK newnode(){
    return malloc(sizeof(ELEMENT));
}

int randomNumber(){
    return rand()%100-31;
}

LINK buildList(){
    LINK head;
    int tmp;

    head = newnode();
    tmp = randomNumber();
    
    if(tmp < 0){
        head = NULL;
    }else{
        head -> d = tmp;
        head -> next = NULL;
        LINK p, tail;
        tail = head;

        tmp = randomNumber();

        while (tmp >= 0){
            p = newnode();
            p -> d = tmp;
            p -> next = NULL;
            tail -> next = p;
            tail = p;

            tmp = randomNumber();
        }

    }
    return head;
}

void printList(LINK list){
    if(list != NULL){
        printf("%d --> ", list->d);
        printList(list -> next);
    }
    else{
        printf("NULL\n");
    }
}

int main (void){
    time_t t;
    srand((unsigned) time(&t));

    LINK myList = NULL;
    myList = buildList();
    printList(myList);

    return 0;
}