/*
- calculate_avg: Scrivere una funzione iterativa che data una lista in input e un intero k, calcoli la media dei primi k nodi nella
                 lista. Si analizzi e motivi la complessità in spazio e in tempo.
                 Es: 5→3→7→1→2→NULL k = 3 Risultato 5

-  delete_nodes: Scrivere una funzione ricorsiva che prenda in input una lista e un intero k positivo e che cancelli dalla lista tutti
                 i nodi a valore k, solo se sono in una posizione pari. Si analizzi e motivi la complessità in spazio e in tempo.
                 Es: 5→3→7→1→3→NULL k = 3 Risultato 5→7→1→3→NULL

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef int DATA;

typedef struct linked_list{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT * LINK;

LINK newnode(){
    return malloc(sizeof(ELEMENT));
}

//Funzioni di Debug

LINK debug_CreateList(){
    LINK p, head, tail;
    int x = rand() % 10 - 2;
    if(x < 0){
        return NULL;
    }else{
        head = newnode();
        head -> d = x;
        head -> next = NULL;
        tail = head;
        x = rand() % 10 -2;
        while(x >= 0){
            p = newnode();
            p -> d = x;
            p -> next = NULL;
            tail -> next = p;
            tail = p;
            x = rand() % 10 - 2;
        }
    }
    return head;
}

void debug_PrintList(LINK list){
    if(list != NULL){
        printf("%d -> ", list -> d);
        debug_PrintList(list -> next);
    }
}

float calculateAvg(LINK list, int k){
    int cnt = 0;
    float sum = 0;
    while(list != NULL && cnt < k){
        sum = sum + list -> d;
        cnt++;
        list = list -> next;
    }
    if(cnt == 0){
        return 0;
    }else{
        return sum / cnt;
    }
}

//Cancella nodi se sono uguale a K ed in posizione pari

void delete_nodes(LINK *list, int k, int isPari){
    //Posizione 1 sicuramente da skippare perchè non è pari
    if(*list != NULL){
    LINK current = *list;
        if((current -> d == k) && isPari){
            *list = current -> next;
            free (current);
            delete_nodes(list, k, !isPari);
        }else{
            delete_nodes(&(current->next), k, !isPari);
        }
    }
}


// pari = 1
void deletepari(LINK lis, int k, int *pari)
{
    if(lis != NULL && lis -> next != NULL)
    {
        if(*pari == 1)
        {
            *pari = 0;
            if(lis -> next -> d == k)
            {
                LINK p = lis -> next;
                lis -> next = lis -> next -> next;
                free(p);
                deletepari(lis, k, pari);
            }
            else deletepari(lis -> next,k, pari);
        }
        else 
        {
            *pari = 1;
            deletepari(lis -> next, k,pari);
        }
    }
}



int main(){
    printf("Esame 22 Giugno 2021\n");

    //Per Debugging
    srand(time(NULL));
    LINK myList = debug_CreateList();
    debug_PrintList(myList);

    printf("\n");

    printf("%f\n", calculateAvg(myList, 3));

    int pari = 0;

    if(0>2){
        printf("Lista vuota\n");
    }else{
        delete_nodes(&myList, 2, pari);
    }
    debug_PrintList(myList);




    return 0;
}