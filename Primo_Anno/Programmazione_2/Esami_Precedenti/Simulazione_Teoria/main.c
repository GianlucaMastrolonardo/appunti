#include <stdio.h>
#include <stdlib.h>

typedef int DATA;

typedef struct linked_list
{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

LINK buildList()
{
    LINK head = newnode();
    int x;
    scanf("%d", &x);
    if (x >= 0)
    {
        head->d = x;
        head->next = buildList();
        return head;
    }
    else
    {
        return NULL;
    }
}

void printList(LINK list)
{
    if (list != NULL)
    {
        printf("%d -> ", list->d);
        printList(list->next);
    }
    else
    {
        printf("NULL\n");
    }
}

/*
Scrivere una funzione ricorsiva che prenda in input una lista e restituisca in output un’altra lista i cui elementi
sono la somma cumulata degli elementi della lista presa in input. Si analizzi e motivi la complessità in tempo e spazio.
Esempio:
L1: 1 -> 3 -> 7 -> 12 -> 22
Output: L2: 1 -> 4 -> 11 -> 23 -> 45
*/

LINK addList(LINK list, int acc)
{
    if (list == NULL)
    {
        return NULL;
    }
    else
    {

        acc = acc + list->d;
        LINK p = newnode();
        p->d = acc;
        p->next = addList(list->next, acc);
        return p;
    }
}

/*
Scrivre in input una funzione iterativa che restiusce da lunghezza della sequenza di numeri cresecenti più lunga
*/

int biggestSequence(LINK list)
{
    if (list == NULL)
    {
        return 0;
    }
    else
    {
        int acc = 0;
        int maxAcc = acc;
        while (list->next != NULL)
        {
            if (list->d <= list->next->d)
            {
                acc++;
                if (maxAcc < acc)
                {
                    maxAcc = acc;
                }
            }
            else
            {
                acc = 0;
            }
            list = list->next;
        }
        return maxAcc;
    }
}

int main()
{
    LINK myList = buildList();

    printList(myList);

    printList(addList(myList, 0));

    printf("Sequence: %d\n", biggestSequence(myList));

    return 0;
}
