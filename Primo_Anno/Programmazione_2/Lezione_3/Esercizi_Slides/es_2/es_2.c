//INFO: Unione Es_2 Es_3 Es_4
//Come es_1 ma informaziono presi da un file di testo.
/*
• da file Scrivere in un file di testo le informazioni sulle persone
• Caricare tali informazioni da file anziché da tasGera
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 5

typedef struct persona
{
  char nome[20];
  char cognome[20];
  int eta;
} persona;


void carica(persona A[], int dim){
  for (int i = 0; i < dim; i++) {
    printf("Inserire il Nome: ");
    scanf("%s", A[i].nome);
    printf("Inserire il Cognome: ");
    scanf("%s", A[i].cognome);
    printf("Inserire l'età: ");
    scanf("%d", &(A[i].eta));

  }
}

void carica_da_file(persona A[], char *fileName, int dim){
  int i = 0;
  FILE *fPtr;
  fPtr = fopen(fileName,"r");
  if (fPtr!=NULL){
    while (feof(fPtr)==0){
      fscanf(fPtr, "%s %s %d\n", A[i].nome, A[i].cognome, &(A[i].eta));
      i++;
    }
  }else{
    printf("Errore Apertura File\n");
  }
}

void stampa(persona A[], int dim){
  for (int i = 0; i < dim; i++) {
    printf("Persona %d:\n", i);
    printf("%s ", A[i].nome);
    printf("%s ", A[i].cognome);
    printf("%d\n", A[i].eta);

  }
}

void stampa_su_file(persona A[], char *fileName, int dim){
  int i = 0;
  FILE *fPtr;
  fPtr = fopen(fileName,"w");
  if (fPtr!=NULL){
    for (int i = 0; i < dim; i++) {
      fprintf(fPtr,"%s %s %d\n", A[i].nome, A[i].cognome, (A[i].eta));
    }
  }else{
    printf("Errore Apertura File\n");
  }
}


void dividi(persona A[], persona B[], persona C[], int *nB, int *nC, int dim){  //B: Maggiorenni | C: Minorenni
    *nB = 0;
    *nC = 0;
    for (int i = 0; i < dim; i++) {
      if(A[i].eta >= 18){
        B[*nB] = A[i];
        *nB = *nB +1;
      }else{
        C[*nC] = A[i];
        *nC = *nC +1;
      }
    }


  }

int main(void) {
  char *fileNameInput = "elenco_input.txt";
  int nB, nC;

  char *fileNameOutputMag = "elenco_mag.txt";
  char *fileNameOutputMin = "elenco_min.txt";


  persona A[N];
  persona B[N];
  persona C[N];


  carica_da_file(A, fileNameInput, N);

  dividi(A,B,C, &nB, &nC, N);

  stampa(A, N);
  stampa(B, nB);
  stampa(C, nC);

  stampa_su_file(B, fileNameOutputMag, nB);
  stampa_su_file(C, fileNameOutputMin, nC);

  return 0;
}
