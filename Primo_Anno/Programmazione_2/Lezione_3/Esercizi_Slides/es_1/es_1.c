/*
• Definire un ve1ore di stru1ure che contenga le
  informazioni su un insieme di persone
• Definire una funzione per caricare da tas5era
  le informazioni contenute nel ve1ore
• Definire una funzione per visualizzare il
  contenuto del ve1ore
• Invocare le due funzioni dal main()
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 5

typedef struct persona
{
  char nome[20];
  char cognome[20];
  int eta;
} persona;


void carica(persona A[], int dim){
  for (int i = 0; i < dim; i++) {
    printf("Inserire il Nome: ");
    scanf("%s", A[i].nome);
    printf("Inserire il Cognome: ");
    scanf("%s", A[i].cognome);
    printf("Inserire l'età: ");
    scanf("%d", &(A[i].eta));

  }
}

void stampa(persona A[], int dim){
  for (int i = 0; i < dim; i++) {
    printf("Persona %d:\n", i);
    printf("%s ", A[i].nome);
    printf("%s ", A[i].cognome);
    printf("%d\n", A[i].eta);

  }
}

int main(void) {
  persona A[N];

  carica(A, N);
  stampa(A, N);

  return 0;
}
