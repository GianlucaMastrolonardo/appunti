//Strutture dati annidati
/*
• Ripetere l’esercizio precedente aggiungendo il campo
  indirizzo alla struNura persona.
• Indirizzo è a sua volta una struNura composta da
  Via, Numero, Cap, Località
• Creare struNura indirizzo
• Aggiungere indirizzo alla struNura persona
• Aggiornare le funzioni di carica e stampa per gesGre il
  campo indirizzo
  fscanf(fp, %s, &A[i].indirizzo.via);
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 5

typedef struct indirizzo{
  char via[20];
  int civico;
  int cap;
  char city[20];
} indirizzo;

typedef struct persona
{
  char nome[20];
  char cognome[20];
  int eta;
  indirizzo i;
} persona;


void carica(persona A[], int dim){
  for (int i = 0; i < dim; i++) {
    printf("Inserire il Nome: ");
    scanf("%s", A[i].nome);
    printf("Inserire il Cognome: ");
    scanf("%s", A[i].cognome);
    printf("Inserire l'età: ");
    scanf("%d", &(A[i].eta));

    //Parte Indirizzo
    printf("Inserire Via: ");
    scanf("%s", A[i].i.via);
    printf("Inserire Civico: ");
    scanf("%d", &A[i].i.civico);
    printf("Inserire Cap: ");
    scanf("%d", &A[i].i.cap);
    printf("Inserire Città: ");
    scanf("%s", A[i].i.city);

  }
}

void carica_da_file(persona A[], char *fileName, int dim){
  int i = 0;
  FILE *fPtr;
  fPtr = fopen(fileName,"r");
  if (fPtr!=NULL){
    while (feof(fPtr)==0){
      fscanf(fPtr, "%s %s %d %s %d %d %s\n", A[i].nome, A[i].cognome, &(A[i].eta), A[i].i.via, &(A[i].i.civico), &(A[i].i.cap), A[i].i.city);
      i++;
    }
  }else{
    printf("Errore Apertura File\n");
  }
}

void stampa(persona A[], int dim){
  for (int i = 0; i < dim; i++) {
    printf("Persona %d:\n", i);
    printf("%s ", A[i].nome);
    printf("%s ", A[i].cognome);
    printf("%d\n", A[i].eta);

    //Parte indirizzo
    printf("%s", A[i].i.via);
    printf(" %d", A[i].i.civico);
    printf(", %d", A[i].i.cap);
    printf(", %s\n", A[i].i.city);

  }
}

void dividi(persona A[], persona B[], persona C[], int *nB, int *nC, int dim){  //B: Maggiorenni | C: Minorenni
    *nB = 0;
    *nC = 0;
    for (int i = 0; i < dim; i++) {
      if(A[i].eta >= 18){
        B[*nB] = A[i];
        *nB = *nB +1;
      }else{
        C[*nC] = A[i];
        *nC = *nC +1;
      }
    }


  }

int main(void) {
  char *fileName = "elenco_input.txt";
  int nB, nC;


  persona A[N];
  persona B[N];
  persona C[N];


  carica_da_file(A, fileName, N);

  dividi(A,B,C, &nB, &nC, N);

  stampa(A, N);
  printf("--- Maggiorenni ---\n");
  stampa(B, nB);
  printf("--- Minorenni ---\n");
  stampa(C, nC);


  return 0;
}
