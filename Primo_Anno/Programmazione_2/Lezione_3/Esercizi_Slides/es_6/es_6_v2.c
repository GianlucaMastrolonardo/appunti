//IDEA Salvo tutto nella struct students composta da Matricola Nome Cognome Media
/*
• Legga due serie di dati e li memorizzi in due vettori di strutture. Nel
    primo vettore M (di dimensione DIMN) vengono memorizzati dati nel
    formato:
    matricola nome cognome
• Si noti che la matricola (di tipo int) identifica univocamente uno studente
• Nel secondo vettore V (di dimensione DIMV) vengono memorizzati dati
nel formato:
    matricola esame voto
• Possono esserci più record con lo stesso numero di matricola che
denotano diversi esami faV dallo stesso studente.
• Tramite opportune procedure, il programma deve leggere i dati in
ingresso e inserirli nei due vettori.
• Successivamente, per ogni studente con matricola X contenuto nel
vettore M, calcoli la media dei suoi voti ottenuti negli esami contenuti nel
vettore V
*/

#include <stdio.h>
#include <stdlib.h>

#define DIMN 4  //Dimensione Studenti
#define DIMV 7  //Dimensione Esami

typedef struct students{
    int id;
    char name[25];
    char surname[25];
    float avg;
} students;

typedef struct exams{
    int doneBy;
    char examName[25];
    int grade;
} exams;

void loadStudentsFromFile(students s[], char *fileName){
    int i = 0;
    FILE *fPtr;
    fPtr = fopen(fileName, "r");
    if(fPtr != NULL){
        while (feof(fPtr)==0){
            fscanf(fPtr, "%d %s %s\n", &(s[i].id), s[i].name, s[i].surname);
            s[i].avg = 0;
            i++;
        }
        fclose(fPtr);
    }else{
        printf("Errore Apertura File\nChiusura Programma...");
        exit(0);
    }
}

void loadExamsFromFile(exams e[], char *fileName){
    int i = 0;
    FILE *fPtr;
    fPtr = fopen(fileName, "r");
    if(fPtr != NULL){
        while (feof(fPtr) == 0){
            fscanf(fPtr, "%d %s %d\n", &(e[i].doneBy), e[i].examName, &(e[i].grade));
            e++;
        }
    }else{
        printf("Errore Apertura File\nChiusura Programma...");
        exit(0);
    }

}

//DEBUG: Da eliminare, serve solo in fase di sviluppo
void stampaStudenti(students A[], int dim){
  for (int i = 0; i < dim; i++) {
    printf("Matricola %d:\n", A[i].id);
    printf("%s ", A[i].name);
    printf("%s \n", A[i].surname);
  }
}

void stampaEsami(exams A[], int dim){
  for (int i = 0; i < dim; i++) {
    printf("Esame svolto da Matricola %d ", A[i].doneBy);
    printf("%s ", A[i].examName);
    printf("%d \n", A[i].grade);
  }
}

void stampaMedie(students A[], int dim){
    for (int i = 0; i < dim; i++){
        printf("Media esami di %s %s: %.2f\n", A[i].name, A[i].surname, A[i].avg);
    }
}

void mediaEsami(students s[], exams e[], int dimS, int dimE){
    int cntGrade;

    //Scorro Studenti
    for(int i = 0; i < dimS; i++){
        cntGrade = 0;
        //Scorro Esami
        for(int j = 0; j < dimE; j++){
            //Esame e Matricole combaciano
            if(s[i].id == e[j].doneBy){
                s[i].avg = s[i].avg + e[j].grade;
                cntGrade++;
            }
        }

        s[i].avg = s[i].avg / cntGrade;

    }
}

int main(){

    students studentsList[DIMN];
    exams examsList[DIMV];

    char *fileNameStudent = "studenti.txt";
    char *fileNameExam = "esami.txt";

    loadStudentsFromFile(studentsList, fileNameStudent);
    loadExamsFromFile(examsList, fileNameExam);

    //STAMPE
    printf("--- Studenti ---\n");
    stampaStudenti(studentsList, DIMN);

    printf("--- Esami ---\n");
    stampaEsami(examsList, DIMV);

    mediaEsami(studentsList, examsList, DIMN, DIMV);
    printf("--- Medie ---\n");
    stampaMedie(studentsList, DIMN);

    return 0;
}