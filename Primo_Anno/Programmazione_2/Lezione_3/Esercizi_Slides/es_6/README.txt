Le differenze sono che:
- in v1:
    Uso un array per salvare le medie e le stampo dentro il ciclo, però fuori dal ciclo perdo il riferimento nome/media.
- in v2:
    Ho cambiato la struct studenti aggiungendo il campo 'avg' inizializzato a zero nella funzione dove carico gli studenti, così ho sempre il riferimento nome/media.