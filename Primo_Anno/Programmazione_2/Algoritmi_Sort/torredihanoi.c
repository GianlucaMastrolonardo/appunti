#include <stdio.h>
#include <stdlib.h>

void move(int n, char *Source, char *Support, char *Target)
{
    if (n == 1)
        printf("moving disc %d from tower %s to tower %s\n", n, Source, Target);
    else
    {
        move(n - 1, Source, Target, Support); /* da Source a Support usando Target
             come supporto */
        printf("moving disc %d from tower %s to tower %s\n", n, Source, Target);
        move(n - 1, Support, Source, Target); /* da Support a Target usando Source
             come supporto */
    }
}

int main()
{
    int n = 3;
    move(n, "A", "B", "C");
    return 0;
}
