#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

// Funzione per scambiare due elementi in un array
void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

// Funzione per controllare se un array è ordinato
bool isSorted(int arr[], int n)
{
    for (int i = 0; i < n - 1; i++)
    {
        if (arr[i] > arr[i + 1])
            return false;
    }
    return true;
}

// Funzione per applicare il Bogo Sort all'array
void bogoSort(int arr[], int n)
{
    // Inizializza il generatore di numeri casuali
    srand(time(NULL));

    // Continua a mescolare l'array fino a quando non è ordinato
    while (!isSorted(arr, n))
    {
        // Scambia elementi casuali nell'array
        for (int i = 0; i < n; i++)
        {
            int j = rand() % n;
            swap(&arr[i], &arr[j]);
        }
    }
}

// Funzione di utilità per stampare l'array
void printArray(int arr[], int n)
{
    for (int i = 0; i < n; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main()
{
    int arr[] = {9, 5, 7, 2, 4, 3, 5, 2, 5, 10};
    int n = sizeof(arr) / sizeof(arr[0]);

    printf("Array originale: ");
    printArray(arr, n);

    bogoSort(arr, n);

    printf("Array ordinato: ");
    printArray(arr, n);

    int arr2[] = {9, 5, 7, 2, 4, 3, 5, 2, 5, 10, 24, 2, 423, 53, 32, 532, 323, 2, 2, 2121, 32, 23, 232, 35, 7686, 45, 456, 45, 4, 32, 2};
    int n2 = sizeof(arr2) / sizeof(arr2[0]);

    printf("Array originale: ");
    printArray(arr, n2);

    bogoSort(arr, n2);

    printf("Array ordinato: ");
    printArray(arr, n2);

    return 0;
}
