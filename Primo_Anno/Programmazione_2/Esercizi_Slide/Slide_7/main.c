/*
   _____ _ _     _        ______ 
  / ____| (_)   | |      |____  |
 | (___ | |_  __| | ___      / / 
  \___ \| | |/ _` |/ _ \    / /  
  ____) | | | (_| |  __/   / /   
 |_____/|_|_|\__,_|\___|  /_/    
                                 
                                 

• Calcolare quante volte un elemento è minore del successivo: minus_elements();
• Data una lista, restituire in output il numero dei nodi della lista che sono somma dei loro due predecessori immediati: sum_elements();

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void printslidenumber() {
    printf("   _____ _ _     _        ______ \n");
    printf("  / ____| (_)   | |      |____  |\n");
    printf(" | (___ | |_  __| | ___      / / \n");
    printf("  \\___ \\| | |/ _` |/ _ \\    / /  \n");
    printf("  ____) | | | (_| |  __/   / /   \n");
    printf(" |_____/|_|_|\\__,_|\\___|  /_/    \n");
    printf("                                 \n");
}

typedef int DATA;

typedef struct linked_list{
    DATA d;
    struct linked_list *next;
}ELEMENT;

typedef ELEMENT * LINK;

LINK newnode(){
    return malloc(sizeof(ELEMENT));
}

LINK debug_buildList(){
    LINK head, p, tail;
    int x = rand() % 30-3;
    if(x < 0){
        return NULL;
    }else{
        head = newnode();
        head -> d = x;
        head -> next = NULL;
        tail = head;
        x = rand() % 30-3;
        while(x>=0){
            p = newnode();
            p->d = x;
            p -> next = NULL;
            tail -> next = p;
            tail = p;
            x = rand() % 30-3;
        }
    }

    return head;
}

void debug_printlist(LINK list){
    while(list != NULL){
        printf("%d -> ", list -> d);
        list = list -> next;
    }
    printf("NULL\n");
}

int minus_elements(LINK list){
        int cnt = 0;
        while(list != NULL && list -> next != NULL){
            if(list->d < list -> next -> d){
                cnt++;
            }
            list = list -> next;
        }
        return cnt;
    }

int sum_elements(LINK list){
    if(list == NULL || list -> next == NULL || list -> next -> next == NULL){
        return 0;
    }else{
        int cnt = 0;
        while(list -> next -> next != NULL){
            if(list -> d + list -> next -> d == list -> next -> next -> d){
                cnt++;
            }
            list = list -> next;
        }
        return cnt;
    }
}

int main(){

    srand(time(NULL));


    printslidenumber();

    LINK myList = debug_buildList();

    printf("\n>>> Stampa Lista:\n");
    debug_printlist(myList);

    printf("\n>>> Elementi minori in sequenza: %d\n", minus_elements(myList));

    printf("\n>>> Elementi somma in sequenza: %d\n", sum_elements(myList));

    return 0;
}