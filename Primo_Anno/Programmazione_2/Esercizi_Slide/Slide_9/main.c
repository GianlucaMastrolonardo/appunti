/*
   _____ _ _     _         ___
  / ____| (_)   | |       / _ \
 | (___ | |_  __| | ___  | (_) |
  \___ \| | |/ _` |/ _ \  \__, |
  ____) | | | (_| |  __/    / /
 |_____/|_|_|\__,_|\___|   /_/





• Data una lista in input, modificare il valore di ogni singolo nodo incrementandolo di una unità: incrlist();
• Dato un valore intero x, creiamo un nuovo nodo con valore uguale a x e lo inseriamo in testa alla lista: headinsert();

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void printSlideNumber()
{
    printf("   _____ _ _     _         ___  \n");
    printf("  / ____| (_)   | |       / _ \\ \n");
    printf(" | (___ | |_  __| | ___  | (_) |\n");
    printf("  \\___ \\| | |/ _` |/ _ \\  \\__, |\n");
    printf("  ____) | | | (_| |  __/    / / \n");
    printf(" |_____/|_|_|\\__,_|\\___|   /_/  \n");
    printf("                               \n");
}

typedef int DATA;

typedef struct linked_list
{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

LINK debug_buildList()
{
    LINK head, p, tail;
    int x = rand() % 30 - 3;
    if (x < 0)
    {
        return NULL;
    }
    else
    {
        head = newnode();
        head->d = x;
        head->next = NULL;
        tail = head;
        x = rand() % 30 - 3;
        while (x >= 0)
        {
            p = newnode();
            p->d = x;
            p->next = NULL;
            tail->next = p;
            tail = p;
            x = rand() % 30 - 3;
        }
    }

    return head;
}

void debug_printlist(LINK list)
{
    while (list != NULL)
    {
        printf("%d -> ", list->d);
        list = list->next;
    }
    printf("NULL\n");
}

void debug_printnode(LINK node)
{
    if (node != NULL)
    {
        printf("Valore %d è in posizione %p\n", node->d, node);
    }
    else
    {
        printf("NULL\n");
    }
}

// Data una lista in input, modificare il valore di ogni singolo nodo incrementandolo di una unità: incrlist();
void incrementa(LINK list)
{
    while (list != NULL)
    {
        (list->d)++;
        list = list->next;
    }
}

void headinsert(LINK *list)
{
    LINK head = newnode();
    printf("Inserire il valore da voler aggiungere in testa: ");
    scanf("%d", &head->d);
    head->next = *list;
    *list = head;
}

LINK headinsert_val(LINK list)
{
    LINK head = newnode();
    printf("Inserire il valore da voler aggiungere in testa: ");
    scanf("%d", &head->d);
    head->next = list;
    return head;
}

void tailinsert(LINK *list)
{

    LINK tail = newnode();
    printf("Inserire il valore da voler aggiungere in coda: ");
    scanf("%d", &tail->d);
    tail->next = NULL;

    if (*list == NULL)
    {
        *list = tail;
    }
    else
    {
        LINK head = *list;

        // Scorro tutta la lista finchè non arrivo all'elemento
        // prima di NULL
        while ((*list)->next != NULL)
        {
            *list = (*list)->next;
        }

        (*list)->next = tail;
        (*list) = head;
    }
}

// Creare una funzione che inserisca un nuovo nodo a valore n prima di un nodo a valore x, se quest’ultimo esiste.
// Fatta da me:
void positioninsert(LINK *list)
{

    if (*list != NULL)
    {
        int numberToFind;
        printf("Dopo che valore inserire il nuovo nodo (se la posizione non è presente non sarà inserito il nuovo elemento): ");
        scanf("%d", &numberToFind);

        if ((*list)->d == numberToFind)
        {
            headinsert(list);
        }
        else
        {
            int founded = 0;
            LINK head = *list;
            while ((*list)->next != NULL)
            {
                if ((*list)->next->d == numberToFind && founded == 0)
                {
                    int x;
                    LINK p = newnode();
                    printf("Valore Trovato!\nInserire il valore del nodo da aggiungere: ");
                    scanf("%d", &x);
                    p->d = x;
                    p->next = (*list)->next;
                    (*list)->next = p;
                    *list = (*list)->next->next;
                    founded = 1;
                }
                else
                {
                    *list = (*list)->next;
                }
            }

            *list = head;
        }
    }
}

int main(int argc, char *argv[])
{

    printSlideNumber();

    srand(time(NULL)); // Serve solo per generare i numeri casuali

    LINK myList;

    if (strcmp(argv[1], "0") == 0)
    {
        myList = NULL;
    }
    else if (strcmp(argv[1], "1") == 0)
    {
        myList = debug_buildList();
    }
    else
    {
        printf(">>> Errore, il parametro inserito è errato!");
        exit(1);
    }

    printf("\n>>> Stampa Lista:\n");
    debug_printlist(myList);

    printf("\n>>> Head Insert:\n");
    headinsert(&myList);
    debug_printlist(myList);

    printf("\n>>> Tail Insert:\n");
    tailinsert(&myList);
    debug_printlist(myList);

    printf("\n>>> In Mezzo Insert:\n");
    positioninsert(&myList);
    debug_printlist(myList);

    return 0;
}