/*
   _____ _ _     _         ___  
  / ____| (_)   | |       / _ \ 
 | (___ | |_  __| | ___  | (_) |
  \___ \| | |/ _` |/ _ \  > _ < 
  ____) | | | (_| |  __/ | (_) |
 |_____/|_|_|\__,_|\___|  \___/ 
                                
                                
                                 
                                 

• Dato un valore x in input, la ricerca per valore restituisce un puntatore al nodo con valore x in una lista p.
  Se x non è presente nella lista, restituisce NULL.
  - Se sono presenti più nodi con lo stesso valore x, viene alla prima occorrenza.
  
  findfirstelement();

• Come esercizio di prima ma trovare l'ultima occorrenza: findlastelement();
• Dato una posizione, la ricerca restituisce un puntatore al nodo in quella posizione: findpositione();
• Trovare un puntatore all’ n-esima occorrenza di un determinato valore x all’interno della lista: findth();
• Nodo precedente a nodo di valore x: findpred();

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void printSlideNumber() {
    printf("   _____ _ _     _         ___  \n");
    printf("  / ____| (_)   | |       / _ \\ \n");
    printf(" | (___ | |_  __| | ___  | (_) |\n");
    printf("  \\___ \\| | |/ _` |/ _ \\  > _ < \n");
    printf("  ____) | | | (_| |  __/ | (_) |\n");
    printf(" |_____/|_|_|\\__,_|\\___|  \\___/ \n");
    printf("                               \n");
}

typedef int DATA;

typedef struct linked_list{
    DATA d;
    struct linked_list *next;
}ELEMENT;

typedef ELEMENT * LINK;

LINK newnode(){
    return malloc(sizeof(ELEMENT));
}

LINK debug_buildList(){
    LINK head, p, tail;
    int x = rand() % 30-3;
    if(x < 0){
        return NULL;
    }else{
        head = newnode();
        head -> d = x;
        head -> next = NULL;
        tail = head;
        x = rand() % 30-3;
        while(x>=0){
            p = newnode();
            p->d = x;
            p -> next = NULL;
            tail -> next = p;
            tail = p;
            x = rand() % 30-3;
        }
    }

    return head;
}

void debug_printlist(LINK list){
    while(list != NULL){
        printf("%d -> ", list -> d);
        list = list -> next;
    }
    printf("NULL\n");
}

void debug_printnode(LINK node){
    if(node != NULL){
        printf("Valore %d è in posizione %p\n", node -> d, node);
    }else{
        printf("NULL\n");
    }

}

LINK findfirstelement(LINK list, int x){
    while(list != NULL){
        if(list -> d == x){
            return list;
        }
        list = list -> next;
    }
    return NULL;
}

LINK foundlastelement(LINK list, int x){
    LINK position = NULL;
    while(list != NULL){
        if(list -> d == x){
            position = list;
        }
        list = list -> next;
    }
    return position;
}

LINK findposition(LINK list, int pos){
    int cnt = 1;
    while(list != NULL){
        if(cnt == pos){
            return list;
        }
        cnt++;
        list = list -> next;
    }
    return NULL;
}

LINK findt(LINK list, int x, int occ){
    int cnt = 1;
    while(list != NULL){
        if(list -> d == x){
            if(cnt == occ){
                return list;
            }
            cnt++;
        }
        list = list -> next;
    }
    return NULL;
}

LINK findpred(LINK list, int x){
    if(list == NULL){
        return NULL;
    }
    else{
        if(list -> d == x){
        printf("Il valore è in cima alla lista.\n");
        return NULL;
        }else{
            while(list -> next != NULL){
                if(list -> next -> d == x){
                    return list;
                }
                list = list -> next;
            }
            return NULL;
        }
    }
}

int main(){

    printSlideNumber();

    srand(time(NULL)); //Serve solo per generare i numeri casuali
    LINK myList = debug_buildList();

    printf("\n>>> Stampa Lista:\n");
    debug_printlist(myList);

    printf("\n>>> Stampa prima occorrenza del valore 3:\n");
    LINK nodePosition = findfirstelement(myList, 3);
    debug_printnode(nodePosition);

    printf("\n>>> Stampa ultima occorrenza del valore 3:\n");
    nodePosition = foundlastelement(myList, 3);
    debug_printnode(nodePosition);

    printf("\n>>> Stampa nodo in quarta posizione:\n");
    nodePosition = findposition(myList, 4);
    debug_printnode(nodePosition);

    printf("\n>>> Stampa nodo del valore 3 nella seconda occorrenza:\n");
    nodePosition = findt(myList, 3, 2);
    debug_printnode(nodePosition);

    printf("\n>>> Stampa nodo precedente all'elemento 3 (prima occorrenza):\n");
    nodePosition = findpred(myList, 3);
    debug_printnode(nodePosition);


    return 0;
}