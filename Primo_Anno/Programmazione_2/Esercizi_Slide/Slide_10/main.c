/*
   _____ _ _     _        __  ___
  / ____| (_)   | |      /_ |/ _ \
 | (___ | |_  __| | ___   | | | | |
  \___ \| | |/ _` |/ _ \  | | | | |
  ____) | | | (_| |  __/  | | |_| |
 |_____/|_|_|\__,_|\___|  |_|\___/


*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

typedef int DATA;

typedef struct linked_list
{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

LINK debug_buildList()
{
    srand(time(NULL));

    int x = rand() % 30 - 3;
    if (x < 0)
    {
        return NULL;
    }
    else
    {
        int cnt = 1;
        LINK head = newnode();
        head->d = x;
        head->next = NULL;

        LINK tail = head;
        LINK p;

        x = rand() % 30 - 3;
        while (x >= 0 && cnt < 6)
        {
            p = newnode();
            p->d = x;
            p->next = NULL;
            tail->next = p;
            tail = p;

            x = rand() % 30 - 3;
            cnt++;
        }
        return head;
    }
}

void debug_printList(LINK list)
{
    if (list != NULL)
    {
        printf("%d -> ", list->d);
        debug_printList(list->next);
    }
    else
    {
        printf("NULL\n");
    }
}

void deleteHead(LINK *list)
{
    if (*list != NULL)
    {
        LINK toBeClean = *list;
        *list = (*list)->next;
        free(toBeClean);
    }
}

void deleteTail(LINK *list)
{
    if (*list != NULL)
    {
        LINK p = *list;
        if ((*list)->next == NULL)
        {
            *list = NULL;
            free(p);
        }
        else
        {
            while ((*list)->next->next != NULL)
            {
                *list = (*list)->next;
            }
            LINK toBeClean = (*list)->next;
            (*list)->next = NULL;
            free(toBeClean);
            *list = p;
        }
    }
}

int main(int argc, char const *argv[])
{
    srand(time(NULL));

    LINK myList;

    if (argc == 2)
    {

        if (strcmp(argv[1], "0") == 0)
        {
            myList = NULL;
        }
        else if (strcmp(argv[1], "1") == 0)
        {
            myList = debug_buildList();
        }
    }
    else
    {
        printf("Errore! Passaggio parametri errato!\n");
        exit(1);
    }

    debug_printList(myList);

    printf(">>> Eliminazion primo nodo:\n");
    deleteHead(&myList);
    debug_printList(myList);

    printf(">>> Eliminazion ultimo nodo:\n");
    deleteTail(&myList);
    debug_printList(myList);

    return 0;
}
