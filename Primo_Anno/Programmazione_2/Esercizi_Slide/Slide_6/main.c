/*
   _____ _ _     _           __  
  / ____| (_)   | |         / /  
 | (___ | |_  __| | ___    / /_  
  \___ \| | |/ _` |/ _ \  | '_ \ 
  ____) | | | (_| |  __/  | (_) |
 |_____/|_|_|\__,_|\___|   \___/ 
                                 

• Stampa di una lista: printlist();
• Dati una lista e un intero k in input stampa i nodi con valore > k: printcondlist(); 
• Stampa nodi in posizioni multiple di un intero k: printmultipleofxlist();
• Somma tutti i valori all'interno di una lista: sumallvalues();
• Stampa i primi n numeri della lista: printpartiallist();

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void printslidenumber() {
    printf("   _____ _ _     _           __  \n");
    printf("  / ____| (_)   | |         / /  \n");
    printf(" | (___ | |_  __| | ___    / /_  \n");
    printf("  \\___ \\| | |/ _` |/ _ \\  | '_ \\ \n");
    printf("  ____) | | | (_| |  __/  | (_) |\n");
    printf(" |_____/|_|_|\\__,_|\\___|   \\___/ \n");
    printf("                                 \n");
}

typedef int DATA;

typedef struct linked_list{
    DATA d;
    struct linked_list *next;
}ELEMENT;

typedef ELEMENT * LINK;

LINK newnode(){
    return malloc(sizeof(ELEMENT));
}

LINK debug_buildList(){
    LINK head, p, tail;
    int x = rand() % 30-3;
    if(x < 0){
        return NULL;
    }else{
        head = newnode();
        head -> d = x;
        head -> next = NULL;
        tail = head;
        x = rand() % 30-3;
        do{
            p = newnode();
            p->d = x;
            p -> next = NULL;
            tail -> next = p;
            tail = p;
            x = rand() % 30-3;
        }while(x>=0);
    }

    return head;
}


void printlist(LINK list){
    while(list != NULL){
        printf("%d -> ", list -> d);
        list = list -> next;
    }
    printf("NULL\n");
}

void printcondlist(LINK list, int k){
    while(list != NULL){
        if(list -> d > k){
            printf("%d -> ", list -> d);
        }
        list = list -> next;
    }
    printf("NULL\n");
}

void printmultipleofxlist(LINK list, int k){
    int cnt = 1;
    while(list != NULL){
        if(cnt % k == 0){
            printf("%d -> ", list -> d);
        }
        cnt++;
        list = list -> next;
    }
    printf("NULL\n");
}

int sumallvalues(LINK list){
    int cnt = 0;
    while(list != NULL){
        cnt = list -> d + cnt;
        list = list -> next;
    }
    return cnt;
}

void printpartiallist(LINK list, int pos){
    int currentpos = 1;
    while(list != NULL && currentpos <= pos){
        printf("%d -> ", list->d);
        list = list -> next;
        currentpos++;
    }
    printf("NULL\n");
}

int main(){

    srand(time(NULL));


    printslidenumber();

    LINK myList = debug_buildList();

    printf("\n>>> Stampa Lista:\n");
    printlist(myList);

    printf("\n>>> Stampa Condizionata (solo elementi > 10):\n");
    printcondlist(myList, 10);

    printf("\n>>> Stampa in base alla posizione (solo elementi multipli di 2):\n");
    printmultipleofxlist(myList, 2);

    printf("\n>>> Somma di tutti i valori all'interno della lista: %d\n", sumallvalues(myList));

    printf("\n>>> Stampa finchè non si arriva ad una determinata posizione (fino a posizione 3):\n");
    printpartiallist(myList, 3);

    return 0;
}