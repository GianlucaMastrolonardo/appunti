//ES8 Ultimo Parente Maggiorenne
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct persona{
  char name[20];
  char surname[20];
  int age;
} DATA;


struct linked_list {
  DATA d;
  struct linked_list *next;
};

typedef struct linked_list ELEMENT;
typedef ELEMENT * LINK;


LINK newnode(void) {
return malloc(sizeof(ELEMENT));
}

void printlis(LINK lis) {
    while (lis != NULL) {
      printf("\nNome: \"%s\" Cognome: \"%s\" Eta': \"%d\" --> ", lis->d.name, lis->d.surname, lis->d.age);
        lis= lis->next;
    }
    printf("NULL\n");
}

DATA addField(){
  DATA d;
  printf("---Se il Nome = end chiudi il programma---\n");
  printf("Inserire il Nome: ");
  scanf("%s", d.name);
  if((strcmp(d.name, "end")) != 0){
    printf("Inserire il Cognome: ");
    scanf("%s", d.surname);
    printf("Inserire l'età: ");
    scanf("%d", &d.age);
  }

  return d;
}

LINK buildlis(){
  DATA x;
  LINK lis, p, last;
  x = addField();
  //Se nome == end creare una lista vuota
  if((strcmp(x.name, "end")) == 0){
    lis = NULL;
  }
  else{
    last = newnode();
    lis = last;
    last -> d = x;
    last -> next = NULL;
    x = addField();
    //Cicla finchè il dato inserito non è > 0
    while((strcmp(x.name, "end")) != 0){
      p = newnode();
      p -> d = x;
      p -> next = NULL;
      last -> next = p;
      last = p;
      x = addField();
    }
  }
  return lis;
}

LINK buildListFromFile(char nomefile[])
{
  LINK lis = NULL;
	LINK p, last;
	char nome[20], cognome[20];
	int eta;
	FILE *f;
	f = fopen(nomefile, "r");
	if(f == NULL)
	{
		printf("Il file non e' stato aperto.\n");
	}
	else
	{
		int end = 0;
		while(!feof(f) && end == 0)
		{
			fscanf(f, "%s", nome);
			if(strcmp(nome, "END") == 0) end = 1;
			else
			{
				fscanf(f, "%s %d\n", cognome, &eta);
			}

			if(end == 0)
			{
				DATA d;
				strcpy(d.name, nome);
				strcpy(d.surname, cognome);
				d.age = eta;
				if(lis == NULL)
				{
					last = newnode();
			        last -> d = d;
			        last -> next = NULL;
			        lis = last;
				}
				else
				{
					p = newnode();
		            p -> d = d;
		            last -> next = p;
		            last = p;
				}
			}
		}
	}
	fclose(f);

    return lis;
}

LINK findList(char *surname, LINK p){
  int find = 0;
  while ((p != NULL) && (!find)){
    if((strcmp(p -> d.surname, surname) == 0) && (p -> d.age >= 18)){
      find = 1;
    }
    else{
      p = p -> next;
    }
  }
  return p;
}

LINK findLastSurnameMagg(char *surname, LINK lis){
  LINK find = NULL;
  while (lis != NULL) {
    if((strcmp(lis -> d.surname, surname) == 0) && ((lis -> d.age) >= 18)){
      find = lis;
    }
    lis = lis -> next;
  }
  return find;
}



void printSpecificItem(LINK p){
  printf("- Nome: \"%s\" Cognome: \"%s\" Eta': \"%d\" --> ", p->d.name, p->d.surname, p->d.age);
}


int main() {

  char *nomefile = "elenco_persone.txt";
  LINK myList = buildListFromFile(nomefile);
  printf("Lista:\n");
  printlis(myList);

  char *cognome ="Marzone";
  printf("Stampa ultimo maggiorenne: %s\n", cognome);

  LINK p = findLastSurnameMagg(cognome, myList);

  printSpecificItem(p);

  return 0;

}
