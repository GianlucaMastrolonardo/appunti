//Visualizzare primi N nodi

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Il DATA è l'intera struct presente qui
typedef struct persona{
  char name[20];
  char surname[20];
  int age;
} DATA;

//Lista
struct linked_list {
  DATA d;
  struct linked_list *next;
};

typedef struct linked_list ELEMENT;
typedef ELEMENT * LINK;


LINK newnode(void) {
return malloc(sizeof(ELEMENT));
}

void printlis(LINK lis) {
    while (lis != NULL) {
      printf("Nome: \"%s\" Cognome: \"%s\" Eta': \"%d\"\n", lis->d.name, lis->d.surname, lis->d.age);
        lis= lis->next;
    }
    printf("NULL \n");
}

LINK buildlisFromFile(char *filename){
  LINK head, p, tail;

  char name[20];
  char surname[20];
  int age;

  FILE *fPtr;
  fPtr = fopen(filename, "r");

  if(fPtr == NULL){
    printf("Errore Apertura File!");
    exit(1);
  } else{
    //Controllo se File Vuoto
    if(feof(fPtr)){
      return NULL;
    }else{
      head = newnode();
      fscanf(fPtr, "%s %s %d", name, surname, &age);
      strcpy(head->d.name, name);
      strcpy(head->d.surname, surname);
      head->d.age = age;
      head->next = NULL;
      tail = head;
      do{
        p = newnode();
        fscanf(fPtr, "%s %s %d", name, surname, &age);
        strcpy(p->d.name, name);
        strcpy(p->d.surname, surname);
        p->d.age = age;
        p->next = NULL;
        tail -> next = p;
        tail = p;
      }while(!feof(fPtr));
    }
  }
  return head;
}

void addNewNodeHead(LINK *list){
  printf("---Inserimento Nomi in Testa---\n");
  LINK p;
  char name[20];
  char surname[20];
  int age;

  printf("Inserire Nome: ");
  scanf("%s", name);
  printf("Inserire Cognome: ");
  scanf("%s", surname);
  printf("Inserire Età: ");
  scanf("%d", &age);

  p = newnode();
  strcpy(p->d.name, name);
  strcpy(p->d.surname, surname);
  p->d.age = age;

  p->next = *list;
  *list = p;
}

void printlisitem(LINK list){
  int pos;
  printf(">>> Inserire fino a che posizione eseguire la stampa: ");
  scanf("%d", &pos);
  int cnt = 1;
  while(list != NULL && cnt <= pos){
      printf("Nome: \"%s\" Cognome: \"%s\" Eta': \"%d\"\n", list->d.name, list->d.surname, list->d.age);
      list= list->next;
      cnt++;
  }
  printf("NULL\n");
}

void printspecificitem(LINK list){
  int pos;
  printf(">>> Inserire posizione del nodo che si vuole stampare: ");
  scanf("%d", &pos);
  int cnt = 1;
  while(list != NULL){
    if(cnt == pos){
      printf("Nome: \"%s\" Cognome: \"%s\" Eta': \"%d\"\n", list->d.name, list->d.surname, list->d.age);
      }
    list= list->next;
    cnt++;
  }
  if(pos > cnt){
    printf("Errore, non esiste la posizione richiesta perche' la lista e' piu' corta.\n");
  }
}

int main() {
  char *fileName = "nomi.txt";
  LINK myList;

  myList = buildlisFromFile(fileName);
  printlis(myList);

  //addNewNodeHead(&myList);
  printlisitem(myList);
  printspecificitem(myList);
  return 0;

}
