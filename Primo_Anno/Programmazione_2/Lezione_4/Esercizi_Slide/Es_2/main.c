#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Il DATA è l'intera struct presente qui
typedef struct persona{
  char name[20];
  char surname[20];
  int age;
} DATA;


struct linked_list {
  DATA d;
  struct linked_list *next;
};

typedef struct linked_list ELEMENT;
typedef ELEMENT * LINK;


LINK newnode(void) {
return malloc(sizeof(ELEMENT));
}

void printlis(LINK lis) {
    while (lis != NULL) {
      printf("Nome: \"%s\" Cognome: \"%s\" Eta': \"%d\" --> ", lis->d.name, lis->d.surname, lis->d.age);
        lis= lis->next;
    }
    printf("NULL \n");
}

DATA addField(){
  DATA d;
  printf("---Se il Nome = end chiudi il programma---\n");
  printf("Inserire il Nome: ");
  scanf("%s", d.name);
  if((strcmp(d.name, "end")) != 0){
    printf("Inserire il Cognome: ");
    scanf("%s", d.surname);
    printf("Inserire l'età: ");
    scanf("%d", &d.age);  
  }

  return d;
}

LINK buildlis(){
  DATA x;
  LINK lis, p, last;
  x = addField();
  //Se nome == end creare una lista vuota
  if((strcmp(x.name, "end")) == 0){
    lis = NULL;
  }
  else{
    last = newnode();
    lis = last;
    last -> d = x;
    last -> next = NULL;
    x = addField();
    //Cicla finchè il dato inserito non è > 0
    while((strcmp(x.name, "end")) != 0){
      p = newnode();
      p -> d = x;
      p -> next = NULL;
      last -> next = p;
      last = p;
      x = addField();
    }
  }
  return lis;
}


int main() {

  LINK myList;

  myList = NULL;

  myList = buildlis();
  printlis(myList);

  return 0;

}
