/*
Esercizi su inserimento di elementi
all'interno di Liste

*/


/* linked list data structure */

#include <stdio.h>
#include <stdlib.h>


typedef int DATA;

struct linked_list {
DATA d;
struct linked_list *next;
};

typedef struct linked_list ELEMENT;
typedef ELEMENT * LINK;


LINK newnode(void) {
return malloc(sizeof(ELEMENT));
}

void printlis(LINK lis) {
    while (lis != NULL) {
      printf("%d --> ", lis->d);
        lis= lis->next;
    }
    printf("NULL \n");
}


LINK headinsert_2(LINK lis, int x) {
  /* passo per valore e restituisco il nuovo puntatore
    alla testa della lista
    a livello di procedura chiamante dovrò aggiornare lis col
     nuovo valore; ad esempio A = headinsert_2(A,5)*/

    LINK p;
    p=newnode();
    p->d=x;
    p->next=lis;
    return p;
}


void headinsert(LINK *lis, int x) {
    LINK p;
    p=newnode();
    p->d=x;
    p->next=*lis;
    *lis=p;
}

LINK buildlis(){
  int x;
  LINK lis, p, last;
  printf("Inserire il valore da inserire nella Lista\n");
  scanf("%d", &x);
  //Se x <= 0 creare una lista vuota
  if(x <= 0){
    lis = NULL;
  }
  else{
    last = newnode();
    lis = last;
    last -> d = x;
    last -> next = NULL;
    printf("Inserire il valore da inserire nella Lista\n");
    scanf("%d", &x);
    //Cicla finchè il dato inserito non è > 0
    while(x > 0){
      p = newnode();
      p -> d = x;
      p -> next = NULL;
      last -> next = p;
      last = p;
      printf("Inserire il valore da inserire nella Lista\n");
      scanf("%d", &x);
    }
  }
  return lis;
}


int main() {
LINK A; // Lista A di tipo LINK
LINK temp;

A = NULL;
int n=6;
int i;

temp = NULL;
temp = buildlis();
printlis(temp);

for(i=0;i<n;i++)
    headinsert(&A, random()%10);

//printlis(A);
//for(i=0;i<n;i++)  A = headinsert_2(A, random()%10);


}
