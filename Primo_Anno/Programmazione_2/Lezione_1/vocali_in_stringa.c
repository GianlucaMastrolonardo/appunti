//Esercizio sui FILE (1)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define DIM 100
#define NEWLINE '\n'

int vocalsCounter(char *s)
{
  int i = 0;
  int cnt = 0;
  while(s[i]!='\0'){
      if(s[i] == 'a' || s[i] == 'e' || s[i] == 'i' || s[i] == 'o' || s[i] == 'u'){
        cnt++;
      }
    i++;
  }
  return cnt;
  }

int main() {

  FILE *fp = NULL;
  char *fileName = "cognomi.txt";
  char stringa[DIM];

  int i = 0;

  fp = fopen(fileName, "r");

  if(fp != NULL){
    printf("Apertura File\n");
    i = 0;
    printf("\n");
    while (feof(fp)==0){
      if (fgets(stringa,DIM,fp)!=NULL){
        printf("stringa %d: %s", i, stringa);
        printf("Vocali Presenti: %d",vocalsCounter(stringa));
        printf("\n\n");
        i++;
      }
    }
  }else{
    //ERRORE APERTURA FILE
    printf("Errore Apertura File\n");
    return 1;
  }


  return 0;
}
