#include <stdio.h>
#include <stdlib.h>

typedef int DATA;

typedef struct linked_list
{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode();
LINK buildList();
void printList(LINK list);
