#include <stdio.h>
#include <stdlib.h>
#include "linked_list.h"

LINK buildList()
{
    int x;
    printf("--- Creazione Lista solo numeri positivi ---\n");
    printf("\nInserire Valore: ");
    scanf("%d", &x);
    if (x < 0)
    {
        printf("Creazione Lista Vuota");
        return NULL;
    }
    else
    {
        LINK head, p, tail;
        head = newnode();
        head->d = x;
        head->next = NULL;
        tail = head;
        printf("\nInserire Valore: ");
        scanf("%d", &x);
        while (x >= 0)
        {
            p = newnode();
            p->d = x;
            p->next = NULL;
            tail->next = p;
            tail = p;
            printf("\nInserire Valore: ");
            scanf("%d", &x);
        }
        return head;
    }
}
