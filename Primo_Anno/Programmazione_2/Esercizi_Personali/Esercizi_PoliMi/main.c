// LINK https://boracchi.faculty.polimi.it/teaching/InfoA/2018_Esercizi_Extra_Campi_ListeSemplici.pdf

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef int DATA;

typedef struct linked_list
{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

LINK debug_BuildList()
{
    int x = rand() % 10 - 3;
    if (x >= 0)
    {
        LINK p = newnode();
        p->d = x;
        p->next = debug_BuildList();
        return p;
    }
    else
    {
        return NULL;
    }
}

void debug_PrintList(LINK list)
{
    if (list != NULL)
    {
        printf("%d -> ", list->d);
        debug_PrintList(list->next);
    }
    else
    {
        printf("NULL\n");
    }
}

// Es1)
/*
    • Si codifichi in C la funzione somma avente il seguente prototipo:
        int somma(ELEMENTO *Testa, int M)
    • Tale funzione riceve come parametro la testa della lista e un intero
        M. Quindi, restituisce la somma dei soli valori della lista che sono
        multipli di M. Se la lista è vuota, la funzione restituisce il valore -1.
*/

// Funzione iterativa
int sumElements(LINK list, int M)
{
    int sum = 0;
    if (list == NULL)
    {
        return -1;
    }

    while (list != NULL)
    {
        if ((list->d) % M == 0)
        {
            sum = list->d + sum;
        }
        list = list->next;
    }
    return sum;
}

// Funzione ricorsiva
int sumElements_Ric(LINK list, int M)
{
    if (list == NULL)
    {
        return -1;
    }
    if (list->next == NULL)
    {
        if ((list->d % M) == 0)
        {
            return list->d;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        if ((list->d % M) == 0)
        {
            return list->d + sumElements_Ric(list->next, M);
        }
        else
        {
            return sumElements_Ric(list->next, M);
        }
    }
}

// Es2)
// Trovare con una funzione ricorsiva l’elemento massimo in una lista

// Iterativa
int maxValue(LINK list)
{
    int max;
    if (list != NULL)
    {
        max = list->d;
    }
    else
    {
        return -999;
    }

    while (list != NULL)
    {
        if (list->d > max)
        {
            list->d = max;
        }

        list = list->next;
    }
    return max;
}

// Ricorsiva
int maxValue_Ric(LINK list)
{
    int max;
    if (list == NULL)
    {
        return -999;
    }
    if (list->next == NULL)
    {
        return list->d;
    }
    else
    {
        max = maxValue_Ric(list->next);
        if (max < (list->d))
        {
            return list->d;
        }
        else
        {
            return max;
        }
    }
}

// Es3)
// restituisce la posizione (puntatore) del primo elemento pari nella lista (restituisce NULL se la lista non contiene elementi pari).

// Iterativa
LINK firstEven(LINK list)
{

    while (list != NULL)
    {
        if ((list->d % 2) == 0)
        {
            return list;
        }

        list = list->next;
    }
    return NULL;
}

// Ricorsiva
LINK firstEven_Ric(LINK list)
{
    if (list != NULL)
    {
        if ((list->d % 2) == 0)
        {
            return list;
        }
        return firstEven(list->next);
    }
    return NULL;
}

// Es4)
//  Restituire la posizione del minimo elemento pari (NULL se la lista non ha elementi pari o è vuota)

// Iterativa (senza richiamare la funzione FirstEven)
LINK findFirstEven(LINK list)
{
    LINK p = NULL;
    int isFirstMin = 1;
    while (list != NULL)
    {
        if (((list->d % 2) == 0) && isFirstMin == 1)
        {
            isFirstMin = 0;
            p = list;
        }
        else if (((list->d % 2) == 0) && isFirstMin == 0)
        {
            if ((p->d) > (list->d))
            {
                p = list;
            }
        }

        list = list->next;
    }
    return p;
}

int main()
{

    srand(time(NULL));

    printf("-------\n");
    LINK myList = debug_BuildList();
    debug_PrintList(myList);

    int multiplo = 3;
    printf(">>> Somma di tutti gli elementi multipli di %d: %d\n", multiplo, sumElements_Ric(myList, multiplo));
    printf(">>> Elemento maggiore nella lista : %d\n", maxValue(myList));
    printf(">>> Posizione primo valore pari:\n");
    debug_PrintList(firstEven_Ric(myList));
    printf(">>> Posizine minore valore pari:\n");
    debug_PrintList(findFirstEven(myList));

    return 0;
}