/*
Creare una Lista con all'interno persone lette da file.
Il campo persona è composto da:
    - Nome
    - Cognome
    - Età

Aggiungere persone in testa e in coda con apposite funzioni.
Salvare tutte le persone aggiunte in un nuovo file.
Formato file da salvare:
|NOME: <!--Nome>|COGNOME: <!--Cognome>|ETÀ: <!--Età>|
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct person{
    char name[30];
    char surname[30];
    int age;
} DATA;

typedef struct linked_list{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT * LINK;

LINK newnode(){
    return malloc(sizeof(ELEMENT));
}

LINK buildListFromFile(char *filename){
    LINK head, p, tail;
    FILE *fPtr;
    fPtr = fopen(filename, "r");
    if(fPtr == NULL){
        printf("Errore Apertura File!\n");
        exit(1);
    }
    else{
        char name[30];
        char surname[30];
        int age;
        fscanf(fPtr,"%s %s %d", name, surname, &age);
        //Se File Vuoto
        if(feof(fPtr)){
            return NULL;
        }
        else{
            head = newnode();
            strcpy(head->d.name, name);
            strcpy(head->d.surname, surname);
            head->d.age = age;
            head -> next = NULL;
            tail = head;
            do{
                fscanf(fPtr,"%s %s %d", name, surname, &age);
                p = newnode();
                strcpy(p->d.name, name);
                strcpy(p->d.surname, surname);
                p->d.age = age;
                p -> next = NULL;
                tail -> next = p;
                tail = p;
            }
            while(!feof(fPtr));
        }
    fclose(fPtr);
    return head;
    }
}

void saveNewPerson(char name[30], char surname[30], int age){
    char *filename = "elenco_persone_aggiunte.txt";
    FILE *fPtr;
    fPtr = fopen(filename, "a");
    if(fPtr == NULL){
        printf("Errore Creazione File!");
        exit(1);
    }else{
        fprintf(fPtr, "|NOME: %s|COGNOME: %s|ETÀ: %d\n", name, surname, age);
    }
    fclose(fPtr);
}

void addNewNodeHead(LINK *list){
    printf("--- Inserimento in Testa ---\n");
    LINK head;
    char name[30];
    char surname[30];
    int age;
    printf("Inserire Nome: ");
    scanf("%s", name);
    printf("Inserire Cognome: ");
    scanf("%s", surname);
    printf("Inserire Età: ");
    scanf("%d", &age);

    head = newnode();
    strcpy(head->d.name, name);
    strcpy(head->d.surname, surname);
    head->d.age = age;

    head -> next = *list;
    *list = head;
    saveNewPerson(name, surname, age);
    printf("Aggiunta in Testa Terminata!\n");
}

void addNewNodeTail(LINK *list){
    printf("--- Inserimento in Coda ---\n");
    LINK head, p;
    head = (*list);

    char name[30];
    char surname[30];
    int age;

    while((*list) -> next != NULL){
        *list = (*list) -> next;
    }

    printf("Inserire Nome: ");
    scanf("%s", name);
    printf("Inserire Cognome: ");
    scanf("%s", surname);
    printf("Inserire Età: ");
    scanf("%d", &age);

    p = newnode();
    strcpy(p->d.name, name);
    strcpy(p->d.surname, surname);
    p->d.age = age;
    (*list) -> next = p;
    p->next = NULL;
    (*list) = head;
    saveNewPerson(name, surname, age);
    printf("Aggiunta in Coda Terminata!\n");
}

void printlist(LINK list){
    printf("--- Lista ---\n");
    while(list != NULL){
        printf("%s %s %d\n", list -> d.name, list -> d.surname, list -> d.age);
        list = list -> next;
    }
    printf("NULL\n");
}

int main(){
    char *filename = "elenco_persone.txt";
    LINK myList;

    myList = buildListFromFile(filename);
    printlist(myList);

    addNewNodeHead(&myList);
    printlist(myList);

    addNewNodeTail(&myList);
    printlist(myList);

    return 0;
}