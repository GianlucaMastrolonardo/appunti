// Lavorare con la lista in modo ricorsivo

#include <stdio.h>
#include <stdlib.h>

typedef int DATA;

typedef struct linked_list
{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

LINK buildList()
{
    LINK head, p, tail;
    int x;
    printf("Inserire un numero naturale: ");
    scanf("%d", &x);
    if (x < 0)
    {
        return NULL;
    }
    else
    {
        head = newnode();
        head->d = x;
        head->next = NULL;
        tail = head;
        printf("Inserire un numero naturale: ");
        scanf("%d", &x);
        while (x >= 0)
        {
            p = newnode();
            p->d = x;
            p->next = NULL;
            tail->next = p;
            tail = p;
            printf("Inserire un numero naturale: ");
            scanf("%d", &x);
        }
        return head;
    }
}
// Stampa Lista in modo Ricorsivo
void printListRic(LINK list)
{
    if (list != NULL)
    {
        printf("%d\n", list->d);
        printListRic(list->next);
    }
}
// Stampa lista dal fondo alla cima in modo ricorsivo
void printListRic_Reverse(LINK list)
{
    if (list != NULL)
    {
        printListRic_Reverse(list->next);
        printf("%d\n", list->d);
    }
}
// Stampa i numeri della lista in posizione multipla di x
// pos inizializzato ad 1

void printListSpecificItemRic(LINK list, int pos, int x)
{
    if (list != NULL)
    {
        if (pos % x == 0)
        {
            printf(">> Valore: %d | Posizione :%d\n", list->d, pos);
        }
        printListSpecificItemRic(list->next, pos + 1, x);
    }
}

int sumAllValues(LINK list)
{
    if (list != NULL)
    {
        return list->d + sumAllValues(list->next);
    }
    else
    {
        return 0;
    }
}

void tail_sumAllValues(LINK list, int *sum)
{
    if (list != NULL)
    {
        *sum = *sum + list->d;
        tail_sumAllValues(list->next, sum);
    }
    else
    {
    }
}

int f(LINK l1, LINK l2, int somma1, int somma2)
{
    if (l1 != NULL && l2 != NULL)
    {
        return f(l1->next, l2->next, somma1 + l1->d, somma2 + l2->d);
    }
    if (l1 != NULL)
    {
        return f(l1->next, l2, somma1 + l1->d, somma2);
    }
    if (l2 != NULL)
    {
        return f(l1, l2->next, somma1, somma2 + l2->d);
    }
    if (somma1 > somma2)
    {
        return somma1;
    }
    else
    {
        return somma2;
    }
}

/*Somma i numeri della lista in posizione multipla di x
(versione non di coda) */
/* si suppone pos inizializzato ad 1 */

int sumSpecificValues(LINK list, int pos, int x)
{
    if (list == NULL)
        return 0;
    else
    {
        if ((pos % x) == 0)
        {
            return list->d + sumSpecificValues(list->next, pos + 1, x);
        }
        return sumSpecificValues(list->next, pos + 1, x);
    }
}

int main()
{
    // int sum = 0;
    LINK myList = buildList();
    LINK myList2 = buildList();

    printf("---Stampa Lista---\n");
    printListRic(myList);

    // printf("---Stampa Lista Reverse---\n");
    // printListRic_Reverse(myList);

    printf("---Stampa Elementi in posizione multipla di 2---\n");
    printListSpecificItemRic(myList, 1, 2);

    printf("---Stampa Somma della Lista---\n");
    printf(">> Somma: %d\n", sumAllValues(myList));

    // printf("---Stampa Somma della Lista---\n");
    // tail_sumAllValues(myList, &sum);
    // printf(">> Somma: %d\n", sum);

    printf("---Stampa Somma Multipli di 2 della Lista---\n");
    printf(">> Somma Multipli: %d\n", sumSpecificValues(myList, 1, 2));

    printf("Somma: %d\n", f(myList, myList2, 0, 0));

    return 0;
}