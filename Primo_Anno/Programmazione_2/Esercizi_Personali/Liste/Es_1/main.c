//Creare una lista

#include <stdio.h>
#include <stdlib.h>

typedef int DATA;

typedef struct linked_list{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT * LINK;

LINK newnode(){
    return malloc(sizeof(ELEMENT));
}

LINK buildlist(){
    LINK head, p, tail;
    int x;
    scanf("%d", &x);
    if(x < 0){
        return NULL;
    }else{
        head = newnode();
        head -> d = x;
        head -> next = NULL;
        tail = head;
        scanf("%d", &x);
        while(x > 0){
            p = newnode();
            p -> d = x;
            p -> next = NULL;
            tail -> next = p;
            tail = p;
            scanf("%d", &x);
        }
    return head;
    }
}

void printlist(LINK list){
    while(list != NULL){
        printf("%d --> ", list->d);
        list = list -> next;
    }
    printf("NULL");
}

int main(){

    LINK myList;
    myList = buildlist();
    printlist(myList);





    printf("\n");
    return 0;
}