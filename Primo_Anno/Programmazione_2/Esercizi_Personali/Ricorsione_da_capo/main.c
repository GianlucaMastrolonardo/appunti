#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef int DATA;
typedef struct linked_list
{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

int randomNumber()
{
    return rand() % 10 - 2;
}

// Creazione Lista ricorsiva
LINK buildList()
{
    LINK head = newnode();
    int x = randomNumber();
    if (x >= 0)
    {
        head->d = x;
        head->next = buildList();
        return head;
    }
    else
    {
        return NULL;
    }
}

// Stampa lista ricorsiva
void printList(LINK list)
{
    if (list != NULL)
    {
        printf("%d -> ", list->d);
        printList(list->next);
    }
    else
    {
        printf("NULL\n");
    }
}

// Duplicazione lista ricorsiva
LINK dupListOnlyEven(LINK list)
{
    if (list != NULL)
    {
        if ((list->d % 2) == 0)
        {
            LINK p = newnode();
            p->d = list->d;
            p->next = dupListOnlyEven(list->next);
            return p;
        }
        else
        {
            return dupListOnlyEven(list->next);
        }
    }
    else
    {
        return NULL;
    }
}

// Crea lista da un file in maniera ricorsiva
LINK buildListFromFile(FILE *fPtr)
{
    int x;
    if (fscanf(fPtr, "%d\n", &x) != EOF)
    {
        LINK p = newnode();
        p->d = x;
        p->next = buildListFromFile(fPtr);
        return p;
    }
    else
    {
        return NULL;
    }
}

// Aggiungi nodo in testa con ritorno in maniera ricorsiva
LINK addNewNodeHead(LINK list)
{
    LINK p = newnode();
    printf("Inserire nuovo valore da aggiungere in testa: ");
    scanf("%d", &p->d);
    p->next = list;
    return p;
}

// Aggiungi nodo in testa senza ritorno
void addNewNodeHead_2(LINK *list)
{
    LINK p = newnode();
    printf("Inserire nuovo valore da aggiungere in testa: ");
    scanf("%d", &p->d);
    p->next = *list;
    *list = p;
}

// Aggiungi valore in coda con ritorno
LINK addNewNodeTail(LINK list)
{
    if (list == NULL)
    {
        LINK p = newnode();
        printf("Inserire valore da aggiungere in coda: ");
        scanf("%d", &(p->d));
        p->next = NULL;
        return p;
    }
    else
    {
        list->next = addNewNodeTail(list->next);
        return list;
    }
}

// Aggiungi nodo in coda senza ritorno
void addNewNodeTail_2(LINK *list)
{
    if (*list == NULL)
    {
        LINK p = newnode();
        printf("Inserire valore da aggiungere in coda: ");
        scanf("%d", &(p->d));
        p->next = NULL;
        (*list) = p;
    }
    else
    {
        addNewNodeHead_2(&(*list)->next);
    }
}

// Crea un nuovo nodo contenente x e lo inserisce dopo la prima occorrenza di y

void addNewNodeAfterValue(LINK list, int x, int y)
{
    if (list != NULL)
    {
        if (list->d == y)
        {
            LINK p = newnode();
            p->d = x;
            p->next = list->next;
            list->next = p;
        }
        else
        {
            addNewNodeAfterValue(list->next, x, y);
        }
    }
}

int main()
{
    srand(time(NULL));

    printf(">>> Lista 1:\n");
    LINK myFirstList = buildList();
    printList(myFirstList);

    printf(">>> Lista 1 Duplicata (solo valori pari):\n");
    printList(dupListOnlyEven(myFirstList));

    FILE *fPtr = fopen("input.txt", "r");
    LINK mySecondList;
    if (fPtr != NULL)
    {
        mySecondList = buildListFromFile(fPtr);
        fclose(fPtr);
    }
    else
    {
        printf("Errore Apertura File, Creata Lista 2 vuota!\n");
        mySecondList = NULL;
    }
    printf(">>> Lista 2:\n");
    printList(mySecondList);

    printf(">>> Nuovo valore in Testa su Lista 1:\n");
    myFirstList = addNewNodeHead(myFirstList);
    printList(myFirstList);

    printf(">>> Nuovo valore in Testa su Lista 2:\n");
    addNewNodeHead_2(&mySecondList);
    printList(mySecondList);

    printf(">>> Nuovo valore in Coda su Lista 1:\n");
    myFirstList = addNewNodeTail(myFirstList);
    printList(myFirstList);

    printf(">>> Nuovo valore in cosa su Lista 2:");
    addNewNodeTail_2(&mySecondList);
    printList(mySecondList);

    return 0;
}
