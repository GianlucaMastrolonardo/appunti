#include <stdio.h>
#include <stdlib.h>

int main() {
    int* arrayi;
    int* arrayj;
    int size = 100000000000;  // Dimensione troppo grande

    arrayi = (int*)malloc(size * sizeof(int));
    arrayj = (int*)malloc(size * sizeof(int));

    // Utilizzo dell'array allocato
    for (int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++){
            arrayj[j] = j;
        }
        arrayi[i] = i;
    }

    return 0;
}
