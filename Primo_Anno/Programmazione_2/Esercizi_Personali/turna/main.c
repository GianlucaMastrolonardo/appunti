#include <stdio.h>
#include <stdlib.h>

typedef int DATA;

typedef struct linked_list
{
    DATA d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

/*Data una lista di interi (letti da tastiera) vogliamo
costruire una funzione ricorsiva che crei la lista*/

LINK buildList()
{
    int x;
    scanf("%d", &x);
    if (x >= 0)
    {
        LINK p = newnode();
        p->d = x;
        p->next = buildList();
        return p;
    }
    else
    {
        return NULL;
    }
}

/*Data una lista, stamparne il contenuto in maniera ricorsiva*/

void printList(LINK list)
{
    if (list != NULL)
    {
        printf("%d -> ", list->d);
        printList(list->next);
    }
    else
    {
        printf("NULL\n");
    }
}

/*Data una lista duplicarne il contenuto*/

LINK dupList(LINK list)
{
    if (list != NULL)
    {
        LINK p = newnode();
        p->d = list->d;
        p->next = dupList(list->next);
        return p;
    }
    else
    {
        return NULL;
    }
}

LINK dupListCond(LINK list, int x)
{
    if (list != NULL)
    {
        if (list->d >= x)
        {
            LINK p = newnode();
            p->d = list->d;
            p->next = dupListCond(list->next, x);
            return p;
        }
        else
        {
            return dupListCond(list->next, x);
        }
    }
    else
    {
        return NULL;
    }
}

LINK addNodeHead(LINK list, int x)
{
    LINK p = newnode();
    p->d = x;
    p->next = list;
    return p;
}

void addNodeHead_2(LINK *list, int x)
{
    LINK p = newnode();
    p->d = x;
    p->next = *list;
    *list = p;
}

int findMaxValue(LINK list)
{
    if (list == NULL)
    {
        return -1;
    }
    else
    {
        int max = findMaxValue(list->next);
        if (max < list->d)
        {
            max = list->d;
        }
        return max;
    }
}

LINK addNodeTail(LINK list, int x)
{
    if (list == NULL)
    {
        LINK p = newnode();
        p->d = x;
        p->next = NULL;
        return p;
    }
    else
    {
        list->next = addNodeTail(list->next, x);
        return list;
    }
}

void addNodeTail_2(LINK *list, int x)
{
    if (*list == NULL)
    {
        LINK p = newnode();
        p->d = x;
        p->next = NULL;
        *list = p;
    }
    else
    {
        addNodeTail_2(&(*list)->next, x);
    }
}

/* crea un nuovo nodo contenente x e lo inserisce dopo la
prima occorrenza di y */

void addNodeAfterY(LINK list, int x, int y)
{
    if (list != NULL)
    {
        if (list->d == y)
        {
            LINK p = newnode();
            p->d = x;
            p->next = list->next;
            list->next = p;
        }
        else
        {
            addNodeAfterY(list->next, x, y);
        }
    }
}

/* crea un nuovo nodo contenente x e lo inserisce dopo OGNI occorrenza di
y */

void addNodeAfterEveryY(LINK list, int x, int y)
{
    if (list != NULL)
    {
        if (list->d == y)
        {
            LINK p = newnode();
            p->d = x;
            p->next = list->next;
            list->next = p;
            addNodeAfterEveryY(list->next->next, x, y);
        }
        else
        {
            addNodeAfterEveryY(list->next, x, y);
        }
    }
}

int main(void)
{

    int x, y;

    printf("Inserire i Valori (per uscire inserire un valore negativo):\n");
    LINK myList = buildList();

    printList(myList);

    printf("Duplicazione Lista:\n");
    printList(dupList(myList));

    printf("Duplicazione Lista condizionata (solo valori >= 10):\n");
    printList(dupListCond(myList, 10));

    printf("Inserire valore per Inserimento in testa: ");
    scanf("%d", &x);
    myList = addNodeHead(myList, x);
    printList(myList);

    printf("Inserire valore per Inserimento in coda: ");
    scanf("%d", &x);
    addNodeTail_2(&myList, x);
    printList(myList);

    printf("Inserire valore da cercare per Inserimento: ");
    scanf("%d", &y);
    printf("Inserire valore da aggiungere per Inserimento: ");
    scanf("%d", &x);
    addNodeAfterEveryY(myList, x, y);
    printList(myList);

    printf("Valore massimo all'interno della lista: %d\n", findMaxValue(myList));

    return 0;
}
