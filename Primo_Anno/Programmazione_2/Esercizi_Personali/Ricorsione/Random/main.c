
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

typedef int DATA;
typedef struct linked_list
{
    DATA d;
    struct linked_list *next;

} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

void printNewLine()
{
    printf("-----------------------------------\n");
}

int getRandomNumber()
{
    return rand() % 10 - 2;
}

void printList(LINK list)
{
    if (list != NULL)
    {
        printf("%d -> ", list->d);
        printList(list->next);
    }
    else
    {
        printf("NULL\n");
    }
}

LINK buildRandomList()
{
    int x = getRandomNumber();
    if (x < 0)
    {
        return NULL;
    }
    else
    {
        LINK p = newnode();
        p->d = x;
        p->next = buildRandomList();
        return p;
    }
}

LINK buildListFromFile(FILE *fPtr)
{
    int x;
    if (fscanf(fPtr, "%d\n", &x) != EOF)
    {
        LINK p = newnode();
        p->d = x;
        p->next = buildListFromFile(fPtr);
        return p;
    }
    else
    {
        return NULL;
    }
}

// Trova il MAX all'interno di una lista
int findMaxValue(LINK list)
{
    if (list == NULL)
    {
        return -999;
    }
    else if (list->next == NULL)
    {
        return list->d;
    }
    else
    {
        int max = findMaxValue(list->next);
        if (max < list->d)
        {
            max = list->d;
        }
        return max;
    }
}

// Duplicazione incondizionata
LINK dupList(LINK list)
{
    if (list != NULL)
    {
        LINK listOut = newnode();
        listOut->d = list->d;
        listOut->next = dupList(list->next);
        return listOut;
    }
    else
    {
        return NULL;
    }
}

LINK dupListCond(LINK list)
{
    if (list != NULL)
    {
        LINK listOut = newnode();
        if ((list->d % 2) == 1)
        {
            listOut->d = list->d;
            listOut->next = dupListCond(list->next);
            return listOut;
        }
        else
        {
            return dupListCond(list->next);
        }
    }
    else
    {
        return NULL;
    }
}

void dupListReverse(LINK list, LINK *prec)
{
    if (list != NULL)
    {
        LINK p = newnode();
        p->d = list->d;
        p->next = (*prec);
        (*prec) = p;
        dupListReverse(list->next, prec);
    }
}

LINK findValue(LINK list, int x)
{
    if (list != NULL)
    {
        if (list->d == x)
        {
            return list;
        }
        else
        {
            return findValue(list->next, x);
        }
    }
    else
    {
        return NULL;
    }
}

int main(int argc, char const *argv[])
{

    LINK myInputList;
    FILE *fPtr;

    if (strcmp(argv[1], "0") == 0)
    {
        printf("Lista vuota creata\n");
        myInputList = NULL;
    }
    else if (strcmp(argv[1], "1") == 0)
    {
        srand(time(NULL));
        printf("Genero lista con valori casuali\n");
        myInputList = buildRandomList();
    }
    else if (strcmp(argv[1], "2") == 0)
    {
        printf("Genero lista da un file chiamato input.txt\n");
        fPtr = fopen("input.txt", "r");
        if (fPtr == NULL)
        {
            printf("Errore apertura file\nChiusura programma!\n");
        }
        else
        {
            myInputList = buildListFromFile(fPtr);
        }
        fclose(fPtr);
    }
    else
    {
        printf("Errore nel passaggio dei paramentri, chiusura programma!");
        exit(1);
    }

    printList(myInputList);
    printNewLine();

    printf("Valore maggiore (se lista vuota -999): %d\n", findMaxValue(myInputList));
    printNewLine();

    printf("Duplicazione lista:\n");
    printList(dupList(myInputList));
    printNewLine();

    LINK tmp = NULL;
    printf("Duplicazione lista al contrario:\n");
    dupListReverse(myInputList, &tmp);
    printList(tmp);
    printNewLine();

    printf("Duplicazione lista condizionata (solo numeri dispari):\n");
    printList(dupListCond(myInputList));
    printNewLine();

    int val;
    printf("Inserire valore da ricercare nella lista: ");
    scanf("%d", &val);
    printList(findValue(myInputList, val));

    return 0;
}
