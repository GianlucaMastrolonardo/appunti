#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef int DATA;
typedef struct linked_list
{
    DATA d;
    struct linked_list *next;
} ELEMENT;
typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

int randomnumber()
{
    return rand() % 10 - 2;
}

// Creazione Lista
LINK buildList()
{
    LINK p = newnode();
    int x = randomnumber();
    if (x >= 0)
    {
        p->d = x;
        p->next = buildList();
        return p;
    }
    else
    {
        return NULL;
    }
}

// Stampa lista
void printList(LINK list)
{
    if (list != NULL)
    {
        printf("%d -> ", list->d);
        printList(list->next);
    }
    else
    {
        printf("NULL\n");
    }
}

// Crea lista da un altra lista (salva solo i valori pari)
LINK dupList(LINK input_list)
{
    if (input_list == NULL)
    {
        return NULL;
    }
    if ((input_list->d % 2) == 0)
    {
        LINK p = newnode();
        p->d = input_list->d;
        p->next = dupList(input_list->next);
        return p;
    }
    else
    {
        return dupList(input_list->next);
    }
}

LINK buildListFromFile(FILE *fPtr)
{
    LINK p = newnode();
    if (feof(fPtr) != 0)
    {
        return NULL;
    }
    else
    {
        fscanf(fPtr, "%d\n", &(p->d));
        p->next = buildListFromFile(fPtr);
        return p;
    }
}

LINK searchPosition(LINK list, int pos, int currentpos)
{
    if (list == NULL)
    {
        return NULL;
    }
    else if (currentpos == pos)
    {
        LINK p = newnode();
        p->d = list->d;
        p->next = NULL;
        return p;
    }
    else
    {
        return searchPosition(list->next, pos, currentpos + 1);
    }
}

LINK searchValue(LINK list, int value)
{
    if (list == NULL)
    {
        return NULL;
    }
    else if (list->d == value)
    {
        LINK p = newnode();
        p->d = list->d;
        p->next = NULL;
        return p;
    }
    else
    {
        return searchValue(list->next, value);
    }
}

LINK searchChoose(LINK list)
{
    int searchType;
    printf("0: Ricerca per posizione.\n1: Ricerca per valore.\nTipo di ricerca: ");
    scanf("%d", &searchType);

    if (searchType == 0)
    {
        int pos;
        printf("Inserire la posizione che si vuole cercare: ");
        scanf("%d", &pos);

        int startPosition = 1;
        return searchPosition(list, pos, startPosition);
    }
    else if (searchType == 1)
    {
        int val;
        printf("Inserire il valore che si vuole cercare: ");
        scanf("%d", &val);
        return searchValue(list, val);
    }
    else
    {
        return NULL;
    }
}

void addNode(LINK *list, int x, int pos, int currentposition)
{
    if (*list != NULL)
    {
        if (pos - 1 == currentposition)
        {
            // printf("OK");
            LINK p = newnode();
            p->d = x;
            p->next = (*list)->next;
            (*list)->next = p;
        }
        else
        {
            addNode((&(*list)->next), x, pos, currentposition + 1);
        }
    }
}

void addNodeChoose(LINK *list)
{
    int pos, x;
    printf("Inserire in che posizione aggiungere il nuovo nodo: ");
    scanf("%d", &pos);

    printf("Inserire il valore da aggiungere nel nuovo nodo: ");
    scanf("%d", &x);

    int startPosition = 1;
    addNode(list, x, pos, startPosition);
}

void dupListReverse(LINK list, LINK *prec)
{
    if (list != NULL)
    {
        LINK p = newnode();
        p->d = list->d;
        p->next = (*prec);
        (*prec) = p;
        dupListReverse(list->next, prec);
    }
}

int main()
{
    srand(time(NULL));

    LINK myList = buildList();
    printList(myList);

    LINK pariList = dupList(myList);
    printList(pariList);

    LINK myListFromFile = NULL;

    FILE *fPtr = fopen("file.txt", "r");
    if (fPtr == NULL)
    {
        printf("Errore Apertura File!\n");
        exit(1);
    }
    else
    {
        myListFromFile = buildListFromFile(fPtr);
    }

    printList(myListFromFile);

    printList(searchChoose(myListFromFile));

    addNodeChoose(&myListFromFile);

    printList(myListFromFile);

    LINK acc = NULL;
    dupListReverse(myListFromFile, &acc);
    printList(acc);

    return 0;
}