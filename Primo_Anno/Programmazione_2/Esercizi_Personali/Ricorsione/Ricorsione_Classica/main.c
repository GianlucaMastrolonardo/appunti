/*
Somma. Prodotto, Fattoriale, Fibonacci, Potenza
*/

#include <stdio.h>
#include <stdlib.h>

int sum(int n, int m){
    if(m == 0){
        return n;
    }else return 1 + sum(n, m-1);
}

int prod(int n, int m){
    if(m == 0){
        return 0;
    }else return n + prod(n, m-1);
}

int fact(int n){
    if(n == 0){
        return 1;
    }else return n * fact(n-1);
}

int fib(int n){
    if(n == 0 || n == 1){
        return n;
    } else return fib(n-1) + fib(n-2);
}

int poww(int base, int exp){
    if(base == 0){
        return 0;
    }
    if(exp == 0){
        return 1;
    }else{
        return base * poww(base, exp-1);
    }
}

int main(){
    int n1, n2;
    int res;
    printf("Inserire primo valore: ");
    scanf("%d", &n1);
    printf("Inserire secondo valore: ");
    scanf("%d", &n2);

    res = sum(n1, n2);
    printf("\nSomma tra %d e %d: %d\n", n1, n2, res);

    res = prod(n1, n2);
    printf("Prodotto tra %d e %d: %d\n", n1, n2, res);

    //res = fact(n1);
    printf("--DISABILITATO-- Fattoriale di %d: %d\n", n1, res = 0);

    res = fib(n1);
    printf("Fibonacci di %d: %d\n", n1, res);

    res = poww(n1,n2);
    printf("Potenza con Base %d ed Esponente %d: %d\n", n1, n2, res);

    return 0;
}