#include <stdio.h>
#include <stdlib.h>

typedef int DATA;
typedef struct linked_list
{
    DATA d;
    struct linked_list *next;

} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

// Crea Lista da un File
LINK buildListFromFile(FILE *fPtr)
{
    LINK head = newnode();
    int x;
    if (fscanf(fPtr, "%d\n", &x) == EOF)
    {
        printf("Lettura File completata, Lista Creata con Successo!\n");
        return NULL;
    }
    else
    {
        head->d = x;
        head->next = buildListFromFile(fPtr);
        return head;
    }
}

// Stampa lista
void printList(LINK list)
{
    if (list != NULL)
    {
        printf("%d -> ", list->d);
        printList(list->next);
    }
    else
    {
        printf("NULL\n");
    }
}

// Stampa lista al contrario
void printListReverse(LINK list)
{
    if (list != NULL)
    {
        printListReverse(list->next);
        printf("%d -> ", list->d);
    }
}

// Stampa i numeri della lista in posizione multipla di x (pos inizializzato ad 1)
void printListOnlyMultiplyPosition(LINK list, int x, int currentPoint)
{
    if (list != NULL)
    {
        if (currentPoint % x == 0)
        {
            printf("%d -> ", list->d);
        }
        printListOnlyMultiplyPosition(list->next, x, currentPoint + 1);
    }
    else
    {
        printf("NULL\n");
    }
}

// Sommatoria (non condizionata) di tutti gli elementi in una lista.
int sumALlElements(LINK list, int *acc)
{
    if (list != NULL)
    {
        *acc = list->d + *acc;
        sumALlElements(list->next, &(*acc));
    }
}

int sumAllElements_2(LINK list)
{
    if (list != NULL)
    {
        return list->d + sumAllElements_2(list->next);
    }
    else
    {
        return 0;
    }
}

int main()
{
    LINK myFirstList;

    FILE *fPtr = fopen("file.txt", "r");
    if (fPtr == NULL)
    {
        printf("Errore nell'apertura del file!\nChiusura Programma!\n");
    }
    else
    {
        myFirstList = buildListFromFile(fPtr);
    }

    printf(">>> Stampa Lista:\n");
    printList(myFirstList);

    /*
    printf(">>> Stampa Lista al contrario:\n");
    printListReverse(myFirstList);
    printf("\n");
    */

    printf(">>> Stampa solo elementi in posizione multipla di 3:\n");
    printListOnlyMultiplyPosition(myFirstList, 3, 1);

    printf(">>> Somma di tutti i valori presenti nella lista:\n");
    printf("Funzione mia: ");
    int acc = 0;
    sumALlElements(myFirstList, &acc);
    printf("%d\n", acc);

    printf("Funzione della prof: %d\n", sumAllElements_2(myFirstList));
    return 0;
}
