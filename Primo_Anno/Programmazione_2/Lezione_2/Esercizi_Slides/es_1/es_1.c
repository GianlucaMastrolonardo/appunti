/*
Definire una struttura che contenga le informazioni
su una persona: nome, cognome, età
    • Definire una funzione per caricare da tastiera una
    variabile strutturata come sopra.
    • La funzione deve ritornare la variabile
    strutturata
    • Definire una funzione per visualizzare il contenuto
    di una variabile strutturata come sopra
    • La funzione deve ricevere la variabile come
    parametro di input (per valore)
    • Invocare le due funzioni dal main()
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct person{
    char name[20];
    char surname[20];
    int age;
} person;

//REVIEW: Io l'ho fatta così, ovvero prendendo la struct come valore invece che
//REVIEW: creare la struct all'interno della funzione (guardare funzione loadValues_2).
//REVIEW: É corretto come approccio?
person loadValues(person p){
    printf("Inserire Nome: ");
    scanf("%s", p.name);
    printf("Inserire Cognome: ");
    scanf("%s", p.surname);
    printf("Inserire Eta': ");
    scanf("%d", &p.age);

    return p;
}

person loadValues_2(){
    person p;
    printf("Inserire Nome: ");
    scanf("%s", p.name);
    printf("Inserire Cognome: ");
    scanf("%s", p.surname);
    printf("Inserire Eta': ");
    scanf("%d", &p.age);

    return p;
}

void printValues(person p){
    printf("Nome: \"%s\" | Cognome : \"%s\" | Eta': \"%d\"\n", p.name, p.surname, p.age);
}

int main(){

    person p1;
    person p2;

    p1 = loadValues(p1);
    printValues(p1);
    p2 = loadValues_2();
    printValues(p2);
    
    return 0;
}