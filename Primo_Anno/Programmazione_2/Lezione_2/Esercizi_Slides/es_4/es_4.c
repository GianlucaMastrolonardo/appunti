/*
• Ridefinire la funzione di caricamento della persona
    • La funzione deve ricevere in i dati di una persona da FILE
    secondo il formato:
                nome <spazio> cognome <spazio> età
• ES.
    Mario Rossi 40
    Giuseppe Verdi 70
• Definire una funzione che stampa la struttura persona su FILE,
    secondo il formato
    [Nome]: Mario [Cognome]: Rossi [Età]: 40
    [Nome]: Giuseppe [Cognome]: Verdi [Età]: 70
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct person{
    char name[25];
    char surname[25];
    int age;
} person;

person loadValuesFromFile(char *fileName){
    FILE *fPtr;
    fPtr = fopen(fileName, "r");

    if(fPtr != NULL){
        person p;
        fscanf(fPtr, "%s %s %d\n", p.name, p.surname, &p.age);
        fclose(fPtr);
        return p;
    }else{
        printf("Errore Apertura File!\nChiusura Programma...");
        exit(0);
    }
}

void printValuesOnFile(char *fileName, person p){
    FILE *fPtr;
    fPtr = fopen(fileName, "w");

    if(fPtr != NULL){
        fprintf(fPtr, "[Nome]: %s [Cognome]: %s [Età]: %d\n", p.name, p.surname, p.age);
        fclose(fPtr);
    }else{
        printf("Errore Creazione File!\nChiusura Programma...");
        exit(0);
    }

}

void printValues(person p){
    printf("Nome: \"%s\" | Cognome : \"%s\" | Eta': \"%d\"\n", p.name, p.surname, p.age);
}

int main(){
    char *fileNameInput = "persona.txt";
    char *fileNameOutput = "persona_output.txt";

    person p1;

    p1 = loadValuesFromFile(fileNameInput);
    printValues(p1);
    printValuesOnFile(fileNameOutput, p1);

    return 0;
}