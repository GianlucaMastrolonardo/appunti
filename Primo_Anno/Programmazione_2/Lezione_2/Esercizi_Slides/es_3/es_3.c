/*
• Ridefinire la funzione di caricamento della persona
    • La funzione deve ricevere in input l’indirizzo della cella di
    memoria che contiene la variabile strutturata da caricare
• Ridefinire la funzione di visualizzazione della persona
    • La funzione deve ricevere in input l’indirizzo della cella di
    memoria che contiene la variabile strutturata da visualizzare
• Dato p, puntatore a struttura, si può accedere ai campi della
struttura in due modi alternativi:
(*p).nome_campo
p->nome_campo
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct person{
    char name[20];
    char surname[20];
    int age;
} person;

void loadValuesPointer(person *p){
    printf("Inserire Nome: ");
    scanf("%s", p->name);
    printf("Inserire Cognome: ");
    scanf("%s", p->surname);
    printf("Inserire Eta': ");
    scanf("%d", &p->age);
}

void printValuesPointer(person *p){
    printf("Nome: \"%s\" | Cognome : \"%s\" | Eta': \"%d\"\n", p->name, p->surname, p->age);

}

int main(){

    person p1;

    loadValuesPointer(&p1);
    printValuesPointer(&p1);

    return 0;
}