# Relazione

## Introduzione

L'esercizio richiede lo sviluppo del codice di un programma **JAS** (Java Assembly Language) eseguibile sull' architettura **MIC-1** di un programma che deve essere in grado di calcolare delle potenze utilizzando un metodo (o funzione).
Le potenze da calcolare hanno tutte la medesima base (il valore scelto per la base è **3**), invece i valori dell'esponente sono il numero della matricola dello studente.

Successivamente bisogna convertire il risultato ottenuto in codice binario.

Nel metodo devono essere passati **base** ed **esponente**, e lui ci fornirà in output il valore del risultato.

In questo caso la base scelta è **3**, quindi l'output sarà:

$3^2: 00000000000000000000000000001001
$

$3^0: 00000000000000000000000000000001$

$3^0: 00000000000000000000000000000001$

$3^5: 00000000000000000000000011110011$

$3^0: 00000000000000000000000000000001$

$3^1: 00000000000000000000000000000011$

$3^1: 00000000000000000000000000000011$

$3^4: 00000000000000000000000001010001$

## Svolgimento

L'approccio utilizzato per lo svolgimento dell'esercizio è *divide et impera*, ovvero suddivere un grosso problema in tanti piccoli problemi.

Valutando il problema ho ritenuto l'approccio migliore quello di suddividere l'esercizio in tre macro-problemi: **Moltiplicazione**, **Potenza** e **Stampa dei Valori**.

La risoluzione di questi tre problemi ci fornisce il **Risultato**.

<img title="" src="file:///home/gianluca/Documents/uni/Architettura/Esercizio_Valutato/Relazione/img/Suddivisione_Problemi.png" alt="Suddivisione_Problemi.png" data-align="center">

*Nello schema i blocchi moltiplicazione e potenza sono collegati bidirezionalmente perché il risultato della moltiplicazione serve al metodo potenza, che però a suo volta utilizza quando richiama il metodo moltiplicazione nel ciclo succesivo.*

### Moltiplicazione

Il metodo, presi due numeri come input ci restituirà come output in cima allo stack il risultato.

#### Problemi riscotrati durante lo svolgimento

All'inizio la scelta era ricaduta su un metodo **iterativo**, e non ricorsivo (come visto a lezione), per verificare se riuscivo a creare il metodo autonomamente.

Il metodo era funzionante, però per calcoli leggermente più complessi il tempo di esecuzione era troppo elevato, e considerando che questo metodo veniva chiamato molteplici volte non era una soluzione sostenibile.
Per questo motivo ho deciso di cambiare metodo passando ad un codice **ricorsivo**, riuscendo così a dimezzare il tempo di esecuzione.
Infatti in questo modo la funzione viene richiamata più volte (occupando più celle nello stack, quindi più Ram) ma l'esecuzione è più veloce.

ll metodo prende due parametri interi, $n$ e $m$, e restituisce il risultato della moltiplicazione di $n$ e $m$ attraverso una tecnica di ricorsione.

La funzione controlla se $n$ è uguale a zero (label *caso base*) usando l'istruzione `IFEQ`, se la condizione è vera restituisce **immediatamente** 0 usando l'istruzione `BIPUSH` e `IRETURN`.

Se $n$ è diverso da zero, la funzione richiama se stessa passando come argomenti $n-1$ e $m$ e somma $m$ al risultato ottenuto dalla chiamata ricorsiva, finchè $n-1$ non sarà uguale a zero.

![Testo del paragrafo.png](/home/gianluca/Documents/uni/Architettura/Esercizio_Valutato/Relazione/img/Testo%20del%20paragrafo.png)

### Potenza

La potenza è composta da un loop che utilizza l'esponente come indice, e finchè questo indice non va a zero la funzione **richiama il metodo moltiplicazione** e salva il risulato.

In particolare il metodo prende in input due parametri: $base$ ed $exp$, rispettivamente il valore della base e dell'esponente.

Il metodo utilizza una variabile $res$ per salvare il risultato parziale di ogni iterazione del ciclo. Inizialmente, $res$ viene impostato uguale alla base.

Il metodo quindi controlla se l'esponente è uguale a 0 e, in tal caso, restituisce 1 (perché ogni numero elevato a 0 è uguale a 1).

Se l'esponente è diverso da 0, il metodo entra in un ciclo che decrementa l'esponente a ogni iterazione finché non diventa uguale a 0.
Ad ogni iterazione il metodo calcola la moltiplicazione tra $res$ e la $base$ (per questo motivo prima del ciclo abbiamo impostato $res$ al valore di $base$) e salva il risultato nella variabile $res$.

Infine, quando l'esponente raggiunge 0, il metodo restituisce il valore di $res$ ovvero il risultato della potenza richiesta.

![Metodo_Potenza.png](/home/gianluca/Documents/uni/Architettura/Esercizio_Valutato/Relazione/img/Metodo_Potenza.png)

### Stampa dei Valori

Il metodo per la Stampa dei Valori è composto da due parti:

    **<mark>Prima Parte</mark>**: per la stampa del valore dell'esponente, per ":" e per lo spazio.

    **<mark style="background-color: cyan">Seconda Parte</mark>**: per la stampa del risultato in Codice Binario composto da 32 bit

Per la prima parte la soluzione è abbastanza semplice, ci servirà caricare sullo stack il valore in esadecimale del **Codice ASCII** del simbolo da stampare, in particolare abbiamo:

- Stampa Esponente: sommiamo 30 in esadecimale al valore dell'esponente per ottenere il **codice ASCII**, questo è possibile perché le cifre nella tabella ASCII sono decodificate dal valore 0x30 al 0x39. Bisogna fare questa operazione perchè l'istruzione `OUT` (istruzione per stampare un valore sulla Console) lavora esclusivamente con il Codice ASCII
  Esempio:
  `3 + 0x30 = 0x33 = '3'`

- Stampa ":": in questo caso basterà caricare in cima allo stack il valore 0x3A, ovvero il valore dei due punti del Codice ASCII e poi usare l'istruzione `OUT`

- Stampa " " (spazio): anche qui basterà caricare 0x20 sullo stack e poi usare l'istruzione `OUT`

La seconda parte invece è più complessa, perché inizia assegnando il valore 32 a $cont$, poi carica il valore di $n$ sullo stack e inizia un ciclo che si ripete fino a quando $cont$ non diventa 0.

Per ogni iterazione, il valore di $cont$ viene caricato sullo stack e viene eseguito un'istruzione `IFEQ` per verificare se è uguale a 0.
Se lo è, il ciclo termina e viene eseguito il codice dopo "fineCiclo".
In caso contrario, viene duplicato il valore di $n$ sullo stack e viene eseguito l'istruzione `IFLT` per verificare se è minore di 0. Se lo è, viene stampato il carattere "1" (codice ASCII 0x31) sulla console, altrimenti viene stampato il carattere "0" (codice ASCII 0x30). Dopo, passado per la label *dopoIF*, il valore di $n$ viene sommatto a quello duplicato e viene decrementato il valore di $cont$ di 1 $(cont -1)$. Infine, il ciclo viene ripetuto.

Dopo che il ciclo è terminato, viene eseguito il codice della label *fineCiclo*, che rimuove l'ultimo valore dallo stack (che sarebbe il valore duplicato di $n$ nell'ultima iterazione), stampa una newline (codice ASCII 0x0A) sulla console e restituisce il valore di $n$.

![Progetto Archittetura (2).png](/home/gianluca/Documents/uni/Architettura/Esercizio_Valutato/Relazione/img/stampa.png)

## Conclusione

In conclusione, la scrittura in codice JAS si è rivelata una sfida interessante e impegnativa, in particolare con la funzione *Potenza* e *Stampa*, e la l'evoluzione della Memoria sullo Stack.

Programmare in un linguaggio di basso livello è risultato abbastanza ostico, anche quando si sviluppano programmi non troppo sofisticati, soprattutto per la fase di Debug.
