#include <stdio.h>
#include <stdlib.h>

// Funzione Carica Array
void loadArray(int A[], int dimensionA)
{
    int i;

    for (i = 0; i < dimensionA; i++)
    {
        scanf("%d", &A[i]);
    }
}

// Funzione Stampa Array
void printArray(int A[], int dimensionA)
{
    int i;

    for (i = 0; i < dimensionA; i++)
    {
        printf("Posizione %d: %d\n", i, A[i]);
    }
}

// Funzione Trova Minimo e Massimo
void findMinAndMaxArray(int A[], int dimensionA, int *minValue, int *maxValue)
{
    int i;
    *minValue = A[0];
    *maxValue = A[0];
    for (i = 1; i < dimensionA; i++)
    {
        if (*minValue > A[i])
        {
            *minValue = A[i];
        }
        if (*maxValue < A[i])
        {
            *maxValue = A[i];
        }
    }
}

// Funzione Somma tutti gli Elementi di Un Array
int sumAllArray(int A[], int dimensionA)
{
    int i, sum = 0;
    for (i = 0; i < dimensionA; i++)
    {
        sum = sum + A[i];
    }
    return sum;
}

// Funzione Differenza tutti gli Elementi di Un Array
int diffAllArray(int A[], int dimensionA)
{
    int i, diff = A[0];
    for (i = 1; i < dimensionA; i++)
    {
        diff = diff - A[i];
    }
    return diff;
}

int main(void)
{
    int dimension = 5, myArray[dimension];
    int i, maxValueInsideArray, minValueInsideArray, sumValueInsideArray, diffValueInsideArray;

    loadArray(myArray, dimension);
    printf("---PRINT ARRAY---\n");

    printArray(myArray, dimension);

    findMinAndMaxArray(myArray, dimension, &minValueInsideArray, &maxValueInsideArray);
    printf("Il Valore Minimo e': %d | Il Valore Massimo e': %d\n", minValueInsideArray, maxValueInsideArray);

    sumValueInsideArray = sumAllArray(myArray, dimension);
    printf("Il Valore della Somma di tutti i Valori nell'Array e': %d\n", sumValueInsideArray);

    diffValueInsideArray = diffAllArray(myArray, dimension);
    printf("Il Valore della Differenza di tutti i Valori nell'Array e': %d\n", diffValueInsideArray);

    return 0;
}
