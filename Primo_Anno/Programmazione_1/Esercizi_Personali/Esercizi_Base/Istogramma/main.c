/*
Creare un Istrogramma in basato su N dati;
*/

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char const *argv[])
{

    int dimension;
    int i, j;

    printf("Dati Presenti nell'Istogramma: ");
    scanf("%d", dimension);
    int dataNumbers[dimension];

    for (i = 0; i < dimension; i++)
    {
        printf("Inserire Dato dell'Elemento %d: ", i);
        scanf("%d", &dataNumbers[i]);
    }

    for (i = 0; i < dataNumbers[i]; i++)
    {
        printf("*");
    }

    return 0;
}
