#include <stdio.h>
#include <stdlib.h>

// Scrittura File

int main(int argc, char const *argv[])
{
    // Dichiarare un File puntatore FILE
    // Creare il File stabilendo la modalità di utilizzo (w,r,a+,...)
    // Scrittura sul File
    // Chiusura File

    // Dichiarare un File puntatore FILE
    FILE *puntatore_file;

    // Creare ed Aprire il file (se il file esiste già lo va semplicemente a sovrascrivere)
    puntatore_file = fopen("file.txt", "w");

    if (puntatore_file != NULL)
    {
        printf("File Ok\n");
        // Scrittura sul File
        fprintf(puntatore_file, "Testo che finirà nel File\nEvvia!");
        printf("Scrittura Avvenuta\n");
    }
    else
    {
        printf("Errore Apertura File\n");
    }

    // Chiusura File
    fclose(puntatore_file);

    return 0;
}
