#include <stdio.h>
#include <stdlib.h>

// Lettura File

int main(int argc, char const *argv[])
{
    FILE *puntatore_file;

    char cognome[100];
    char nome[100];

    puntatore_file = fopen("cognomenome.txt", "r");

    if (puntatore_file != NULL)
    {
        printf("| File Letto!\n");
        while (!feof(puntatore_file))
        {
            fscanf(puntatore_file, "%s %s\n", &cognome, &nome);
            printf("%s | %s\n", cognome, nome);
        }
        printf("| File Del File!\n");
    }
    else
    {
        printf("File Assente!");
    }

    fclose(puntatore_file);

    return 0;
}
