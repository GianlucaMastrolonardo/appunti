#include <stdio.h>
#include <stdlib.h>

void es2(int a[], int dim, int x, char name_file[20], int *howManyTimesHighest)
{

    for (int i = 0; i < dim; i++)
    {
        if (a[i] < x)
        {
            a[i] = -1;
        }
        else
        {
            *howManyTimesHighest = *howManyTimesHighest + 1;
        }
        printf("%d ", a[i]);
    }

    FILE *fp;

    fp = fopen(name_file, "w");

    if (fp != NULL)
    {
        printf("\nFile Creato");
        for (int i = 0; i < dim; i++)
        {
            if ((i % 3) == 0)
            {
                fprintf(fp, "\n*%d", a[i]);
            }
            else
            {
                fprintf(fp, "|%d", a[i]);
            }
        }
        fprintf(fp, "\n@");
    }
    else
    {
        printf("ERRORE!");
    }
    fclose(fp);
}

int main(int argc, char const *argv[])
{
    int dim = 5;
    int myArray[] = {3, 2, 5, 2, 2};
    int howManyTimesHighest = 0;
    int x;
    char filename[20];

    printf("Inserire il valore di X: ");
    scanf("%d", &x);

    printf("Inserire il nome del file: ");
    scanf("%s", &filename);

    es2(myArray, dim, x, filename, &howManyTimesHighest);

    printf("\n%d", howManyTimesHighest);

    return 0;
}
