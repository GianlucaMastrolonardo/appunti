#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int main(int argc, char const *argv[])
{
    int a, b;

    printf("A: ");
    scanf("%d", &a);
    printf("\nB: ");
    scanf("%d", &b);

    int i = a;
    int x = 1;
    int j = 0;

    while ((i > a) && (i < a + b))
    {
        j = 0;
        while (j < b)
        {
            x = x * a;
            j = (j + 1) * 2;
        }
        assert(!(j < b));
        i = i + (a - b);
    }
    assert((i <= a || i >= (a + b)));

    printf("X: %d\ni + x - j: ", x, i + x - j);

    return 0;
}
