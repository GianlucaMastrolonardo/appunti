#include <stdio.h>
#include <stdlib.h>

#define ROW 3
#define COL 4

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void printMatrix(int row, int col, int matrix[row][col])
{
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}

void findMinMaxForRow(int row, int col, int matrix[row][col], int min[], int max[])
{
    int i = 0;
    int j = 0;
    int posArr = 0;
    int temp_min, temp_max;

    while (i < row)
    {
        temp_min = matrix[i][0];
        temp_max = matrix[i][0];

        while (j < col)
        {
            if (temp_min > matrix[i][j])
            {
                temp_min = matrix[i][j];
            }
            if (temp_max < matrix[i][j])
            {
                temp_max = matrix[i][j];
            }
            j++;
        }
        j = 0;
        min[posArr] = temp_min;
        max[posArr] = temp_max;

        i++;
        posArr++;
    }
}

int main(void)
{
    int myMatrix[ROW][COL] = {
        {6, 2, -4, 8},
        {8, 1, 11, 6},
        {-3, 0, -2, -4}};

    int minArr[ROW] = {999, 999, 999};
    int maxArr[ROW] = {999, 999, 999};

    printMatrix(ROW, COL, myMatrix);

    findMinMaxForRow(ROW, COL, myMatrix, minArr, maxArr);

    printf("Min: ");
    printArr(minArr, ROW);
    printf("Max: ");
    printArr(maxArr, ROW);

    return 0;
}
