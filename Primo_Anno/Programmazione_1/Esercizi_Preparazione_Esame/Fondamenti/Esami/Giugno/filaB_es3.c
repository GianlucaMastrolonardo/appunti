#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 9

int fun(int arr1[], int arr2[], int dim1, int dim2, int x)
{
    int i = 0;
    int j = 0;

    int cnt = 0;
    while (arr1[i] != -1 && arr2[j] != -1)
    {
        if (arr1[i] + arr2[j] >= x)
        {
            cnt++;
        }
        i++;
        j++;
    }

    while (arr1[i] != -1)
    {
        if (arr1[i] >= x)
        {
            cnt++;
        }
        i++;
    }

    while (arr2[j] != -1)
    {
        if (arr2[j] >= x)
        {
            cnt++;
        }
        j++;
    }
    return cnt;
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 12 - 1;

    while (i < dim - 1 && x > 0)
    {
        arr[i] = x;
        i++;
        x = rand() % 12 - 1;
    }
    arr[i] = -1;
}

int main(void)
{
    srand(time(NULL));
    int arr1[DIM];
    int arr2[DIM];
    int x;

    populateArr(arr1, DIM);
    populateArr(arr2, DIM);

    printArr(arr1, DIM);
    printArr(arr2, DIM);

    printf("Inserire il valore di x: ");
    scanf("%d", &x);

    int res = fun(arr1, arr2, DIM, DIM, x);

    printf("RES: %d\n", res);

    return 0;
}
