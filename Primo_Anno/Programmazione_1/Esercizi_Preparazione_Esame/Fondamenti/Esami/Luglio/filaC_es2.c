#include <stdio.h>
#include <stdlib.h>

#define ROW 3
#define COL 4

void myFunction(int n, int m, int mat1[n][m], int mat2[n][m], int res[n][2])
{
    for (int i = 0; i < n; i++)
    {
        res[i][0] = 0; // Imposto il valore di entrambe le righe
        res[i][1] = 0; // a zero
        for (int j = 0; j < m; j++)
        {
            if (mat1[i][j] > mat2[i][j])
            {
                res[i][0] = res[i][0] + 1;
            }
            else if (mat2[i][j] > mat1[i][j])
            {
                res[i][1] = res[i][1] + 1;
            }
        }
    }
}

void printMatrix(int n, int m, int mat[n][m])
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            printf("%d ", mat[i][j]);
        }
        printf("\n");
    }
}

int main(void)
{
    int MatrixA[ROW][COL] = {
        {1, 2, -4, 5},
        {3, 4, 5, -1},
        {-3, 0, -2, -4}};

    int MatrixB[ROW][COL] = {
        {5, 2, -4, 1},
        {3, 1, 2, -3},
        {4, 2, -5, 1}};

    int res[ROW][2];

    myFunction(ROW, COL, MatrixA, MatrixB, res);

    printf("--- Matrice A ---\n");
    printMatrix(ROW, COL, MatrixA);
    printf("\n");
    printf("--- Matrice B ---\n");
    printMatrix(ROW, COL, MatrixB);
    printf("\n");
    printf("--- Matrice Risultato ---\n");
    printMatrix(ROW, 2, res);

    return 0;
}
