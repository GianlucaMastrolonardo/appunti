#include <stdio.h>
#include <stdlib.h>

#define ROW 3
#define COL 4

void myFunction(int n, int m, int matrix[n][m], int res[n][2])
{
    for (int i = 0; i < n; i++)
    {
        int sum = 0;
        int prod = 1;

        for (int j = 0; j < m; j++)
        {
            sum += matrix[i][j];
            prod *= matrix[i][j];
        }
        res[i][0] = sum;
        res[i][1] = prod;
    }
}

void printMatrix(int n, int m, int mat[n][m])
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            printf("%d ", mat[i][j]);
        }
        printf("\n");
    }
}

int main(void)
{
    int myMatrix[ROW][COL] = {
        {3, 1, -2, 1},
        {2, 0, 1, 6},
        {10, 2, 3, 4}};

    int res[ROW][2];

    myFunction(ROW, COL, myMatrix, res);
    printMatrix(ROW, COL, myMatrix);
    printf("------\n");
    printMatrix(ROW, 2, res);

    return 0;
}
