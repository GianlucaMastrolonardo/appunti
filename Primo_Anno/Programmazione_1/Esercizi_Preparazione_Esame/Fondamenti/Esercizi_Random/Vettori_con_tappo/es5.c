/*
Dati due vettori con tappo A e B di dimensione n, costruire
un nuovo vettore C (di dimensione 2n) ottenuto concatendando gli
elementi da posizione 0 a posizione k di A con quelli da posizione h
in poi di B (h < n, k < n).

Si noti che non può essere fatta alcuna ipotesi sulla lunghezza
di A e B.

Si richiede inoltre di risolvere l'esercizio senza adottare funzioni
che contino il numero di elementi in A e B.

Esempio:
A: 3, 5, -1
B: 1, 9, 7, 8, 9, 6, 4, 3, -1

K = 4 | H = 2

O: 3, 5, 7, 8, 9, 6, 4, 3, -1
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 5

void myFunction(int arr1[], int arr2[], int res[], int dim, int dimRes)
{
    int k,h;

    printf("Valore di K: ");
    scanf("%d", &k);
    while(!(k < dim && k > 0)){
        printf("Inserisci un valore minore di DIM: ");
        scanf("%d", &k);
    }
    printf("Valore di H: ");
    scanf("%d", &h);
    while(!(h < dim && k > 0)){
        printf("Inserisci un valore minore di DIM: ");
        scanf("%d", &h);
    }

    int i = 0;
    int j = 0;
    int z = 0;

    while(arr1[i] != -1 && i <= k){
        res[z] = arr1[i];
        i++;
        z++;
    }

    while(arr2[j] != -1){
        if(j >= h){
            res[z] = arr2[j];
            z++;
        }
        j++;
    }

    res[z] = -1;

}

void printAllArray(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }

    printf("\n");
}

void populateArray(int arr[], int dim)
{
    int x = rand() % 12 - 1;
    int i = 0;
    while ((i < dim - 1) && (x > 0))
    {
        arr[i] = x;
        i++;
        x = rand() % 12 - 1;
    }
    arr[i] = -1;
}

int main()
{
    srand(time(NULL));

    int firstArray[DIM];
    int secondArray[DIM];
    int resArray[DIM*2];

    populateArray(firstArray, DIM);
    populateArray(secondArray, DIM);

    printf("Array A\n");
    printAllArray(firstArray, DIM);

    printf("Array B\n");
    printAllArray(secondArray, DIM);

    myFunction(firstArray,secondArray,resArray, DIM, DIM*2);

    printf("Array R\n");
    printAllArray(resArray, DIM*2);

    return 0;
}