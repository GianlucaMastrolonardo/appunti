// Dati due vettori A e B, costruire il vettore C con la somma posizionale dei vettori A e B, fermandosi alla fine del più lungo
//(x padded dove x è un valore passato per valore)

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 5

void myFunction(int arr1[], int arr2[], int res[], int x, int dim1, int dim2, int dim3)
{
    int i = 0;
    int j = 0;
    int k = 0;
    while (arr1[i] != -1 && arr2[j] != -1)
    {
        res[k] = arr1[i] + arr2[j];
        i++;
        j++;
        k++;
    }
    while (arr1[i] != -1)
    {
        res[k] = arr1[i] + x;
        i++;
        k++;
    }
    while (arr2[j] != -1)
    {
        res[k] = arr2[j] + x;
        j++;
        k++;
    }
    res[k] = -1;
}

void printArray(int arr[], int dim)
{
    int i = 0;
    while (arr[i] != -1)
    {
        printf("%d ", arr[i]);
        i++;
    }
    printf("%d\n", arr[i]);
}

void populateArray(int arr[], int dim)
{
    int x = rand() % 12 - 1;
    int i = 0;
    while ((i < dim - 1) && (x > 0))
    {
        arr[i] = x;
        i++;
        x = rand() % 12 - 1;
    }
    arr[i] = -1;
}

int main()
{
    srand(time(NULL));

    int firstArray[DIM];
    int secondArray[DIM];

    int resArray[DIM];

    int x;

    populateArray(firstArray, DIM);
    populateArray(secondArray, DIM);

    printf("Array A\n");
    printArray(firstArray, DIM);

    printf("Array B\n");
    printArray(secondArray, DIM);

    printf("Inserire il valore di X: ");
    scanf("%d", &x);

    myFunction(firstArray, secondArray, resArray, x, DIM, DIM, DIM);

    printf("Array Res\n");
    printArray(resArray, DIM);

    return 0;
}