// Dato un vettore con tappo eliminare il primo valore di x

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 5

void myFunction(int arr[], int x, int dim)
{
    int i = 0;
    int isDeleted = 0;

    while (arr[i] != -1 && isDeleted == 0)
    {
        if (arr[i] == x)
        {
            int j = i;
            while (arr[j] != -1)
            {
                arr[j] = arr[j + 1];
                j++;
            }
            arr[j] = -1;
            isDeleted = 1;
        }
        i++;
    }
}

void printAllArray(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }

    printf("\n");
}

void populateArray(int arr[], int dim)
{
    int x = rand() % 12 - 1;
    int i = 0;
    while ((i < dim - 1) && (x > 0))
    {
        arr[i] = x;
        i++;
        x = rand() % 12 - 1;
    }
    arr[i] = -1;
}

int main()
{
    srand(time(NULL));

    int firstArray[DIM];

    int x;

    populateArray(firstArray, DIM);

    printf("Array A\n");
    printAllArray(firstArray, DIM);

    printf("Inserire il valore di X: ");
    scanf("%d", &x);

    myFunction(firstArray, x, DIM);

    printf("Array A\n");
    printAllArray(firstArray, DIM);

    return 0;
}