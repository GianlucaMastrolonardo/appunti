#include <stdio.h>
#include <stdlib.h>

/*
Dati due Vettori con tappo trovare quante volte sono uguali nella stessa posizione
*/

int howManyTimesArrayAreSame(int a[], int b[], int dim)
{
    int count = 0;
    int i = 0;

    while (a[i] != -1 && b[i] != -1)
    {
        if (a[i] == b[i])
        {
            count = count + 1;
        }
        i++;
    }

    return count;
}

int main(int argc, char const *argv[])
{
    int dim = 7;
    int arrayA[7] = {4, 4, 2, 0, 2, 4, -1};
    int arrayB[4] = {1, 4, 5, -1};

    // Stampo Array

    for (int i = 0; i < dim; i++)
    {
        printf("Posizione: %d | Valore Array A: %d | Valore Array B: %d\n", i, arrayA[i], arrayB[i]);
    }

    int sameValues = howManyTimesArrayAreSame(arrayA, arrayB, dim);

    printf("%d", sameValues);

    return 0;
}
