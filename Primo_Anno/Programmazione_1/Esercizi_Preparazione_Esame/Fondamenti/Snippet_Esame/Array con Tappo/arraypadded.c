#include <stdio.h>
#include <stdlib.h>

/*
Dati due Vettori con tappo scorrere fino alla fine del più lungo (il vettore più lungo sarà compararato con dei Padded)
contare quante volte A[i] > B[i]
*/

int manyTimesAbiggerthenB(int a[], int b[], int x, int dim)
{
    int i = 0;
    int j = 0;
    int count = 0;

    while (a[i] != -1 && b[j] != -1)
    {
        if (a[i] > b[i])
        {
            printf("A > B\n");
            count++;
        }
        i++;
        j++;
    }

    while (a[i] != -1)
    {
        if (a[i] > x)
        {
            printf("A > x\n");
            count++;
        }
        i++;
    }

    while (b[j] != -1)
    {
        if (b[j] < x)
        {
            printf("B < x\n");
            count++;
        }
        j++;
    }

    return count;
}

int main(void)
{
    int dim = 7;
    int arrayA[7] = {4, 4, 2, 0, 2, 4, -1};
    int arrayB[4] = {1, 3, 5, -1};

    for (int i = 0; i < dim; i++)
    {
        printf("Posizione: %d | Valore Array A: %d | Valore Array B: %d\n", i, arrayA[i], arrayB[i]);
    }

    int x;

    printf("Valore X: ");
    scanf("%d", &x);

    int cazzi = manyTimesAbiggerthenB(arrayA, arrayB, x, dim);

    printf("\nSe funziono bene questo vale: %d", cazzi);

    return 0;
}
