/*
L'Insertion Sort è un algoritmo di ordinamento che opera suddividendo la lista in due parti: una parte ordinata e una parte non ordinata.

Inizia con un elemento dalla parte non ordinata e lo inserisce nella posizione
corretta all'interno della parte ordinata, spostando gli elementi più grandi
di un posto verso destra. Questo processo viene ripetuto finché tutti gli elementi
sono stati inseriti nella parte ordinata, risultando in una lista completamente ordinata.

L'Insertion Sort è efficiente per piccole liste ma diventa lento su liste molto grandi, poiché richiede un tempo quadratico per l'ordinamento.
*/

#include <stdio.h>
#include <stdlib.h>

#define DIM 10

// Insertion Sort
void insertion_sort(int arr[], int dim)
{
    int i, insert, moveitem;
    for (i = 1; i < dim; i++)
    {
        insert = arr[i];
        moveitem = i;
        while (moveitem > 0 && arr[moveitem - 1] > insert)
        {
            arr[moveitem] = arr[moveitem - 1];
            moveitem--;
        }
        arr[moveitem] = insert;
    }
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main(int argc, char const *argv[])
{
    int myArray[DIM] = {3, 4, 6, 1, 2, 5, 7, 9, 8, 0};

    printArr(myArray, DIM);

    insertion_sort(myArray, DIM);
    printArr(myArray, DIM);

    return 0;
}
