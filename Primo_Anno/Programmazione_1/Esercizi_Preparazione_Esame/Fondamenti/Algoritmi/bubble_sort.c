/*
Il Bubble Sort è un algoritmo di ordinamento che confronta ripetutamente
coppie di elementi adiacenti e scambia posizione a quelli fuori ordine fino a quando l'intera lista è ordinata.

In ogni passaggio, il più grande (o il più piccolo, a seconda dell'ordine desiderato) elemento "galleggia" verso la sua posizione finale.
Questo processo viene ripetuto finché non sono più necessari scambi, indicando che la lista è completamente ordinata.

Il Bubble Sort è semplice da implementare ma non è efficiente su liste molto grandi,
poiché richiede un tempo quadratico per l'ordinamento e può richiedere molte iterazioni per liste disordinate.
*/

#include <stdio.h>
#include <stdlib.h>

#define DIM 10

void bubble_sort(int arr[], int dim)
{
    int i, j, tmp;
    for (i = dim - 1; i > 0; i--)
    {
        for (j = 0; j < i; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
            }
        }
    }
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main(int argc, char const *argv[])
{
    int myArray[DIM] = {3, 4, 6, 1, 2, 5, 7, 9, 8, 0};

    printArr(myArray, DIM);

    bubble_sort(myArray, DIM);
    printArr(myArray, DIM);
    return 0;
}
