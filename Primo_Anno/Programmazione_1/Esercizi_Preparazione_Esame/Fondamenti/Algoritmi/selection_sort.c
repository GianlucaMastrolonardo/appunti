#include <stdio.h>
#include <stdlib.h>

#define DIM 10

// Selection Sort swap
void swap(int arr[], int first, int second)
{
    int tmp = arr[first];
    arr[first] = arr[second];
    arr[second] = tmp;
}

// Selection Sort (Trovo il minimo e lo metto in posizione i)
void selection_sort(int arr[], int dim)
{
    int i = 0;
    while (i < dim)
    {
        int j = i + 1;
        int smallest = i;
        while (j < dim)
        {
            if (arr[smallest] > arr[j])
            {
                smallest = j;
            }
            j++;
        }
        swap(arr, smallest, i);

        i++;
    }
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main(int argc, char const *argv[])
{
    int myArray[DIM] = {3, 4, 6, 1, 2, 5, 7, 9, 8, 0};

    printArr(myArray, DIM);

    selection_sort(myArray, DIM);
    printArr(myArray, DIM);
    return 0;
}
