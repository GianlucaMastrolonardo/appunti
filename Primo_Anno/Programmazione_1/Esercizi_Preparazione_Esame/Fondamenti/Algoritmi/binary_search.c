#include <stdio.h>
#include <stdlib.h>

#define DIM 10

int binary_search(int arr[], int dim, int x)
{
    int start = 0;
    int end = dim - 1;

    int med = (start + end) / 2;

    int pos = -1;

    while ((start <= end) && (pos == -1))
    {
        if (arr[med] == x)
        {
            pos = med;
        }
        else
        {
            if (arr[med] > x)
            {
                end = med - 1;
            }
            else
            {
                start = med + 1;
            }
            med = (start + end) / 2;
        }
    }
    return pos;
}

int main(void)
{
    int myArray[DIM] = {1, 2, 4, 6, 10, 13, 14, 15, 16, 17}; // L'Array deve essere ordinato

    int x;

    printf("Inserire valore da cercare all'intero dell'Array: ");
    scanf("%d", &x);

    printf("Il valore si trova in posizione: %d", binary_search(myArray, DIM, x));

    return 0;
}
