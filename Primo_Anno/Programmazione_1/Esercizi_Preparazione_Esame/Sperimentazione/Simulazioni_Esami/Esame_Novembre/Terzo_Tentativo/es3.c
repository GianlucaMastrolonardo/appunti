#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 9

void myFun(int arr1[], int arr2[], int dim1, int dim2)
{
    int i = 0;
    while (arr1[i] != -1 && arr2[i] != -1)
    {
        if (arr1[i] > arr2[i])
        {
            arr1[i]++;
        }
        else if (arr2[i] > arr1[i])
        {
            arr2[i]++;
        }
        i++;
    }
    if (arr2[i] == -1 && arr1[i] != -1)
    {
        arr1[i] = -1;
    }

    while (arr2[i] != -1 && arr1[i] == -1 && i < dim1 - 1)
    {

        arr1[i] = arr2[i] * 2;
        arr1[i + 1] = -1;
        i++;
    }
}

void printArr(int arr[], int dim)
{
    int i = 0;
    while (i < dim)
    {
        printf("%d ", arr[i]);
        i++;
    }
    printf("\n");
}

void populateArr(int arr[], int dim)
{
    int x = rand() % 12 - 1;
    int i = 0;
    while (x > 0 && i < dim - 1)
    {
        arr[i] = x;
        i++;
        x = rand() % 12 - 1;
    }
    arr[i] = -1;
}

int main()
{
    srand(time(NULL));
    int firstArr[DIM] = {};
    int secondArr[DIM] = {};

    populateArr(firstArr, DIM);
    populateArr(secondArr, DIM);

    printArr(firstArr, DIM);
    printArr(secondArr, DIM);

    printf("-----\n");
    myFun(firstArr, secondArr, DIM, DIM);
    printArr(firstArr, DIM);
    printArr(secondArr, DIM);

    return 0;
}