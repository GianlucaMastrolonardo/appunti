#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int main()
{

    int a, b;
    scanf("%d", &a);
    scanf("%d", &b);

    int i = a;
    int j = 0;
    int x = 1;

    while (a < i && i < a + b)
    {
        j = 0;
        while (j < b)
        {
            x = x * a;
            j = (j + 1) * 2;
        }
        assert(!(j < b));
        i = i + (a - b);
    }
    assert(!(a < i && i < a + b));

    printf("%d", x);
    printf("%d", i + x - j);

    return 0;
}
