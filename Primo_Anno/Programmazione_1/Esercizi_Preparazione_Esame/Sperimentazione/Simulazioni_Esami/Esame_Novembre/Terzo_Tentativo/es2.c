#include <stdio.h>
#include <stdlib.h>

#define ROW 3
#define COL 3

void myFunction(int row, int col, int matrix[row][col], char filename[], int x, int *cnt)
{
    *cnt = 0;
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            if (matrix[i][j] < x)
            {
                matrix[i][j] = -1;
            }
            else if (matrix[i][j] > x)
            {
                *cnt = *cnt + 1;
            }
        }
    }

    FILE *fPtr = fopen(filename, "w");
    if (fPtr == NULL)
    {
        printf("Errore creazione file\nChiusura programma...\n");
        exit(1);
    }
    else
    {
        for (int i = 0; i < row; i++)
        {
            fprintf(fPtr, "*");
            for (int j = 0; j < col; j++)
            {
                fprintf(fPtr, "%d", matrix[i][j]);
                if (j < col - 1)
                {
                    fprintf(fPtr, "|");
                }
            }
            fprintf(fPtr, "\n");
        }
        fprintf(fPtr, "@");
    }

    fclose(fPtr);
}

void printMatrix(int row, int col, int matrix[row][col])
{
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}

int main()
{
    int myMatrix[ROW][COL] = {
        {1, 2, 3},
        {1, 2, 3},
        {3, 6, 7}};

    int x;
    int cnt = 0;
    char filename[40] = "es2_fileOutput.txt";

    printMatrix(ROW, COL, myMatrix);

    printf("Valore da cercare: ");
    scanf("%d", &x);

    myFunction(ROW, COL, myMatrix, filename, x, &cnt);

    return 0;
}