/*
Scrivere una funzione che presi come input una matrice di interi, un nome di file, e un intero X, modifichi la matrice sostituendo
a tutti i valori minori di X il valore di -1 e restituisca in una variabile passata per riferimento il numero di volte che è presente un valore maggiore di X nella matrice,
e salva la matrice modificata sul file corrispondente al nome passato come input come mostrato in esempio:
Esempio:
matrice in input
1 2 3 1 2 3 3 6 7

X =3
Matrice modificata
-1 -1 3 -1 -1 3 3 6 7
File salvato
*-1|-1|3
*-1|-1|3
*3|6|7
@
*/

#include <stdio.h>
#include <stdlib.h>

#define ROW 3
#define COL 3

void myFunction(int rowDim, int colDim, int matrix[rowDim][colDim], char filename[], int x, int *cnt)
{
    FILE *fPtr = fopen(filename, "w");
    if (fPtr == NULL)
    {
        printf("Impossibile aprire il file!\n");
        exit(1);
    }
    else
    {
        for (int i = 0; i < rowDim; i++)
        {
            fprintf(fPtr, "*");
            for (int j = 0; j < colDim; j++)
            {
                if (matrix[i][j] < x)
                {
                    matrix[i][j] = -1;
                }
                else if (matrix[i][j] > x)
                {
                    *cnt = *cnt + 1;
                }
                fprintf(fPtr, "%d", matrix[i][j]);
                if (j < colDim - 1)
                {
                    fprintf(fPtr, "|");
                }
            }
            fprintf(fPtr, "\n");
        }
        fprintf(fPtr, "@");
    }
    /*
    FILE *fPtr = fopen(filename, "w");
    if (fPtr == NULL)
    {
        printf("Impossibile aprire il file!\n");
        exit(1);
    }
    else
    {
        for (int i = 0; i < rowDim; i++)
        {
            fprintf(fPtr, "*");
            for (int j = 0; j < colDim; j++)
            {
                fprintf(fPtr, "%d", matrix[i][j]);
                if (j < colDim - 1)
                {
                    fprintf(fPtr, "|");
                }
            }
            fprintf(fPtr, "\n");
        }
        fprintf(fPtr, "@");
    }
    */
}

int main()
{
    int myMatrix[ROW][COL] = {
        {1, 2, 3},
        {1, 2, 3},
        {3, 6, 7}};

    char fileName[20] = "es2_fileOutput.txt";

    int x;
    printf("Inserire valore: ");
    scanf("%d", &x);

    int cnt = 0;

    myFunction(ROW, COL, myMatrix, fileName, x, &cnt);

    return 0;
}