/*
Definire una funzione che presi in input due vettori a tappo. Si modifichi il primo nel seguente modo:
Confrontando elemento per elemento, si aumenti di una unita l’elemento maggiore indipendentemente del vettore di appartenenza.
Se non esiste un elemento corrispondente nel secondo per effettuare il confronto, il valore dell’elemento del primo vettore viene cancellato.
se non esiste un elemento nel primo vettore ma esiste nel secondo, si inserisca un nuovo elemento nel primo
vettore contente il valore doppio dell’elemento in questione del secondo vettore (se possibile).
In tutti gli altri casi non si faccia nulla
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 10

void fun(int arr1[], int arr2[], int dim1, int dim2)
{
    int i = 0;
    int j = 0;

    while (arr1[i] != -1 && arr2[i] != -1)
    {
        if (arr1[i] > arr2[j])
        {
            arr1[i]++;
        }
        else if (arr1[i] < arr2[j])
        {
            arr2[j]++;
        }
        i++;
        j++;
    }

    if (arr2[j] == -1 && arr1[i] != -1)
    {
        arr1[i] = -1;
    }
    while (arr2[j] != -1 && i < dim1 - 1)
    {
        arr1[i] = arr2[j] * 2;
        arr1[i + 1] = -1;
        i++;
        j++;
    }
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 12 - 2;

    while (i < dim - 1 && x > 0)
    {
        arr[i] = x;
        i++;
        x = rand() % 12 - 2;
    }
    arr[i] = -1;
}

int main()
{
    srand(time(NULL));

    int firstArr[DIM] = {};
    int secondArr[DIM] = {};

    populateArr(firstArr, DIM);
    populateArr(secondArr, DIM);

    printArr(firstArr, DIM);
    printArr(secondArr, DIM);

    fun(firstArr, secondArr, DIM, DIM);
    printf("---POST FUNZIONE---\n");
    printArr(firstArr, DIM);
    printArr(secondArr, DIM);

    return 0;
}