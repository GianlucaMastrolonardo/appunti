#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 10

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 10 - 2;

    while (i < dim - 1 && x > 0)
    {
        arr[i] = x;
        x = rand() % 15 - 2;

        i++;
    }
    arr[i] = -1;
}

void funzione(int a[], int b[], int dimA, int dimB)
{
    int i = 0, j = 0;

    //  Confrontando elemento per elemento,
    // si aumenti di una unita l’elemento maggiore indipendentemente del vettore di appartenenza.
    while (a[i] != -1 && b[i] != -1)
    {
        if (a[i] > b[j])
        {
            a[i] = a[i] + 1;
        }
        else if (b[j] > a[i])
        {
            b[j] = b[j] + 1;
        }
        i = i + 1;
        j = j + 1;
    }

    //	Se non esiste un elemento corrispondente nel secondo per effettuare il confronto,
    // il valore dell’elemento del primo vettore viene cancellato
    if (a[i] != -1 && b[j] == -1)
    { // è richiesto di cancelalre tutti gli lementi in eccesso di a, quindi basta posizionare il tappa nel punto giusto
        a[i] = -1;
    }

    // se non esiste un elemento nel primo vettore ma esiste nel secondo,
    // si inserisca un nuovo elemento nel primo vettore contente il valore doppio dell’elemento in questione del secondo vettore (se possibile).
    while (b[j] != -1 && a[i] == -1 && (i < dimA - 1)) // l'ultimo congiunto serve per controllare se ho ancora spazio in A
    {

        a[i] = b[j] * 2; // metto il nuovo valore
        a[i + 1] = -1;   // metto il tappo dopo il nuovo valore, che è stato inserito in fondo

        j = j + 1;
        i = i + 1;
    }
    return;
}

int main()
{
    srand(time(NULL));

    int firstArray[DIM] = {};
    int secondArray[DIM] = {};

    populateArr(firstArray, DIM);
    populateArr(secondArray, DIM);

    printf("Array1: ");
    printArr(firstArray, DIM);
    printf("Array2: ");
    printArr(secondArray, DIM);

    funzione(firstArray, secondArray, DIM, DIM);
    printf("Array1 modificato: ");
    printArr(firstArray, DIM);

    printf("Array2 [DEBUG]: ");
    printArr(secondArray, DIM);
    return 0;
}