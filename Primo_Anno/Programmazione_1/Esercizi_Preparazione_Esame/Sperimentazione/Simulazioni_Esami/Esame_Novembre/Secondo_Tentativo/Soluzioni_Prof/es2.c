#include <stdio.h>
#include <stdlib.h>

void funzione(int riga, int colonna, int matrice[riga][colonna], int x, char *nome, int *contatore)
{
    int i, j;
    FILE *fp;

    *contatore =0; 

    for(i=0;i<riga;i++)
    {
        for(j=0;j<colonna;j++)
        {
            if(matrice[i][j]<x)//conto gli elementi che soddisfanno la caratteristica di ricerca
            {
                matrice[i][j]=-1;
            }
            if(matrice[i][j]>x) //modifico il contenuto della matrice se necessario
            {
                *contatore = *contatore+1;
            }
        }
    }
   
    fp=fopen(nome, "w"); //apro il file
    if(fp!=NULL) //controllo che l'apertura del file sia andata a buon fine solo in questo caso posso scrivere su file
    {
        for(i=0;i<riga;i++)
        {
            fprintf(fp,"*"); //simbolo di inzio riga 
            for(j=0;j<colonna;j++)
            {
                fprintf(fp,"%d ",matrice[i][j]); //valore
                if(j!=colonna-1) //necessario per non aggiungere un | in più a fine di ogni riga
                {
                    fprintf(fp,"|");  //separatore
                }
            }
            fprintf(fp, "\n");
        }
        fprintf(fp, "@");
    }
    else printf("errore apertura file\n");
   
    fclose(fp); //chiudo il file

    return;
}