/*
Esercizio 2
Scrivere una funzione che presi come input una matrice di interi, un nome di file, e un intero X, modifichi la matrice sostituendo
a tutti i valori minori di X il valore di -1 e restituisca in una variabile passata
per riferimento il numero di volte che è presente un valore maggiore di X nella matrice,
e salva la matrice modificata sul file corrispondente al nome passato come input come mostrato in esempio:
Esempio:
matrice in input
1 2 3
1 2 3
3 6 7

X =3
Matrice modificata
-1 -1 3
-1 -1 3
3 6 7

File salvato
*-1|-1|3
*-1|-1|3
*3|6|7
@
*/

#include <stdio.h>
#include <stdlib.h>

#define ROW 3
#define COL 3

void myFunction(int row, int col, int myMatrix[row][col], char filename[], int x, int *cnt)
{
    *cnt = 0;
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            if (myMatrix[i][j] < x)
            {
                myMatrix[i][j] = -1;
            }
            else if (myMatrix[i][j] > x)
            {
                *cnt = *cnt + 1;
            }
        }
    }

    FILE *fPtr = fopen(filename, "w");
    if (fPtr == NULL)
    {
        printf("Errore Creazione File, CHIUSURA PROGRAMMA\n");
        exit(1);
    }
    else
    {
        for (int i = 0; i < row; i++)
        {
            fprintf(fPtr, "*");
            for (int j = 0; j < col; j++)
            {
                fprintf(fPtr, "%d", myMatrix[i][j]);
                if (j < col - 1)
                {
                    fprintf(fPtr, "|");
                }
            }
            fprintf(fPtr, "\n");
        }
        fprintf(fPtr, "@");
    }
    fclose(fPtr);
}

void debugPrintMatrix(int row, int col, int myMatrix[row][col])
{
    for (int i = 0; i < ROW; i++)
    {
        for (int j = 0; j < COL; j++)
        {
            printf("%d ", myMatrix[i][j]);
        }
        printf("\n");
    }
}

int main(void)
{

    int myMatrix[ROW][COL] = {
        {1, 2, 3},
        {1, 2, 3},
        {3, 6, 7}};

    int x;
    int cnt = 0;
    char fileOutput[50] = "es2_file_output.txt";

    debugPrintMatrix(COL, ROW, myMatrix);

    printf("X: ");
    scanf("%d", &x);

    myFunction(ROW, COL, myMatrix, fileOutput, x, &cnt);
    debugPrintMatrix(COL, ROW, myMatrix);
    printf("Valori Maggiori di %d: %d\n", x, cnt);

    return 0;
}