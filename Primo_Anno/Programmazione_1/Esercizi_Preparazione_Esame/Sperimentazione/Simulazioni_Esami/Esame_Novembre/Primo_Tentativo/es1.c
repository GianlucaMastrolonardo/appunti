#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int main()
{

    int a, b;
    printf("Inserire valore di A:");
    scanf("%d", &a);

    printf("Inserire valore di B:");
    scanf("%d", &b);

    int i = a;
    int j = 0;
    int x = 1;
    while ((a < i) && (i < (a + b)))
    {
        j = 0;
        while (j < b)
        {
            x = x * a;
            j = (j + 1) * 2;
        }
        assert(!(j < b));
        i = i + (a - b);
    }
    assert(!((a < i) && (i < (a + b))));
    x = i + x - j;
    printf("X: %d", x);

    return 0;
}