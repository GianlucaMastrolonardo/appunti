/*
Scrivere una funzione che presi come input una matrice di interi, un nome di file, e un intero X, modifichi la matrice sostituendo a tutti i valori
minori di X il valore di -1 e restituisca in una variabile passata per riferimento il numero di volte che è presente un valore maggiore di X nella matrice,
e salva la matrice modificata sul file corrispondente al nome passato come input come mostrato in esempio:

Esempio:
matrice in input
1 2 3 1 2 3 3 6 7

X = 3
Matrice modificata
-1 -1 3 -1 -1 3 3 6 7
File salvato
*-1|-1|3
*-1|-1|3
*3|6|7
@
*/

#include <stdlib.h>
#include <stdio.h>
#define COL 3
#define ROW 3

void printMatrix(int row, int col, int matrix[row][col]){
    for(int i = 0; i < row; i++){
        for(int j = 0; j < col; j++){
            printf("%d ",matrix[i][j]);
        }
        printf("\n");
    }
    printf("----\n");
}

void myFunc(int row, int col, int matrix[row][col], char namefile[], int x, int *cnt){
    for(int i = 0; i < row; i++){
        for(int j = 0; j < col; j++){
            if(x > matrix[i][j]){
                matrix[i][j] = -1;
            }else{
                *cnt = *cnt+1;
            }
        }
    }

    FILE *fPtr = fopen(namefile, "w");
    if(fPtr == NULL){
        printf("Errore creazione file!\nChiusura programma.");
        exit(1);
    }else{
        for(int i = 0; i < row; i++){
            fprintf(fPtr, "*");
            for(int j = 0; j < col; j++){
                fprintf(fPtr, "%d", matrix[i][j]);
                if(j < col-1){
                    fprintf(fPtr, "|");
                }
            }
            fprintf(fPtr, "\n");
        }
        fprintf(fPtr, "@");
    }

    fclose(fPtr);
}

int main(){

    int myMatrix[ROW][COL] = {
        {1, 2, 3},
        {1, 2, 3},
        {3, 6, 7}
    };
    printMatrix(ROW, COL, myMatrix);


    int cnt = 0;
    int x;

    char namefile[25] = "file.txt";

    printf("Inserire valore per la ricerca: ");
    scanf("%d", &x);

    myFunc(ROW, COL, myMatrix, namefile, x, &cnt);
    printMatrix(ROW, COL, myMatrix);
    printf("CNT: %d", cnt);

    return 0;
}
