/*
Definire una funzione che presi in input due vettori a tappo. Si modifichi il primo nel seguente modo:
Confrontando elemento per elemento, si aumenti di una unita l’elemento maggiore indipendentemente del vettore di appartenenza.
Se non esiste un elemento corrispondente nel secondo per effettuare il confronto, il valore dell’elemento del primo vettore viene cancellato.
se non esiste un elemento nel primo vettore ma esiste nel secondo, si inserisca un nuovo elemento nel primo vettore
contente il valore doppio dell’elemento in questione del secondo vettore (se possibile).
In tutti gli altri casi non si faccia nulla 

*/

#include <stdio.h>
#include <stdlib.h>

#define DIM 5

void myFunction(int arrayA[], int arrayB[], int dim){
    int i = 0, j = 0;

    while(arrayA[i] != -1 && arrayB[j] != -1){
        if(arrayA[i] > arrayB[j]){
            arrayA[i] = arrayA[i]+1;
        }else if(arrayB[j] > arrayA[i]){
            arrayB[j] = arrayB[j]+1;
        }

        i++;
        j++;
    }

    //Se non esiste un elemento corrispondente nel secondo per effettuare il confronto, il valore dell’elemento del primo vettore viene cancellato.
    if(arrayB[j] == -1 && arrayA[i] != -1){
        arrayA[i] = -1;
    }

    /*  
    Se non esiste un elemento nel primo vettore ma esiste nel secondo, si inserisca un nuovo elemento nel primo vettore
    contente il valore doppio dell’elemento in questione del secondo vettore (se possibile).
    */
    while(arrayA[i] == -1 && arrayB[i] != -1 && (i < dim - 1)){
        arrayA[i]=arrayB[j]*2;  //metto il nuovo valore 
        arrayA[i+1]= -1; //metto il tappo dopo il nuovo valore, che è stato inserito in fondo 

        j=j+1;
        i=i+1;
    }
}

void buildArrayTappo(int arrayTappo[], int dim){
    int cnt = 0;
    int x;

    printf("Inserire valore in posizione %d: ", cnt);
    scanf("%d", &x);
    while(x >= 0 && cnt < dim -1){
        arrayTappo[cnt] = x;
        cnt++;
        if(cnt < dim -1){
            printf("Inserire valore in posizione %d: ", cnt);
            scanf("%d", &x);
        }
        
    }
    arrayTappo[cnt] = -1;

}

void printArrayTappo(int arrayTappo[], int dim){
    int cnt = 0;
    while(arrayTappo[cnt] != -1){
        printf("%d ", arrayTappo[cnt]);
        cnt++;
    }
    printf("\n");
}

void printArray(int array[], int dim){
    for(int i = 0; i < dim; i++){
        printf("%d ", array[i]);
    }
    printf("\n");
}

int main(){

    int myFirstArrayTappo[DIM];
    int mySecondArrayTappo[DIM];

    buildArrayTappo(myFirstArrayTappo, DIM);
    printArray(myFirstArrayTappo, DIM);

    buildArrayTappo(mySecondArrayTappo, DIM);
    printArray(mySecondArrayTappo, DIM);

    printf("Array Originali:\n");
    printArray(myFirstArrayTappo, DIM);
    printArray(mySecondArrayTappo, DIM);

    printf("Array Modificati:\n");
    myFunction(myFirstArrayTappo, mySecondArrayTappo, DIM);
    printArray(myFirstArrayTappo, DIM);
    printArray(mySecondArrayTappo, DIM);


    return 0;
}