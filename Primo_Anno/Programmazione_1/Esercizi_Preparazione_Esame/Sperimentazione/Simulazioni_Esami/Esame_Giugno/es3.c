/*
Scrivere una funzione che presi in input due vettori a tappo, modifichi il primo vettore a tappo nel seguente
modo:
- Se l’elemento del primo vettore a tappo e minore del corrispondente elemento del secondo viene
    sovrascritto con un valore che è il prodotto dei due
- Se i due elementi sono uguali l’elemento del primo vettore a tappo viene sovrascritto con -1
- Se non è possibile effettuare il confronto si mette il valore presente nel secondo vettore a tappo
    fino a quando è possibile.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 10

void myFunction(int arr1[], int arr2[], int dim1, int dim2)
{
    int i = 0;
    int j = 0;
    // int exitWhile = 0;

    while (arr1[i] != -1 && arr2[j] != -1)
    {
        if (arr1[i] < arr2[j])
        {
            arr1[i] = arr1[i] * arr2[j];
        }

        if (arr1[i + 1] == arr2[j + 1])
        {
            arr1[i + 1] = -1;
        }

        i++;
        j++;
    }

    while (arr1[i] == -1 && arr2[j] != -1 && (i < dim1 - 1))
    {
        arr1[i] = arr2[j];
        i++;
        j++;
        arr1[i] = -1;
    }
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 12 - 1;

    while (i < dim - 1 && x > 0)
    {

        arr[i] = x;

        /* HARDCODED per DEBUG
        if (i == 4)
        {
            arr[i] = 6;
        }
        */

        i++;

        x = rand() % 15 - 2;
    }

    arr[i] = -1;
}

int main(void)
{
    srand(time(NULL));

    int firstArr[DIM] = {};
    int secondArr[DIM] = {};

    populateArr(firstArr, DIM);
    populateArr(secondArr, DIM);

    printf("Array1: ");
    printArr(firstArr, DIM);
    printf("Array2: ");
    printArr(secondArr, DIM);

    myFunction(firstArr, secondArr, DIM, DIM);

    printf("-----POST FUNZIONE-----\nArray1: ");
    printArr(firstArr, DIM);
    printf("Array2: ");
    printArr(secondArr, DIM);

    return 0;
}
