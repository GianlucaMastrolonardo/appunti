#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int main()
{
    int n, m;
    scanf("%d", &n);
    scanf("%d", &m);

    int out = 0;
    int i = 0;
    int j = 2;

    while ((-n < i) && (i < n))
    {
        assert(((-n < i) && (i < n)));
        j = 0;
        while (j < m)
        {
            out = out + n;
            j = j + 2;
        }
        assert(!(j < m));
        i = m - n;
    }

    printf("%d\n", out);
    printf("%d\n", n + m + i);

    return 0;
}