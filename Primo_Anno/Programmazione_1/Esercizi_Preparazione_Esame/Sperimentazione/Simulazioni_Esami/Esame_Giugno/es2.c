/*
Scrivere una funzione che presa in input una matrice di interi e il nome di un file, restituisca due interi che
rappresentano le coordinare dell’ultima istanza del il minimo nella matrice. Entrambi i valori devono essere
restituito tramite variabili passate per riferimento.

La matrice venga salvata sul file indicato nel seguente modo all’inizio del primo elemento una parentesi
quadra aperta, dopo l’ultimo elemento una parentesi quadra chiusa. Su ogni riga del file una riga della
matrice, gli elementi sono separate tra di loro tra *
Esempio di file salvato
[1*2*5
3*0*4
2*3*4
2*2*2]
*/

#include <stdio.h>
#include <stdlib.h>

#define ROW 4
#define COL 3

void function(int row, int col, int mat[row][col], char filename[], int *lastX, int *lastY)
{
    // Considerando una matrice composta da almeno un elemento
    int min = mat[0][0];
    *lastX = 0;
    *lastY = 0;

    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            if (mat[i][j] <= min)
            {
                min = mat[i][j];
                *lastX = j;
                *lastY = i;
            }
        }
    }

    FILE *fPtr = fopen(filename, "w");
    if (fPtr == NULL)
    {
        printf("Errore apertura file!\n");
        exit(1);
    }
    else
    {
        fprintf(fPtr, "[");
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                fprintf(fPtr, "%d", mat[i][j]);
                if (j < col - 1)
                {
                    fprintf(fPtr, "*");
                }
            }
            if (i < row - 1)
            {
                fprintf(fPtr, "\n");
            }
            else
            {
                fprintf(fPtr, "]");
            }
        }
    }
    fclose(fPtr);
}

void printMatrix(int row, int col, int mat[row][col])
{
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("%d ", mat[i][j]);
        }
        printf("\n");
    }
}

int main()
{

    int myMatrix[ROW][COL] = {
        {1, 2, 5},
        {3, 0, -4},
        {2, 3, 4},
        {2, 2, 2}};

    char fileOutput[50] = "es2_output.txt";
    int posX, posY;

    printMatrix(ROW, COL, myMatrix);

    function(ROW, COL, myMatrix, fileOutput, &posX, &posY);
    printf("Ultima posizione:\n\tX: %d\n\tY: %d\n", posX, posY);

    return 0;
}