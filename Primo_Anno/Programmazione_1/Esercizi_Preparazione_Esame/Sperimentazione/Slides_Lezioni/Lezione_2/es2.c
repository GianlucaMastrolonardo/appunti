/*
Si scriva un programma in linguaggio C per poter analizzare una sequenza di numeri positivi o nulli.
Si chieda all’utente di dire quanti numeri N vuole inserire.
L’utente può interrompere prima la sequenza se inserisce il valore di ‘-1’
Dati N numeri interi letti da tastiera si vogliono calcolare e stampare su schermo diversi risultati:
• quanti la somma di tutti i numeri, la media dei numeri inseriti (double)
• quanti sono i numeri pari e dispari
• quanti sono i numeri positivi e nulli
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n;
    int inputNumber;

    int accPari = 0, accDispari = 0, accTotal = 0, accPos = 0, accNull = 0, sum = 0;
    double avg = 0;

    printf("Inserire lunghezza lista: ");
    scanf("%d", &n);

    if (n > 0)
    {
        printf("Inserire valore: ");
        scanf("%d", &inputNumber);
        while (accTotal <= n-1 && inputNumber != -1)
        {
            accTotal++;
            if (inputNumber % 2 == 0)
            {
                accPari++;
            }
            else
            {
                accDispari++;
            }
            if (inputNumber > 0)
            {
                accPos++;
            }
            else
            {
                accNull++;
            }
            sum = inputNumber + sum;
            printf("Inserire valore: ");
            scanf("%d", &inputNumber);
        }
    }
    if(accTotal > 1){
        avg = sum / accTotal;
    }
    

    printf("Numeri Totali: %d\nNumeri Pari: %d\nNumeri Dispari: %d\nNumeri Positivi: %d\nNumeri Nulli: %d\nMedia: %lf", accTotal, accPari, accDispari, accPos, accNull, avg);

    return 0;
}