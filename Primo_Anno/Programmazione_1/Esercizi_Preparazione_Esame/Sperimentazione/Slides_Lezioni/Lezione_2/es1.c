/*

Si scriva un programma in linguaggio C per il calcolo dei quadrati perfetti per una sequenza di numeri.
Il programma deve prima leggere un numero N inserito da tastiera, e quindi stampare i primi quadrati N perfetti sino al quadrato del numero.

Un numero Y si definisce quadrato perfetto se è possibile descriverlo come Y = X*X

*/

#include <stdio.h>
#include <stdlib.h>

int main(){
    int n;

    printf("Inserire il valore: ");
    scanf("%d", &n);

    int i = 0;
    while(i < n){
        printf("%d\n", i);
        i++;
        i = i*i;
    }

    return 0;
}