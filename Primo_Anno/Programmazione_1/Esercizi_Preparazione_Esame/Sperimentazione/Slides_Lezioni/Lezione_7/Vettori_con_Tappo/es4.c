/*
Funzione che preso un vettore a tappo e un valore X, e cancella tutte le occorrenza di X all’interno del vettore


Posso itererare tante volte la soluzione precedente? Oppure c’è un modo più efficiente?

*/

#include <stdio.h>
#include <stdlib.h>
#define DIM 8

void deleteX(int array[], int dim, int x)
{
    int i = 0;
    int j = 0;

    while (array[i] != -1)
    {
        if (array[i] != x)
        {
            array[j] = array[i];
            j++;
        }
        i++;
    }
    array[j] = -1;
}

void debugBuildArray(int array[], int dim)
{
    int i = 0;
    int x;

    printf("Inserire valore: ");
    scanf("%d", &x);

    while (x != -1 && (i < dim - 1))
    {
        array[i] = x;
        i++;

        // SOLO PER NON CHIEDERE UN INPUT INUTILE
        if (i < dim - 1)
        {
            printf("Inserire valore: ");
            scanf("%d", &x);
        }

        array[i] = -1;
    }
}

void debugPrintArray(int array[], int dim)
{
    int i = 0;
    while (array[i] != -1)
    {
        printf("%d ", array[i]);
        i++;
    }
    printf("\n");
}

int main()
{

    int myArray[DIM] = {};
    int x;

    debugBuildArray(myArray, DIM);
    debugPrintArray(myArray, DIM);

    printf("Inserire il valore che si desidera eliminare: ");
    scanf("%d", &x);

    deleteX(myArray, DIM, x);
    debugPrintArray(myArray, DIM);

    return 0;
}