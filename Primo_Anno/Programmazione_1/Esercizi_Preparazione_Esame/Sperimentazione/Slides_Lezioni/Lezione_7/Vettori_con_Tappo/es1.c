/*
Scrivere una funzione per inserire valori all’interno di un vettore a tappo.
Si termina quando l’utente mette -1 oppure quando non c’è più spazio nel vettore.

Se l’inserimento termina tramite l’input dell’utente restituire 1 altrimenti -1
*/

#include <stdio.h>
#include <stdlib.h>

#define DIM 5

int buildArray(int a[], int dim){
    int x;
    int cnt = 0;

    printf("Inserire valore da inserire nell'array: ");
    scanf("%d", &x);

    while(cnt < dim-1 && x != -1){
        a[cnt] = x;
        cnt++;

        //IDEA SOLO PER NON CHIEDERE IN INPUT UN VALORE CHE SAREBBE COMUNQUE SCARTATO
        if(cnt < dim -1){
            printf("Inserire valore da inserire nell'array: ");
            scanf("%d", &x);
        }
    }

    a[cnt] = -1;

    if(x == -1){
        return 1;
    }else{
        return -1;
    }
}

void printAllArray(int array[], int dim){
    printf("Stampa globale array: ");
    for(int i = 0; i < dim; i++){
        printf("%d ", array[i]);
    }
    printf("\n");
}

void printArrayMarker(int array[], int dim){
    int i = 0;
    printf("Stampa array fino a tappo: ");
    while(array[i] != -1 && i < dim){
        printf("%d ", array[i]);
        i++;
    }

    if(array[i] != -1){
        printf("ERRORE\nL'array non ha il tappo!");
    }

    printf("\n");
}

int main(){

    int myArray[DIM] = {};
    printf("DIMENSIONE ARRAY: %d\n", DIM);

    printf("Output Funzione: %d\n",buildArray(myArray, DIM));

    printArrayMarker(myArray, DIM);



    return 0;
}