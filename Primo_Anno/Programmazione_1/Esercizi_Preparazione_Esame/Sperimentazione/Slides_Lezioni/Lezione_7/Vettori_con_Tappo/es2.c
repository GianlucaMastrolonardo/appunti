/*
Funzione che preso un vettore a tappo ne stampi il contenuto a video: ogni elemento deve essere separato dal successivo da * il tappo non deve essere stampato

Es.

1*2*5*2*6
*/

#include <stdio.h>
#include <stdlib.h>

#define DIM 5

void buildArray(int array[], int dim){
    int i = 0;
    int x;

    printf("Inserire valore da inserire nell' Array: ");
    scanf("%d", &x);

    while((i < dim -1) && (x != -1)){
        array[i] = x;
        i++;

        //IDEA SOLO PER NON CHIEDERE IN INPUT UN VALORE CHE SAREBBE COMUNQUE SCARTATO
        if(i < dim-1){
            printf("Inserire valore da inserire nell' Array: ");
            scanf("%d", &x);
        }
    }

    array[i] = -1;
}

void printArray(int array[], int dim){
    int i = 0;
    while(array[i] != -1){
        printf("%d", array[i]);
        if(array[i+1] != -1){
            printf("*");
        }
        i++;
    }
    printf("\n");
}

int main(){
    int myArray[DIM] = {};

    buildArray(myArray, DIM);
    printArray(myArray, DIM);


    return 0;
}