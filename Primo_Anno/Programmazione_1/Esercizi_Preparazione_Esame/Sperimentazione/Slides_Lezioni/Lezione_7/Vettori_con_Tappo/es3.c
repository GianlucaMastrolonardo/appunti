/*
Funzione che preso un vettore a tappo e un valore X, e cancella la prima occorrenza di X all’interno del vettore

Esiste X?
Sposto tutti gli elementi successivi rispetto alla posizione di X indietro di una posizione
*/

#include <stdio.h>
#include <stdlib.h>
#define DIM 8

void deleteX(int array[], int dim, int x)
{
    int isFounded = 0;
    int i = 0;

    while (array[i] != -1)
    {
        if (array[i] == x && isFounded == 0)
        {
            isFounded = 1;
        }

        if (isFounded == 1)
        {
            array[i] = array[i + 1];
        }
        i++;
    }
}

void debugBuildArray(int array[], int dim)
{
    int i = 0;
    int x;

    printf("Inserire valore: ");
    scanf("%d", &x);

    while (x != -1 && (i < dim - 1))
    {
        array[i] = x;
        i++;

        // SOLO PER NON CHIEDERE UN INPUT INUTILE
        if (i < dim - 1)
        {
            printf("Inserire valore: ");
            scanf("%d", &x);
        }

        array[i] = -1;
    }
}

void debugPrintArray(int array[], int dim)
{
    int i = 0;
    while (array[i] != -1)
    {
        printf("%d ", array[i]);
        i++;
    }
    printf("\n");
}

int main()
{

    int myArray[DIM] = {};
    int x;

    debugBuildArray(myArray, DIM);
    debugPrintArray(myArray, DIM);

    printf("Inserire il valore che si desidera eliminare: ");
    scanf("%d", &x);

    deleteX(myArray, DIM, x);
    debugPrintArray(myArray, DIM);

    return 0;
}