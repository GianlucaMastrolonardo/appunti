/*
Funzione inserimento di nuovo valore newn prima di ogni occorrenza di x.

Richiesta di gestire l’overwlow.
Segnalare tramite il valore di ritorno:
    tutto ok (fatti tutti gli inserimenti): 1
    nessun inserimento possibile: 0
    fatti alcuni inserimenti e poi fallito: -1 (se non c'è spazio)
*/

#include <stdio.h>
#include <stdlib.h>

#define DIM 8

int insertBeforeX(int array[], int dim, int x, int value)
{
    int i = 0;
    int j = 0;
    int insertionCount = 0;

    while (array[i] != -1)
    {
        if (array[i] == x)
        {
            if (j < dim - 1)
            {
                // Inserisci il valore prima di x
                array[j] = value;
                j++;
                insertionCount++;
            }
            else
            {
                return -1;
            }
        }

        array[j] = array[i];
        j++;
        i++;
    }

    if (insertionCount > 0)
    {
        array[j] = -1;
        return 1;
    }
    return 0;

    return 0;
}

void debugPrintArray(int array[], int dim)
{
    int i = 0;
    while (array[i] != -1)
    {
        printf("%d ", array[i]);
        i++;
    }
    printf("\n");
}

void debugPrintAll(int array[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", array[i]);
    }
    printf("\n");
}

void debugBuildArray(int array[], int dim)
{
    int x;
    int i = 0;

    printf("Inserire valore: ");
    scanf("%d", &x);

    while ((i < dim - 1) && x != -1)
    {
        array[i] = x;
        i++;

        if (i < dim - 1)
        {
            printf("Inserire valore: ");
            scanf("%d", &x);
        }
    }
    array[i] = -1;
}

int main()
{
    int myArray[DIM] = {};
    int x, value;

    debugBuildArray(myArray, DIM);
    debugPrintAll(myArray, DIM);

    printf("Inserire valore da cercare: ");
    scanf("%d", &x);

    printf("Inserire valore da inserire: ");
    scanf("%d", &value);

    insertBeforeX(myArray, DIM, x, value);
    debugPrintAll(myArray, DIM);

    return 0;
}