/*
Scrivere una funzione che presi in input due vettori a tappo copi il contenuto del primo nel secondo

Output tramite variabile passata per riferimento: 1 se sono stati copiati tutti i valori, -1 altrimenti
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 8

void func(int arr1[], int arr2[], int dim1, int dim2, int *output)
{
    int i = 0;
    int j = 0;
    while (arr1[i] != -1 && i < dim1 - 1 && j < dim2 - 1)
    {
        arr2[j] = arr1[i];
        i++;
        j++;
        printf("COPIO\n");
    }
    arr2[j] = -1;
    if (arr2[j] == -1 && arr1[i] == -1)
    {
        *output = 1;
    }
    else
    {
        *output = -1;
    }
}

void debugPopulateArray(int array[], int dim)
{
    int i = 0;
    int x = rand() % 10 - 2;

    while (i < dim - 1 && x > 0)
    {
        array[i] = x;
        i++;
        x = rand() % 10 - 2;
    }
    array[i] = -1;
}

void printArray(int array[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", array[i]);
    }
    printf("\n");
}

int main()
{

    int firstArray[DIM] = {};
    int secondArray[DIM] = {};
    int output;
    srand(time(NULL));

    debugPopulateArray(firstArray, DIM);
    printf("Array 1: ");
    printArray(firstArray, DIM);

    debugPopulateArray(secondArray, DIM);
    printf("Array 2: ");
    printArray(secondArray, DIM);

    func(firstArray, secondArray, DIM, DIM, &output);
    printf("Output: %d\nArray Copiato: ", output);
    printArray(secondArray, DIM);

    return 0;
}