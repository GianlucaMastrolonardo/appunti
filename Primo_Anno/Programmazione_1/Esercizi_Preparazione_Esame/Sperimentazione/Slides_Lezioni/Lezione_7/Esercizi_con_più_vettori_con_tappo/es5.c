/*
Scrivere una funzione che presi in input tre vettori a tappo metta nel terzo la somma degli elementi validi dei primi due (se uno è finito si copia l’elemento dell’unicovettore con elementi validi).

Valore di ritorno: -1 se si è dovuto interrompere la procedura per la dimensione del terzo vettore , 1 altrimenti
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 9

int sumArrs(int arr1[], int arr2[], int arrSum[], int dim1, int dim2, int dimSum)
{
    int i = 0;
    int j = 0;
    int z = 0;

    int isFull = 1;

    while (arr1[i] != -1 && arr2[j] != -1 && isFull == 1)
    {
        arrSum[z] = arr1[i] + arr2[j];
        i++;
        j++;
        z++;
        if (z >= dimSum - 1)
        {
            isFull = -1;
        }
    }

    while (arr1[i] != -1 && isFull == 1)
    {
        arrSum[z] = arr1[i];
        i++;
        z++;
        if (z >= dimSum - 1)
        {
            isFull = -1;
        }
    }

    while (arr2[j] != -1 && isFull == 1)
    {
        arrSum[z] = arr2[j];
        j++;
        z++;
        if (z >= dimSum - 1)
        {
            isFull = -1;
        }
    }

    arrSum[z] = -1;

    return isFull;
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 10 - 2;

    while (x > 0 && i < dim - 1)
    {
        arr[i] = x;
        i++;

        x = rand() % 10 - 2;
    }
    arr[i] = -1;
}

int main()
{
    srand(time(NULL));

    int firstArr[DIM] = {};
    int secondArr[DIM] = {};
    int thirdArr[DIM] = {};

    populateArr(firstArr, DIM);
    populateArr(secondArr, DIM);

    printArr(firstArr, DIM);
    printArr(secondArr, DIM);

    printf("Output: %d\n", sumArrs(firstArr, secondArr, thirdArr, DIM, DIM, DIM));
    printArr(thirdArr, DIM);

    return 0;
}