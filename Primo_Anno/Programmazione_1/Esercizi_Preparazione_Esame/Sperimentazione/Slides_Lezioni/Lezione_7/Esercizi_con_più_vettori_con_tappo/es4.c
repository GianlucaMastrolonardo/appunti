/*
Scrivere una funzione che presi in input due vettori a tappo copi gli elementi in posizione pari del primo nel secondo vettore

Valore di ritorno: il numero di elementi copiati.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 7

int copyPareElements(int arr1[], int arr2[], int dim1, int dim2)
{
    int i = 0;
    int cnt = 0;

    while (arr1[i] != -1 && arr2[i] != -1)
    {
        if (i % 2 == 0)
        {
            arr2[i] = arr1[i];
            cnt++;
        }
        i++;
    }

    return cnt;
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 10 - 2;

    while (x > 0 && i < dim - 1)
    {
        arr[i] = x;
        i++;

        x = rand() % 10 - 2;
    }
    arr[i] = -1;
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main()
{
    srand(time(NULL));

    int firstArray[DIM] = {};
    int secondArray[DIM] = {};

    populateArr(firstArray, DIM);
    populateArr(secondArray, DIM);

    printf("Array 1: ");
    printArr(firstArray, DIM);
    printf("Array 2: ");
    printArr(secondArray, DIM);

    printf("Array 2 dopo copia pari: ");
    copyPareElements(firstArray, secondArray, DIM, DIM);
    printArr(secondArray, DIM);

    return 0;
}