/*
Scrivere una funzione che presi in input due vettori a tappo dica se hanno lo stesso contenuto

Output tramite variabile passata per riferimento: 1 se sono stati uguali, -1 altrimenti
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 9

void areSameArray(int arr1[], int arr2[], int dim1, int dim2, int *output)
{
    int i = 0;

    *output = 1;

    // Così controllo anche se un array ha subito il tappo e l'altro no
    if (arr1[i] != arr2[i])
    {
        *output = -1;
    }

    while (arr1[i] != -1 && arr2[i] != -1 && *output != -1)
    {
        if (arr1[i] != arr2[i])
        {
            *output = -1;
        }
        i++;
    }
}

// Creata per comodità
void debugDupArr(int arrSrc[], int arrDst[], int dimSrc, int dimDst)
{

    int i = 0;
    while (arrSrc[i] != -1 && i < dimDst - 1)
    {
        arrDst[i] = arrSrc[i];
        i++;
    }

    arrDst[i] = -1;

    if (arrDst[i] != arrSrc[i])
    {
        printf("Errore nel copiare gli array\n");
    }
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 10 - 2;

    while (x > 0 && i < dim - 1)
    {
        arr[i] = x;
        i++;
        x = rand() % 10 - 2;
    }
    arr[i] = -1;
}

int main()
{

    srand(time(NULL));

    int firstArray[DIM] = {};
    int secondArray[DIM] = {};
    int output;

    populateArr(firstArray, DIM);
    printf("Array 1: ");
    printArr(firstArray, DIM);

    // Commenta debugDupArr se vuoi array diversi
    debugDupArr(firstArray, secondArray, DIM, DIM);
    // populateArr(secondArray, DIM);

    printf("Array 2: ");
    printArr(secondArray, DIM);

    areSameArray(firstArray, secondArray, DIM, DIM, &output);

    printf("Output funzione: %d\n", output);

    return 0;
}