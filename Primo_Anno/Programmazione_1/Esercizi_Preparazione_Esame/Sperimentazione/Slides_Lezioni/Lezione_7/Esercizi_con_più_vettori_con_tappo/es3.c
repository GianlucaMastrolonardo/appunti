/*
Scrivere una funzione che presi in input due vettori a tappo dica quanti elementi del primo vettore hanno un valore maggiore rispetto al secondo.
Se il secondo vettore finisce prima gli elementi rimanenti nel primo sono considerati automaticamente maggiori.

Valore di ritorno: il numero di elementi del primo vettore maggiore dei corrispondenti nel secondo
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 7

int howGrater(int arr1[], int arr2[], int dim1, int dim2)
{
    int i = 0;
    int cnt = 0;

    while (arr1[i] != -1 && arr2[i] != -1)
    {
        if (arr1[i] > arr2[i])
        {
            cnt++;
        }

        i++;
    }

    while (arr1[i] != -1)
    {
        cnt++;
        i++;
    }

    return cnt;
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 10 - 2;
    while (i < dim - 1 && x > 0)
    {
        arr[i] = x;
        i++;

        x = rand() % 10 - 2;
    }
    arr[i] = -1;
}

int main(void)
{
    int firstArray[DIM] = {};
    int secondArray[DIM] = {};

    srand(time(NULL));

    populateArr(firstArray, DIM);
    populateArr(secondArray, DIM);

    printf("Array 1: ");
    printArr(firstArray, DIM);

    printf("Array 2: ");
    printArr(secondArray, DIM);

    printf("Valore di ritorno: %d\n", howGrater(firstArray, secondArray, DIM, DIM));

    return 0;
}
