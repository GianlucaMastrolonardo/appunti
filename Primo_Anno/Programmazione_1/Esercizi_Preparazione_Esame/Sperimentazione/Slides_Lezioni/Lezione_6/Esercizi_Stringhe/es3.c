/*
Scrivere un programma che gestisca una stringa (input) di 100 caratteri (settate il suo valore in modo statico a piacere)
La stringa può contenere sia caratteri maiuscoli sia caratteri minuscoli, sia caratteri non alfabetici 
Il programma dovrà stampare a video le seguenti informazioni:
il numero di consonanti presenti nella stringa
il numero di vocali presenti nella stringa.
per ognuna delle vocali la sua frequenza (ovvero quanto è presente rispetto al numero globale di caratteri) non discreminando se maiuscola o minuscola.

Per ogni ciclo scrivere l’opportuna assert che garantisce le proprietà per ogni iterazioni (invariante di ciclo) e l’opportuna assert alla fine (postcondizione).
*/

/*
65	41	A   25
66	42	B
67	43	C
68	44	D
69	45	E   21
70	46	F
71	47	G
72	48	H
73	49	I   17
74	4A	J
75	4B	K
76	4C	L
77	4D	M
78	4E	N
79	4F	O   11
80	50	P
81	51	Q
82	52	R
83	53	S
84	54	T
85	55	U   5
86	56	V
87	57	W
88	58	X
89	59	Y
90	5A	Z
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define DIM 100

void myFunction(char string[], int dim){
    char copyString[DIM];

    int cons = 0;
    int vocals[26] = {};
    for(int i = 0; i < 26; i++){
        vocals[i] = 0;
    }

    strcpy(copyString, string);

    for (int i = 0; copyString[i]; i++){
        if((copyString[i] >= 65 && copyString[i] <= 90) || (copyString[i] >= 97 && copyString[i] <= 122)){
            copyString[i] = copyString[i] - 32;
                if(copyString[i] == 'A' || copyString[i] == 'E' || copyString[i] == 'I'|| copyString[i] == 'O'|| copyString[i] == 'U'){
                    vocals[90-copyString[i]] = vocals[90-copyString[i]]+1;
                }else{
                    cons++;
                }
            }
    }

    printf("Stringa :%s\n\tNella stringa sono presenti:\n\ta: %d | e: %d | i: %d | o: %d | u: %d\n\tConsonanti: %d\n", string, vocals[25],vocals[21],vocals[17],vocals[11],vocals[5], cons);
}

int main(){
    char myString[DIM] = "Nel mezzo dei num3ri";

    myFunction(myString, DIM);
    
    return 0;
}