/*
Si scriva una funzione che riceva in ingresso due stringhe (input). 

Crei una terza stringa (output) che è la concatenazione delle due stringhe. Inoltre la nuova stringa deve avere al posto di ogni vocale il carattere ’*’.

HINT: eseguite da terminale 
	man strcat
	man strcpy
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DIM 40

void myFunction(char firstString[], char secondString[], char outputString[], int dim){
    strcpy(outputString, firstString);
    strcat(outputString, secondString);
    

    for(int i = 0; outputString[i] != '\0'; i++){
        if(outputString[i] == 'a' || outputString[i] == 'e' || outputString[i] == 'i' || outputString[i] == 'o' || outputString[i] == 'u' ){
            outputString[i] = '*';
        }
    }
}

int main(){

    char firstString[DIM] = "primastringa";
    char secondString[DIM] = "secondastringa";

    char outputString[DIM] = "";

    myFunction(firstString, secondString, outputString, DIM);
    printf("%s\n", outputString);

    return 0;
}