/*
Si scriva una funzione che date due stringhe (input). 
Il programma deve verificare e segnalare (output) se la seconda stringa inserita è contenuta almeno una volta all’interno della prima parola
(ossia se la seconda parola è una sottostringa della prima parola) o viceversa o se sono uguali.

HINT: eseguite da terminale 
	man strstr
e vedete cosa fa la funzione
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define DIM 25

void isSubString(char string1[], char string2[], int dim){
    if(strcmp(string1, string2) == 0){
        printf("La parola %s è uguale alla parola %s\n", string1, string2);
    }else{
        char *pointer = strstr(string1, string2);
        if(pointer != NULL){
        printf("La parola %s è contenuta in %s\n", string2, string1);
        }else{
            pointer = strstr(string2, string1);
            if(pointer != NULL){
            printf("La parola %s è contenuta in %s\n", string1, string2);
            }else{
                printf("Le parola non si contengono e non sono uguali!\n");
            }
        }
    }
}

int main(){
    char myString[DIM] = "canemenestrello";
    char mySecondString[DIM] = "canecancello";

    isSubString(myString, mySecondString, DIM);
    return 0;
}