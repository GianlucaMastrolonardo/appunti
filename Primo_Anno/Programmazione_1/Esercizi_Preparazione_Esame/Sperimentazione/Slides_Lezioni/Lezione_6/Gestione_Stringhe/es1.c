/*
Scrivere un programma che crei una la stringa "Informatica" e che poi crei una nuova stringa invertita "acitamrofnI", stampare tramite for
*/

#include <stdlib.h>
#include <stdio.h>

int main(){
    char myString[12];

    for(int i = 0; i < 11; i++){
        printf("Inserire carattere: ");
        scanf(" %c", &myString[i]);
    }

    myString[11] = '\0';

    printf("%s", myString);

    return 0;
}