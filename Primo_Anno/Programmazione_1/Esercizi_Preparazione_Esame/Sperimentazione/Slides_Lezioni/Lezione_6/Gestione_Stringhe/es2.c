/*
SENZA USARE string.h
Scrivere le funzioni:
Funzione che restituisca la lunghezza della stringa
		int lunghezza(char s[])
Tip: qual è il simbolo che determinana la fine stringa?
 
Funzione che dice se una stringa è palindroma: valore di ritorno: 0 se palimendroma 
                                                                  1 se non palindroma.
        (e.s "ama" 🡪 0 "Anna" 🡪 1 "anna" 🡪 0 "amami" 🡪 1)
 		int palindroma(char s[])
Tip: attenzione a capire quanti confronti si devono fare, non farne più del necessario.
Funzione che inverta una stringa
		void inverti (char s[])
Tip: si sta modificando la stringa stessa, attenzione a capire quanti scambi si devono fare
*/

#include <stdio.h>
#include <stdlib.h>
#define DIM 25

void buildString(char string[], int dim){
    string[0] = 'a';
    string[1] = 'n';
    string[2] = 'n';
    string[3] = 'a';
    string[4] = '\0';
}

int lunghezzastringa(char string[], int dim){
    int i = 0;
    int cnt = 0;
    while(string[i] != '\0'){
        cnt++;
        i++;
    }
    return cnt;
}

int palindroma(char string[], int dim){
    int length = lunghezzastringa(string, dim) -1; //Meno uno perchè senno prende il \0
    int compare;
    if(length % 2 == 0){
        compare = length;
    }else{
        compare = length -1;
    }

    int i = 0;
    int isPalindrome = 0;
    while(i < compare && isPalindrome != 1){
        printf("%c = %c | iterazione %d\n", string[i], string[length-i], i);
        if(string[i] != string[length-i]){
            isPalindrome = 1;
        }
        i++;
    }
    return isPalindrome;
}

void inverti(char string[], int dim){
    int length = lunghezzastringa(string, dim) -1;
    
    char support[dim];

    //Copio il contenuto dell'array in un array di supporto
    for(int i = 0; i < dim; i++){
        support[i] = string[i];
    }

    int i = 0;
    while(length >= 0){
        string[i] = support[length];
        i++;
        length--;
    }
    string[i] = '\0';


}

int main(){
    char myString[DIM] = "paolocannone";
    //buildString(myString, DIM);

    printf("%s\n", myString);
    printf("Lunghezza: %d\n", lunghezzastringa(myString, DIM));

    printf("Palindroma: %d", palindroma(myString, DIM));

    inverti(myString, DIM);
    printf("\n%s\n", myString);

    return 0;
}