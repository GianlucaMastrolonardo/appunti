/*
Leggi da file un array e creane un vettore con tappo
Esempio file:

2 4 5 2 1 6 2
*/

#include <stdio.h>
#include <stdlib.h>

#define DIM 5

void printArr(int arr[], int dim)
{
    int i = 0;
    while (i < dim)
    {
        printf("%d ", arr[i]);
        i++;
    }
    printf("\n");
}

void buildArrFromFile(int arr[], int dim, char filename[])
{
    FILE *fPtr = fopen(filename, "r");
    if (fPtr == NULL)
    {
        printf("File assente\nChiusura programma\n");
        exit(1);
    }
    else
    {
        int i = 0;
        while ((fscanf(fPtr, "%d ", &arr[i]) != EOF) && (i < dim - 1))
        {
            i++;
        }
        arr[i] = -1;
    }

    fclose(fPtr);
}

int main()
{

    char filename[50] = "es1_fileInput.txt";
    int myArray[DIM] = {};

    buildArrFromFile(myArray, DIM, filename);

    printArr(myArray, DIM);

    return 0;
}