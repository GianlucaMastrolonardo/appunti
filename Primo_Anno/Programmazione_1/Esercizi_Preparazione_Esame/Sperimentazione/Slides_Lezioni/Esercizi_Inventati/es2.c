/*
Fun1: Funzione che preso un vettore a tappo e un valore X, e cancella la prima occorrenza di X all’interno del vettore

Esiste X?
Sposto tutti gli elementi successivi rispetto alla posizione di X indietro di una posizione



Fun2: Funzione che preso un vettore a tappo e un valore X, e cancella tutte le occorrenza di X all’interno del vettore

Posso itererare tante volte la soluzione precedente? Oppure c’è un modo più efficiente?



Fun3: Funzione inserimento di nuovo valore newn prima della prima di ogni occorrenza di x.

Richiesta di gestire l’overwlow.
Segnalare tramite il valore di ritorno:
tutto ok (fatti tutti gli inserimenti)
nessun inserimento possibile
fatti alcuni inserimenti e poi fallito

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 9

void Fun1(int arr[], int x)
{
    int i = 0;
    int isFounded = 0;
    while (arr[i] != -1)
    {
        if (arr[i] == x)
        {
            isFounded = 1;
        }
        if (isFounded == 1)
        {
            arr[i] = arr[i + 1];
        }
        i++;
    }
}

void Fun2(int arr[], int x)
{
    int i = 0;
    int cnt = 0;
    while (arr[i] != -1)
    {
        if (arr[i] == x)
        {
            cnt++;
        }
        i++;
        if (cnt > 0)
        {
            arr[i - cnt] = arr[i];
        }
    }
}

void Fun3(int arr[], int x, int n, int dimArr)
{
    /*
    for (int i = 0; i < dimArr; i++)
    {
        helpArr[i] = arr[i];
    }
    */

    int i = 0;
    int pos = -1;

    while (arr[i] != -1 && pos == -1)
    {
        if (arr[i] == x)
        {
            pos = i;
        }
        i++;
    }

    if (pos != -1)
    {
        int tmp;
        // Trovato un valore == x
        printf("POS: %d\n", pos);
        tmp = arr[i];
        arr[i] = n;
        arr[i + 1] = tmp;
        i = i + 2;
        while (arr[i] != -1 && i < dimArr - 1)
        {
            tmp = arr[i + 1];
            arr[i] = arr[i + 1];
            i++;
        }
    }
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 12 - 1;
    while (x > 0 && i < dim - 1)
    {
        arr[i] = x;
        i++;
        x = rand() % 12 - 1;
    }
    arr[i] = -1;
}

int main()
{
    srand(time(NULL));

    int myArr[DIM] = {};
    populateArr(myArr, DIM);

    printArr(myArr, DIM);

    int x, n;
    printf("Inserire valore da cercare: ");
    scanf("%d", &x);

    printf("Inserire valore da inserire: ");
    scanf("%d", &n);

    Fun3(myArr, x, n, DIM);
    printArr(myArr, DIM);

    return 0;
}