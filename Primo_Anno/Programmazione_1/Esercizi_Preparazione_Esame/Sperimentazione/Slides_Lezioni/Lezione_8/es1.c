/*
ESERCIZIO 1:
scrivere un programma che confronta il contenuto di 2 file di testo e
stampa a video la prima differenza tra i file (carattere file1 e carattere
file 2)
*/

#include <stdio.h>
#include <stdlib.h>

char readCharFromFile(char fileName[])
{
    char returnedValue;
    FILE *fPtr = fopen(fileName, "r");
    if (fPtr == NULL)
    {
        printf("ERRORE FILE!\n>>> Impossibile aprire il file!\n");
        exit(1);
    }
    else
    {
        if (fscanf(fPtr, "%c", &returnedValue) != EOF)
        {
            fclose(fPtr);
            return returnedValue;
        }
        else
        {
            printf("ERRORE FILE!\n>>> Nel file non è presente un carattare!\n");
            exit(1);
        }
    }
}

int main()
{
    char file1Name[30] = "es1_file1.txt";
    char file2Name[30] = "es1_file2.txt";

    char outputFile1 = readCharFromFile(file1Name);
    char outputFile2 = readCharFromFile(file2Name);

    printf("File1: %c\n", outputFile1);
    printf("File2: %c\n", outputFile2);

    return 0;
}