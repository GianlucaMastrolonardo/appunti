/*
Considerati file di strighe tutte sulla stessa riga stampare a video le
differenze (le strighe del secondo file)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DIM_STRING 50

void readStringsFromFile(char file1Name[], char file2Name[], char fileOutputName[])
{
    FILE *fPtr1 = fopen(file1Name, "r");
    if (fPtr1 == NULL)
    {
        printf("Errore Apertura File 1\n");
        exit(1);
    }

    FILE *fPtr2 = fopen(file2Name, "r");
    if (fPtr2 == NULL)
    {
        printf("Errore Apertura File 2\n");
        exit(1);
    }

    FILE *fPtrO = fopen(fileOutputName, "w");
    if (fPtrO == NULL)
    {
        printf("Errore Creazione File Output\n");
        exit(1);
    }

    char temp1String[DIM_STRING];
    char temp2String[DIM_STRING];

    char outputString[(DIM_STRING * 2) + 1];

    while (fscanf(fPtr1, "%s", temp1String) != EOF && fscanf(fPtr2, "%s", temp2String) != EOF)
    {
        if (strcmp(temp1String, temp2String) != 0)
        {
            int i = 0;
            int j = 0;
            int z = 0;

            while (temp1String[i] != '\0' && temp2String[j] != '\0')
            {
                if (temp1String[i] != temp2String[j])
                {
                    outputString[z] = temp1String[i];
                    z++;
                    outputString[z] = temp2String[j];
                    z++;
                }
                i++;
                j++;
            }

            while (temp1String[i] != '\0')
            {
                outputString[z] = temp1String[i];
                z++;
                i++;
            }
            while (temp2String[j] != '\0')
            {
                outputString[z] = temp2String[j];
                z++;
                j++;
            }

            outputString[z] = '\0';
            fprintf(fPtrO, "%s ", outputString);
        }
    }

    while (fscanf(fPtr2, "%s", temp2String) != EOF)
    {
        fprintf(fPtrO, "%s ", temp2String);
    }

    fclose(fPtr1);
    fclose(fPtr2);
    fclose(fPtrO);
}

int main()
{
    char file1Name[DIM_STRING] = "es3_file1.txt";
    char file2Name[DIM_STRING] = "es3_file2.txt";
    char fileOutputName[DIM_STRING] = "es3_file_output.txt";

    readStringsFromFile(file1Name, file2Name, fileOutputName);

    return 0;
}