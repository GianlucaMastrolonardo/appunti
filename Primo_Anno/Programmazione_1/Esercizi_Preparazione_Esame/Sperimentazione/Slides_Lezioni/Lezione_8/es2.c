/*
scrivere un programma che confronta il contenuto (stringa) di 2 file di testo e
stampa salvi su file tutte le differenze
*/

#include <stdio.h>
#include <stdlib.h>

#define DIM_STRING 50

void writeDifference(char fileName[], char string1[], char string2[])
{
    char outputString[DIM_STRING * 2];

    int i = 0;
    int j = 0;
    int z = 0;

    while (string1[i] != '\0' && string2[j] != '\0')
    {
        if (string1[i] != string2[j])
        {
            outputString[z] = string1[i];
            z++;
            outputString[z] = string2[j];
            z++;
        }
        i++;
        j++;
    }

    while (string1[i] != '\0')
    {
        outputString[z] = string1[i];
        z++;
        i++;
    }

    while (string2[j] != '\0')
    {
        outputString[z] = string2[j];
        z++;
        j++;
    }

    outputString[z] = '\0';

    // Da qui scrivo su file
    FILE *fPtr = fopen(fileName, "w");
    if (fPtr == NULL)
    {
        printf("ERRORE FILE\nImpossibile creare il file!\n");
        exit(1);
    }
    else
    {
        fprintf(fPtr, "%s", outputString);
        fclose(fPtr);
    }
}

void readStringFromFile(char fileName[], char outputString[])
{
    FILE *fPtr = fopen(fileName, "r");
    if (fPtr == NULL)
    {
        printf("ERRORE FILE\nImpossibile aprire il file!\n");
        exit(1);
    }
    else
    {
        if (fscanf(fPtr, "%s", outputString) == EOF)
        {
            printf("ERRORE FILE\nImpossibile leggere la stringa!\n");
            exit(1);
        }
        else
        {
            fclose(fPtr);
        }
    }
}

int main()
{
    char file1Name[DIM_STRING] = "es2_file1.txt";
    char file2Name[DIM_STRING] = "es2_file2.txt";
    char fileOutputName[DIM_STRING] = "es2_file_output.txt";

    char outputFile1[DIM_STRING];
    char outputFile2[DIM_STRING];

    readStringFromFile(file1Name, outputFile1);
    readStringFromFile(file2Name, outputFile2);

    printf("File 1: %s\n", outputFile1);
    printf("File 2: %s\n", outputFile2);

    writeDifference(fileOutputName, outputFile1, outputFile2);

    return 0;
}