/*
Si scriva un programma in linguaggio C per poter analizzare una sequenza di numeri.
La sequenza termina con una valore sentinella, tale valore è -1000
I numeri sono letti da tastiera e si vuole calcolare e stampare su schermo diversi risultati:
• la somma di tutti i numeri
• se la sequenza dei numeri inseriti e crescente, decrescente oppure né crescente né decrescente. ´

Una sequenza e crescente se ogni numero è maggiore del precedente, decrescente se ogni numero è minore del precedente, né crescente né decrescente in tutti gli altri casi.
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{

    int inputNumber;
    int oldNumber;
    int cresCnt = 0, decrCnt = 0, numberCnt = 0;

    printf("Inserire un valore: ");
    scanf("%d", &inputNumber);

    oldNumber = inputNumber;

    while (inputNumber != -1000)
    {
        if (oldNumber < inputNumber)
        {
            decrCnt = 0;
            cresCnt++;
        }
        else if (inputNumber < oldNumber)
        {
            decrCnt++;
            cresCnt = 0;
        }
        numberCnt++;

        oldNumber = inputNumber;
        //PER DEBUG printf("decrCnt: %d | cresCnt: %d | numberCnt: %d\n", decrCnt, cresCnt, numberCnt);

        printf("Inserire un valore: ");
        scanf("%d", &inputNumber);
    }

    printf("La sequenza inserita e': ");
    if(numberCnt -1 == cresCnt){
        printf("Cresente");
    }else if(numberCnt -1 == decrCnt){
        printf("Decrescente");
    }else{
        printf("Ne Crescente ne Decrescente");
    }

    return 0;
}