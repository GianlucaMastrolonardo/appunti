/*
Si realizzi un programma in linguaggio C che legga un numero intero N e visualizzi a video un quadrato di lato N cosi fatto:
N=7
		*++++++ 
		**+++++ 
		***++++
		****+++
		*****++
		******+
		*******
*/

#include <stdio.h>
#include <stdlib.h>

int main(){

    int n;

    printf("Inserire dimensione: ");
    scanf("%d", &n);

    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            if(i < j){
                printf("+");
            }else{
                printf("*");
            }
        }
        printf("\n");
    }

    return 0;
}