/*
ES1:
Scrivere la funzione che inizializzi un array di interi leggendoli da tastiera
void lettura(int A[], int dimA)

ES2:
Scrivere la funzione che stampi a video un array di interi
void stampa(int A[], int dimA)

ES3:
Scrivere la funzione che inizializzi un array di interi C come somma di due altri array di interi
void somma(int A[], int dimA, int B[], int dimC, int C[], int dimC)

ES4:
Col cazzo che ricopio la formula
int funzionematematica(int a[], int b[], int dimA, int dimB)

*/

#include <stdio.h>
#define DIM 5

//Es1
void lettura(int a[], int dim){
    for(int i = 0; i < dim; i++){
        printf("Inserire valore: ");
        scanf("%d", &a[i]);
    }
}

//Es2
void stampa(int a[], int dim){
    for(int i = 0; i < dim; i++){
        printf("%d ", a[i]);
    }
    printf("\n");
}

//Es3
void somma(int a[], int b[], int c[], int dimA, int dimB, int dimC){
    int i = 0;
    while(i < dimA && i < dimB && i < dimC){
        c[i] = a[i]+b[i];
        i++;
    }
}

//Es4
int funzionematematica(int a[], int b[], int dimA, int dimB){
    int prod;
    int sumB = 0;
    int sumA = 0;

    for(int j = 0; j < dimB; j++){
        sumB += b[j];
    }

    for(int i = 0; i < dimA; i++){
        prod = a[i] * sumB;
        sumA = a[i] + prod + sumA;
    }

    return sumA;
}

int main(){
    int myFirstArray[DIM];
    lettura(myFirstArray, DIM);
    printf("Array A: ");
    stampa(myFirstArray, DIM);

    int mySecondArray[DIM] = {3,5,14,1,5};
    printf("Array B: ");
    stampa(mySecondArray, DIM);

    int sumArray[DIM] = {0,0,0,0,0}; //Array vuoto

    somma(myFirstArray, mySecondArray, sumArray, DIM, DIM, DIM);
    printf("Array C: ");
    stampa(sumArray, DIM);
    printf("Risultato funzione matematica: %d", funzionematematica(myFirstArray, mySecondArray, DIM, DIM));

    return 0;
}