/*
Una funzione che dato un array calcolare il massimo (restituito come valore di ritorno) e il numero di sue istanze restituito come variabile passata per riferimento

Ver difficile: con un solo ciclo 

int maxcount(int A[], int dimA, int *maxcount) 
*/

#include <stdio.h>
#include <stdlib.h>
#define DIM 5

int maxcount(int a[], int dimA, int *maxcount){
    *maxcount = 1;
    int max = a[0];
    for(int i = 1; i < dimA; i++){
        if(a[i] > max){
            max = a[i];
            *maxcount = 1;
        }else if(a[i] == max){
            *maxcount = *maxcount +1;
        }
    }
    return max;
}

int main(){

    int myArray[DIM] = {5,5,2,100,1};
    int maxCount = 0;

    printf("Max: %d",maxcount(myArray, DIM, &maxCount));
    printf("\nMaxCount: %d", maxCount);

    return 0;
}