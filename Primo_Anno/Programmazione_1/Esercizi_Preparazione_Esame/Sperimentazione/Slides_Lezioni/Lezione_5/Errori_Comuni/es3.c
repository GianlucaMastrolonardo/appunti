/*
Una funzione che dato un array calcolare il massimo e sostituirlo con il valore di -1

Ver difficile: con un solo ciclo 

void  maxcount(int A[], int dimA) 

*/

#include <stdio.h>
#include <stdlib.h>
#define DIM 5

void stampa(int a[], int dim){
    for(int i = 0; i < dim; i++){
        printf("%d ", a[i]);
    }
    printf("\n");
}

void maxcount(int a[], int dimA){
    int max = a[0];
    int maxpos = 0;
    for(int i = 1; i < dimA; i++){
        if(a[i] >= max){
            max = a[i];
            maxpos = i;
        }
    }
    a[maxpos] = -1;
}

int main(){
    int myArray[DIM] = {4,2,7,1,7};
    maxcount(myArray, DIM);
    stampa(myArray, DIM);
    return 0;
}