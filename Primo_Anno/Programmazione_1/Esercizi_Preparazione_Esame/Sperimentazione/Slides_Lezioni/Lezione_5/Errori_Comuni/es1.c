/*
Una funzione che dato un array calcolare il minimo e il massimo e restituirli esternamente 
*/

#include <stdio.h>
#include <stdlib.h>
#define DIM 5

void findMinMax(int array[], int dim, int *min, int *max){
    *min = array[0];
    *max = array[0];

    for(int i = 1; i < dim; i++){
        if(*min > array[i]){
            *min = array[i];
        }
        if(*max < array[i]){
            *max = array[i];
        }
    }
}

int main(){

    int array[DIM] = {3,4,1,2,9};
    int min, max;

    findMinMax(array, DIM, &min, &max);
    printf("Min: %d | Max: %d", min, max);

    return 0;
}