/*
All'inizio di un anno, un'agenzia deve adeguare gli importi delle assicurazioni dei sui clienti sulla base degli incidenti registrati nell'anno precedente.
Se un'automobile non ha subito incidenti, l'importo è ridotto del 4%; in caso contrario, l'importo è aumentato del 12%. 
Scrivere un programma che richieda per ogni cliente il numero di auto e che poi per ogni auto l'importo e il numero di incidenti dell'anno precedente.

Determini e visualizzi il nuovo importo da pagare per ogni cliente. 
Il programma deve inoltre stampare il totale degli importi previsti  e la varaizione rispetto al precedente.
*/

#include <stdio.h>
#include <stdlib.h>

int main(){

    int clientsNumber, carsNumber, import, crashs;
    int totalImportClient = 0, totalImportGlobalOld = 0, totalImportGlobalNew = 0;

    printf("Inserire il numero dei clienti: ");
    scanf("%d", &clientsNumber);

    for(int i = 1; i <= clientsNumber; i++){
        totalImportClient = 0;

        printf("----- Cliente %d -----\n", i);
        printf("\tInserire il numero di automobili del cliente %d: ", i);
        scanf("%d", &carsNumber);
        for(int j = 1; j <= carsNumber; j++){
            printf("\t\tInserire l'importo corrente dell'assicurazione per l'auto %d: ", j);
            scanf("%d", &import);
            totalImportGlobalOld += import;
            printf("\t\tInserire il numero di incidenti per l'auto %d: ", j);
            scanf("%d", &crashs);
            if(crashs > 0){
                import = import + (import * 12/100);
            }else{
                import = import - (import * 4/100);
            }

            printf("\t\tNuovo importo della macchina %d: %d\n", j, import);
            totalImportClient += import;
            totalImportGlobalNew += import;
        }
        printf("\tIl nuovo importo del cliente %d e' di %d euro\n", i, totalImportClient);
    }

    printf("Totale Vecchi importi: %d\nTotale Nuovi importi: %d", totalImportGlobalOld, totalImportGlobalNew);

    return 0;
}