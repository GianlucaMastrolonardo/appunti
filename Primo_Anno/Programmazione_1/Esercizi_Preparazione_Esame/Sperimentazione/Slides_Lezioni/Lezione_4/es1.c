/*
Si sviluppi un programma che, come nel caso di una macchina distributrice di caffè, permetta di
scegliere tra diversi prodotti tra a cui è assegnato un prezzo
(tip: serie di printf che mostrano le copie Prodotto Prezzo e un numero identificativo e l’utente sceglierà digitando un numero)
L’utente dovrà pagare il prezzo corrispondente.
Successivamente alla scelta sarà chiesto all’utente di inserire delle "monete"(corrispondente ad un importo in centesimi) tramite l’inserimento di numeri interi.
Si dovrà controllare se il numero corrisponde ad una moneta «reale» ovvero può essere solo 1, 5, 10, 20 e 50 centesimi.
Il programma deve ripetere l’acquisizione di "monete" fino a che l’importo richiesto N viene raggiunto o superato.
Si interrompe l’acquisizione delle "monete" e si restituisce a video la composizione del resto ovvero quante "monete" da 5 servono e quante da 1,
dando preferenza a quelle da 5
(tip) l’esercizio è composto da dei cicli (scelta del prodotto,  inserimento «monete», controllo «moneta»), è RICHIESTO per ognuno dei cicli di scrivere le opportune  assert posizionate (corrispondono alle invarianti di ciclo) come prima istruzione del ciclo
*/

#include <stdio.h>
#include <stdlib.h>

void printAsciiArt()
{
    printf("  __  __                _     _            _   _              _      _     _____       __  __ __  \n");
    printf(" |  \\/  |              | |   (_)          | | | |            | |    | |   / ____|     / _|/ _|\\_\\ \n");
    printf(" | \\  / | __ _  ___ ___| |__  _ _ __   ___| |_| |_ __ _    __| | ___| |  | |     __ _| |_| |_ ___ \n");
    printf(" | |\\/| |/ _` |/ __/ __| '_ \\| | '_ \\ / _ \\ __| __/ _` |  / _` |/ _ \\ |  | |    / _` |  _|  _/ _ \\\n");
    printf(" | |  | | (_| | (_| (__| | | | | | | |  __/ |_| || (_| | | (_| |  __/ |  | |___| (_| | | | ||  __/\n");
    printf(" |_|  |_|\\__,_|\\___\\___|_| |_|_|_| |_|\\___|\\__|\\__\\__,_|  \\__,_|\\___|_|   \\_____|\\__,_|_| |_| \\__|\n");
    printf("                                                                                                  \n");
    printf("                                                                                                 \n");
}

int main()
{
    int productId, productPrice;
    int resto;
    int fiveCentCoinCnt = 0, oneCentCoinCnt = 0;
    int insertedCash, totalCash = 0;

    printAsciiArt();

    printf("----- PRODOTTI -----\n");
    printf("1. Caffe': 30 Centesimi\n2. The: 25 Centesimi\n3. Acqua: 12 Centesimi\n4. Ginseng: 90 Centesimi\n");

    printf(">>> Inserire Codice Prodotto: ");
    scanf("%d", &productId);

    while (productId < 1 || productId > 4)
    {
        printf(">>> Errore: hai selezionato un prodotto non valido, riselezionare prodotto: ");
        scanf("%d", &productId);
    }

    if (productId == 1)
    {
        productPrice = 30;
        printf(">>> Hai selezionato \"Caffe'\"\n");
    }
    else if (productId == 2)
    {
        productPrice = 25;
        printf(">>> Hai selezionato \"The\"\n");
    }
    else if (productId == 3)
    {
        productPrice = 12;
        printf(">>> Hai selezionato \"Acqua\"\n");
    }
    else
    {
        productPrice = 90;
        printf(">>> Hai selezionato \"Ginseng\"\n");
    }

    while (totalCash < productPrice)
    {
        printf(">>> Inserire Importo: ");
        scanf("%d", &insertedCash);
        ;
        if (insertedCash == 1 || insertedCash == 5 || insertedCash == 10 || insertedCash == 20 || insertedCash == 50)
        {
            totalCash = insertedCash + totalCash;
        }
        else
        {
            printf(">>> Hai inserito una moneta finta!\n");
        }
        if (productPrice - totalCash > 0)
        {
            printf("Devi ancora inserire %d centesimi\n", productPrice - totalCash);
        }
    }

    printf(">>> Erogazione Prodotto!\n");

    resto = totalCash - productPrice;
    printf("Il resto e' di %d Centesimi\n", resto);
    while (resto > 0)
    {
        if (resto % 5 == 0)
        {
            resto = resto - 5;
            fiveCentCoinCnt++;
        }
        else
        {
            resto = resto - 1;
            oneCentCoinCnt++;
        }
    }
    printf("Il resto e' suddiviso in:\n\t%d monete da 5 Centesemi\n\t%d monete da 1 Centesimo", fiveCentCoinCnt, oneCentCoinCnt);

    return 0;
}