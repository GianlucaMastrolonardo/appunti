// Inserire un valore nell'array

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 10

int findDimensionArr(int arr[], int dim)
{
    int i = 0;
    while (arr[i] != -1)
    {
        i++;
    }
    return i + 1;
}

void addNewValue(int arr[], int dim, int value, int pos)
{
    int sizeArr = findDimensionArr(arr, dim);
    printf("%d\n", sizeArr);

    if (pos >= 0 && pos < sizeArr && sizeArr < dim - 1)
    {
        for (int i = sizeArr; i > pos; i--)
        {
            arr[i] = arr[i - 1];
        }
        arr[pos] = value;
    }
    else
    {
        printf("Impossibile aggiungere il nuovo valore\n");
    }
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 12 - 1;

    while (x > 0 && i < dim - 1)
    {
        arr[i] = x;
        i++;
        x = rand() % 12 - 1;
    }
    arr[i] = -1;
}

int main()
{
    srand(time(NULL));
    int myArray[DIM] = {};
    int pos, value;

    populateArr(myArray, DIM);
    printArr(myArray, DIM);

    printf("Inserire nuovo valore: ");
    scanf("%d", &value);
    printf("Inserire la posizione per il nuovo valore: ");
    scanf("%d", &pos);

    addNewValue(myArray, DIM, value, pos);
    printArr(myArray, DIM);
    return 0;
}
