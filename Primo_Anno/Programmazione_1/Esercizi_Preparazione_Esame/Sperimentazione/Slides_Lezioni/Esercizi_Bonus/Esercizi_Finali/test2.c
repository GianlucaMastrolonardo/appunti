// Aggiungere un elemento nell'array

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define DIM 9

int findSizeArr(int arr[], int dim)
{
    int i = 0;
    while (arr[i] != -1)
    {
        i++;
    }
    return i + 1;
}

void addNewElement(int arr[], int dim, int value, int pos)
{
    int size = findSizeArr(arr, dim);
    if (pos >= 0 && pos < size && size < dim - 1)
    {
        for (int i = size; i > pos; i--)
        {
            arr[i] = arr[i - 1];
        }
        arr[pos] = value;
    }
    else
    {
        printf("Errore\n");
        exit(1);
    }
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 12 - 1;

    while ((i < dim - 1) && x > 0)
    {
        arr[i] = x;
        x = rand() % 12 - 1;
        i++;
    }
    arr[i] = -1;
}

int main(void)
{
    srand(time(NULL));
    int myArr[DIM] = {};

    populateArr(myArr, DIM);
    printArr(myArr, DIM);

    int value, pos;

    printf("Valore: ");
    scanf("%d", &value);
    printf("Posizione: ");
    scanf("%d", &pos);

    addNewElement(myArr, DIM, value, pos);
    printArr(myArr, DIM);
    return 0;
}
