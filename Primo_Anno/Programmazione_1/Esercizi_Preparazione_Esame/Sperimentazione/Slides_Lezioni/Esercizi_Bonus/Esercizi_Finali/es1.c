/*
Definire una funzione che presi in input due vettore a tappo, modifichi il secondo vettore confrontanto gli elementi corrispondenti tra i due vettori.
Se l’elemento del secondo vettore è minore del corrispondente nel primo vettore, si scriva al suo posto quest’ultimo.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 9

void myFun(int arr1[], int arr2[], int dim1, int dim2)
{
    int i = 0;
    int j = 0;
    while (arr1[i] != -1 && arr2[i] != -1)
    {
        if (arr2[j] < arr1[i])
        {
            arr1[i] = arr2[j];
        }
        i++;
        j++;
    }
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void printArrTap(int arr[], int dim)
{
    int i = 0;
    while (arr[i] != -1 && i < dim)
    {
        printf("%d ", arr[i]);
        i++;
    }
    printf("\n");
    if (arr[i] != -1)
    {
        printf("Attenzione, l'Array è senza tappo, chiusura programma...\n");
        exit(1);
    }
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 12 - 1;

    while ((x > 0) && (i < dim - 1))
    {
        arr[i] = x;
        i++;
        x = rand() % 12 - 1;
    }
    arr[i] = -1;
}

int main()
{
    srand(time(NULL));
    int arr1[DIM] = {};
    int arr2[DIM] = {};

    populateArr(arr1, DIM);
    populateArr(arr2, DIM);

    printf("Array A: ");
    printArr(arr1, DIM);
    printf("Array B: ");
    printArr(arr2, DIM);

    printf("---Post Funzione---\n");
    myFun(arr1, arr2, DIM, DIM);
    printf("Array A: ");
    printArr(arr1, DIM);
    printf("Array B: ");
    printArr(arr2, DIM);

    return 0;
}
