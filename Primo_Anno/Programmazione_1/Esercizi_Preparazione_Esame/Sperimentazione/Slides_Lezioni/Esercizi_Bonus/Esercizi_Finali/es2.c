/*
Definire una funzione che presi in input un vettore a tappo, un nome di un file e un intero X,
aggiunga dopa le istanze di X nel vettore a tappo un nuovo elemento contenente il valore X+1 (se possibile).
La funzione restituisca come valore di ritorno il numero di nuovi elementi inseriti.
Si salvi il vettore cosi modificato nel seguente modo:
*) tutti i valori devono essere salvati sulla prima riga del file
*) ogni valore è separato dal successivo da un punto e virgola
*) il valore del tappo è scritto sul file
Esempio di salvataggio su file
Dato il vettore a tappo così modificato:
1 3 50 20 -1 2 233 2 8 19


Si ottiene un file cosi formato:
1;3;50;20;-1

*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 9

void myFun(int arr[], int dim, int x)
{
    int i = 0;
    int tmp;
    while (arr[i] != -1 && (i < dim - 1))
    {
        if (arr[i] == x)
        {
            tmp = arr[i + 1];
        }
    }
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 12 - 1;

    while (x > 0 && (i < dim - 1))
    {
        arr[i] = x;
        i++;
        x = rand() % 12 - 1;
    }
    arr[i] = -1;
}

int main()
{
    srand(time(NULL));
    int myArray[DIM] = {};
    populateArr(myArray, DIM);
    printArr(myArray, DIM);

    return 0;
}
