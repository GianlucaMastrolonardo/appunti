/*
Esercizio di base

Definire un programma che abbia come variabili due matrici di interi A e B (le matrici sono di dimensione definita) e che:
 1) Stampa le due matrici
 2) Chieda una valore x all’utente e trovi le istanze nella matrice A
 3) Chieda una valore y all’utente e trovi le istanze nella matrice B
 4) Sommi A e B
 5) Stampi a video la matrice risultante
 6) Moltiplichi A e B
 7) Stampi a video la matrice risultante

Nota:
risultato di somma e moltiplicazione deve essere memorizzato in una opportuna matrice
prima di eseguire somma e moltiplicazione bisogna controllare che sia possibile effettuarle

Il codice deve essere strutturato tramite funzioni in modo più generale possibile


NOTA Per fissare ad una funzione un qualunque vettore multimensionale la sintassi non è identica
a quella dell’array con una singola dimensione. Nel caso della matrice (array bidimensionale di interi) la tipatura della funzione deve essere:
    tipo_del_valore_di_ritorno	 funzione(int H, int K, tipo_di_dato A[H][K], altri parametri)
*/

#include <stdio.h>
#include <stdlib.h>

#define ROW 3
#define COL 3

// 1) Stampa le due matrici
void fun1Print(int matrow, int matcol, int matrix[matrow][matcol])
{
    for (int i = 0; i < matrow; i++)
    {
        for (int j = 0; j < matcol; j++)
        {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}

// 2) e 3) Chieda una valore x all’utente e trovi le istanze nella matrice
int fun2FindInstance(int row, int col, int matrix[row][col], int x)
{
    int cnt = 0;
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            if (x == matrix[i][j])
            {
                cnt++;
            }
        }
    }

    return cnt;
}

// 4) Sommi A e B
void fun4SumMat(int mat1row, int mat1col, int matrix1[mat1row][mat1col], int mat2row, int mat2col, int matrix2[mat2row][mat2col], int matResultrow, int matResultcol, int matrixResult[matResultrow][matResultcol])
{
    if (matResultrow >= mat1row && matResultrow >= mat2row && matResultcol >= mat1col && matResultcol >= mat2col)
    {
        // C'è spazio per la somma
        for (int i = 0; i < matResultcol; i++)
        {
            for (int j = 0; j < matResultrow; j++)
            {
                matrixResult[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
    }
}

int main()
{

    int firstMatrix[ROW][COL] = {
        {4, 2, 6},
        {7, 4, 3},
        {7, 3, 9}};

    int secondMatrix[ROW][COL] = {
        {7, 8, 2},
        {6, 2, 4},
        {1, 1, 2}};

    int resultMatrix[ROW][COL];

    int x;

    printf("\n>>> Funzione 1:\n");
    printf("Matrice A:\n");
    fun1Print(ROW, COL, firstMatrix);
    printf("\nMatrice B:\n");
    fun1Print(ROW, COL, secondMatrix);

    printf("\n>>> Funzione 2:\n");

    printf("Inserire valore da cercare nella Matrice A: ");
    scanf("%d", &x);

    printf("%d è presente %d volte\n", x, fun2FindInstance(ROW, COL, firstMatrix, x));

    printf("Inserire valore da cercare nella Matrice B: ");
    scanf("%d", &x);

    printf("%d è presente %d volte\n", x, fun2FindInstance(ROW, COL, secondMatrix, x));

    printf("\n>>> Funzione 4:\n");
    fun4SumMat(ROW, COL, firstMatrix, ROW, COL, secondMatrix, ROW, COL, resultMatrix);
    fun1Print(ROW, COL, resultMatrix);

    return 0;
}