/*
Scrivere una funzione che prenda in input due vettori a tappo modifichino gli elementi validi del primo nel seguente modo:
Se gli elementi corrispondenti validi sono uguali, si assegna all’elemento del primo 0
Se l’elemento del secondo è maggiore, si assegna all’elemento del primo quel valore
Altrimenti, lo si incrementi di una unità.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 10

void function(int arr1[], int arr2[], int dim1, int dim2)
{
    int i = 0;
    int j = 0;
    while (arr1[i] != -1 && arr2[j] != -1)
    {
        if (arr1[i] == arr2[j])
        {
            arr1[i] = 0;
        }
        else if (arr2[j] > arr1[i])
        {
            arr1[i] = arr2[j];
        }
        else
        {
            arr1[i]++;
        }
        i++;
        j++;
    }
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 10 - 1;

    while ((i < dim - 1) && x > 0)
    {
        arr[i] = x;
        i++;
        x = rand() % 10 - 1;
    }

    arr[i] = -1;
}

int main()
{
    srand(time(NULL));

    int firstArray[DIM] = {};
    int secondArray[DIM] = {};

    populateArr(firstArray, DIM);
    populateArr(secondArray, DIM);

    printf("Array1: ");
    printArr(firstArray, DIM);

    printf("Array2: ");
    printArr(secondArray, DIM);

    function(firstArray, secondArray, DIM, DIM);

    printf("-----DOPO FUNZIONE-----\nArray1: ");
    printArr(firstArray, DIM);

    printf("Array2: ");
    printArr(secondArray, DIM);

    return 0;
}