/*
Scrivere una funzione che preso in input il nome di un file e un vettore con tappo ne scriva il contenuto sul file nel seguente formato
Tutti i valori su una sola riga
Tra un elemento e un altro validi si metta il –
Non deve essere salvato il tappo

Esempio:
1-2-4-3-5-2
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 10

void fun(int arr[], int dim, char filename[])
{
    FILE *fPtr = fopen(filename, "w");
    if (fPtr == NULL)
    {
        printf("Errore creazione file\n");
        exit(1);
    }
    else
    {
        int i = 0;
        while (arr[i] != -1)
        {
            fprintf(fPtr, "%d", arr[i]);
            if (arr[i + 1] != -1)
            {
                fprintf(fPtr, "-");
            }

            i++;
        }
    }
    fclose(fPtr);
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void populateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 11 - 1;

    while (x > 0 && (i < dim - 1))
    {
        arr[i] = x;
        x = rand() % 11 - 1;

        i++;
    }
    arr[i] = -1;
}

int main()
{

    srand(time(NULL));
    int myArray[DIM] = {};

    char filenameOutput[50] = "es1_output.txt";

    populateArr(myArray, DIM);
    printArr(myArray, DIM);

    fun(myArray, DIM, filenameOutput);

    return 0;
}