/*
Scrivere una funzione che preso in input un nome di file legga dal file in questione
il contenuto del vettore a tappo e lo riempia, il formato del file in input è il seguente:
Su ogni riga c’è un valore
Il tappo è salvato nel file.

Se ci sono più elementi rispetto la dimensione del vettore a tappo passata, si restituisca come output -1
e il vettore a tappo in questione dovrà essere vuoto, altrimenti si restituisce il numero di elementi inseriti.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 10

int fun(char filename[], int arr[], int dim)
{
    int cnt = 0;
    FILE *fPtr = fopen(filename, "r");
    if (fPtr == NULL)
    {
        printf("Errore apertura file\n");
        exit(1);
    }
    else
    {
        int i = 0;
        int tappoFounded = 0;
        while (i < dim && tappoFounded == 0)
        {
            fscanf(fPtr, "%d\n", &arr[i]);
            if (arr[i] == -1)
            {
                tappoFounded = 1;
            }
            else
            {
                i++;
                cnt++;
            }
        }
        fclose(fPtr);

        if (tappoFounded == 0 /*oppure arr[i] != -1*/)
        {
            printf("Nel file non era presente il tappo, oppure l'array è stato dichiarato con una dimensione inferiore\nWipe Array...\n");
            arr[0] = -1;

            return -1;
        }
        else
        {
            return i;
        }
    }
}

void printArr(int arr[], int dim)
{
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main()
{

    srand(time(NULL));
    int myArray[DIM] = {};

    char filenameInput[50] = "es2_input.txt";

    printf("Output: %d\n", fun(filenameInput, myArray, DIM));

    printArr(myArray, DIM);

    return 0;
}