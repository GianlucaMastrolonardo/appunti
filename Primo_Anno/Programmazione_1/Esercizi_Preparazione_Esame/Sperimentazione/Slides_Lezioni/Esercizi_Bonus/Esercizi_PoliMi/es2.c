/*
Scrivere un programma C che legge due
stringhe da tastiera, le concatena in un’unica
stringa e stampa la stringa così generata

Non si può usare <string.h>
*/

#include <stdio.h>
#include <stdlib.h>

#define DIM 20

void concatenateStrings(char string1[], char string2[], char stringOutput[], int dim1, int dim2, int dimOutput){
    int i = 0;
    int outputIndex = 0;

    while(string1[i] != '\0'){
        stringOutput[outputIndex] = string1[i];
        i++;
        outputIndex++;
    }

    i = 0;

    while(string2[i] != '\0'){
        stringOutput[outputIndex] = string2[i];
        i++;
        outputIndex++;
    }

    stringOutput[outputIndex] = '\0';
}

int main(){
    char string1[DIM] = "Ciao";
    char string2[DIM] = "Bella";
    char stringOutput[DIM*2];

    printf("Strigna1: %s\n", string1);
    printf("Strigna2: %s\n", string2);
    concatenateStrings(string1, string2, stringOutput, DIM, DIM, DIM*2);
printf("\n----OUTPUT FUNZIONE----\n");
    printf("Strigna1: %s\n", string1);
    printf("Strigna2: %s\n", string2);
printf("Strigna Output: %s\n", stringOutput);
    return 0;
}