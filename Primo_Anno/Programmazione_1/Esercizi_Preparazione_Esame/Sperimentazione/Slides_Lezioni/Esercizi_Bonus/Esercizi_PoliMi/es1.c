/*
es1 (sumArrayValue):    Scrivere un programma in C che legge un
                        vettore con tappo di interi di dimensione fissata e ne
                        stampa la somma

es2 (invertArray):      Scrivere un programma in C che legge un
                        vettore di interi di dimensione fissata, inverte
                        il vettore e lo stampa.
                        (utilizzato una funzione di supporto chiamata getSizeArr)

es3 (allDiffsInArray):  Scrivere un programma C che legge un vettore
                        di lunghezza arbitraria e stampa 1 se il vettore
                        contiene solo valori diversi, 0 altrimenti.

es4 (decimalToBin):     Legge una sequenza di numeri interi e quei
                        numeri compresi tra 0 e 1023 vengono
                        memorizzati in un vettore di nome V. La lettura
                        termina quando nel vettore sono stati inseriti 10
                        numeri
                        – Per ogni numero in V il programma esegue la
                        conversione in binario, memorizza i resti ottenuti
                        in un vettore R opportunamente dimensionato e
                        stampa il contenuto di R

*/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdint.h>

#define DIM 10
#define N 3

int getSizeArr(int arr[], int dim)
{
    int i = 0;
    while (arr[i] != -1)
    {
        i++;
    }
    return i + 1;
}

int sumArrayValue(int arr[], int dim)
{
    int i = 0;
    int sum = 0;
    while (arr[i] != -1)
    {
        sum = arr[i] + sum;
        i++;
    }

    return sum;
}

void invertArray(int arr[], int dim)
{
    if (arr[0] != -1)
    {
        int sizeArr = getSizeArr(arr, dim) - 2; // Escludo il vettore con tappo
        for (int i = 0; i <= (sizeArr / 2); i++)
        {
            int tmp = arr[sizeArr - i];
            arr[sizeArr - i] = arr[i];
            arr[i] = tmp;
        }
    }
}

int allDiffsInArray(int arr[], int dim)
{
    // int supportArray[DIM] = {};

    int i = 0;
    int AllDifferent = 1;

    while (arr[i] != -1 && AllDifferent == 1)
    {
        int j = i + 1;
        while (arr[j] != -1 && AllDifferent == 1)
        {
            if (arr[i] == arr[j])
            {
                AllDifferent = 0;
            }
            j++;
        }
        i++;
    }

    return AllDifferent;
}

void decimalToBin()
{
    int sequenceNumbers[10] = {};

    // Leggo dall'utente tutti gli N numeri
    for (int i = 0; i < N; i++)
    {
        printf("Inserire il valore in posizione %d: ", i);
        scanf("%d", &sequenceNumbers[i]);
    }

    for (int i = 0; i < N; i++)
    {
        // Calcolo quanti bit servono per rappresentare il numero.
        float bitsRAW = log10(sequenceNumbers[i] + 1) / log10(2);
        int bits = round(bitsRAW);
        // Arrotondo per difetto
        if (bitsRAW > bits)
        {
            bits++;
        }
        printf("Per %d servono %d Bit\n", sequenceNumbers[i], bits);
        int BinArr[bits] = {};
    }
}

void debug_PrintArr(int arr[], int dim)
{
    printf(">>> ");
    for (int i = 0; i < dim; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void debug_PopulateArr(int arr[], int dim)
{
    int i = 0;
    int x = rand() % 12 - 1;

    while (x > 0 && i < dim - 1)
    {
        arr[i] = x;
        x = rand() % 12 - 1;
        i++;
    }
    arr[i] = -1;
}

int main(void)
{
    srand(time(NULL));
    int myArray[DIM] = {};
    debug_PopulateArr(myArray, DIM);
    debug_PrintArr(myArray, DIM);

    // es1
    int sum = sumArrayValue(myArray, DIM);
    printf("Somma: %d\n", sum);

    // es2
    invertArray(myArray, DIM);
    debug_PrintArr(myArray, DIM);

    // es3
    int areDifferent = allDiffsInArray(myArray, DIM);
    printf("Valori tutti diversi [1: Si | 0: No]: %d\n", areDifferent);

    // es4
    decimalToBin();
    return 0;
}
