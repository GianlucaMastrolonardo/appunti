#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *fp;
    int a;
    int myArray[10] ={};
    int i = 0;

    fp = fopen("numeri.txt", "r");
    if(fp == NULL){
        printf("File Vuoto...\n");
    }
    else{
        printf("Fino a qui tutto bene\n");
        while(!feof(fp)){
            fscanf(fp, "%d\n", &myArray[i]);
            printf("OK!\n");
            i++;
        }
    }

    for(i = 0; i < 10; i++){
        printf("%d\n", myArray[i]);
    }


    return 0;
}
