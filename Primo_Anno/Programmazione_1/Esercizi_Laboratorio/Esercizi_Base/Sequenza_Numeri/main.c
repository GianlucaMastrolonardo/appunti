/*
Si scriva un programma in linguaggio C per poter analizzare una sequenza di numeri (positivi o nulli).
Si chieda all’utente di dire quanti numeri N vuole inserire.
L’utente può interrompere prima la sequenza se inserisce il valore di ‘-1’ (non è da considerarsi per i conteggi)
Dati N numeri interi letti da tastiera si vogliono calcolare e stampare su schermo diversi risultati:
• quanti sono i numeri positivi, nulli e negativi
• quanti sono i numeri pari e dispari

NON USARE GLI ARRAY
*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    int sequenceLength, insertedValue, counter = 0;
    int nullValues = 0, posValues = 0, negValues = 0, evenValues = 0, oddValues = 0;

    printf("Inserire la Lunghezza della Sequenza dei Numeri da Inserire: ");
    scanf("%d", &sequenceLength);

    while (counter < sequenceLength && insertedValue != -1)
    {
        printf("Inserire un Valore: ");
        scanf("%d", &insertedValue);

        if (insertedValue != -1 && insertedValue == 0)
        {
            evenValues++;
            nullValues++;
        }
        else if (insertedValue != -1 && insertedValue > 0)
        {
            posValues++;
        }
        else
        {
            negValues++;
        }
        if (insertedValue != -1 && (insertedValue % 2) == 0)
        {
            evenValues++;
        }
        else
        {
            oddValues++;
        }

        counter++;
    }

    printf("Sulla Base della sequenza Inserita abbiamo:\n%d Valori Positivi\n%d Valori Nulli\n%d Valori Negativi\n%d Valori Pari\n%d Valori Dispari", posValues, nullValues, negValues, evenValues, oddValues);

    return 0;
}
