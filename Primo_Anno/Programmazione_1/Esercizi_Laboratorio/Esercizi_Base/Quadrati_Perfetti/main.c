/*
Si scriva un programma in linguaggio C per il calcolo dei quadrati perfetti per una sequenza di numeri.
Il programma deve prima leggere un numero inserito da tastiera, e quindi stampare i primi quadrati perfetti sino al quadrato del numero.

Un numero N si definisce quadrato perfetto se è possibile descriverlo come N = X*X

*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    int i = 1, n;
    printf("Quadrati Perfetti da 1 fino ad N*N\n");
    printf("Inserire un Numero N: ");
    scanf("%d", &n);

    while (i <= n)
    {
        printf("%d\n", i * i);
        i++;
    }

    return 0;
}
