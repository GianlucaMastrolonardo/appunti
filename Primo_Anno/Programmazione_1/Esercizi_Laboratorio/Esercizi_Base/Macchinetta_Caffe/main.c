/*
Si sviluppi un programma che, come nel caso di una macchina distributrice di caffè, permetta di
scegliere tra diversi prodotti tra a cui è assegnato un prezzo (tip: serie di printf che mostrano le
copie Prodotto Prezzo e un numero identificativo e l’utente sceglierà digitando un numero)

L’utente dovrà pagare il prezzo corrispondente.

Successivamente alla scelta sarà chiesto all’utente di inserire delle "monete"(corrispondente ad
un importo in centesimi) tramite l’inserimento di numeri interi.

Si dovrà controllare se il numero corrisponde ad una moneta «reale» ovvero può essere solo 1,
5, 10, 20 e 50 centesimi.

Il programma deve ripetere l’acquisizione di "monete" fino a che l’importo richiesto N viene
raggiunto o superato

Si interrompe l’acquisizione delle "monete" e si restituisce a video la composizione del resto
ovvero quante "monete" da 5 servono e quante da 1, dando preferenza a quelle da 5

(tip) l’esercizio è composto da dei cicli (scelta del prodotto, inserimento «monete», controllo
«moneta»), è RICHIESTO per ognuno dei cicli di scrivere le opportune assert posizionate
(corrispondono alle invarianti di ciclo) come prima istruzione del ciclo
*/

#include <stdlib.h>
#include <stdio.h>

int main(void)
{
    int coffeePrice = 50, cappuccinoPrice = 65, lattePrice = 70, thePrice = 70;                          // Prezzi
    int selector, cashInserted, cashInsertedSum = 0, cashRest = 0, cashRestCoin5 = 0, cashRestCoin1 = 0; // Azioni svolte dall'Utente

    printf("Macchinetta del Caffè\n");
    printf("1) Caffe: %d Centesimi\n2) Cappuccino: %d Centesimi\n3) Latte: %d Centesimi\n4) The: %d Centesimi\nSeleziona il Numero del Prodotto Desiderato: ", coffeePrice, cappuccinoPrice, lattePrice, thePrice);
    scanf("%d", &selector);

    // Controllo se il Selettore inserito dal Cliente è corretto
    while (selector < 1 || selector > 4)
    {
        printf("Valore Sbagliato, Re-Inseriscilo: ");
        scanf("%d", &selector);
    }

    if (selector == 1) // Caso Caffe
    {
        printf("Hai Selezionato: Caffe\n");
        while (cashInsertedSum < coffeePrice)
        {
            printf("Hai inserito %d Centesimi | Devi ancora inserire %d Centesimi\n", cashInsertedSum, coffeePrice - cashInsertedSum);
            scanf("%d", &cashInserted);
            if (cashInserted != 1 && cashInserted != 5 && cashInserted != 10 && cashInserted != 20 && cashInserted != 50) // Controllo Monete Inserite
            {
                printf("Hai inserito una Moneta non Esistente\nRe-Inseriscila\n");
                cashInserted = 0;
            }
            cashInsertedSum = cashInserted + cashInsertedSum;
        }

        printf("Bravo, Tieni il Tuo Merdoso Caffè\n");
        // Resto
        cashRest = cashInsertedSum - coffeePrice;

        if ((cashRest % 5) == 0)
        {
            cashRestCoin5 = cashRest / 5;
        }
        else
        {
            cashRestCoin1++;
        }

        printf("Resto:\n%d Monete da 5 Centesimi\n%d Monete da 1 Centesimi", cashRestCoin5, cashRestCoin1);
    }
    else if (selector == 2) // Caso Cappuccino
    {
        printf("Hai Selezionato: Cappuccino\nInserire %d Centesimi\n", cappuccinoPrice);
        while (cashInsertedSum < cappuccinoPrice)
        {
            printf("Hai inserito %d Centesimi | Devi ancora inserire %d Centesimi\n", cashInsertedSum, cappuccinoPrice - cashInsertedSum);
            scanf("%d", &cashInserted);
            if (cashInserted != 1 && cashInserted != 5 && cashInserted != 10 && cashInserted != 20 && cashInserted != 50) // Controllo Monete Inserite
            {
                printf("Hai inserito una Moneta non Esistente\nRe-Inseriscila\n");
                cashInserted = 0;
            }
            cashInsertedSum = cashInserted + cashInsertedSum;
        }

        printf("Bravo, Tieni il Tuo Merdoso Cappuccino\n");
        // Resto
        cashRest = cashInsertedSum - cappuccinoPrice;

        if ((cashRest % 5) == 0)
        {
            cashRestCoin5 = cashRest / 5;
        }
        else
        {
            cashRestCoin1++;
        }

        printf("Resto:\n%d Monete da 5 Centesimi\n%d Monete da 1 Centesimi", cashRestCoin5, cashRestCoin1);
    }
    else if (selector == 3) // Caso Latte
    {
        printf("Hai Selezionato: Latte\nInserire %d Centesimi\n", lattePrice);
        while (cashInsertedSum < lattePrice)
        {
            printf("Hai inserito %d Centesimi | Devi ancora inserire %d Centesimi\n", cashInsertedSum, lattePrice - cashInsertedSum);
            scanf("%d", &cashInserted);
            if (cashInserted != 1 && cashInserted != 5 && cashInserted != 10 && cashInserted != 20 && cashInserted != 50) // Controllo Monete Inserite
            {
                printf("Hai inserito una Moneta non Esistente\nRe-Inseriscila\n");
                cashInserted = 0;
            }
            cashInsertedSum = cashInserted + cashInsertedSum;
        }

        printf("Bravo, Tieni il Tuo Merdoso Latte\n");
        // Resto
        cashRest = cashInsertedSum - lattePrice;

        if ((cashRest % 5) == 0)
        {
            cashRestCoin5 = cashRest / 5;
        }
        else
        {
            cashRestCoin1++;
        }

        printf("Resto:\n%d Monete da 5 Centesimi\n%d Monete da 1 Centesimi", cashRestCoin5, cashRestCoin1);
    }
    else // Caso The
    {
        printf("Hai Selezionato: The\nInserire %d Centesimi\n", thePrice);
        while (cashInsertedSum < thePrice)
        {
            printf("Hai inserito %d Centesimi | Devi ancora inserire %d Centesimi\n", cashInsertedSum, thePrice - cashInsertedSum);
            scanf("%d", &cashInserted);
            if (cashInserted != 1 && cashInserted != 5 && cashInserted != 10 && cashInserted != 20 && cashInserted != 50) // Controllo Monete Inserite
            {
                printf("Hai inserito una Moneta non Esistente\nRe-Inseriscila\n");
                cashInserted = 0;
            }
            cashInsertedSum = cashInserted + cashInsertedSum;
        }

        printf("Bravo, Tieni il Tuo Merdoso The\n");
        // Resto
        cashRest = cashInsertedSum - thePrice;

        if ((cashRest % 5) == 0)
        {
            cashRestCoin5 = cashRest / 5;
        }
        else
        {
            cashRestCoin1++;
        }
        printf("Resto:\n%d Monete da 5 Centesimi\n%d Monete da 1 Centesimi", cashRestCoin5, cashRestCoin1);
    }

    return 0;
}