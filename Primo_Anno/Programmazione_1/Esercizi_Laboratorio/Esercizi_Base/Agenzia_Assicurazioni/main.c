/*
All'inizio di un anno, un'agenzia deve adeguare gli importi delle
assicurazioni dei sui clienti sulla base degli incidenti registrati nell'anno
precedente.

Se un'automobile non ha subito incidenti, l'importo è ridotto del 4%; in
caso contrario, l'importo è aumentato del 12%.

Scrivere un programma che richieda per ogni cliente il numero di auto e
che poi per ogni auto l'importo e il numero di incidenti dell'anno
precedente.

Determini e visualizzi il nuovo importo da pagare per ogni cliente.

Il programma deve inoltre stampare il totale degli importi previsti e la
varaizione rispetto al precedente.
*/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int i, j;
    int clients, cars, carValue, hasIncident, sumOldCarValue = 0, sumNewCarValue = 0;

    printf("Numero di Clienti: ");
    scanf("%d", &clients);

    for (i = 1; i <= clients; i++)
    {
        printf("Quanti Veicoli ha il Cliente numero %d: ", i);
        scanf("%d", &cars);
        for (j = 1; j <= cars; j++)
        {
            printf("Quanto ammonta il Valore del Veicolo Numero %d: ", j);
            scanf("%d", &carValue);

            sumOldCarValue = sumOldCarValue + carValue;

            printf("Quanti Incidenti ha avuto il Veicolo Numero %d: ", j);
            scanf("%d", &hasIncident);

            printf("Il Valore Iniziale del Veicolo Numero %d e' %d ", j, carValue);

            if (hasIncident >= 1)
            {
                carValue = carValue + ((carValue * 12) / 100);
                printf("L'Importo e' Aumentato del 12percento. Nuovo Valore %d\n", carValue);
                sumNewCarValue = carValue + sumNewCarValue;
            }
            else
            {
                carValue = carValue - ((carValue * 4) / 100);
                printf("L' Importo e' Diminuito del 4percento. Nuovo Valore %d\n", carValue);
                sumNewCarValue = carValue + sumNewCarValue;
            }
        }
        printf("Numero Veicoli del Cliente %d: %d\nVecchio Ammontare %d\nNuovo Ammontare %d\n", j, cars, sumOldCarValue, sumNewCarValue);
        sumNewCarValue = 0;
        sumOldCarValue = 0;
    }

    return 0;
}
