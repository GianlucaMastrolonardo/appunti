/*
Si scriva una funzione che riceva in ingresso due stringhe (input).

Crei una terza stringa (output) che è la concatenazione delle due.
Inoltre La nuova stringa deve al posto di ogni vocale avere il carattere ’*’.

HINT: eseguite da terminale
    man strcat
    man strcpy

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int getDimension(char a[])
{
    int i = 0;
    while (a[i] != '\0')
    {
        i++;
    }
    return i;
}

// Usata per il Debug
void printString(char a[], int dim)
{
    printf("La Stringa %s è composta da:\n", a);
    for (int i = 0; i < dim; i++)
    {
        printf("%c\n", a[i]);
    }
}

void linkStrings(char a[], char b[], char c[])
{
    strcpy(c, a);
    strcat(c, b);

    int i = 0;

    while (c[i] != '\0')
    {
        if (c[i] == 'a' || c[i] == 'e' || c[i] == 'i' || c[i] == 'o' || c[i] == 'u')
        {
            c[i] = '*';
        }
        i++;
    }
}

int main(int argc, char const *argv[])
{
    int dimensionFirstString, dimensionSecondString, dimensionLinkString;
    char myFirstString[1000];
    char mySecondString[1000];

    char myLinkedString[1000];

    printf("Inserire la Prima Stringa: ");
    scanf("%s", myFirstString);
    dimensionFirstString = getDimension(myFirstString);

    myFirstString[dimensionFirstString];

    printf("Inserire la Seconda Stringa: ");
    scanf("%s", mySecondString);
    dimensionSecondString = getDimension(mySecondString);

    mySecondString[dimensionSecondString];

    printf("La Prima Stringa è: %s\n", myFirstString);
    printf("La Seconda Stringa è: %s\n", mySecondString);

    linkStrings(myFirstString, mySecondString, myLinkedString);

    printf("\nLa Stringa Concatenata è: %s", myLinkedString);

    return 0;
}
