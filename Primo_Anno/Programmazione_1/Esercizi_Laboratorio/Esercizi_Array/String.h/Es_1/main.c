/*
Si scriva una funzione che date due stringhe (input).
Il programma deve verificare e segnalare (output) se la seconda stringa inserita è contenuta almeno una volta all’interno della prima parola (ossia se la seconda parola è una sottostringa della prima parola) o viceversa o se sono uguali.

HINT: eseguite da terminale
    man strstr
e vedete cosa fa la funzione
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int concatenazione(char stringA[], char stringB[])
{

    if (strcmp(stringA, stringB) == 0)
    {
        printf("Le stringe sono uguali");
        return 0;
    }
    else
    {

        if (strstr(stringA, stringB) != NULL)
        {
            printf("La seconda stringa é conteuta\n");
            return 1;
        }
        else
        {
            printf("La seconda stringa non è contenuta\n");
            return 2;
        }
        if (strstr(stringB, stringA) != NULL)
        {
            printf("La prima stringa é conteuta\n");
            return -1;
        }
        else
        {
            printf("La prima stringa non è contenuta\n");
            return -2;
        }
    }
}

int main(int argc, char const *argv[])
{
    char stringA[100], stringB[100];

    printf("Inserire Stringa A: ");
    scanf("%s", stringA);

    printf("Inserire Stringa B: ");
    scanf("%s", stringB);

    printf("\n\nRitorno: %d", concatenazione(stringA, stringB));

    return 0;
}
