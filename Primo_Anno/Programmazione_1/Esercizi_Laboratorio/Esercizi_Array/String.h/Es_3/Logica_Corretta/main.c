/*
Scrivere un programma che gestisca una stringa (input) di 100 caratteri (settate il suo valore in modo statico a piacere)
La stringa può contenere sia caratteri maiuscoli sia caratteri minuscoli, sia caratteri non alfabetici
Il programma dovrà stampare a video le seguenti informazioni:
il numero di consonanti presenti nella stringa
il numero di vocali presenti nella stringa.
per ognuna delle vocali la sua frequenza (ovvero quanto è presente rispetto al numero globale di caratteri) non discriminando se maiuscola o minuscola.

Per ogni ciclo scrivere l’opportuna assert che garantisce le proprietà
per ogni iterazioni (invariante di ciclo) e l’opportuna assert alla fine (postcondizione).
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    char myString[100];
    int accConsonanti = 0, accVocali[5], accTotalVocali = 0;
    int counter = 0;

    // Inizializzo tutto l'array a 0
    for (int i = 0; i < 5; i++)
    {
        accVocali[i] = 0;
    }

    printf("Inserire la Stringa: ");
    scanf("%s", myString);

    while (myString[counter] != '\0')
    {
        if (myString[counter] >= 97 && myString[counter] <= 122)
        {
            if (myString[counter] == 97)
            {
                accVocali[0]++;
            }
            else if (myString[counter] == 101)
            {
                accVocali[1]++;
            }
            else if (myString[counter] == 105)
            {
                accVocali[2]++;
            }
            else if (myString[counter] == 111)
            {
                accVocali[3]++;
            }
            else if (myString[counter] == 117)
            {
                accVocali[4]++;
            }
            else
            {
                accConsonanti++;
            }
        }
        else if (myString[counter] >= 65 && myString[counter] <= 90)
        {
            myString[counter] = myString[counter] + 32;

            if (myString[counter] == 97)
            {
                accVocali[0]++;
            }
            else if (myString[counter] == 101)
            {
                accVocali[1]++;
            }
            else if (myString[counter] == 105)
            {
                accVocali[2]++;
            }
            else if (myString[counter] == 111)
            {
                accVocali[3]++;
            }
            else if (myString[counter] == 117)
            {
                accVocali[4]++;
            }
            else
            {
                accConsonanti++;
            }

            myString[counter] = myString[counter] - 32;
        }

        counter++;
    }

    for (int i = 0; i < 5; i++)
    {
        accTotalVocali = accVocali[i] + accTotalVocali;
    }

    printf("La Stringa inserita e': %s\n", myString);

    printf("Nella Stringa sono presenti %d Consonanti\nNella Stringa sono presenti %d Vocali:\n%d volte la vocale a\n%d volte la vocale e\n%d volte la vocale i\n%d volte la vocale o\n%d volte la vocale u", accConsonanti, accTotalVocali, accVocali[0], accVocali[1], accVocali[2], accVocali[3], accVocali[4]);

    return 0;
}
