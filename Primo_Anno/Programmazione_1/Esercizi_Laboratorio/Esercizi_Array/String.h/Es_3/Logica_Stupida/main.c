/*
Scrivere un programma che gestisca una stringa (input) di 100 caratteri (settate il suo valore in modo statico a piacere)
La stringa può contenere sia caratteri maiuscoli sia caratteri minuscoli, sia caratteri non alfabetici
Il programma dovrà stampare a video le seguenti informazioni:
il numero di consonanti presenti nella stringa
il numero di vocali presenti nella stringa.
per ognuna delle vocali la sua frequenza (ovvero quanto è presente rispetto al numero globale di caratteri) non discriminando se maiuscola o minuscola.

Per ogni ciclo scrivere l’opportuna assert che garantisce le proprietà
per ogni iterazioni (invariante di ciclo) e l’opportuna assert alla fine (postcondizione).
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char myString[1000];
    int acc;
    int consonantiCounter = 0, vocaliCounter[5] = {0, 0, 0, 0, 0};

    printf("Inserire la  Stringa: ");
    scanf("%s", myString);

    while (myString[acc] != '\0')
    {
        printf("%c: Ascii: %d\n", myString[acc], myString[acc]);

        if ((myString[acc] >= 65 && myString[acc] <= 90) || (myString[acc] >= 97 && myString[acc] <= 122))
        {
            // Controllo Valori Maiuscoli
            if (myString[acc] >= 65 && myString[acc] <= 90)
            {
                printf("MAIUSCOLA TROVATA\n");
                myString[acc] = myString[acc] + 32;

                // Lettera a
                if (myString[acc] == 97)
                {
                    vocaliCounter[0] = vocaliCounter[0] + 1;
                }
                else

                    // Lettera e
                    if (myString[acc] == 101)
                    {
                        vocaliCounter[1] = vocaliCounter[1] + 1;
                    }

                    else

                        // Lettera i
                        if (myString[acc] == 105)
                        {
                            vocaliCounter[2] = vocaliCounter[2] + 1;
                        }
                        else

                            // Lettera o
                            if (myString[acc] == 111)
                            {
                                vocaliCounter[3] = vocaliCounter[3] + 1;
                            }
                            else

                                // Lettera u
                                if (myString[acc] == 117)
                                {
                                    vocaliCounter[4] = vocaliCounter[4] + 1;
                                }
                                else
                                {
                                    consonantiCounter = consonantiCounter + 1;
                                }

                // Rimetto la lettera al valore di partenza
                myString[acc] = myString[acc] - 32;
            }
            else

            // Controllo Valori Minuscoli
            {
                // Lettera a
                if (myString[acc] == 97)
                {
                    vocaliCounter[0] = vocaliCounter[0] + 1;
                }
                else

                    // Lettera e
                    if (myString[acc] == 101)
                    {
                        vocaliCounter[1] = vocaliCounter[1] + 1;
                    }

                    else

                        // Lettera i
                        if (myString[acc] == 105)
                        {
                            vocaliCounter[2] = vocaliCounter[2] + 1;
                        }
                        else

                            // Lettera o
                            if (myString[acc] == 111)
                            {
                                vocaliCounter[3] = vocaliCounter[3] + 1;
                            }
                            else

                                // Lettera u
                                if (myString[acc] == 117)
                                {
                                    vocaliCounter[4] = vocaliCounter[4] + 1;
                                }
                                else
                                {
                                    consonantiCounter = consonantiCounter + 1;
                                }
            }
        }

        acc++;
    }

    printf("\nLa Stringa Inserita è: %s\n", myString);

    printf("La a è presente %d volte\nLa e è presente %d volte\nLa i è presente %d volte\nLa o è presente %d volte\nLa u è presente %d volte\n", vocaliCounter[0], vocaliCounter[1], vocaliCounter[2], vocaliCounter[3], vocaliCounter[4]);
    int totalCounterVocali = 0;
    for (int i = 0; i < 5; i++)
    {
        totalCounterVocali = vocaliCounter[i] + totalCounterVocali;
    }

    printf("Nella stringa sono prensenti %d Vocali\nNella stringa sono prensenti %d Consonanti\n", totalCounterVocali, consonantiCounter);

    return 0;
}
