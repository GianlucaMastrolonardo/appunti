#include <stdio.h>
#include <stdlib.h>

/*
Singolo Apici Carattere, Doppio Apice Stringa
L'array ha un elemento in più (in ultima posizione della stringa) che contiene l'elemento '\0'
*/

// Stringa Normale, Stringa Invertita e Palindroma

int getDimension(char a[])
{
    int i = 0;
    while (a[i] != '\0')
    {
        i++;
    }
    return i;
}

void printString(char a[], int dimension)
{
    int i = 0;
    printf("----------\n");
    while (a[i] != '\0')
    {
        printf("Posizione %d: %c\n", i, a[i]);
        i++;
    }
    printf("----------\n");
}

void invertString(char a[], char invertedA[], int dimension)
{
    int i = 0;
    int tappoPosition = 0;
    int j = 0;

    while (a[i] != '\0')
    {
        i++;
    }
    tappoPosition = i - 1;
    i = 0;

    for (j = tappoPosition; j >= 0; j--)
    {
        invertedA[i] = a[j];
        i++;
    }
    invertedA[i] = '\0';
}

int isPalindrome(char a[], char invertedA[], int dimension)
{
    int i = 0;
    int counter = 0;
    int isPalindrome;
    for (i = 0; i < dimension; i++)
    {
        if (a[i] == invertedA[i])
        {
            counter++;
        }
    }
    if (counter == dimension)
    {
        isPalindrome = 1;
    }
    else
    {
        isPalindrome = 0;
    }

    return isPalindrome;
}

int main()
{
    int i = 0;
    int dimension;
    char myInvertedString[1000]; // Imposto una Dimensione Temporanea
    char myString[1000];         // Che verrà sovrascritta con la funzione getDimension

    printf("Inserire una Stringa: ");
    scanf("%s", myString);
    dimension = getDimension(myString);

    printf("La Stringa Inserita è: %s\n", myString);

    printf("La Stringa è Lunga %d Caratteri\n", dimension);

    printString(myString, dimension);
    invertString(myString, myInvertedString, dimension);

    printf("La Stringa Invertita è: %s\n", myInvertedString);
    printString(myInvertedString, dimension);

    if (isPalindrome(myString, myInvertedString, dimension) == 1)
    {
        printf("La Stringa è Palindrome con la sua Inversa");
    }
    else
    {
        printf("La Stringa non è Palindroma con la sua Inversa");
    }

    return 0;
}
