/*
Scrivere una funzione per inserire valori all’interno di un vettore a tappo.
Si termina quando l’utente mette -1 oppure quando non c’è più spazio nel vettore.

Se l’inserimento termina tramite l’input dell’utente restituire 1 altrimenti -1
*/

#include <stdio.h>
#include <stdlib.h>

int loadArrayWithTop(int a[], int dim)
{
    int acc = 0;

    while (a[acc - 1] != -1 && dim > 1)
    {
        printf("Inserire il Valore in posizione %d: ", acc);
        scanf("%d", &a[acc]);

        acc++;
        dim--;
        a[acc + 1] = -1;
    }

    if (dim == 1)
    {
        return -1;
    }
    else
    {
        return 1;
    }
}

int main(int argc, char const *argv[])
{
    int myArray[100];
    int dimension;
    int acc = 0;

    printf("Inserire la Dimensione del Array con Tappo (Valore -1 Incluso): ");
    scanf("%d", &dimension);

    myArray[dimension];

    printf("Output Funzione: %d\n", loadArrayWithTop(myArray, dimension));
    while (myArray[acc] != -1)
    {
        printf("Posizione %d: %d\n", acc, myArray[acc]);
        acc++;
    }

    return 0;
}
