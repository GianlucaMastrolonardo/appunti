/*
Scrivere una funzione che presi in input due vettori a tappo copi il contenuto del primo nel secondo
Output tramite variabile passata per riferimento: 1 se sono stati copiati tutti i valori, -1 altrimenti
*/

#include <stdio.h>
#include <stdlib.h>

void loadArraywithCap(int Array[], int dim)
{
    int i = 0;
    while ((Array[i - 1] != -1) && (i != dim - 1))
    {
        printf("Inserire il Valore in Posizione %d: ", i);
        scanf("%d", &Array[i]);

        i++;
    }
    Array[i] = -1;
}

void copyArray(int ArrayA[], int ArrayB[], int dim)
{
    int i = 0;
    int itemPresent[2] = {0, 0};
    int sumItems = 0;

    while (ArrayA[i] != -1)
    {
        itemPresent[0]++;
        i++;
    }
    printf("Item Present ArrayA: %d\n", itemPresent[0]);

    i = 0;
    while (ArrayB[i] != -1)
    {
        itemPresent[1]++;
        i++;
    }
    printf("Item Present ArrayB: %d\n", itemPresent[1]);
    sumItems = itemPresent[0] + itemPresent[1];

    if (sumItems < dim - 1)
    {
        for (int j = 0; j < itemPresent[0]; j++)
        {
            ArrayB[j + itemPresent[0]] = ArrayB[j];
            printf("OK: %d\n", ArrayB[j + itemPresent[0]]);
        }

        for (int j = 0; j < dim; j++)
        {
            printf("ARRAY: %d\n", ArrayB[j]);
        }
        for (int k = 0; k < itemPresent[1]; k++)
        {
            ArrayB[k] = ArrayA[k];
        }
    }
    else
    {
        printf("NON FACCIO COPIA\n");
    }
}

int main(int argc, char const *argv[])
{
    int arrayDimension = 5;
    int ArraywithCap1[arrayDimension], ArraywithCap2[arrayDimension];
    int i = 0;

    printf("Array 1\n");
    loadArraywithCap(ArraywithCap1, arrayDimension);

    printf("Array 2\n");
    loadArraywithCap(ArraywithCap2, arrayDimension);

    printf("Array 1\n");
    while (ArraywithCap1[i] != -1)
    {
        printf("%d\n", ArraywithCap1[i]);
        i++;
    }

    i = 0;
    printf("Array 2\n");
    while (ArraywithCap2[i] != -1)
    {
        printf("%d\n", ArraywithCap2[i]);
        i++;
    }

    copyArray(ArraywithCap1, ArraywithCap2, arrayDimension);
    /*
    i = 0;
    printf("Array Output\n");
    while (ArraywithCap2[i] != -1)
    {
        printf("%d\n", ArraywithCap2[i]);
        i++;
    }*/

    printf("DEBUG\n");
    for (int k = 0; k < arrayDimension; k++)
    {
        printf("%d\n", ArraywithCap2[k]);
    }

    return 0;
}
