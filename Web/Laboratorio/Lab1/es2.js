//Esercizio 2
/*
Realizzare un programma JS per gestire la lista di alcuni film.
- Definire i nomi dei film, inizialmente come una stringa dove i titoli dei film sono separati da
virgole, per esempio, “Poor Things, Oppenheimer, Killers of the Flower Moon, Dune: Part
Two”
- Creare un array che contenga i titoli, uno per posizione, assicurandosi che non ci siano spazi
“extra”
- Creare un secondo array calcolando gli acronimi dei film (cioè, le lettere iniziali dei nomi), per
esempio, Killers of the Flower Moon -> KOTFM. Gli acronimi devono essere tutti maiuscoli
- Stampare la lista risultante di acronimi e titoli
*/

"use strict";

const nameMoviesString = "Poor Things, Oppenheimer, Killers of the Flower Moon, Dune: Part Two";
let nameMovies = nameMoviesString.split(", ");

console.log(nameMovies);

const shortNameMovies = [];

for (let name of nameMovies){
  let nameSplit = name.split(" ");
  let firstLetters = "";
  for (let word of nameSplit){
    firstLetters += word[0]
  }
  shortNameMovies.push(firstLetters.toUpperCase());
}

console.log(shortNameMovies);
