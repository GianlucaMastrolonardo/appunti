//Esercizio 1
/*Sviluppare un semplice programma JS per gestire l’indice di gradimento di un film:
- Definire un array contenente tutti e solo i punteggi assegnati da vari utenti, inizializzando
l’array in modo statico, con valori da 0 a 100
- Copiare l'array ed eliminare sia il punteggio più alto che quello più basso
- Dividere l’array rimanente in due metà, la prima metà contentente i punteggi più bassi e la
seconda metà contenente i punteggi più alti
- Calcolare, per ognuna delle due metà, la media dei punteggi
- Aggiungere all’array questi due nuovi punteggi “mediati”
- Stampare a schermo sia l’array iniziale che quello finale e confrontare i punteggi prima e
dopo il ‘cleaning’ mostrando anche la media (arrotondata) in entrambi i casi.
*/
"use strict";

//Note: Potevi farlo meglio ordinando l'array, pensavo che l'ordine fosse importante


//Definire un array contenente tutti e solo i punteggi assegnati da vari utenti, inizializzando
//l’array in modo statico, con valori da 0 a 100
const points = [22, 45, 11 ,96, 70, 65, 86];
console.log(`>>> Array Iniziale: ${points}`);

//Copiare l'array ed eliminare sia il punteggio più alto che quello più basso

let copyPoints = Array.from(points); //oppure usi ...points

let posMin = copyPoints.indexOf(Math.min(...copyPoints));
copyPoints.splice(posMin,1);

let posMax = copyPoints.indexOf(Math.max(...copyPoints));
copyPoints.splice(posMax,1);

console.log(`>>> Array Copiato valori MIN e MAX eliminati: ${copyPoints}`);

//Dividere l’array rimanente in due metà, la prima metà contentente i punteggi più bassi e la
//seconda metà contenente i punteggi più alti

let sum = 0;
for (var point of copyPoints){
  sum += point;
}

const averagePoint = sum/copyPoints.length;

let worstPoints = [];
let bestPoints = [];
for (var point of copyPoints){
  if(point < averagePoint){
    worstPoints.push(point);
  }else{
    bestPoints.push(point);
  }
}

console.log(`>>> Array punteggi peggiori: ${worstPoints}`);
console.log(`>>> Array punteggi migliori: ${bestPoints}`);

//Calcolare, per ognuna delle due metà, la media dei punteggi

sum = 0;
for (var point of bestPoints){
  sum += point;
}
const averageBestPoint =  Math.round(sum/bestPoints.length);

sum = 0;
for (var point of worstPoints){
  sum += point;
}
const averageWorstPoint = Math.round(sum/worstPoints.length);


console.log(`>>> Media punteggi peggiori: ${averageWorstPoint}`);
console.log(`>>> Media punteggi migliori: ${averageBestPoint}`);

//Aggiungere all’array questi due nuovi punteggi “mediati”
copyPoints.push(averageWorstPoint);
copyPoints.push(averageBestPoint);

console.log(`>>> Array Copiato dopo push elementi: ${copyPoints}`);
