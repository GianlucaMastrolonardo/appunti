"use strict";
let readLineSync = require('readline-sync');
/*
Prima di tu@o, controlla che la tua installazione di Node.js funzioni all’interno di Visual Studio Code.
Crea un nuovo proge@o che, per ogni stringa in un array, ritorni una nuova stringa composta dai primi
due e dagli ulAmi due cara@eri della stringa originale. La nuova stringa dovrà rimpiazzare quella vecchia
nello stesso array.
Per esempio, ‘estate’ genererà ‘este’
Se la stringa è più corta di due cara@eri, ritornare la stringa vuota.
Si consiglia di provare il modulo readline-sync che tornerà uAle nell’esercizio vero e proprio, da usare qui
per chiedere all’utente di inserire le stringhe da inserire nell’array.
Suggerimen):
1. Per leggere dal terminale, puoi usare il modulo readline-sync: h@ps://www.npmjs.com/
package/readline-sync. Per installare il modulo, lancia un terminale, vai nella cartella del proge@o e
digita “npm install readline-sync” (avrai bisogno di una connessione Internet per farlo).
2. Hai almeno 2 opzioni per eseguire il programma :1
a. manualmente, nel terminale (quello integrato in Visual Studio Code o in quello di sistema);
b. in Visual Studio Code, per leggere qualcosa dal terminale, devi creare una launch configuraAon
(launch.json) da Run acAvity (nella “AcAvity Bar”, sulla sinistra) e aggiungere una nuova
configurazione: “console”: “integratedTerminal”
3. Per creare e gesAre le date, puoi usare l’ogge@o Date.
*/

//let myArr = ["estate", "mare", "napoli", "tre", "a"];
let myArr = [];

let stringNum = readLineSync.questionInt("Inserisci il numero di stringhe che vuoi inserire: ");
for (let i = 0; i < stringNum; i++) {
  myArr.push(readLineSync.question(`Stringa ${i}: `));
}

console.log(`>>> Array iniziale:`);
console.log(myArr);

myArr = myArr.map((el) => {
  if (el.length < 2) {
    return "";
  }
  else {
    return el[0] + el[1] + el[el.length - 2] + el[el.length - 1];
  }
});

console.log(`>>> Array finale:`);
console.log(myArr);

