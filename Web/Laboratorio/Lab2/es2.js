"use strict";
let readLineSync = require('readline-sync');

function addFilm(filmName, isMustWatch, visibility, deadline, filmList) {
  const film = { filmName: filmName, isMustWatch: isMustWatch, visibility: visibility, deadline: deadline }
  filmList.push(film);

  filmList.sort((a, b) => {
    const nameA = a.filmName.toUpperCase().trim();
    const nameB = b.filmName.toUpperCase().trim();

    if (nameA < nameB) {
      return -1;
    }

    if (nameA > nameB) {
      return 1;
    }

    return 0;
  })
}

function addFilmFromInput(filmList) {
  let filmName = readLineSync.question(">>> Inserire il nome del film: ");
  while (filmName.length == 0) {
    console.error("Nome mancante, per favore inserire un nome valido!");
    filmName = readLineSync.question(">>> Inserire il nome del film: ");
  }
  let isMustWatch = readLineSync.question(">>> Scrivere \"si\" se è un film imperdibile: ");
  isMustWatch = (isMustWatch.toLowerCase().trim() === 'si' || isMustWatch.toLowerCase().trim() === 'sì') ? true : false;

  let setVisibility = readLineSync.question(`>>> Scrivere \"si\" se si vuole aggiungere \"${filmName}\" alla WishList pubblica: `);
  setVisibility = (setVisibility.toLowerCase().trim() === 'si' || setVisibility.toLowerCase().trim() === 'sì') ? 'public' : 'private';

  let setDeadline = readLineSync.question(">>> Scrivere \"si\" se si vuole inserire una scadenza: ");

  let setDate = null;
  let setTime = null;
  if (setDeadline.toLowerCase().trim() === "si" || setDeadline.toLowerCase().trim() === "sì") {
    do {
      setDate = readLineSync.question(">>> Inserisci la data nel formato YYYY-MM-DD: ");
      setDate = new Date(setDate);
      setTime = readLineSync.question(">>> Inserire l'ora nel formato HH:MM (se vuoi ignorare il campo premi invio): ");
      if (setTime.length != 0) {
        setTime = setTime.split(":");
        setDate.setHours(parseInt(setTime[0]) + 1);
        setDate.setMinutes(parseInt(setTime[1]));
      }
      if (setDate == 'Invalid Date') {
        console.error("Formato Data/Ora errato, per favore inserire i campi nel formato valido!");
      }
    }
    while (setDate == 'Invalid Date');
  }


  addFilm(filmName, isMustWatch, setVisibility, setDate, filmList);
}

function removeFilms(filmsToDelete, myFilms) {
  filmsToDelete.forEach(filmToDelete => {
    let index = myFilms.findIndex(film => film.filmName === filmToDelete); //Uso findIndex invece che indexOf() perchè accetta CallBack function
    if (index != -1) {
      myFilms.splice(index, 1);
    }
  });
}

function removeFilmsFromInput(myFilms) {
  let filmsToDelete = readLineSync.question("Inserisci il nome di uno o più film che vuoi eliminare (separati con una virgola): ");
  filmsToDelete = filmsToDelete.split(/\s*,\s*/);
  removeFilms(filmsToDelete, myFilms);
}

function printWishlist(myFilms) {
  let currentDate = new Date();
  currentDate.setHours(currentDate.getHours() + 1); //Imposto fuso orario corretto (fa schifo come soluzione) 
  myFilms.forEach(film => {
    if (film.deadline && (currentDate - film.deadline) >= 0) {
      console.warn(`Il film \"${film.filmName}\" è stato eliminato dalla Wishlist`);
      myFilms.splice(myFilms.indexOf(film), 1);
    }
  });

  console.log(myFilms);
}


function main() {
  console.info("                          _)                       _)        |      | _)        |   ");
  console.info(" __ `__ \\    _ \\  \\ \\   /  |   _ \\      \\ \\  \\   /  |   __|  __ \\   |  |   __|  __| ");
  console.info(" |   |   |  (   |  \\ \\ /   |   __/       \\ \\  \\ /   | \\__ \\  | | |  |  | \\__ \\  |   ");
  console.info("_|  _|  _| \\___/    \\_/   _| \\___|        \\_/\\_/   _| ____/ _| |_| _| _| ____/ \\__| ");

  const myFilms = [];

  const operations = `1. Inserire un nuovo Film\n2. Rimuovere uno o più Film\n3. Visualizzare Wishlist\n4. Chiusura programma`;
  console.log(operations);
  let input = readLineSync.questionInt(">>> Inserisci il numero dell'operazione che vuoi effettuare: ");
  while (input != 4) {
    switch (input) {
      case 1:
        addFilmFromInput(myFilms);
        break;
      case 2:
        removeFilmsFromInput(myFilms);
        break;
      case 3:
        printWishlist(myFilms);
      default:
        break;
    }
    console.log("\n\n" + operations);
    input = readLineSync.questionInt(">>> Inserisci il numero dell'operazione che vuoi effettuare: ");
  }
  console.log("\nGoodbye Space Cowboy...");
}


main();
