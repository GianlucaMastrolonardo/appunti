"use strict";

// require
const express = require('express')
const morgan = require('morgan')
const sqlite3 = require('sqlite3').verbose();
const app = express()
const port = 4000

// init
app.use(express.static('public'));
app.use(morgan('tiny'))

const db = new sqlite3.Database("movies.db");

// routes
app.get('/ciao', (req, res) => {
  res.send('Ciao Mondo!')
})

app.get('/buongiorno', (req, res) => {
  res.send('Buongiorno Mondo!')
})

app.get('/movies', (req, res) => {
  const sql = "SELECT * FROM film";

  db.all(sql, (err, rows) => {
    if (err) {
      console.error(err);
      res.status(500).json(err, "ok");
      //throw err;
    }
    else {
      if (rows.length > 0) {
        const movies = rows.map((row) => {
          console.log(row);
          return row;
        })
        res.send(movies);
      }
    }
  })
})

app.get('/movies/:movieId', (req, res) => {

  const movieId = req.params.movieId;
  res.send(`I dati del film ${movieId} sono questi....`)
})

app.listen(port, () => {
  console.log(`Questo server ascolta alla porta ${port}`)
})
