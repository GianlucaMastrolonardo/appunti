// Prepara la carne e specifica come cucinarla
function getBeef(cottura){
    let beef = {howcooked: cottura}; 
    return beef;
}

// Cucina la carne secondo il modo di cottura specificato
function cookBeef(beef){
    let cookedBeef = {cooked: beef.howcooked} 
    return cookedBeef;
    // Se usi invece il codice sottostante
    // aspetta 1 secondo prima di cucinare la carne
    // return setTimeout((beef)=>{cooked: beef.howcooked},1000,beef) 
}

// Prepara i panini e specifica quale condimento usare
function getBuns(condimento){
    
    let buns = {topping: condimento}; 
    return buns;
}

// Metti la carne cucinata tra due panini
function putBeefBetweenBuns(buns,cookedBeef) {    
   let burger = {beef: cookedBeef, buns: buns }; 
   
   return burger;
}

// Servi l'hamburger con le specifiche della carne e del condimento
function serve(burger) {
    console.log(`Ho preparato un un panino con dentro un hamburger `+
    `${burger.beef.cooked} e condito con ${burger.buns.topping} `); 
    
}

// makeBurger() prepara l'hamburger in modalità sincrona
const makeBurger = () => {
    const beef = getBeef('mediamente cotto'); // Prendi l'hamburger 
    const cookedBeef = cookBeef(beef); // Cucina la carne
    const buns = getBuns('cipolla'); // Prendi i panini
    const burger = putBeefBetweenBuns(buns, cookedBeef); // Metti la carne tra i panini
    return burger; // Restituisci l'hamburger pronto
};

const burger = makeBurger(); // Prepara l'hamburger
serve(burger); // Servi l'hamburger
