"use strict";

//Creare un oggetto (Sintassi ES6)

class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
}

const person1 = new Person('Michele', 22);
console.log(person1);

//Per creare i metodi di una classe basta aggiungeri normalmente

class Dog {
  constructor(age, pedigree) {
    this.age = age;
    this.pedigree = pedigree;
  }
  bark() {
    console.log("Bau bau");
  }
}

const jackie = new Dog(9, null);
console.log(jackie);
jackie.bark();

//Viaggio nel tempo...
//Nonostante jackie sia const puoi comunque modificare i suoi valori
jackie.age = 2;

console.log(jackie);

//Con il freeze non puoi più modificare i valori all'interno dell'oggetto
Object.freeze(jackie);

try {
  jackie.age = 6;
} catch (error) {
  console.error("Errore, stai modificando un oggetto freezato...:");
  console.error(error);
}


//Funzioni, volendo possono assumere parametri con valori di default

function myProd(num1, num2 = 2) {
  return num1 * num2;
}

function counter() {
  let value = 0;
  const getNext = () => {
    value++;
    return value;
  }
  return getNext;
}

const counter1 = counter();
for (let i = 0; i < 5; i++) {
  console.log(counter1());
}

console.log("\n\n\nIIFE")
//IIFE
const c = (
  function() {
    let n = 0;
    return {
      count: function() {
        return n++;
      },
      reset: function() {
        n = 0;
      }
    };
  })();

console.log(c.count());
console.log(c.count());
console.log(c.count());
console.log(c.count());
c.reset();
console.log(c.count());
console.log(c.count());
console.log(c.count());
console.log(c.count());
