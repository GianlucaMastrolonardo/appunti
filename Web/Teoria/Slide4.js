"use strict";

function wait(duration) {
  return new Promise((resolve, reject) => {
    if (duration < 0) {
      reject(new Error('Duration < 0'));
    }

    setTimeout(resolve, duration);
  })
}


console.log("Ciao");
wait(10000000);
console.log("a tutti");
