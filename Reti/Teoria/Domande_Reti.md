# Domande Reti 🌐

Cerca la keyword `Lab` per cercare tutte le domande inerenti al laboratorio.

Cerca la keyword `Esercizio` per cercare tutti gli esercizi.

## Domande File

**Domanda 1.2:**

Spiegare la differenza tra commutazione di circuito
e quella di pacchetto spiegandone vantaggi e svantaggi.

**Risposta:**

La principale differenza è che nella commutazione di circuito
la connessione viene prenotata, ed appena avviene la prenotazione
nessun'altro host può comunicare con i due host.

La commutazione di circuito può essere vista come una connessione telefonica,
ovvero che quando due persone stanno effettuando una chiamata tra di loro
nessun'altro utente può chiamare i due utenti, perché la loro linea è occupata.

Il vantaggio principale è che in questo modo è possibile avere una
bandwidth garantita tra i due host.

Un grosso svantaggio però è lo spreco delle risorse, infatti se due
utenti si connettono tra di loro ma non scambiano nessun informazione
le risorse rimangono comunque occupate per quella connessione.

Quindi possiamo affermare che la commutazione di circuito ha un problema di scalabilità.

La commutazione a pacchetto funziona in maniera completamente diversa, infatti
i dati vengono suddivisi in pacchetti. Ogni pacchetto contiene una parte dei dati
originale, insieme a informazioni di controllo aggiuntive che includono:

+ l'indirizzo di destinazione;
+ l'indirizzo di origine;
+ il numero del pacchetto.

Una volta creati i pacchetti quest'ultimi vanno inviati attraverso la rete.
Ogni pacchetto può seguire un percorso diverso.

Una volta che il pacchetto raggiunge la destinazione viene scompattato e ne
vengono prelevate le informazioni al suo interno.

**Domanda 1.3:**

Spiegare cosa significano i seguenti acronimi: ISP, TCP, UDP, IP, RFC, DSL

**Risposta:**

+ ISP: Internet Service Provider, sono le aziende che offrono ai loro clienti
l'infrastrutta per collegarsi alla rete internet. I vari ISP sono collegati tra di
loro attraverso gli ISP globali.
+ TCP: Transmission Control Protocol, è un protocollo situato nel transport
layer che fa della affidabilità
la sua colonna portante. Infatti questo protocollo prima di iniziare
una transmissione di dati effettua il Three-Way Handshake, ovvero un inizializzazione
della connessione, dove sender e receiver, utilizzando un flag del protocollo chiamato
`SYN`, creano una connessione TCP.
Questo protocollo è considerato affidabile perché ogni volta che il receiver
non riceve un pacchetto oppure il sender non riceve
l'ACK prima dello scadere del timeout avviene una
ritrasmissione.
+ UDP: User Datagram Protocol, è un protocollo situato nel transport layer
il quale punto di forza è la velocità,
infatti a differenza di TCP i pacchetti vengono inviati senza particolare controlli.
Si può notare la differenza con TCP osservando semplicemente l'header del pacchetto,
infatti troviamo molti meno campi, e quindi un header molto più leggero.
UDP è un protocollo Best Effort, ovvero che non c'è nessuna garanzia che i pacchetti
arrivini effettivamente a destinazione.
+ IP: Internet Protocol, è un protocollo del livello di rete che
svolge il compito di indirizzare i pacchetti.
È questo protocollo che si occupa di fornire ad ogni host un proprio ed unico indirizzo
IP (che può essere scelto staticamente dall'utente oppure scelto
automaticamente mediante il protocollo DHCP).
Esistono due versioni principali del protocollo IP:
  + IPv4: con indirizzi da 32 bit;
  + IPv6: con indirizzi da 128 bit.
+ RFC: Request for Comments, è un documento pubblicato dall IETF che riporta le
informazioni e specifiche riguardanti un nuovo protocollo o un aggiornamento ad un
protocollo specifico. Sono identificati da un codice univoco.
+ DSL: Digital Subscriber line, è una rete fisica che porta una connessione nelle
abitazioni. La sua caratteristica è che la connessione tra il modem DSL ed il DSLAM
(strumento fisico dove arrivano tutte le connessioni DSL) è dedicata, e quindi non
condivisa con altri utenti.

## Esercizi Slide Lezioni

### Slide Transport Layer

**Esercizio Slide 135:**

![D1](https://imgur.com/Wyc2Q2W.png)
![D2](https://imgur.com/sKcx5cR.png)

**Risposta:**

![S1](https://imgur.com/JcqZcPr.png)
![S2](https://imgur.com/K39EXv6.png)

## Domande Slide

### Capitolo 1

**Domanda 1:**

Definire il concetto di protocollo e poi spiegare come funziona un protocollo
particolare a scelta.

Perché gli standard sono importanti nei protocolli?

**Risposta:**

>Definire il concetto di protocollo e poi spiegare come funziona un protocollo
particolare a scelta.

Un protocollo è una serie di regole che due dispositivi (comunemente chiamati host)
utilizzano per comunicare.
Possiamo prendere come analogia la lingua italiana, infatti due persone
comunicano utilizzando un protocollo (vocaboli che entrambe le persone
capiscono, frasi strutturate in uno specifico modo, etc...).
In questo modo entrambe le persone riescono a capire reciprocamente
cosa si stanno dicendo.

Entriamo più nello specifico analizzando il protocollo HTTP.
In questo protocollo troviamo un client (ovvero un utente
che sta utilizzando un browser e sta facendo una
richiesta per accedere ad una determinata pagina) ed un
server (ovvero colui che fornirà, se presente, la pagina richiesta).

In particolare quando un client fa una richiesta ad un server
sta avvenendo una `HTTP Request`. All'interno di questa richiesta
sono contenute varie informazioni, tra cui:

+ il tipo di richiesta (GET, POST, ...);

+ l'URL della pagina che viene richiesta;

+ il browser;

+ ed ulterioriori informazioni.

La risposta ad una `HTTP Request` viene chiamata `HTTP Response`, dove
troveremo le informazioni che il server ci ha fornito.
In particolare troviamo:

+ lo status code, ovvero un codice identificativo per sapere se
la richiesta è andata a buon fine oppure no;

+ lo stato della connessione, se è ancora aperta o se è chiusa;

+ il codice HTML della pagina richiesta;

+ ed ulterioriori informazioni.

>Perché gli standard sono importanti nei protocolli?

Gli standard nei protocolli sono importanti perché ci permettono di
avere una comunicazione sensata e mai incerta. Per questo motivo i
protocolli hanno una sintassi molto rigida.

**Domanda 2:**

Quale vantaggio presenta una rete a commutazione di circuito
rispetto ad una a commutazione di pacchetto?

Quali vantaggi ha TDM rispetto a FDM in una rete a commutazione di circuito?

**Risposta:**

>Quale vantaggio presenta una rete a commutazione di circuito
rispetto ad una a commutazione di pacchetto?

Un grosso vantaggio della rete a commutazione di circuito è che la
bandwith è garantita. Per avere questo vantaggio però dobbiamo
prenotare la connessione tra i due host e tenendola occupata.
Anche se non avvienisse nessuno scambio di informazioni finché
è la connessione è presente rimane prenotata, togliendo la possibilità
ad altri host di comunicare con i due host prenotati.

>Quali vantaggi ha TDM rispetto a FDM in una rete a commutazione di circuito?

+ TDM (Time Devision Multiplexing): la suddivisione dei canali avviene
  in periodi di tempo uguali per tutti gli utenti.

+ FDM (Frequency Devision Multiplexing): la suddivisione dei canali avviene
  sulla frequenza del segnale.

Il vantaggio principale del TDM è che quando un utente ha accesso alla connessione
ne ha anche la completa frequenza, dato che non viene condivisa con nessuno.

**Domanda 3:**

Considerate l’invio di un pacchetto da un host ad un altro lungo un percorso fisso.
Elencate le componenti di ritardo complessivo, indicando quali
sono costanti e quali variabili.

**Risposta:**

Le componenti che formano il ritardo complessivo sono 4:

+ Nodal Processing ($d_{proc})$: è il tempo che impiega il router ad analizzare
il pacchetto. In particolare controlla se ci sono errori
(facendo il calcolo del checksum), e determinare il link d'uscita
del router. È un tempo costante.

+ Queueing Delay ($d_{queue}$): è il tempo di accodamento, questo avviene perché
  i router utilizzano una politica FIFO, quindi se sono già presenti dei pacchetti
  nel buffer del router il pacchetto appena arrivato dovrà aspettare il suo turno,
  mentre gli altri veranno processati dal router.

  Questo tempo varia in base alla congestione della rete.
  Infatti può essere nullo (per esempio quando il buffer è vuoto)
  oppure tendere ad $\infin$ se la rete è completamente satura.

+ Transimission Delay ($d_{trans}$): è il tempo di trasimissione dato dalla formula
$d_{trans} = \frac{L}{R}$, dove $L$ è la dimensione del pacchetto, ed $R$ è la
bandwidth. Quindi varia in base ad $L$ ed $R$.

+ Propagation Delay ($d_{prop}$): è il tempo di propagazione dei dati in un cavo.
Infatti la trasmssione di un bit da una sorgente $A$ ad una destinazione $B$ si calcola
prendendo la lunghezza del collegamento $\overline{AB}$, che per comodità chiameremo
$d$ diviso la velocità della luce ($3*10^8 m/s$) . Quindi abbiamo $d_{proc} = \frac{d}{s}$.
Questo ritardo varia in base alla lunghezza del cavo, ma è sempre presente.

**Domanda 4:**

Quanto tempo impiega un pacchetto di lunghezza di $1000 byte$ per propagarsi
su un collegamento lungo $2500 km$, con una velocità di
propagazione di $2.5 \times 10^8 m/s$ e velocità di trasmissione $2Mbps$?

> Esercizio

+ Il tempo di trasmissione $T_{TX}$ di un pacchetto su un canale
è il periodo di tempo in cui il trasmettitore è impegnato ad inviare
i bit del pacchetto sul canale. La durata del tempo di
trasmissione è il rapporto tra il numero di bit di cui
è composto il pacchetto e la velocità di
trasmissione sul canale (espressa in bit/s).
+ Il tempo di propagazione $T_P$ è il tempo necessario ad ogni bit (o, più precisamente,
al segnale che lo rappresenta) per percorrere il canale fino al nodo successivo.
La durata del tempo di propagazione dipende esclusivamente
dalla velocità di propagazione del segnale nel mezzo trasmissivo (espressa in m/s)
e dalla lunghezza (espressa in m) del mezzo stesso.

**Risposta:**

![Soluzione](https://i.imgur.com/bk0242m.png)

**Domanda 5:**

Il tempo di propagazione dipende dalla lunghezza del pacchetto?
Dipende dalla velocità di trasmissione?

**Risposta:**

No, il tempo di propagazione non dipende ne dalla lunghezza del pacchetto,
ne dalla velocità di trasmissione. Dipende esclusivamente dalla distanza
del collegamento.

**Domanda 6:**

Si consideri una topologia di rete lineare composta da un singolo canale con
velocità di trasmissione pari a 1 Mbit/s, come illustrato nella figura.
Il nodo S deve trasmettere un file di dimensione pari a 9500 byte verso il nodo D.
Si ipotizzi che:

+ Non vi siano errori di trasmissione
+ Il tempo di propagazione sul canale sia pari a 5 ms
+ La dimensione massima dei pacchetti trasmessi sul
canale sia pari a 1500 byte (si trascurino le intestazioni)

Determinare il tempo necessario affinché il nodo D riceva completamente il file.

> Esercizio

**Risposta:**

![Soluzione](https://i.imgur.com/x4oeGuH.png)

**Domanda 7:**

L’host A vuole inviare un file voluminoso all’Host B.
Il percorso tra l’Host A e l’Host B ha
tre collegamenti con frequenze R1 = 500kbps, R2 = 2Mbps e R3 = 1Mbps, rispettivamente.

+ Assumendo che non vi sia altro traffico nella rete,
qual è il throughput per il trasferimento del file? [circa 286kbps]
+ Se il file è di 4 MByte, dividendo tale grandezza per il throughput,
approssimativamente quanto tempo occorrerà per trasferire il file
all’Host B? [112 s]
+ Rispondere alle precedenti domande supponendo R2 = 100kbps [circa 416s]

> Esercizio

**Risposta:**

![Soluzione](https://imgur.com/m6ejXOR.png)

**Domanda 8:**

Quali sono i cinque livelli della pila di protocolli Internet?
Quali sono i loro principali compiti?

**Risposta:**

I cinque livelli della pila sono:

+ Application Layer: è il livello più alto della pila, ed è quella
che interagisce con gli utenti finali.
Si occupa di far scambiare dati tra applicazioni che utilizzano lo
stesso protocollo.
Alcuni esempi di protocolli dell'Application Layer sono HTTP, DNS, SSH, POP3, IMAP.
+ Transport Layer: si occupa di trasportare i pacchetti da un processo
  dell'host A ad un processo dell' host B.
In particolare abbiamo due possibili trasferimenti:
  + Trasferimento affidabile: quando vogliamo un trasferimento affidabile da un
    host A ad un host B utilizziamo il protocollo TCP. In questo modo abbiamo la
    _quasi_ certezza che il pacchetto arrivi a destinazione.

    Quaste garanzie nel trasporto aumentano la dimensione dell'header del protocollo,
    e quindi ne aumentano anche la sua dimensione in byte.
  + Trasferimento best-effort: è l'opposto del trasferimento affidabile,
    infatti in questo caso il protocollo utilizzato si chiama UDP, e non ci fornisce
    nessuna garanzia che il pacchetto arrivi effettivamente a destiazione.

    Viene spesso utilizzato come transport layer nelle applicazioni real time,
    come gaming online o streaming.
+ Network Layer: Il livello di rete ha il compito di trasportare i segmenti
  (cioè le informazioni del livello di trasporto) sotto forma di datagrammi da
  un host A a un host B. Inoltre, si occupa dell'instradamento dei pacchetti
  tramite protocolli come ICMP, OSPF e BGP, e assegna un indirizzo
  IP a ogni host connesso alla rete.
  
  È da notare come il livello di rete non aggiunga nessuna garanzia
  sull'effettivo arrivo dei pacchetti (o del loro corretto ordine),
  infatti la garanzia può essere introdotta solo dal Transport
  Layer.

+ Data Link Layer: è il layer che si occupa del trasferimento di data frame
  tramite indirizzi fisici, chiamati MAC Address.
  Inoltre si occupa della frammentazione e del riassemblaggio dei dati.

+ Physical Layer: è il layer fisico, ovvero colui che trasferisce bit.

**Domanda 9:**

Spiegare i campi del datagram IP.

**Risposta:**

Dato il seguente Datagram:

![Datagram_IP](https://imgur.com/pkyr6Ar.png)

+ Version Number: serve per far capire al router che versione dell'IP stiamo utilizzando,
  dato che i flag successivi variano notevolmente in base lla versione del protocollo
  che stiamo usando;

+ Header Length: dato che l'header IP può contenere un numero variabile di flag,
  un flag apposito per salvare la lunghezza dell'header è necessario;

+ Type of Service (TOS): viene utilizzato per capire la tipologia del pacchetto.
  In questo modo i router possono gestire in maniera particolare questi
  determinati datagram;

+ Datagram length: è la lunghezza dell'intero datagram;

+ 16 Bit Identifier, More fragments flags, 13-bit fragmentation offset:
  questi campi vengono usati per la frammentazione e l'assemblamento di un pacchetto.
  In particolare l'identifier serve per capire l'id del segmento.
  Invece i flags servono per capire se ci sono ancora frammenti di quel
  segmento oppure no. Infine offset (memorizzato in byte) serve
  per andare nella posizione esatta dell'segmento frammentato.

  L'operazione di frammentazione viene svolta dai router
  quando la dimensione del segmento è
  maggiore del MTU del router.

  ![Funzionamento_FragIP](https://imgur.com/cyqpAyk.png)

+ Time To Leave (TTL): è un valore che viene impostato dalla macchina host
  quando invia il datagram, per poi essere decrementato da ogni
  router fino a che il datagram non raggiunge
  la destinazione oppure il TTL vale zero.

  Se il TTL vale zero significa che ha già attraversato tanti router, ma
  non ha ancora raggiunto la destionazione. Probabilmente è
  successo qualcosa di strano con l'instradamento di quel pacchetto, quindi
  quando un router riceve un datagram con TTL a zero viene scartato.

+ Upper Layer Protocol: contiene quale protocollo di transporto è stato utilizzato
  (per esempio TCP o UDP). Questa informazione è fondamentale per poter spacchettare
  correttamente il datagram.

+ Header checksum: utilizzato per controllare se ci sono errori
  nel datagram appena ricevuto.
  Il router calcola il checksum e lo confronta con quello salvato in questo campo,
  se sono uguali contianua l'instradamento, se sono diversi lo scarta.

+ Source IP e Destination IP: rispettivamente gli indirizzi della sorgente
  e della destionazione del pacchetto.

+ Options: viene impostato se per caso abbiamo necessita di inserire ulteriori
  informazioni.

+ Data: le informazioni presenti nel segmento, ovvero il contenuto che stiamo trasportando.

**Domanda 10:**

Come è composto l’header di un pacchetto TCP e che compito svolgono i campi?

**Risposta:**

![TCP_Diagram](https://imgur.com/I3Km5Ru.png)

+ Source Port, Destination Port: questi due campi vengono utilizzati in fase di
  Multiplexing e Demultiplexing, azione fondamentale per gestire multipli socket,
  e quindi multple connessione in ingresso ed in uscita.
  In particolare abbiamo:
  + Multiplexing: il processo devo capire a quale porta inviare il pacchetto,
    utilizza il contenuto del source port (assegnato dal dispositivo sorgente),
    in questo modo è in grado di inviare
    il pacchetto al mittente corretto.
    Spesso la source port viene generata casualmente.
  + Demultiplexing: è l'azione inversa a quella appena descritta, ovvero il pacchetto
    al livello di trasporto viene inviato ad uno dei molteplici processi attivi.
    Per individuare i processi corretti utilizza la Destination port.
    La Destination Port assume il valore in base a quale protocollo stiamo usando,
    per esempio per il protocollo HTTP utilizziamo la porta 80, per l'SMTP utilizzeremo
    25.

+ Sequence Number, ACK Number, ACK Flag: questi due campi servono al
  protocollo TCP per tenere traccia dei pacchetti inviati dalla
  sorgente e ricevuti dalla destinazione.
  In particolare abbiamo che il sequence number è il numero
  del primo byte di dati nel segmento TCP
  corrente inviato dalla sorgente,
  invece l'ACK è il numero del prossimo byte che
  il destinatario si aspetta di ricevere, indicando implicitamente che tutti
  i byte fino a quel numero sono stati ricevuti correttamente (ACK cumulativo).
  Quando il reciver deve spedire al sender
  il valore dell'ACK, oltre a spedire il valore imposta il flag ACK ad 1.

  All'inizio vengono impostati dei valori di SEQ number e ACK
  casuali, evitando così di essere confusi con segmenti vecchi.

+ Header length: questo campo assume la dimensione dell'header,
  che dato che può essere variabile va specificata la dimensione.

+ Unused: nel protocollo TCP sono presenti alcuni bit che non sono
  ancora stati utilizzati, ma utilizzabili con nuovi aggiornamento del protocollo.

+ CWR, ECE, URG, ACK, PSH, RST, SYN, FIN Flags:
  + CWR ed ECE: vengono utilizzati quando la connessione è congestionata.
    _Non visti a lezione._

  + URG: se ad 1 significa che il pacchetto è urgente e quindi con maggiore
    priorità.

  + ACK: come discusso prima, serve al sender per capire che il receiver ha ricevuto
    correttamente le informazioni fino al valore dell'ACK.

  + PSH: utilizzato per pushare il pacchetto direttamente al layer applicativo,
    senza nessun controllo del checksum.

  + RST: utilizzato per resettare la connessione, quindi terminarla immediatamente.

  + SYN: utilizzato durante il 3-way Handshake
    per inizializzare una connessione TCP.

  + FIN: utilizzato per chiudere una connessione TCP.

  + Receive Window: è la dimensione della receive window del receiver, grazie a questa
    informazione possiamo evitare di sovraccaricare il buffer del receiver e
    quindi evitare
    ritrasmissioni di pacchetti.

  + Checksum: è il checksum del pacchetto calcolato del sender.
    Questo valore deve essere confrontato con il checksum appena calcolato dal receiver,
    e se uguali il receiver
    restituisce al sender l'ACK, sennò scarta il pacchetto senza restituire nulla.

  + Urgent Data pointer: puntatore che punta all'inizio dei dati considerati importanti
    salvati nel campo data.

  + Options: ulteriori opzioni aggiuntive.

  + Data: le informazioni che stiamo trasportando.

**Domanda 11:**

Si consideri una topologia di rete lineare composta da una sequenza
di due canali con velocità di trasmissione pari a 1 Mbit/s,
come illustrato in figura sotto.
Il nodo S deve trasmettere un file di dimensione pari a 9500 byte verso il nodo D
attraverso un nodo intermedio N che opera in modalità store-and-forward.
Si ipotizzi che:

+ non vi siano errori di trasmissione;
+ i tempi di elaborazione nel nodo N siano trascurabili, ma non il tempo per lo store-forward;
+ il nodo N abbia capacità di memorizzazione infinita;
+ la lunghezza del primo canale sia pari a 400 km,
  la lunghezza del secondo pari a 600 km;
+ la dimensione massima dei pacchetti trasmessi sul canale sia pari a 1500 byte
  (si trascurino le intestazioni).

Si determini il tempo necessario affinché il nodo D riceva completamente il file.

> Esercizio

**Risposta:**

![Soluzione](https://imgur.com/wYrYleN.png)

### Capitolo 2

**Domanda 1:**

Discutere, fare esempi e confrontare le architetture client-server e P2P.

**Risposta:**

L'architettura client-server classica prevede uno o più host chiamati client
che richiedono informazioni (una pagina web, un file da scaricare, etc...)
ad un singolo host chiamato server.
In questa architettura tutti i file di un sito vengono
salvati su un singolo server, che deve rimanere sempre acceso, in modo tale da
poter fornire sempre le informazioni.

Questa architetture può presentare problemi come il single point of failure,
dato che è una singola macchina a contenere le informazioni, per questo motivo
in realtà non esistono singoli server, ma data center che contengono al loro interno
fino a centinaia di migliaia di server.
In questo modo il problema del single point of failure viene ridotto, ma non azzerato.

Invece, nell'architettura peer to peer (P2P) i file sono divisi in file più piccoli
chiamati chunk, e che quando riassemblati creano il file completo.
I chunk vengono distrubuiti su più macchine, in modo tale da non avere più il problema
del single point of failure presente nell'architettura client-server.

Inoltre i concetti di client e server non sono più presenti nell'architetture P2P,
perché un host può essere contemporaneamente sia client che server.
(Esempio: sto scaricando un file da 10GB, ne ho già scaricati 2GB,
questi 2GB sono resi a disposizione da parte mia per altri utenti).

Questa architettura si basa su quanti utenti partecipano alla rete, e soprattutto
che un utente rimanga il più possibile connesso in modo tale
da fornire anche ad altri utenti i dati in suo possesso.

Nella rete BitTorrent, in completa contrapposizione con l'architettura Client-Server
più utenti sono connessi maggiore è la velocità con la quale un utente può scaricare
un file, dato che tutti gli utenti che hanno quel file lo rendono a disposizione.

Il più grande vantaggio dell'architettura P2P è la scalabità.

**Domanda 2:**

Se una transizione tra client e server deve essere la più veloce possibile,
cosa è meglio tra TCP e UDP (motivare la risposta)?

**Risposta:**

Se una transizione tra client e server deve essere la più veloce possibile
conviene utilizzare il protocollo UDP.
Questo è dovuto da due fattori:

+ dalla semplicità (e quindi leggerezza) del header del protocollo,
  offrendo pochi campi.

+ dal fatto che è un protocollo best effort
  (ovvero che a differenza di TCP non prevede nessuna garanzia
  per il ricevimento a destinazione dei datagram spediti), e quindi non esegue
  nessuna ritrasmissione.

Invece TCP in questo caso sarebbe risultato una pessima scelta a causa della sua
elevata dimensione dell'header (dati i suoi numerosi campi) e delle sue ritrasmissioni.

**Domanda 3:**

Che cosa si intende per handshaking e spiegare contesto/motivazioni del suo uso.

**Risposta:**

L'handshaking permette a due host di inizializzare la connessione tra di loro, scambiandosi
informazioni utili in modo tale da potersi sincronizzare.

Uno degli handshake più famosi è chiamato 3-Way Handshake,
e viene utilizzato dal protocollo di transporto TCP.
TCP utilizza il 3-way handshake per poter creare connessioni
affidabili, ordinate, e che provochino poche ritrasmissioni di pacchetti,
infatti durante questa fase viene inizializzata anche la receiving
window del receiver, ed in questo modo il sender sa quanti
pacchetti può spedire senza sovraccaricare il receiver
(queste operazioni vengono chiamate Flow e Congestion Control).

Nel dettaglio il 3-way handshake nel protocollo TCP funziona in questa maniera:

1. Client invia un pacchetto TCP con un sequence number (SEQ)
    casuale (che chiamiamo X) e con il flag SYN ad 1.

2. Il server può decidere se accettare o rifiutare la nuova connessioni, quindi abbiamo:

    + Server accetta la connessione: spedisce al client un pacchetto con
      ACK = X +1 ed SEQ diverso ma nuovamente generato
      casualmente (che chiamamiamo Y), e con flag SYN ad 1.

    + Server rifiuta la connessione: spedisce al client un pacchetto con
      il flag RST ad 1, oppure semplicemente non gli risponde.

3. Il client riceve il pacchetto inviato dal server e se non ha
    il flag RST ad 1 rispedisce al server un pacchetto
    con ACK = Y + 1 ma senza il flag SYN ad 1.

**Domanda 4:**

Si supponga che Alice debba mandare un messaggio di posta elettronica a
Bob e che quest’ultimo scarichi la posta con il POP3.
Descrivere il “viaggio” di questo messaggio dettagliando i protocolli coinvolti.

**Risposta:**

Appena Alice finisce di scrivere la sua mail e preme "Invia" sul suo Client
di posta elettronica (chiamato anche User Agent) invia un pacchetto dal suo PC
al suo server Mail, mediante il protocollo SMTP.

Il server di Alice mette nella coda di E-Mail da spedire la nuova E-Mail appena
ricevuta, e dopo poco tempo (ms) la invia al Server Mail di Bob senza passare per
nessun server intermedio (utilizzando sempre SMTP).

Se Bob non ha la mailbox del suo server mail piena riceve la nuova Mail di Alice,
e con il protocollo POP3 la scarica sul suo PC in locale.

Dopo che Bob scarica la mail sul suo PC il protocollo POP3 potrebbe eliminare o
tenere sul server la Mail appena scaricata, dipende dalla configurazione di Bob.

Tutti i protocolli menzionati usano TCP come Transport Protocol per avere la
garanzia della ricezione della mail.

**Domanda 5:**

Che differenza c’e’ tra POP3 e IMAP?

**Risposta:**

+ Con IMAP quando decidediamo di leggere una mail dal nostro User Agent non scarichiamo
  la mail nel nostro PC, ma la leggiamo direttamente dal server.
  Questo ci permette di avere una sincronizzazione tra più client,
  infatti appena leggiamo una mail quest'ultima viene flaggata come letta
  anche su tutti gli altri client.
  Inoltre permette l'organizzazione in categorie/cartelle.

+ POP3 scarica sempre la mail in locale sul PC e, una volta scaricata
  la mail viene eliminata o mantenuta sul server (dipende dalla configurazione).

**Domanda 6:**

Descrivere la sequenza di system call sia per il client, sia per il server,
per il protocollo TCP.

>Lab

**Risposta:**

+ Server:

  + Socket: crea un nuovo endpoint per la comunicazione,
    specificando il tipo di indirizzi
    che vogliamo usare ed il tipo di socket (che varia da che protocollo
    di tranporto vogliamo usare).
    La funziona ritorna il File Descriptor dell'endpoint appena creato.

  + Bind: assegna l'indirizzo e la porta al socket creato in precedenza.
    Qui è dove specifichiamo il numero di porta che il nostro
    applicativo utilizzerà.
    Ritorna -1 se c'è stato un errore, sennò ritorna 0.

  + Listen: segnala che il socket è pronto ad accettare connessioni in ingresso.
    Oltre al FileDescriptor della socket si passa anche il numero massimo di
    connessioni pendenti che il server vuole gestire. Se il numero massimo
    di richieste pendenti viene raggiunto le successive richieste vengono ignorate.

  + Accept: crea un nuovo socket con la prima richiesta pendente presente nella coda.
    Questa SystemCall viene utilizzata in tutti quei protocolli
    connection-orinted come TCP.

  + Read: legge il valore che il client gli ha inviato.

  + Write: invia un valore al client.

  + Close: chiude il socket. Dato che TCP è connection-orinted
    vanno chiusi anche tutti i socket creati dalla accept.

+ Client:

  + Socket: crea il socket per la comunicazione ...

  + Connect: Prova a collegarsi all'endpoint passato come parametro.
    Ritorna 0 se la connect avviene con successo, altrimenti ritorna -1.

  + Write: invia un valore al server.

  + Read: riceve una risposta dal server.

  + Close: chiude il socket.

**Domanda 7:**

Descrivere la sequenza di system call sia per il client, sia per il server,
per il protocollo UDP.

>Lab

**Risposta:**
