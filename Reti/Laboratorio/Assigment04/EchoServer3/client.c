#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

void clean_buffer(void *buffer, size_t buffer_size) {
  char *bufferChar = (char *)buffer;
  for (size_t i = 0; i < buffer_size; ++i) {
    bufferChar[i] = '\0';
  }
}

int main(int argc, char *argv[]) {
  char valueToSend[256] = {'\0'};
  char valueToReceive[256] = {'\0'};
  char iterationsServer[256] = {'\0'};

  printf("Inserire il numero di iterazioni: ");
  scanf("%s", iterationsServer);

  int simpleSocket = 0;
  int simplePort = 0;
  int returnStatus = 0;
  char buffer[256] = "";
  struct sockaddr_in simpleServer;

  if (argc != 3) {
    fprintf(stderr, "Usage: %s <server> <port>\n", argv[0]);
    exit(1);
  }

  /* create a streaming socket      */
  simpleSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

  if (simpleSocket == -1) {
    fprintf(stderr, "Could not create a socket!\n");
    exit(1);
  } else {
    fprintf(stderr, "Socket created!\n");
  }

  /* retrieve the port number for connecting */
  simplePort = atoi(argv[2]);

  /* setup the address structure */
  /* use the IP address sent as an argument for the server address  */
  // bzero(&simpleServer, sizeof(simpleServer));
  memset(&simpleServer, '\0', sizeof(simpleServer));
  simpleServer.sin_family = AF_INET;
  // inet_addr(argv[2], &simpleServer.sin_addr.s_addr);
  simpleServer.sin_addr.s_addr = inet_addr(argv[1]);
  simpleServer.sin_port = htons(simplePort);

  /*  connect to the address and port with our socket  */
  returnStatus = connect(simpleSocket, (struct sockaddr *)&simpleServer,
                         sizeof(simpleServer));

  if (returnStatus == 0) {
    fprintf(stderr, "Connect successful!\n");
  } else {
    fprintf(stderr, "Could not connect to address!\n");
    close(simpleSocket);
    exit(1);
  }

  // Inviando il numero di iterazioni al server
  write(simpleSocket, iterationsServer, strlen(iterationsServer));

  // Aspettando l'ACK
  read(simpleSocket, valueToReceive, sizeof(valueToReceive));
  if (strcmp(valueToReceive, "ACK") != 0) {
    fprintf(stderr, "Invalid ACK");
    exit(0);
  }
  printf("%s\n", valueToReceive);

  int iterations = atoi(iterationsServer);
  for (int i = 0; i < iterations; ++i) {
    clean_buffer((void *)valueToReceive, sizeof(valueToReceive));
    printf("Inserire il valore da voler inviare al server: ");
    scanf("%s", valueToSend);
    write(simpleSocket, valueToSend, strlen(valueToSend));
    read(simpleSocket, valueToReceive, sizeof(valueToReceive));
    printf("\nEcho: %s\n", valueToReceive);
  }

  // Mando BYE
  clean_buffer((void *)valueToSend, sizeof(valueToSend));
  snprintf(valueToSend, sizeof(valueToSend), "BYE");
  write(simpleSocket, valueToSend, sizeof(valueToSend));

  close(simpleSocket);
  return 0;
}
