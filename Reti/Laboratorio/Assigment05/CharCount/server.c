#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  int simpleSocket = 0;
  int simplePort = 0;
  int returnStatus = 0;
  struct sockaddr_in simpleServer;

  if (argc < 1 || argc > 2) {
    fprintf(stderr, "Usage: %s <port>\n", argv[0]);
    exit(1);
  }

  simpleSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

  if (simpleSocket == -1) {

    fprintf(stderr, "Could not create a socket!\n");
    exit(1);

  } else {
    fprintf(stderr, "Socket created!\n");
  }

  /* retrieve the port number for listening */
  if (argc == 2) {
    simplePort = atoi(argv[1]);
    if (simplePort < 10000 || simplePort > 12000) {
      fprintf(stderr, "Port must be in range [10000, 12000]\n");
      exit(1);
    }
  } else {
    srand(time(NULL));
    simplePort = (rand() % 1999) + 10000;
  }

  /* setup the address structure */
  /* use INADDR_ANY to bind to all local addresses  */
  memset(&simpleServer, '\0', sizeof(simpleServer));
  simpleServer.sin_family = AF_INET;
  simpleServer.sin_addr.s_addr = htonl(INADDR_ANY);
  simpleServer.sin_port = htons(simplePort);

  /*  bind to the address and port with our socket */
  returnStatus = bind(simpleSocket, (struct sockaddr *)&simpleServer,
                      sizeof(simpleServer));

  if (returnStatus == 0) {
    fprintf(stderr, "Bind completed on port %d!\n", simplePort);
  } else {
    fprintf(stderr, "Could not bind to address!\n");
    close(simpleSocket);
    exit(1);
  }

  /* lets listen on the socket for connections      */
  returnStatus = listen(simpleSocket, 5);

  if (returnStatus == -1) {
    fprintf(stderr, "Cannot listen on socket!\n");
    close(simpleSocket);
    exit(1);
  }

  struct sockaddr_in clientName = {0};
  int simpleChildSocket = 0;
  int clientNameLength = sizeof(clientName);

  /* wait here */
  while (1) {
    char valueToSend[256] = {'\0'};
    char valueToRead[256] = {'\0'};

    simpleChildSocket =
        accept(simpleSocket, (struct sockaddr *)&clientName, &clientNameLength);

    if (simpleChildSocket == -1) {
      fprintf(stderr, "Cannot accept connections!\n");
      close(simpleSocket);
      exit(1);
    }

    // Leggendo il numero di richieste da svolgere
    read(simpleChildSocket, valueToRead, sizeof(valueToRead));
    int iterations = atoi(valueToRead);
    printf("Numero iterazioni: %d\n", iterations);

    // Mando ACK al Client
    snprintf(valueToSend, sizeof(valueToSend), "ACK");
    write(simpleChildSocket, valueToSend, sizeof(valueToSend));

    for (int i = 0; i < iterations; ++i) {
      // Pulisco i buffer
      memset(valueToRead, '\0', sizeof(valueToRead));
      memset(valueToSend, '\0', sizeof(valueToSend));

      // Leggo il valore ricevuto dal client e gli restituisco la lunghezza
      read(simpleChildSocket, valueToRead, sizeof(valueToRead));
      snprintf(valueToSend, sizeof(valueToSend), "%zu", strlen(valueToRead));
      write(simpleChildSocket, valueToSend, sizeof(valueToSend));
    }

    memset(valueToRead, '\0', sizeof(valueToRead));
    memset(valueToSend, '\0', sizeof(valueToSend));

    // Aspetto il BYE dal client
    read(simpleChildSocket, valueToRead, sizeof(valueToRead));

    if (strcmp(valueToRead, "BYE") != 0) {
      fprintf(stderr, "Invalid BYE\n");
      exit(1);
    }
    printf("\tBYE RECEIVED\n");

    // Invio ACK al Server
    snprintf(valueToSend, sizeof(valueToSend), "ACK");
    write(simpleChildSocket, valueToSend, sizeof(valueToSend));

    close(simpleChildSocket);
  }

  close(simpleSocket);
  return 0;
}
