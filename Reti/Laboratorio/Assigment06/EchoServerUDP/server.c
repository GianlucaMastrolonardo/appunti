#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define MAXSIZEBUFFER 256

int main(int argc, char *argv[]) {
  int simpleSocket = 0;
  int simplePort = 0;
  int returnStatus = 0;
  struct sockaddr_in serverAddr;
  socklen_t serverSize;

  char buffer[MAXSIZEBUFFER] = {'\0'};

  if (argc < 1 || argc > 2) {
    fprintf(stderr, "Usage: %s <port>\n", argv[0]);
    exit(1);
  }

  // Creating UDP Socket
  simpleSocket = socket(AF_INET, SOCK_DGRAM, 0);

  if (simpleSocket == -1) {
    fprintf(stderr, "Could not create a socket!\n");
    exit(1);
  } else {
    printf("Socket created!\n");
  }

  // if available use argv[1] for port number, else generate a randome one
  if (argc == 2) {
    simplePort = atoi(argv[1]);
    if (simplePort < 10000 || simplePort > 12000) {
      fprintf(stderr, "Port must be in range [10000, 12000]\n");
      exit(1);
    }
  } else {
    srand(time(NULL));
    simplePort = (rand() % 1999) + 10000;
    // simplePort = 11100;
  }

  // Setup the address structure of the destination
  // use INADDR_ANY to bind to all local addresses
  serverSize = sizeof(serverAddr);
  memset(&serverAddr, '\0', sizeof(serverAddr));
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serverAddr.sin_port = htons(simplePort);

  returnStatus =
      bind(simpleSocket, (struct sockaddr *)&serverAddr, sizeof(serverAddr));

  if (returnStatus != 0) {
    fprintf(stderr, "Error, Impossible to Bind Socket\n");
    exit(1);
  } else {
    printf("Bind Complete on %d!\n", simplePort);
  }

  while (1) {
    struct sockaddr_in clientAddr;
    socklen_t clientSize = sizeof(clientAddr);

    // Recive from client
    memset(buffer, '\0', sizeof(buffer));

    recvfrom(simpleSocket, buffer, MAXSIZEBUFFER, 0,
             (struct sockaddr *)&clientAddr, &clientSize);

    printf("Received: '%s' | Sender IP: '%s'\n", buffer,
           inet_ntoa(clientAddr.sin_addr));

    // Send to client
    sendto(simpleSocket, buffer, strlen(buffer), 0,
           (struct sockaddr *)&clientAddr, clientSize);
  }

  return 0;
}
