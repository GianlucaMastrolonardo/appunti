#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define MAXSIZEBUFFER 256

int main(int argc, char *argv[]) {
  int simpleSocket = 0;
  int simplePort = 0;
  int returnStatus = 0;
  struct sockaddr_in serverAddr, clientAddr;
  socklen_t serverSize, clientSize;

  char buffer[MAXSIZEBUFFER] = {'\0'};

  if (argc != 3) {
    fprintf(stderr, "Usage: %s <server> <port>\n", argv[0]);
    exit(1);
  }

  simplePort = atoi(argv[2]);

  simpleSocket = socket(AF_INET, SOCK_DGRAM, 0);

  if (simpleSocket == -1) {
    fprintf(stderr, "Could not create a socket!\n");
    exit(1);
  } else {
    printf("Socket created!\n");
  }

  // Connected UDP need Bind to work
  returnStatus =
      bind(simpleSocket, (struct sockaddr *)&clientAddr, sizeof(clientAddr));

  if (returnStatus != 0) {
    fprintf(stderr, "Error, Impossible to Bind Socket\n");
    exit(1);
  } else {
    printf("Bind Complete on %d!\n", simplePort);
  }

  memset(&clientAddr, '\0', sizeof(clientAddr));
  clientAddr.sin_family = AF_INET;
  clientAddr.sin_addr.s_addr = inet_addr(argv[1]);
  clientAddr.sin_port = htons(simplePort);

  clientSize = sizeof(clientAddr);

  printf("Inserisci un valore: ");
  scanf("%s", buffer);

  // Using write instead of sendto
  write(simpleSocket, buffer, strlen(buffer));

  // Waiting for Echo and cleaning buffer just for checking if read works
  // properly
  memset(buffer, '\0', sizeof(buffer));

  read(simpleSocket, buffer, sizeof(buffer));
  printf("Echo from server: %s\n", buffer);

  return 0;
}
