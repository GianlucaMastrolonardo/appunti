# Appunti Java

## Sviluppo di un programma Java:

Un requisito necessario per procedere con queste fasi è quello di avere una **JDK** installata sul proprio computer, all'interno della JDK è presente tutto il necessario per sviluppare e compilare applicazioni Java.
In particolare all'interno della JDK troviamo:

- le API standard;

- il compilatore `javac` per convertire il codice sorgente in ByteCode;

- la JVM per eseguire il ByteCode, è una macchina virtuale che interpreta il ByteCode e lo esegue su diverse piattaforme hardware e software.

All'inizio si crea un file `.java` con all'interno tutte le sue classi con tutti i suoi metodi, questo sarà il codice sorgente del programma, di solito questa fase si chiama **Edit**.

Dopo aver concluso la fase di Edit il codice sorgente deve essere compilato (ovvero convertire il codice sorgente in ByteCode), per farlo bisogna utilizzare il comanda `javac` , se il programma compila correttamente il compilatore produce un file `.class`, che contiene la versione compilata. Questa fase si chiama **Compile**.

Il punto forte di Java è quello che <mark>il suo ByteCode è **portabile**</mark>, infatti le istruzioni presenti all'interno del ByteCode sono indipendenti dalla piattaforma.
Questo significa che il file `.class` <mark>può essere eseguto su qualsiasi piattaforma</mark> contenente una JVM.

Per invocare la JVM si utilizza il comando `java`. In particolare il comando `java` carica il programma in memoria. Questa fase si chiama **Load**.

Poi dopo la JVM si assicura che il ByteCode sia valido e sicurom questa fase si chiama **Verify**.

In fine abbiamo l'ultima fase che è l'esecuzione del ByteCode (**Execute**).

Ricapitolando abbiamo che:

- **<u>Edit</u>:** il codice `.java` viene scritto dal programmatore, implementando cosa dovrà fare il programma;

- **<u>Compile</u>:** il codice sorgente `.java` viene compilato con il comando `javac`, se la compilazione avviene con successo otteniamo il ByteCode, ovvero il file `.class`;

- **<u>Load</u>**: il ByteCode viene caricato nella JVM, significa che l'utente ha lanciato il comando `java`;

- **Verify**: il ByteCode viene analizzato dalla JVM;

- **Execute:** il ByteCode viene eseguito.

Le parti sottolineate necessitano di operazioni da parte dell'utente e/o programmatore.

---

## Metodi e Classi:

In Java quando vogliamo eseguire un operazione specifica chiamiamo un **metodo**. All'interno del metodo sono preseneti tutte le istruzione che quella operazione necessita per essere risolta.

La **Classe** è il contenitore che contiene l'insieme dei metodi. In Java una classe si realizza scrivendo all'interno di un file `.java` le seguenti keyword:

```java
public class nomeClasse{

}
```

<mark>N.B: Il nome di una classe public deve sempre essere lo stesso del file.</mark>

Il concetto di Access Modifiers (Public, Private, Protected), sarà visto più avanti.

### Istanze:

Per construire un oggetto dobbiamo trovarci all'interno di una classe, a questo punto possiamo creare il nostro oggetto (o di un'istanza della classe).

### Package:

Le classi che contengono metodi utili o utilizzati frenquementente possono essere raggrappate in dei **package**, per esempio la Java API (Application Programming Interface) è un grande package con all'interno molte classi.

Per imporatare un package nel nostro progetto java basterà usare la parola chiave `import`.

Di default il package `java.lang` è automaticamente importante.

---


