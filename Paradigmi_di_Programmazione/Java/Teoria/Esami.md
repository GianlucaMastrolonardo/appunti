# Esami Teoria Java:

## Domande inventate:

### Domanda 1:

Qual è la differenza tra `is-a` ed `has-a`?

### Risposta:

`is-a` e `has-a` definiscono il tipo di relazione che c'è tra classi:

- `is-a`: è una relazione di **eredarietà**, si verifica quando una classe specifica un altra classe più specifica, per esempio se diciamo che uno `Studente` è un tipo di  `Persona` significa che la classe `Studente` eredita tutti i membri della classe `Persona` . Per farlo si utilizza la keyword `extends`.

- `has-a`: è una relazione di **composizione**, si verifica quando un oggetto di una classe contiene un riferimento di un oggetto di un'altra classe come suo membro o attributo. Prendiamo per esempio una classe `Macchina`, ogni macchina ha un `Motore` (che è una classe separata con i suoi valori).

## Esame 02/02/2024:

### Domanda 1:

Qual è la differenza tra metodo static e metodo non static? Cosa cambia nel caso in cui accediate a delle variabili
d'istanza da un tipo di metodo e l'altro?

### Risposta:

Un metodo static può essere invocato direttamente utilizzando il nome della classe, mentre un metodo non static richiede prima la creazione di un istanza di un oggetto di quella classe, e poi successivamente si può utilizzare il metodo sull'oggetto stesso.

Accedere a delle variabili d'istanza non static in un metodo static non è consentito, perché appunto necessitano prima della creazione di un istanza, invece accedere a delle variabi d'istanza static in metodi non static è consentito.

### Domanda 4:

A cosa servono le classi astratte e le interfaccie e perché usare l'una oppure l'altra?

### Risposta:

Le classi astratte e le interfacce sono delle funzionalità fornite da Java in modo tale da organizzare al meglio il nostro progetto, in particolare abbiamo che:

- le **classi astratte** sono classi hanno attributi e metodi (che possono essere astratti), ma non permettono di istanziare oggetti di quella classe. Prendiamo per esempio una classe come `Animal`, ogni animale avrà un sesso ed una specie, in più ogni animale emette un verso (diverso da ogni animale). Possiamo supporre che ogni animale (Cane, Gatto) siano classi che estendono la classe astratta `Animal` (utilizzando la keyword `extends`). In questo modo abbiamo uno scheletro (fornito dalla classe astratta) per ogni animale, senza dover ricreare a mano ogni getter e setter (per esempio getSpecie). In più la classe astratta ci fornisce la possibilità di creare metodi astratti (definiti scrivendo esclusivamente la visibilità, il tipo ed il nome del metodo, ma senza il body del metodo). L'implementazione del metodo va fatta in ogni classe che estende la classe astratta (è una good practice aggiungere @Override sopra al metodo che si sta implementando).
  Riassumendo le classi astratte si usano per mettere in relazioni classi molto simili tra di loro.

- le **interfacce** sono delle classi dove sono presenti esclusivamente i metodi che verrano poi implementati dalle classi che implementano (utilizzando la keyword `implements` ) l'interfaccia. Per esempio appena implementiamo l'interfaccia `Comparable` dobbiamo definire la nostra `compareTo`. Volendo nelle interfacce è possibile definire anche delle variabili, ma queste devono essere `static` e `final`, perché non è possibile definire costruttori (e quindi istanze delle interfacce). 
  Riassumendo le interfacce si usano per mettere in relazioni classi completamente differenti tra di loro, ma che necessitano di un metodo specifico.














