// Fig. 9.6: BasePlusCommissionEmployee.java
// BasePlusCommissionEmployee class represents an employee that receives
// a base salary in addition to commission.

public class BasePlusCommissionEmployee  // extends CommissionEmployee
{
   private double baseSalary; // base salary per week
   public CommissionEmployee commissionEmployee;

   // six-argument constructor
   public BasePlusCommissionEmployee(String firstName, String lastName, String socialSecurityNumber, double grossSales, double commissionRate, double baseSalary )
   {
      // explicit call to superclass CommissionEmployee constructor
      // super(firstName, lastName, socialSecurityNumber, grossSales, commissionRate);
      commissionEmployee = new CommissionEmployee(firstName, lastName, socialSecurityNumber, grossSales, commissionRate);

      // if baseSalary is invalid throw exception
      if (baseSalary < 0.0 ) {
         throw new IllegalArgumentException("Base Salary must be >= 0.0");
      }

      this.baseSalary = baseSalary;
   } // end six-argument BasePlusCommissionEmployee constructor

   // set base salary
   public void setBaseSalary( double baseSalary )
   {
      if (baseSalary < 0.0) {
         throw new IllegalArgumentException("Base Salary must be >= 0.0");
      }
      this.baseSalary = baseSalary;
   } // end method setBaseSalary

   // return base salary
   public double getBaseSalary()
   {
      return baseSalary;
   } // end method getBaseSalary

   // calculate earnings
   public double earnings()
   {
      return getBaseSalary() + commissionEmployee.earnings();
   } // end method earnings

   // return String representation of BasePlusCommissionEmployee
   @Override
   public String toString()
   {
      return String.format( 
         "%s %s%n%s: %.2f", "base-salaried",
         super.toString(),
         "base salary", getBaseSalary() );
   } // end method toString

} // end class BasePlusCommissionEmployee




/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
