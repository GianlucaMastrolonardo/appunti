import java.util.Scanner;

public class Addition {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Inserisci il primo valore intero: ");
        int number1 = input.nextInt();

        System.out.println("Inserisci il secondo valore intero: ");
        int number2 = input.nextInt();

        int sum = number1 + number2;

        System.out.printf("La somma è: %d", sum);
    }
}