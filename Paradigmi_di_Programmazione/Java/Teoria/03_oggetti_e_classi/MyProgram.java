import java.util.Scanner;

public class MyProgram {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Account FirstUser = new Account();

        System.out.printf("Nome iniziale: %s\n", FirstUser.getName());

        System.out.printf("Inserire il nome dell'Utente: ");
        String tempName = input.nextLine();
        FirstUser.setName(tempName);

        System.out.printf("Nome nella classe: %s", FirstUser.getName());

    }
}
