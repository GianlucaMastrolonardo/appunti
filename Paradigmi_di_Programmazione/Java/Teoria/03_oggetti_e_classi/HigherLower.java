import java.util.Scanner;

public class HigherLower {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.printf("Valore di A: ");
        int a = input.nextInt();

        System.out.printf("Valore di B: ");
        int b = input.nextInt();

        if (a == b) {
            System.out.printf("I Valori sono uguali");
        } else if (a > b) {
            System.out.printf("A è maggiore di B");
        } else {
            System.out.printf("B è maggiore di A");
        }

    }
}