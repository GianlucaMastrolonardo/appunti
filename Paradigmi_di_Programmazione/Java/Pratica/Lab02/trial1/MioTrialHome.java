import java.util.Scanner;

public class MioTrialHome {

	static Scanner tastiera = new Scanner(System.in);

	static String[] id = new String[3];
	static int[] annoNascita = new int[3];
	static int[] registrazioneG = new int[3]; // Giorni
	static int[] registrazioneM = new int[3]; // Mesi
	static int[] registrazioneA = new int[3]; // Anno

	static int numPazienti = 8;

	public static void menu() {
		System.out.println("Cosa vuoi fare?\nDigita la cifra corrispondente.");
	}

	public static void esegui(int scelta) {
		// System.out.println(scelta);
		switch (scelta) {
			case 1:
				inserisci();
				break;
			case 2:
				break;
		}
	}

	public static void inserisci() {
		// ID
		System.out.println(">>> Inserire l'ID del pazziente:");
		String id = tastiera.nextLine();
		while (id == "" || id == " ") {
			System.out.println(">>> Hai inserito un valore nullo, riprova.");
			id = tastiera.nextLine();
		}
		// Anno di Nascita
		System.out.printf(">>> Ciao ID: %s, inserisci il tuo anno di nascita:\n", id);
		int annoNascita = tastiera.nextInt();
		tastiera.nextLine();
		while (annoNascita <= 1910 || annoNascita >= 2024) {
			System.out.println(">>> Hai inserito un anno di nascita errato, riprova.");
			annoNascita = tastiera.nextInt();
			tastiera.nextLine();
		}
		// Giorno, Mese, Anno di Registrazione
		System.out.println(">>> Inserisci il tuo giorno di registrazione");
		int giornoRegistrazione = tastiera.nextInt();
		tastiera.nextLine();
		while (giornoRegistrazione <= 0 || giornoRegistrazione >= 31) {
			System.out.println(">>> Hai inserito un giorno di registrazione errato, riprova.");
			giornoRegistrazione = tastiera.nextInt();
			tastiera.nextLine();
		}
		System.out.println(">>> Inserisci il tuo mese di registrazione");
		int meseRegistrazione = tastiera.nextInt();
		tastiera.nextLine();
		while (meseRegistrazione <= 0 || meseRegistrazione >= 13) {
			System.out.println(">>> Hai inserito un mese di registrazione errato, riprova.");
			meseRegistrazione = tastiera.nextInt();
			tastiera.nextLine();
		}
		System.out.println(">>> Inserisci il tuo anno di registrazione");
		int annoRegistrazione = tastiera.nextInt();
		tastiera.nextLine();
		while (annoRegistrazione <= 1910 || annoRegistrazione >= 2024) {
			System.out.println(">>> Hai inserito un mese di registrazione errato, riprova.");
			annoRegistrazione = tastiera.nextInt();
			tastiera.nextLine();
		}
	}

	public static void main(String[] args) {
		System.out.println("«Benvenuto nel software EDC dell’UPO»");
		int scelta = -1;

		while (scelta != 100) {
			menu();
			scelta = tastiera.nextInt();
			tastiera.nextLine(); // É stato aggiunta questa next line perchè il metodo .nextInt() non va alla
									// riga successiva, quindi ogni valto devi fare la nextLine()
			esegui(scelta);
		}
	}

}
