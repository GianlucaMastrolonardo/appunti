import java.util.Scanner;
import java.util.Arrays;

public class MioTrial {

	static Scanner tastiera = new Scanner(System.in);

	static String[] id = new String[3];
	static int[] annoNascita = new int[3];
	static int[] registrazioneG = new int[3]; // Giorni
	static int[] registrazioneM = new int[3]; // Mesi
	static int[] registrazioneA = new int[3]; // Anno
	// Questi array sono visti da tutti metodi all'interno di questo codice

	static double[][] temperatura = new double[3][4]; // ricorda che viene inizializzato tutto a zero, che in questo
														// caso andrebbe anche bene perché la temperetura corporea a
														// zero non può esistere
														// sennò lo riempi di NaN o NEGATIVE_INFINITY o
														// POSITIVE_INFINITY

	static String[][] pressione = new String[3][4]; // normale bassa alta, sarebbe meglio fare l'enumrate con enum per
													// avere una matrice di interi e poi "covertirli"

	static int numPazienti = 0;

	public static void stampaPaziente(String id, int annoNascita, int registrazioneG, int registrazioneM,
			int registrazioneA) {
		System.out.printf(
				"+++ Informazioni Riguardo al Paziente +++\n\t- ID: %s\n\t- Anno di Nascita: %d\n\t- Giorno Registrazione %d\n\t- Mese Registrazione: %d\n\t- Anno Registrazione: %d\n\n",
				id, annoNascita, registrazioneG, registrazioneM, registrazioneA);
	}

	public static void menu() {
		System.out.println(
				"Cosa vuoi fare?\nDigita la cifra corrispondente.\n1 Inserisci\n2 Ricerca\n3 Ricerca Età\n100 Uscita");
	}

	public static void esegui(int scelta) {
		// System.out.println(scelta);
		switch (scelta) {
			case 1:
				inserisci();
				break;
			case 2:
				ricerca();
				break;
		}
	}

	public static void inserisci() {
		// ID
		System.out.println(">>> Inserire l'ID del paziente:");
		String idLetto = tastiera.nextLine();
		while (idLetto == "" || idLetto == " ") {
			System.out.println(">>> Hai inserito un valore nullo, riprova.");
			idLetto = tastiera.nextLine();
		}
		// Anno di Nascita
		System.out.printf(">>> Ciao ID: %s, inserisci il tuo anno di nascita:\n", idLetto);
		int annoNascitaLetto = tastiera.nextInt();
		tastiera.nextLine();
		while (annoNascitaLetto <= 1910 || annoNascitaLetto >= 2024) {
			System.out.println(">>> Hai inserito un anno di nascita errato, riprova.");
			annoNascitaLetto = tastiera.nextInt();
			tastiera.nextLine();
		}
		// Giorno, Mese, Anno di Registrazione
		System.out.println(">>> Inserisci il tuo giorno di registrazione");
		int giornoRegistrazione = tastiera.nextInt();
		tastiera.nextLine();
		while (giornoRegistrazione <= 0 || giornoRegistrazione >= 32) {
			System.out.println(">>> Hai inserito un giorno di registrazione errato, riprova.");
			giornoRegistrazione = tastiera.nextInt();
			tastiera.nextLine();
		}
		System.out.println(">>> Inserisci il tuo mese di registrazione");
		int meseRegistrazione = tastiera.nextInt();
		tastiera.nextLine();
		while (meseRegistrazione <= 0 || meseRegistrazione >= 13) {
			System.out.println(">>> Hai inserito un mese di registrazione errato, riprova.");
			meseRegistrazione = tastiera.nextInt();
			tastiera.nextLine();
		}
		System.out.println(">>> Inserisci il tuo anno di registrazione");
		int annoRegistrazione = tastiera.nextInt();
		tastiera.nextLine();
		while (annoRegistrazione <= 1910 || annoRegistrazione >= 2024) {
			System.out.println(">>> Hai inserito un mese di registrazione errato, riprova.");
			annoRegistrazione = tastiera.nextInt();
			tastiera.nextLine();
		}

		boolean output = inserisciCM(idLetto, annoNascitaLetto, giornoRegistrazione, meseRegistrazione,
				annoRegistrazione);
		if (output == false) {
			System.out.println("Non è stato possibile inserire l'utente");
		} else {
			stampaPaziente(id[numPazienti - 1], annoNascita[numPazienti - 1], registrazioneG[numPazienti - 1],
					registrazioneM[numPazienti - 1], registrazioneA[numPazienti - 1]);
		}
	}

	public static boolean inserisciCM(String idCurr, int annoNascitaCurr, int giornoCurr, int meseCurr, int annoCurr) {
		// Questo serve per il debug, NON DEVONO ESSERCI INTERAZIONI CON l'utente. CM =
		// Controller Model

		// Qui fai tutti i controlli dei tipi di dati, se ci sono problemi restituisci
		// false, sennò fai true

		if (idCurr.equals("") || idCurr.equals(" ")) {
			System.out.println("+++ Errore ID +++");
			return false;
		}

		if (annoNascitaCurr <= 1910 || annoNascitaCurr >= 2024) {
			System.out.println("+++ Errore Anno Nascita +++");
			return false;
		}

		if (giornoCurr <= 0 || giornoCurr >= 32) {
			System.out.println("+++ Errore Giorno +++");
			return false;
		}

		if (meseCurr <= 0 || meseCurr >= 13) {
			System.out.println("+++ Errore Mese +++");
			return false;
		}

		if (annoCurr <= 1910 || annoCurr >= 2024) {
			System.out.println("+++ Errore Anno Reg +++");
			return false;
		}

		if (annoNascitaCurr > annoCurr) {
			System.out.println("+++ Errore Anno di Nascita maggiore dell' Anno di Registrazione +++");
			return false;
		}

		if (numPazienti >= id.length) {
			id = Arrays.copyOf(id, id.length * 2); // Cosi puoi aumentare la dimensione dell'array
			annoNascita = Arrays.copyOf(annoNascita, id.length * 2);
			registrazioneG = Arrays.copyOf(registrazioneG, id.length * 2);
			registrazioneM = Arrays.copyOf(registrazioneM, id.length * 2);
			registrazioneA = Arrays.copyOf(registrazioneA, id.length * 2);
			// da fare per tutti

		}
		// a questo punto devo inserire l'oggetto

		id[numPazienti] = idCurr;
		annoNascita[numPazienti] = annoNascitaCurr;
		registrazioneG[numPazienti] = giornoCurr;
		registrazioneM[numPazienti] = meseCurr;
		registrazioneA[numPazienti] = annoCurr;
		numPazienti++;

		return true;
	}

	public static void ricerca() {
		if (numPazienti == 0) {
			System.out.printf(
					"\n+++ Impossibile effetuare la ricerca, non è presente nessun paziente nel database +++\n\n");
		} else {
			System.out.println(">>> Inserire l'ID del paziente da ricercare:");
			String idScelto = tastiera.nextLine();

			int pos = -1; // Lo metto fuori scala così se non è stato trovato lo so
			int i;
			for (i = 0; i < numPazienti; i++) { // occhio ad usare numPazienti e non id.length perché con il secondo
												// metodo provi ad accedere a valori null
				if (id[i].equals(idScelto)) {
					pos = i;
				}
			}

			if (pos != -1) {
				stampaPaziente(id[pos], annoNascita[pos], registrazioneG[pos], registrazioneM[pos],
						registrazioneA[pos]);
			}
		}
		/*
		 * In realtà sarebbe stato meglio il for normale ma ha scelto il foreach per
		 * // farcelo vedere
		 * for (String element : id) { // se fosse stato intero metti Integer e non int,
		 * ti avrebbe dato errore, sono classi Wrapper
		 * if (idScelto.equals(element)) { // non puoi fare doppio uguale perchè sono
		 * oggetti e tu così compareresti i
		 * // puntatori
		 * pos = curPos;
		 * }
		 * curPos++;
		 * }
		 * 
		 * // In realtà sarebbe stato ancora meglio usare il binarySearch di Arrays di
		 * // Java, più cose di java usi meglio è
		 */
		// System.out.println(Arrays.binarySearch(id, idScelto)); non si può usare
		// perchè dovresti ordinare prima l'array però così perdi la referenza al dato.
	}

	public static void ricercaEta() {

	}

	public static void statistischeTemperatura() {
		double max = Double.MIN_VALUE; // guarda (https://docs.oracle.com/javase/8/docs/api/java/lang/Double.html)
		double min = Double.MAX_VALUE;
		double avg = 0.0;
		int cnt = 0;
		/*
		 * DA ERRORE PERCHÉ DICE CHE NAN non è un tipo di dato
		 * for(Double[] element: temperatura){
		 * for(Double value : element){
		 * if(value != Double NaN && max < value) max = value; //Attenzione ai NaN
		 * //Gli operatori sono lazy, partono dalla sinistra ed il primo che trovano che
		 * cambia la condizione si fermano
		 * 
		 * }
		 * }
		 */
	}

	public static void main(String[] args) {
		System.out.println("«Benvenuto nel software EDC dell’UPO»");
		int scelta = -1;

		while (scelta != 100) {
			menu();
			scelta = tastiera.nextInt();
			tastiera.nextLine(); // É stato aggiunta questa next line perchè il metodo .nextInt() non va alla
									// riga successiva, quindi ogni valto devi fare la nextLine()
			esegui(scelta);
		}
	}

}
