package upo20049568.trials;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RecordTest {

    Paziente r;

    @BeforeEach // fa l'inizializzazione prima di tutti gli altri metodi
    void beforeEach()
    {
        r = new Paziente("Emanuele", "15");
    }

    @Test
    void creaRecordNotNull(){
        Assertions.assertNotNull(r);
    }

    @Test
    void testValorizzazioneNome()
    {
        Assertions.assertEquals("Emanuele", r.nome);
        //assertEquals su getNome
    }

    @Test
    void testSetNomeOk()
    {
        r.setNome("Luca");
        Assertions.assertEquals("Luca", r.nome);
    }

    @Test
    void testSetNomeSbagliato(){
        String oldName = r.nome;
        Assertions.assertThrows(IllegalArgumentException.class, ()->{
           r.setNome(null);
        });

        Assertions.assertEquals(oldName,r.nome);
    }

}
