public class PositionMain {
    public static void main(String[] args) {
        //quando chiamo costruttore fa la stampa del costruttore
        PositionModel m = new PositionModel();
        PositionView v = new PositionView();

        m.addPropertyChangeListener(v);

        //Non ho chiamato l'addlistener
        m.moveRight();
        m.moveRight();
        m.moveRight();
        m.moveRight();
        m.moveRight();
        m.moveRight();
    }
}
