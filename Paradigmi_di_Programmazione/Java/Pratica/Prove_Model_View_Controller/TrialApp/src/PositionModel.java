import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Properties;

public class PositionModel {
    //devi istanziare un PropertyChangeSupport e PropertyChangeEvent
    private PropertyChangeSupport support;
    int pos;

    public PositionModel() {
        this.pos = 0;

        //Ha bisogno di un parametro, ovvero chi sta facendo uscire questo elemento, si usa quasi sempre this, in questo modo gli passi tutto model
        this.support = new PropertyChangeSupport(this);
    }

    //Unico modo in cui il modello sa della visita
    public void addPropertyChangeListener(PropertyChangeListener listener){
        //Così leghi le due cose
       this.support.addPropertyChangeListener(listener);
    }


    public void moveRight(){
        this.changePos(this.pos, this.pos+1);
        //dobbiamo sparare questo evento, OGNI VOLTA CHE CAMBIA LA POSITION. (quindi anche in moveleft)
    }

    public void moveLeft(){
        this.changePos(this.pos, this.pos-1);
    }


    //Così è fatto meglio
    private void changePos(int oldValue, int newValue){
        this.pos = newValue;
        //Position è un nome scelto da noi
        this.support.firePropertyChange("position",oldValue,newValue);
    }
}
