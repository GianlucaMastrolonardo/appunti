package upo20049568.trials;

import java.util.*;

public class Paziente {

    static Set<String> ids;
    static{
        //questo codice viene eseguito una volta sola
        ids = new HashSet<>();
    }
    protected String nome;
    protected String id;

    ArrayList<Visit> visita;

    public Paziente(String nome, String id) {
        if(ids.contains(id)){
            throw new IllegalArgumentException();
        }
        this.id = id;
        this.nome = nome;
        this.visita = new ArrayList<Visit>();
        ids.add(id);
    }

    public void setNome(String nome) {
        if(nome == null) throw new IllegalArgumentException();
        this.nome = nome;
    }

    public void addVisit(Visit v){ //Passo direttamente la visita, volendo potresti passare i paramentri ma se vuoi aggiungere parametri è un casino
        this.visita.add(v);
    }

    public List<Visit> getVisitsByParameter(String parameter){
        List<Visit> result = new ArrayList<>();
        for (Visit v : this.visita){
            if(parameter.equals("temperatura") || v.temperatura != null){
                result.add(v);
            }
        }
        return result;
    }

    //Non va tanto bene perché in ogni sotto classe devi reimprementare questo
    /*
    @Override
    public String toString() {
        return "Paziente{" +
                "nome='" + this.nome + '\'' +
                '}';
    }
    */
    @Override
    public String toString(){
        return "[nome: " + this.nome + "]";
    }

    @Override //Se non metti ovveride lui in automatico rileva se stai modificando un metodo che non era stato implementato prima
    public boolean equals(Object o) {
        if (this == o) return true; //Faccio l'uguaglianza per identità
        if (!(o instanceof Paziente)) return false;
        Paziente paziente = (Paziente) o; //Faccio il cast
        return this.nome.equals(paziente.nome); //True se i nomi sono uguali sennò false
    }

    @Override //Ha letteralmente detto di non toccarlo
    public int hashCode() {
        return Objects.hash(nome);
    }

    public static void main(String[] args) {
        Paziente p = new Paziente("Mario", "2");
        Paziente p2 = new Paziente("Mario", "3");
        System.out.println(p);
        System.out.println(p.equals(p2)); //Restituisce True a causa dell'override, sennò restituisce false perché equals di java il controllo per identità.
    }
}
