package upo20049568.trials;

import java.util.Comparator;
import java.util.Scanner;
import java.util.Arrays;
import java.security.SecureRandom;

public class MioTrial {
    //le variabili stanno sopra a tutto, dentro la classe
    static Scanner tastiera = new Scanner(System.in);

    static String[] id = new String[3];
    static int[] annoNascita = new int[3];
    static int[] registrazioneG = new int[3];
    static int[] registrazioneM = new int[3];
    static int[] registrazioneA = new int[3];

    //matrice
    static double[][] temperatura = new double[3][4];
    static String[][] pressione = new String[3][4];

    static int numPazienti = 0; //contatore pazienti

    public static void menu() {
        System.out.print("""
            Cosa vuoi fare?
            Digita il numero corrispondente.
            - Per uscire scrivere 100
            1 = Inserisci un paziente
            2 = Ricerca
            3 = Ricerca con età
            4 = Aggiungi visita
            5 = Statistiche Temperatura
            6 = Statistiche Pressione
            7 = Inserisci un paziente senza id
            Input:\s
            """);
    }

    public static void esegui(int scelta) {
        //System.out.println(scelta);
        switch (scelta) {
            case 1:
                inserisci();
                break;
            case 2:
                ricerca();
                break;
            case 3:
                ricercaEta();
                break;
            case 4:
                aggiungiVisita();
                break;
            case 5:
                stastisticheTemperatura();
                break;
            case 6:
                stastistichePressione();
                break;
            case 7:
                inserisciNoId();
                break;
            case 100:
                break;
        }
    }


    public static void inserisci() {
        //leggere valori con scanner e inserirli negli array
        //raccolta dati
        tastiera.nextLine(); // svuota il buffer
        String idLetto = "";
        int an = -1;
        int gr = -1;
        int mr = -1;
        int ar = -1;

        while (idLetto.isEmpty()) {
            System.out.println("\nInserisci id:");
            idLetto = tastiera.nextLine().trim();
            if (idLetto.isEmpty()) System.out.println("Id errato, riprovare!");
        }

        while (an < 1900 || an > 2023) {
            System.out.println("Inserisci l'anno di nascita:");
            an = tastiera.nextInt();
            if (an < 1900 || an > 2023) System.out.println("Anno di nascita non valido, riprovare!");
        }

        while (gr < 1 || gr > 31) {
            System.out.println("Inserisci il giorno registrazione:");
            gr = tastiera.nextInt();
            if (gr < 1 || gr > 31) System.out.println("Giorno non valido, riprovare!");
        }

        while (mr < 1 || mr > 12) {
            System.out.println("Inserisci il mese registrazione:");
            mr = tastiera.nextInt();
            if (mr < 1 || mr > 12) System.out.println("Mese non valido, riprovare!");
        }

        while (ar < 1900 || ar > 2023) {
            System.out.println("Inserisci l'anno registrazione:");
            ar = tastiera.nextInt();
            if (ar < 1900 || ar > 2023) System.out.println("Anno non valido, riprovare!");
        }

        boolean output = inserisciCM(idLetto, an, gr, mr, ar);
        if (!output) {
            System.out.println("Non è stato possibile inserire l'utente");
        }

        //parte da mettere nel nuovo metodo

    }

    public static boolean inserisciCM(String idCurr, int anCurr, int grCurr, int mrCurr, int arCurr) // non deve interagire con l'utente
    {
        //fare controlli sui parametri
        //inserimento dati
        // array sufficientemente grandi
        if (idCurr.isEmpty() || idCurr.equals(" ")) {
            System.out.println("+++ Errore ID +++");
            return false;
        }

        for (String el : id){
            if(idCurr.equals(el)) {
                System.out.println("+++ Errore ID già presente nel database +++");
                return false;
            }
        }

        if (anCurr <= 1910 || anCurr >= 2024) {
            System.out.println("+++ Errore Anno Nascita +++");
            return false;
        }

        if (grCurr <= 0 || grCurr >= 32) {
            System.out.println("+++ Errore Giorno +++");
            return false;
        }

        if (mrCurr <= 0 || mrCurr >= 13) {
            System.out.println("+++ Errore Mese +++");
            return false;
        }

        if (arCurr <= 1910 || arCurr >= 2024) {
            System.out.println("+++ Errore Anno Reg +++");
            return false;
        }

        if (anCurr > arCurr) {
            System.out.println("+++ Errore Anno di Nascita maggiore dell' Anno di Registrazione +++");
            return false;
        }

        if (numPazienti >= id.length || numPazienti >= annoNascita.length ||
                numPazienti >= registrazioneG.length || numPazienti >= registrazioneM.length ||
                numPazienti >= registrazioneA.length || numPazienti == pressione.length || numPazienti == temperatura.length) //array troppo piccolo
        {
            id = Arrays.copyOf(id, id.length * 2);
            annoNascita = Arrays.copyOf(annoNascita, annoNascita.length * 2);
            registrazioneG = Arrays.copyOf(registrazioneG, registrazioneG.length * 2);
            registrazioneM = Arrays.copyOf(registrazioneM, registrazioneM.length * 2);
            registrazioneA = Arrays.copyOf(registrazioneA, registrazioneA.length * 2);

            //Non c'è più spazio nell'array
            int nuovaDimensione = pressione.length * 2;
            String[][] nuovaPressione = new String[nuovaDimensione][pressione[0].length];

            // Copiare i dati dalla matrice originale alla nuova matrice
            for (int i = 0; i < pressione.length; i++) {
                nuovaPressione[i] = Arrays.copyOf(pressione[i], pressione[i].length);
            }

            double[][] nuovaTemperatura = new double[nuovaDimensione][temperatura[0].length];

            for (int i = 0; i < temperatura.length; i++) {
                nuovaTemperatura[i] = Arrays.copyOf(temperatura[i], temperatura[i].length);
            }

            pressione = nuovaPressione;
            temperatura = nuovaTemperatura;
        }


        //INSERIMENTI
        id[numPazienti] = idCurr;
        annoNascita[numPazienti] = anCurr;
        registrazioneG[numPazienti] = grCurr;
        registrazioneM[numPazienti] = mrCurr;
        registrazioneA[numPazienti] = arCurr;

        //incremento il num dei pazienti
        numPazienti++;

        return true;
    }

    public static void inserisciNoId() {
        //leggere valori con scanner e inserirli negli array
        //raccolta dati
        tastiera.nextLine(); // svuota il buffer
        int an = -1;
        int gr = -1;
        int mr = -1;
        int ar = -1;


        while (an < 1900 || an > 2023) {
            System.out.println("Inserisci l'anno di nascita:");
            an = tastiera.nextInt();
            if (an < 1900 || an > 2023) System.out.println("Anno di nascita non valido, riprovare!");
        }

        while (gr < 1 || gr > 31) {
            System.out.println("Inserisci il giorno registrazione:");
            gr = tastiera.nextInt();
            if (gr < 1 || gr > 31) System.out.println("Giorno non valido, riprovare!");
        }

        while (mr < 1 || mr > 12) {
            System.out.println("Inserisci il mese registrazione:");
            mr = tastiera.nextInt();
            if (mr < 1 || mr > 12) System.out.println("Mese non valido, riprovare!");
        }

        while (ar < 1900 || ar > 2023) {
            System.out.println("Inserisci l'anno registrazione:");
            ar = tastiera.nextInt();
            if (ar < 1900 || ar > 2023) System.out.println("Anno non valido, riprovare!");
        }

        boolean output = inserisciCM(an, gr, mr, ar);
        if (!output) {
            System.out.println("Non è stato possibile inserire l'utente");
        }

        //parte da mettere nel nuovo metodo

    }

    /*
    Override metodo inserisciCM
    Per testare il metodo bisogna verificare:
    - Se il metodo senza id funzioni correttamente, restituisca true
    - Se inserendo più pazienti abbiano tutti id diversi
    - Se gli id generati sono nel range specificato

     */
    public static boolean inserisciCM(int anCurr, int grCurr, int mrCurr, int arCurr) // non deve interagire con l'utente
    {

        // Crea un'istanza di SecureRandom
        SecureRandom secureRandom = new SecureRandom();

        // Genera un numero casuale compreso tra 1000 e 9999
        int numeroCasuale = secureRandom.nextInt(9000) + 1000;
        String idCurr = ""+numeroCasuale;

        for (String el : id){
            if(idCurr.equals(el)) {
                numeroCasuale = secureRandom.nextInt(9000) + 1000;
                idCurr = ""+numeroCasuale;
            }
        }

        System.out.println("\n-Id generato: "+idCurr+"\n");

        return inserisciCM(idCurr, anCurr,  grCurr,  mrCurr,  arCurr);
    }

    public static void ricerca() {
        tastiera.nextLine(); // svuota il buffer
        System.out.println("Inserisci id da cercare:");
        String idScelto = tastiera.nextLine();
        int pos = -1;
        int curPos = 0;
        for (String curr: id) //da usare un for normale
        {
            if (idScelto.equals(curr)) //confronta due stringhe
            {
                pos = curPos;
            }
            curPos++;
        }
        //stampare i valori richiesti
        if (pos != -1) {
            System.out.printf("\n- Paziente trovato!\nId: %s\nEtà: %d\nData registrazione: %d/%d/%d\n\n",
                    id[pos], 2023 - annoNascita[pos], registrazioneG[pos], registrazioneM[pos], registrazioneA[pos]);
        } else {
            System.out.println("\nId non trovato! :(\n");
        }
    }

    /*serve per la ricerca con parametri*/
    public static int ricercaPar(String idScelto) {
        int pos = -1;
        int curPos = 0;
        for (String curr: id) //da usare un for normale
        {
            if (idScelto.equals(curr)) //confronta due stringhe
            {
                pos = curPos;
            }
            curPos++;
        }
        //stampare i valori richiesti
        return pos;
    }

    public static void ricercaEta() {
        tastiera.nextLine(); // svuota il buffer
        System.out.println("Inserisci età minima da cercare:");
        int etaMin = tastiera.nextInt();
        System.out.println("Inserisci età massima da cercare:");
        int etaMax = tastiera.nextInt();

        String[] pazTrovati = new String[id.length];
        int etap;
        int j = 0;
        for (int i = 0; i < annoNascita.length; i++) {
            etap = 2023 - annoNascita[i];
            if (etap <= etaMax && etap >= etaMin) {
                pazTrovati[j] = id[i];
                j++;
            }
        }

        if (pazTrovati[0] != null) {
            System.out.println("Pazienti trovati:");
            for (String idP: pazTrovati) {
                if (idP != null) System.out.println("id: " + idP + "\n");
            }
        } else System.out.println("Nessun paziente trovato! :(\n");

    }

    public static void aggiungiVisita() {
        tastiera.nextLine(); // svuota il buffer
        System.out.println("Inserisci id da cercare:");
        String idUser = tastiera.nextLine();
        int pos = ricercaPar(idUser);

        if (pos == -1) {
            System.out.println("ID non trovato");
        } else {
            System.out.println("Inserisci la temperatura del paziente:");
            double temp = tastiera.nextDouble();
            while (temp > 40 || temp < 34) {
                System.out.println("Hai inserito una temperatura errata\nInserisci la temperatura del paziente:");
                temp = tastiera.nextDouble();
            }

            tastiera.nextLine();
            System.out.println("Inserisci la pressione del pressione:");
            String press = tastiera.nextLine();
            while (!((press.equals("bassa") || (press.equals("normale")) || (press.equals("alta"))))) {
                System.out.println("Hai inserito una pressione errata\nInserisci la pressione del paziente:");
                press = tastiera.nextLine();
            }

            int i;
            for(i = 0; i < pressione[0].length; i++){
                if(pressione[pos][i] == null && temperatura[pos][i] == 0){
                    pressione[pos][i] = press;
                    temperatura[pos][i] = temp;
                    break;
                }
            }
            if (i == pressione[0].length){
                System.out.println("Massimo di 4 visite raggiunto");
            }


        }

    }

    public static void stastisticheTemperatura() {
        double max = Double.MIN_VALUE;
        double min = Double.MAX_VALUE;
        double media = 0.0;
        double tempTot = 0.0;
        int count = 0;

        for (double[] curr: temperatura) {
            for (double el: curr) {
                if (el != 0) {
                    if (max < el) max = el; // attenzione ai NaN o comunque le temperature non valide
                    if (min > el) min = el;
                    tempTot += el;
                    count++;
                }
            }
        }
        if (count > 0) media = tempTot / count;

        if(max != Double.MIN_VALUE && min != Double.MAX_VALUE) {
            System.out.printf("-Statistiche temperatura\nmax: %.2f\nmin: %.2f\nmedia: %.2f\n", max, min, media);
        }

        System.out.println("Inserisci il numero di visita >= 1: ");
        int numVisita = tastiera.nextInt() - 1;

        double[] tempPerVisite = new double[numPazienti];
        for(int i = 0; i < numPazienti; i++){
            if(numVisita < temperatura[i].length){
                if(temperatura[i][numVisita] != 0){
                    tempPerVisite[i] = temperatura[i][numVisita];
                }
            }
        }
        Arrays.sort(tempPerVisite);

        for (int i = 0; i < tempPerVisite.length; i++) {
            System.out.printf("Temperatura[%d]: %.2f\n", i, tempPerVisite[i]);
        }

    }

    public static void stastistichePressione() {
        String max = pressione[0][0];
        String min = pressione[0][0];


        for (String[] curr: pressione) {
            for (String el: curr) {
                if (el != null) {
                    if(el.equals("alta")) max = el;
                    else if(el.equals("normale") && !max.equals("alta")) max = el;
                    else if(el.equals("bassa") && !max.equals("alta") && !max.equals("normale"))max = el;

                    if(el.equals("bassa")) min = el;
                    else if(el.equals("normale") && !min.equals("bassa")) min = el;
                    else if(el.equals("alta") && !min.equals("bassa") && !min.equals("normale")) min = el;
                }
            }
        }


        System.out.printf("-Statistiche Pressione\nmax: %s\nmin: %s\n", max, min);

        System.out.println("Inserisci il numero di visita >= 1: ");
        int numVisita = tastiera.nextInt() - 1;

        String[] stampaCopia = new String[numPazienti];
        for(int i = 0; i < numPazienti; i++) {
            if (numVisita < pressione[i].length) {
                if (pressione[i][numVisita] != null) {
                    stampaCopia[i] = pressione[i][numVisita];
                }
            }
        }

        for (int i = 0; i < stampaCopia.length; i++) {
            if (stampaCopia[i] == null) {
                stampaCopia[i] = ""; // cambia le celle null con "" per permettere la sort
            }
        }
        //utilizzo un comparatore con una lambda expression per determinare un ordine personalizzato su cui ordinare
        Arrays.sort(stampaCopia, Comparator.comparingInt(s -> switch (s) {
            case "bassa" -> 1;
            case "normale" -> 2;
            case "alta" -> 3;
            default -> 0;
        }));

        for (int i = 0; i < stampaCopia.length; i++) {
            System.out.printf("Pressione[%d]: %s\n", i, stampaCopia[i]);
        }
    }


    public static void main(String[] args) {
        System.out.println("MioTrial avviato!");

        int scelta = -1;

        while (scelta != 100) {
            menu();
            scelta = tastiera.nextInt();
            esegui(scelta);
        }

    }

}