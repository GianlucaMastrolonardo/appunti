import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class PositionView implements PropertyChangeListener {
    public PositionView() {
        System.out.println("Ciao, il tuo elemento è al centro");
    }

    public void showPosition(String toShow){
        System.out.println(toShow);
    }

    //Hai bisogno di questo override di propertyChange, proveniente da PropertyChangeListener
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        //puoi avere più propertychangeevents
        if(evt.getPropertyName().equals("position")){
            this.showPosition("La posizione è cambiata da " +evt.getOldValue() + " a " + evt.getNewValue());
        }
    }
}
