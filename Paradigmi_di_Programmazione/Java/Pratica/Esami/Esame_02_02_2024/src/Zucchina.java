import java.util.Random;

public
class Zucchina extends Ortaggio {
    Random rand = new Random();
    final int INNAFFIA_ZUCCHINE_MAX = 10;


    protected
    Zucchina() {
        super();
        INNAFFIA_MAX = INNAFFIA_ZUCCHINE_MAX;
    }

    @Override
    public
    void innaffia() {
        innaffiaCnt++;
        if (rand.nextInt(5) == 0) {
            innaffiaCnt++;
        }
    }


}
