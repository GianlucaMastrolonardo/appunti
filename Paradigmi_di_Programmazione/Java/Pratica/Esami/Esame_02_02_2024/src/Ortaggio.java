public
abstract
class Ortaggio {

    protected int INNAFFIA_MAX;

    protected StatoOrtaggio stato;

    protected int innaffiaCnt = 0;

    Ortaggio() {
        stato = StatoOrtaggio.ACERBO;
    }


    public abstract
    void innaffia();

    public
    StatoOrtaggio raccogli() {
        int status = INNAFFIA_MAX - innaffiaCnt;
        if (status == 0) {
            return StatoOrtaggio.MATURO;
        } else if (status > 0) {
            return StatoOrtaggio.ACERBO;
        } else {
            return StatoOrtaggio.MARCIO;
        }
    }


}
