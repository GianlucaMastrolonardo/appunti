import java.util.Random;

/**
 * La classe {@code OrtoMagicoTest} mostra l'uso delle classi
 * {@code Ortaggio}, {@code Zucchina},
 * {@code Peperone}, e {@code Pomodoro}.
 *
 * @author Giancarlo Ruffo
 * @version 1.0
 * @since [2024/02/02]
 */


public
class OrtoMagicoTest {
    public static
    void main(String[] args) {
        Random rand = new Random();

        int numZucchine = rand.nextInt(20) + 5;
        int numPeperoni = rand.nextInt(20) + 5;
        int numPomodori = rand.nextInt(20) + 5;

        int numOrtaggi = numZucchine + numPeperoni + numPomodori;

        int numAcerbi = 0;
        int numMaturi = 0;
        int numMarci = 0;

        Ortaggio[] ortaggi = new Ortaggio[numOrtaggi];
        StatoOrtaggio[] raccolto = new StatoOrtaggio[numOrtaggi];

        // semina: aggiungi ortaggi alla lista
        /*
         * scrivere qui il codice che crea tutti gli ortaggi e aggiungerli alla lista
         *
         */
        for (int i = 0; i < numZucchine; i++) {
            ortaggi[i] = new Zucchina();
        }
        for (int i = numZucchine; i < numZucchine + numPeperoni; i++) {
            ortaggi[i] = new Peperone();
        }
        for (int i = numZucchine + numPeperoni; i < numZucchine + numPeperoni + numPomodori; i++) {
            ortaggi[i] = new Pomodoro();
        }

        System.out.printf("Il nostro orto ha %d ortaggi: %d zucchine, %d peperoni, %d pomodori.%n",
                ortaggi.length + 1, numZucchine, numPeperoni, numPomodori);

        // inaffiare: invoca il metodo innaffia un certo numero casuale di volte per
        // ogni ortaggio
        /*
         * scrivere qui il codice che innaffia tutti gli ortaggi
         *
         */

        for (int k = 0; k < numOrtaggi; k++) {
            for (int j = 0; j < rand.nextInt(7) + 5; j++) {
                ortaggi[k].innaffia();
            }

            System.out.println(ortaggi[k].raccogli());
            if (ortaggi[k].raccogli() == StatoOrtaggio.MATURO) {
                numMaturi++;
            } else if (ortaggi[k].raccogli() == StatoOrtaggio.MARCIO) {
                numMarci++;
            } else {
                numAcerbi++;
            }
        }

        // raccogli: invoca il metodo raccogli per tutti gli ortaggi e controlla il loro stato
        /*
         * scrivere qui il codice che raccoglie tutti gli ortaggi e per ognuno controlla
         * se lo stato è acerbo, maturo o marcio. Quindi aggiorna di conseguenza i contatori
         * dichiarati ad inizio del main: numAcerbi, numMaturi e numMarci.
         */


        System.out.printf("Abbiamo raccolto %d ortaggi acerbi, %d ortaggi maturi e %d ortaggi marci.%n",
                numAcerbi, numMaturi, numMarci);

    }
}