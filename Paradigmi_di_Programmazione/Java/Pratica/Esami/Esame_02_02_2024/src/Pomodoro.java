import java.util.Random;

public
class Pomodoro extends Ortaggio {
    Random rand = new Random();
    final int INNAFFIA_POMODORO_MAX = 6;


    protected
    Pomodoro() {
        super();
        INNAFFIA_MAX = INNAFFIA_POMODORO_MAX;
    }

    @Override
    public
    void innaffia() {
        innaffiaCnt++;
        if (rand.nextInt(10) == 0) {
            innaffiaCnt++;
        }
    }


}
