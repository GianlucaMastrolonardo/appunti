import java.util.Collection;
import java.util.Iterator;

public
class Implementazione implements Palinsesto {

    Implementazione(Programma programma, Palinsesto.Canale canale, FasciaGiornaliera fascia) {
        addProgramma(programma, canale, fascia);
    }


    @Override
    public
    void addProgramma(Programma programma, Palinsesto.Canale canale, FasciaGiornaliera fascia) throws IllegalArgumentException {
        int position;
        switch (fascia) {
            case MATTINO:
                position = 0;
                break;
            case POMERIGGIO:
                position = 1;
                break;
            case SERA:
                position = 2;
                break;
            default:
                throw new IllegalArgumentException("Fascia non esisitente");
        }
        System.out.println(canale.palinsestocanale[position]);
    }

    @Override
    public
    Collection<Programma> getByFascia(FasciaGiornaliera fascia) {
        return null;
    }

    @Override
    public
    Iterator<Programma> iterator() {
        return null;
    }
}
