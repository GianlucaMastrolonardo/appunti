import java.util.Objects;

public
class Programma {
    private String nome;

    public
    Programma(String nome) {
        this.nome = nome;
    }

    public
    String getNome() {
        return nome;
    }

    @Override
    public
    boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Programma programma = (Programma) o;
        return nome.equals(programma.nome);
    }

    @Override
    public
    int hashCode() {
        return Objects.hash(nome);
    }

    @Override
    public
    String toString() {
        return "Programma{" +
                "nome='" + nome + '\'' +
                '}';
    }


}
