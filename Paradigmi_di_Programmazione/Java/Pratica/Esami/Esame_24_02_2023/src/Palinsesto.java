import java.util.ArrayList;
import java.util.Collection;

/**
 * NOTA BENE: la classe gestisce un palinsesto estremamente semplificato. Immaginate che la programmazione
 * di un canale sia la stessa tutti i giorni della settimana, e che le fasce giornaliere siano SOLO
 * MATTINO, POMERIGGIO e SERA. Per ogni canale, avete quindi solo da gestire un programma al mattino, uno al
 * pomeriggio, ed uno alla sera.
 * <p>
 * NOTA BENE2: l'iterazione su this deve essere fatta su TUTTI I PROGRAMMI INSERITI in this.
 */
public
interface Palinsesto extends Iterable<Programma> {

    /**
     * Aggiunge un programma a this nel canale e nella fascia dati come parametri.
     *
     * @param programma il programma da aggiungere
     * @param canale    il canale sul quale aggiungere il programma
     * @param fascia    la fascia sulla quale aggiungere il programma
     * @throws IllegalArgumentException se qualche parametro non è valorizzato, se il programma è già inserito in qualche altro canale/fascia, se il canale ha già un altro programma in tale fascia.
     */

    static ArrayList<Programma> programmi = new ArrayList<>();

    public
    void addProgramma(Programma programma, Canale canale, FasciaGiornaliera fascia) throws IllegalArgumentException;

    /**
     * Restituisce una Collection di tutti i programmi in onda in una determinata fascia giornaliera.
     *
     * @param fascia la fascia giornaliera.
     * @return una Collection di tutti i programmi in onda nella fascia
     */
    public
    Collection<Programma> getByFascia(FasciaGiornaliera fascia);

    class Canale {

        public Programma[] palinsestocanale = new Programma[3];
        private String nomeCanale;

        Canale(String nomeCanale) {
            if (!nomeCanale.isEmpty()) {
                this.nomeCanale = nomeCanale;
                Programma[] palinsestocanale = new Programma[3];
            } else throw new IllegalArgumentException("Impossibile avere un canale senza nome");
        }

        public
        String getNomeCanale() {
            return this.nomeCanale;
        }
    }
}
