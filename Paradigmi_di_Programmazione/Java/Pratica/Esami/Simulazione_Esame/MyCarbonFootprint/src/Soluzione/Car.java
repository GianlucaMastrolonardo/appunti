package Soluzione;

/**
 * L'interfaccia {@code CarbonFootprint} fornisce un framework per calcolare
 * il carbon footprint di vari oggetti.
 *
 * <p> Questa interfaccia è pensata per essere implementata da diverse classi
 * che rappresentano entità in grado di avere il carbon footprint, come
 * veicoli, edifici, prodotti, ecc. Implementando questa interfaccia, una classe
 * accetta di fornire una implementazione concreta del metodo
 * {@code GetCarbonFootprint}, il quale calcolerà e restituirà il carbon footprint.
 *
 * <p> Esempi di classi che potrebbero implementare questa interfaccia includono
 * automobili, biciclette, edifici, e così via. Ciascuna di queste classi avrà
 * una propria logica specifica per calcolare il carbon footprint,
 * basandosi su fattori come il consumo di carburante, l'uso dell'energia,
 * la produzione di rifiuti, ecc.
 *
 * <p> Metodo principale:
 * <ul>
 * <li>{@code GetCarbonFootprint()}: Calcola e restituisce l'impronta di carbonio
 * dell'oggetto.
 * </ul>
 *
 * @author Pippis De' Pippis
 * @version 1.0
 * @since [2018/01/12]
 */
public
class Car implements CarbonFootprint {
    private double litriBenzina;

    public
    Car(double litriBenzina) {
        this.litriBenzina = litriBenzina;
    }

    // Con un'auto a benzina si possono produrre circa
    // 2,37 kg di CO2 per ogni litro di carburante.
    public
    void GetCarbonFootprint() {
        System.out.printf("Un auto che ha usato %.2f litri di benzina ha prodotto %.2f kg di CO2%n",
                litriBenzina, litriBenzina * 2.37);
    }
} 
