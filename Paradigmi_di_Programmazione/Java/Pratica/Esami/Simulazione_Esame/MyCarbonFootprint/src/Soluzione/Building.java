package Soluzione;

/**
 * La classe {@code Building} rappresenta un edificio e implementa
 * l'interfaccia {@code CarbonFootprint} per calcolare il suo carbon footprint.
 *
 * <p> Questa classe tiene traccia della metratura di un edificio e utilizza
 * una formula semplificata per calcolare il suo carbon footprint.
 * La formula considera diversi materiali costruttivi come il legno,
 * il seminterrato, il calcestruzzo e l'acciaio.
 *
 * <p> Utilizzo:
 * <pre>
 *    Building building = new Building(500); // 500 metri quadrati
 *    building.GetCarbonFootprint();
 * </pre>
 *
 * @author Pippis De' Pippis
 * @version 1.0
 * @since [2018/01/12]
 */
public
class Building implements CarbonFootprint {
    private int metratura;

    public
    Building(int metratura) {
        this.metratura = metratura;
    }

    // Formula semplificata: Moltiplicare la metratura dell'edificio per 50
    // per la struttura in legno, per 20 per il seminterrato,
    // per 47 per il calcestruzzo e per 17 per l'acciaio.
    public
    void GetCarbonFootprint() {
        System.out.printf("Un edificio che ha metratura %d produce %d kg di CO2%n",
                metratura, metratura * (50 + 20 + 47 + 17));
    }
}
