package Soluzione;

/**
 * La classe {@code Bicycle} rappresenta una bicicletta e implementa
 * l'interfaccia {@code CarbonFootprint}.
 *
 * <p> Poiché le biciclette non emettono direttamente CO2 durante il loro utilizzo,
 * il carbon footprint calcolato da questa classe è pari a 0. Questo serve a
 * enfatizzare il basso impatto ambientale della bicicletta come mezzo di trasporto.
 *
 * <p> Utilizzo:
 * <pre>
 *    Bicycle bicycle = new Bicycle();
 *    bicycle.GetCarbonFootprint();
 * </pre>
 *
 * @author Pippis De' Pippis
 * @version 1.0
 * @since [2018/01/12]
 */
public
class Bicycle implements CarbonFootprint {
    public
    void GetCarbonFootprint() {
        System.out.println("Bicicletta: 0");
    }
} 

