package Soluzione;

/**
 * La classe {@code CarbonFootPrintTest} mostra l'utilizzo dell'interfaccia
 * CarbonFootprint e delle sue implementazioni.
 *
 * <p> Questa classe inizializza un array di oggetti {@code CarbonFootprint},
 * che rappresentano varie entità che possono avere una carbon footprint,
 * come biciclette, edifici e auto. Calcola e visualizza quindi
 * la carbon footprint per ciascuna di queste entità.
 *
 * <p> Lo scopo di questa classe è fornire un esempio semplice di polimorfismo
 * e utilizzo delle interfacce in Java. Dimostra come diversi oggetti
 * (Bicycle, Building e Car) possano essere trattati in modo uniforme attraverso
 * un'interfaccia comune (CarbonFootprint).
 *
 * <p> Nota: Questa classe è a scopo dimostrativo e presuppone l'esistenza di
 * un'interfaccia {@code CarbonFootprint} e delle sue implementazioni
 * ({@code Bicycle}, {@code Building} e {@code Car}).
 *
 * @author Pippis De' Pippis
 * @version 1.0
 * @since [2018/01/12]
 */

public
class CarbonFootPrintTest {
    public static
    void main(String[] args) {
        CarbonFootprint[] list = new CarbonFootprint[3];

        // aggiungi elementi alla lista
        list[0] = new Bicycle();
        list[1] = new Building(2500);
        list[2] = new Car(10);

        // mostra il carbon footprint per ogni oggetto
        for (int i = 0; i < list.length; i++) {
            list[i].GetCarbonFootprint();
        }
    }
}

