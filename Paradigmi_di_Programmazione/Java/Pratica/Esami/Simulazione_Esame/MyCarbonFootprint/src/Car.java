public
class Car implements CarbonFootprint {
//2,37 kg di CO2 per ogni litro di carburante

    double carFootPrint;

    Car(int litri) {
        if (litri <= 0) throw new IllegalArgumentException("Inserire Litri");
        this.carFootPrint = litri * 2.37;
    }

    @Override
    public
    void GetCarbonFootprint() {
        System.out.println(carFootPrint);
    }
}
