public
class Building implements CarbonFootprint {

    private int squareMeters;
    private String material;
    private boolean haveSemiInterrato;

    private int buildingFootPrint;

    Building(int squareMeters, String material, boolean haveSemiInterrato) {
        int buildingFootPrint;
        if (squareMeters > 0) {
            this.squareMeters = squareMeters;
            switch (material.toLowerCase()) {
                case "legno":
                    buildingFootPrint = 10;
                    break;
                case "calcestruzzo":
                    buildingFootPrint = 20;
                    break;
                case "acciaio":
                    buildingFootPrint = 40;
                    break;
                default:
                    throw new IllegalArgumentException("È stato inserito un materiale errato");
            }
            this.material = material;
            if (haveSemiInterrato) {
                buildingFootPrint += 5 * squareMeters;
            }
            this.haveSemiInterrato = haveSemiInterrato;
            buildingFootPrint *= squareMeters;
            this.buildingFootPrint = buildingFootPrint;
        } else {
            throw new IllegalArgumentException("Impossibile avere dei metri quadri <= di 0");
        }
    }

    @Override
    public
    void GetCarbonFootprint() {
        System.out.println("Costruire un abitazione di " + squareMeters + " metri quadri in " + material + " inquina " + buildingFootPrint + "KG di CO2\nSeminterrato: " + haveSemiInterrato);
    }
}
