/*
 * PRIMA PARTE: direttive package e import.
 *    package: indica il package (ossia la 'libreria') dove e' contenuta la nostra classe.
 *    import: serve ad importare altre classi/package. E' simile alla #include di C:
 *    - sintassi: import [NOME_COMPLETO_PACKAGE] oppure import [PERCORSO].*
 *    - non include direttamente il bytecode di altre classi, ma indica alla JVM quali usare.
 *    - non crea file di dimensioni maggiori.
 *    
 *    Sia le direttive package che quelle import possono non essere presenti:
 *    - se package manca, la/le classe/i vengono associate ad un package anonimo associato a questa directory
 *    - se manca import, semplicemente non state utilizzando nessuna libreria
 */
//package lezione1;

import java.util.Scanner;

//**************************************************************************************

/*
 * SECONDA PARTE: definizione della classe. Ogni file .java contiene, al suo interno, la definizione di una classe
 * (o di altri "elementi", che vedremo in seguito) con lo STESSO NOME del file (senza il suffisso .java).
 */
public class EsempioClasse {

	static int annoCorrente = 2023; //quelle che in C erano variabili globali, in Java richiedono il modificatore STATIC (lo vedrete nella parte teorica).
	static Scanner tastiera = new Scanner(System.in); //questo, per ora, ignoratelo. Aggiungete questa riga in ogni classe nella quale vi serve leggere da tastiera.
	
	/*
	 * CONVENZIONE SCRITTURA CODICE: in Java esistono convenzioni di scrittura codice per renderlo piu' leggibile.
	 * Ne trovate una descrizione qui (https://google.github.io/styleguide/javaguide.html).
	 * Non e' necessario che le usiate tutte, ma almeno:
	 * - le variabili devono avere NOMI SIGNIFICATIVI, in maniera che si possa intuire cosa contengono.
	 * - i nomi devono essere scritti in camelCase:
	 *    metodi, variabili: se singola parola, tutta minuscola (es. autore); se piu' parole, dalla seconda in poi iniziano con maiuscola (es. numeroPagine)
	 *    classi: come metodi e variabili, ma con anche la prima lettera maiuscola (es. Studente, MiaClasse) 
	 *    costanti: tutte maiuscole (es. TIPO), se piu' parole intervallate da _ (es. ANNO_ACCADEMICO)
	 */
	
	/*
	 * FUNZIONI: di seguito, un esempio di funzione (come quelle di C). In Java prendono il nome di METODI STATICI ed e' necessario il modificatore STATIC.
	 * La sintassi (tipo di ritorno, parametri, ...) e' molto simile a quella di C, che gia' conoscete.
	 */
	public static int calcolaEta(int annoNascita) { 
		int eta = annoCorrente - annoNascita;
		return eta;
	}
	
	public static int[] generaArray(int dimensione) {
		/*
		 * ARRAY (li vedremo meglio in seguito, questa e' solo una base): il loro utilizzo e' molto simile a quello di C,
		 * con alcune differenze sostanziali. 
		 */
		
		//per dichiarare un array, si usa la sintassi tipo[] nomeArray (nota: in Java non si vedono - ma ci sono - i puntatori)
		int[] mioArray;
		
		//per inizializzarlo, si usa la parola chiave new
		mioArray = new int[dimensione];
		
		for(int i =0;i<mioArray.length;i++) { //nomeArray.length indica la LUNGHEZZA dell'array (e' una costante).
			mioArray[i] = i; //come in C, gli array godono dell'ACCESSO CASUALE
		}
		
		return mioArray;
	}
	
	/*
	 * METODO MAIN: ha la stessa funzionalita' del main di C. E' OPZIONALE.
	 */
	public static void main(String[] args) {
		
		//La stampa a terminale si fa con il metodo System.out.println().
		System.out.println("Come ti chiami?");
		String nome = tastiera.nextLine();
		System.out.println("Ciao, "+nome+", in che anno sei nato/a?");
		int anno = tastiera.nextInt();
		int eta = calcolaEta(anno);
		/*
		 * Java mette a disposizione buona parte delle strutture di controllo di C (if-else, while, switch-case).
		 * La sintassi e' in molti casi identica, o comunque molto simile. Le vedrete tutte, nel dettaglio, in
		 * seguito.
		 */
		if(eta > 100) {
			System.out.println("Complimenti!");
		}
		else System.out.println("Allora quest'anno hai "+calcolaEta(anno)+" anni");
	}

}
