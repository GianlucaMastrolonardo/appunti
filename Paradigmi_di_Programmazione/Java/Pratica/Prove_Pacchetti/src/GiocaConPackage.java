//per il caso 3 avete 2 scelte di import: singolo metodo o tutto (*). Nel caso di tutto, devo poi usare il nome della classe nel richiamare il metodo (es. MioMath.sum)
import static upo.packA.MioMath.sum;
//import upo.packA.MioMath.*;

public class GiocaConPackage {
	public static void main(String[] args) {
		System.out.println("*** Inizio esecuzione ***"); //caso 2: metodo/classe in package java.lang
		MioStampa.stampaErrore("Questo e' un errore"); //caso 1: stesso package (in questo caso e' il default)
		
		int x = 3;
		int y = 4;
		int somma = sum(3,4); //caso 3: preceduto da direttiva di import
		//int somma = upo.packA.MioMath.sum(3,4); //caso 4: nome completo
		System.out.println("La somma di "+x+" e "+y+" e' "+somma);
		
		System.out.println("*** Fine esecuzione ***");
	}
}
