public class Kata1 {
    public static int[] squareOrSquareRoot(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (Math.sqrt(array[i]) % 1 == 0) {
                array[i] = (int) Math.sqrt(array[i]);
            } else {
                array[i] *= array[i];
            }
        }
        return array;
    }

    public static void main(String[] args) {
        System.out.println("Kata Excercise 1:");
        int[] myArray = new int[] { 4, 3, 9, 7, 2, 1 };

        int[] resultArray = squareOrSquareRoot(myArray);

        for (int i = 0; i < resultArray.length; i++) {
            System.out.println(resultArray[i]);
        }
    }
}
