"""
Write a recursive program that calculates the N-th Fibonacci
number:
- FIBN+1 = FIBN + FIBN-1 for N>0
- FIB1 = 1
- FIB0 = 0
"""


def fib(n):
    if n == 0:
        return 0
    if n == 1:
        return 1

    return fib(n - 1) + fib(n - 2)


n = 14
print("Fibonacci di " + str(n) + ":")
print(fib(n))
