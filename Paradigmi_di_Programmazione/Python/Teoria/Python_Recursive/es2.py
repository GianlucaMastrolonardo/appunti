"""
Write a recursive program that, given a word, finds all possible
anagrams of that word. Mathematically, this means finding all
the N! permutations of the characters included in the word in
input.
- E.g., “dog” -> dog, dgo, god, gdo, odg, ogd
"""


def make_anagrams(word, partial, anagrams):
    if len(word) == 0:
        anagrams.append(partial)
        return

    for i, char in enumerate(word):
        partial += char
        new_word = word[0:i] + word[i + 1 :]
        make_anagrams(new_word, partial, anagrams)
        partial = partial[0 : len(partial) - 1]
        # in questo modo rimetto i valori che ho giù usato


word = "paolo"
partial = ""
anagrams = []


make_anagrams(word, partial, anagrams)
print(set(anagrams))
