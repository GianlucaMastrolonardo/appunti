'''
Write a program that:
- gets a string from the user;
- counts the frequency of all the words in the string.
'''

my_string = input("Inserisci una frase: ")
my_sequence = my_string.split()

my_dictionary = {}

for string in my_sequence:
    if string in my_dictionary:
        my_dictionary[string] += 1
    else:
        my_dictionary[string] = 1 


print(my_dictionary)