'''
• Write a program that:
- gets two string from the user
- creates and prints two string in wich the first characters are swapped
- if one or both the strings are empty, the program prints an error
message
• e.g., if the inputs are foo and bar, the output is boo and far
'''

string1Input = input("Inserire stringa 1: ")
string2Input = input("Inserire stringa 2: ")

if not (string1Input and string2Input):
    print("Stringa Vuota")
else:
    print("Stringa1: "+ string1Input)
    print("Stringa2: "+ string2Input)

    string1Output = string2Input[0] + string1Input[1:]
    string2Output = string1Input[0] + string2Input[1:]

    print("Output Stringa1: "+string1Output)
    print("Output Stringa1: "+string2Output)