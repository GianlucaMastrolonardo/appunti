# imperative programming
result = []
for n in range(21):
  if n % 2 == 0:
    result.append('even')
  else:
    result.append('odd')

print(result)
print()

# using list comprehension
result = ['even' if n%2 == 0 else 'odd' for n in range(21)]
print(result)
