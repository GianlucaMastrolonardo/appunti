# Using a for-loop
def words_to_length1(words):
  wordsLength = []
  for word in words:
    wordsLength.append(len(word))
  return wordsLength

# Using the higher order function map()
def words_to_length2(words):
  return map(len, words)

#  Using list comprehensions
def words_to_length3(words):
  return [len(word) for word in words]

print(words_to_length1(['This', 'is', 'a', 'program']))
print(list(words_to_length2(['to', 'create', 'a', 'list', 'of'])))
print(words_to_length3(['integers', 'from', 'a', 'list', 'of', 'words']))
