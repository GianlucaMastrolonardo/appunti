def filter_long_words(words, n):
  return filter(lambda x: len(x) > n, words)

my_list = ['that', ' was', 'not', 'funny', 'at', 'all']
print(list(filter_long_words(my_list, 3)))
