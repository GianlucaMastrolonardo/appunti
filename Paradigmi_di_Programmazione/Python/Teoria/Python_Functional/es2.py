'''
Write an infinite generator for the Fibonacci series, where
– Fib[0] = 0
– Fib[1] = 1
– Fib[n+2] = Fib[n] + Fib[n+1]
'''

def fib():
    a = 0
    b = 1
    c = 1
    while True:
        yield c
        c = a+b
        a = b
        b = c



Fib = fib()

for i in list(range(1,15)):
    print("Fibonacci di "+str(i)+" è "+str(next(Fib)))