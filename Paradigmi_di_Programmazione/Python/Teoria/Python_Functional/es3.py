'''
Given a list of words, write a program that creates and prints a
list of integers representing the lengths of the corresponding
words. Try to solve the exercise in different ways, by defining
three equivalent functions to create the list of integers:
– using a for-loop; method1
– using the higher order function map( ); method2
– using list comprehensions. method3
'''

def method1(string):
    string = string.split()
    length = []
    for word in string:
        length.append(len(word))
    return length

def how_long(word):
    return len(word)

def method2(string):
    string = string.split()
    length = list(map(how_long, string))
    return length


def method3(string):
    string = string.split()
    length = [len(word) for word in string]
    return length

my_string = "ciao a tutti"

print(my_string)

print(method1(my_string))
print(method2(my_string))
print(method3(my_string))