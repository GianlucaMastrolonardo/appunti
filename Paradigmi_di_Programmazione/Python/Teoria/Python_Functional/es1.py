'''
Given a sequence of numbers from 1 to 20, write a program that
produces a list containing the word ‘even’ if the corresponding
number is even, ‘odd’ otherwise. Result would look like
– [‘odd’, ‘odd’, ‘even’, …]
'''

my_list = list(range(1,21))

print(my_list)

number_type = ["even" if (num % 2 == 0) else "odd" for num in my_list ]

print(number_type)