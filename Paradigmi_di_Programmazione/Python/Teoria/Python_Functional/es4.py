'''
Using the higher order function filter() and lambda expressions,
define a function filter_long_words() that takes a list of words
and an integer n and returns the list of words that are longer
than n
'''

def filter_long_words(string, n):
    string = string.split()
    return list(filter(lambda p: len(p) > n,string))


my_string = "ciao bellissimi come state"
num = 4

print(filter_long_words(my_string, num))