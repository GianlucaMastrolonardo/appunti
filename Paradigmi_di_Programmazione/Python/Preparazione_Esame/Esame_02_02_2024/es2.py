def serie_armonica(n):
    cnt = 1
    old_num = 0
    while cnt < n:
        num = (1 / cnt) + old_num
        cnt += 1
        yield num
        old_num = num


MAX = 10
it = serie_armonica(MAX)

for val in range(1, MAX):
    print(next(it))
