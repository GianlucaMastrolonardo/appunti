my_list = [1, -100, -12, 10, 2, -3, -4, 239, 42, 565]

output = [val for val in my_list if val > 0 and val % 2 == 0]

print(output)
