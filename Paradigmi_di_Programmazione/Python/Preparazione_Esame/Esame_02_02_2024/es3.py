def rimuovi_divisibili(l, x):
    if not l:
        return []

    if l[0] % x != 0:
        return [l[0]] + rimuovi_divisibili(l[1:], x)
    return rimuovi_divisibili(l[1:], x)


my_list = list(range(1, 11))
x = 2

print(rimuovi_divisibili([1, 2, 3, 4, 5], 2))
