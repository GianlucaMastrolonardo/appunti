"""
EXERCISE 1 – TODO LIST
Given a list of tasks (i.e., actions that the user wants to do in the future) implement a todo_manager program to perform
4 actions:
1. insert a new task (a string of text);
2. remove a task (by typing its content, exactly);
3. show all existing tasks;
4. close the program.
At startup, the program shows a menu with the 4 options and, for each choice, performs the requested action. After the
action (except action 4), the program returns to the prompt for actions


EXERCISE 2 – TODO LIST SAVED TO FILE
Extend the program developed in the previous exercise to save and retrieve the list of tasks to/from a text file.
Consequently, at startup, the program takes the list of tasks from the file and saves the changes to the file as soon as the
user decides to close the program.
For the exercise, you can create a task_list.txt file (saved in the same folder of the Python file) by saving each task as a
separate line.


EXERCISE 3 – TASKS DELETION EXTENSION
Modify the program developed in the previous exercise to remove all the tasks that contains a specified substring. For
example, when user types “shopping”, the program will use the provided string to delete all tasks that contain the
substring “shopping”.
"""
FILENAME = "task_list.txt"


def menu():
    print(
        "1. insert a new task (a string of text);"
        "\n2. remove a task (by typing its content, exactly);"
        "\n3. remove a task (by typing substring content);"
        "\n4. show all existing tasks;"
        "\n5. close the program.")


def insert_task(tasks):
    task = input("Insert task: ")
    tasks.append(task)


def remove_task(tasks):
    task = input("What task do you want delete: ")
    try:
        tasks.pop(tasks.index(task))
    except ValueError:
        print("Is " + task + " a task?" + "\nCan't delete an unknown task...")


def remove_generic_task(tasks):
    sub_task = input("What task do you want delete: ")

    new_tasks = tasks[:]

    for task in new_tasks:
        if sub_task in str(task.split()):
            tasks.remove(task)


def show_tasks(tasks):
    for task in tasks:
        print("\t>>> " + task)


def load_file(filename, tasks):
    file = open(filename)

    lines = file.readlines()

    for line in lines:
        tasks.append(line.strip())
    file.close()


def save_file(filename, tasks):
    file = open(filename, "w")
    for task in tasks:
        file.write(str(task) + "\n")
    file.close()


def main():
    tasks = []
    load_file(FILENAME, tasks)

    menu()

    while True:
        choose = int(input("Input: "))
        if choose == 1:
            insert_task(tasks)
        elif choose == 2:
            remove_task(tasks)
        elif choose == 3:
            remove_generic_task(tasks)
        elif choose == 4:
            show_tasks(tasks)
        elif choose == 5:
            break
        save_file(FILENAME, tasks)


if __name__ == "__main__":
    main()
