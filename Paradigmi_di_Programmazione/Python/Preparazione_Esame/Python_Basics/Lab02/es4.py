"""
EXERCISE 4 - FIND URGENT TASKS - DICTIONARIES
Given a list of tasks modeled as dictionaries, like this one (i.e., tasks):
task1 = {"todo": "call John for project organization", "urgent": True}
task2 = {"todo": "buy a new mouse", "urgent": True}
task3 = {"todo": "find a present for Angelina"s birthday", "urgent": False}
task4 = {"todo": "organize mega party (last week of April)", "urgent": False}
task5 = {"todo": "book summer holidays", "urgent": False}
task6 = {"todo": "whatsapp Mary for a coffee", "urgent": False}
tasks = [task1, task2, task3, task4, task5, task6]
write a program that returns a new list that contains only the urgent tasks, i.e., take the dictionaries that have a True value
in the urgent field.
"""


def only_urgent_tasks(tasks):
    urgent_task = []
    for task in tasks:
        if task["urgent"]:
            urgent_task.append(task)

    return urgent_task


task1 = {"todo": "call John for project organization", "urgent": True}
task2 = {"todo": "buy a new mouse", "urgent": True}
task3 = {"todo": "find a present for Angelina's birthday", "urgent": False}
task4 = {"todo": "organize mega party (last week of April)", "urgent": False}
task5 = {"todo": "book summer holidays", "urgent": False}
task6 = {"todo": "whatsapp Mary for a coffee", "urgent": False}

tasks = [task1, task2, task3, task4, task5, task6]

print(only_urgent_tasks(tasks))
