"""
Exercise!
11/10/22 Python basics 141
• Write a program that:
– gets a string from the user;
– counts the frequency of all the words in the string.
"""

input_string = input("Insert a string: ")
input_string = input_string.lower()

chars_cnt = {}
for char in input_string:
    chars_cnt[char] = input_string.count(char)

print(chars_cnt)
