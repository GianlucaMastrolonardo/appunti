"""
EXERCISE 1 – SIMPLE MATH
Write a program that asks you for two numbers (interactively), sums the numbers, and prints the result on screen
"""
while True:
    try:
        num1 = int(input("Inserisci un valore: "))
        num2 = int(input("Inserisci un altro valore: "))
        break
    except ValueError:
        print("Non hai inserito un valore corretto, riprova")

print("La somma vale: " + str(num1 + num2))
