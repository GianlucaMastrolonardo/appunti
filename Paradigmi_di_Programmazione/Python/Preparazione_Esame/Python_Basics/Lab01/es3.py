"""
EXERCISE 3 – ANOTHER STRING EXPERIMENT
Given a string, create and print a string without odd numbered characters
e.g., ‘winter’ yields ‘wne’
If the string is empty, print an error message
"""


def my_fun(phrase):
    if phrase:
        return phrase[::2]
    else:
        print("Errore, hai inserito una stringa vuota")


my_string = input("Inserire una stringa: ")
print(my_fun(my_string))
