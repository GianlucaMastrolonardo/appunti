"""
EXERCISE 2 – STRING EXPERIMENT
Given a string, create and print a string made of the first two and the last two chars of the original string.
e.g., ‘spring’ yields ‘spng’
If the string is shorter than two characters, return the empty string
"""


def my_fun(phrase):
    if len(phrase) < 2:
        return ""
    else:
        return phrase[:2] + phrase[len(phrase) - 2:]


my_string = input("Inserire una stringa: ")
print(my_fun(my_string))
