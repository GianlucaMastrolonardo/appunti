"""
EXERCISE 4 – CONVERT PENNIES
Write a program to convert pennies into dollars and cents. The program asks you for a number (i.e., how many pennies to
convert), and prints the corresponding dollars and cents.
e.g., 1729 pennies yield 17 dollars and 29 cents
"""

pennies = input("Inserire pennies: ")

cents = pennies[len(pennies) - 2:]
dollars = 0
if len(pennies) > 2:
    dollars = pennies[:len(pennies) - 2]

print(pennies + " pennies yield " + dollars + " dollars and " + cents + " cents")
