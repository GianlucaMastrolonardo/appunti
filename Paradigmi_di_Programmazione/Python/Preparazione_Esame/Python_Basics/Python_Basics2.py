"""
Exercise!
11/10/22 Python basics 139
• Given a string and a tuple of numbers of the same length, write a
program that creates and prints a string in which each character
is incremented by the corresponding number in the tuple
• e.g.: if the string is ‘abcde’ and the tuple is (1,1,2,-1,3), the output
is ‘bcech’
"""


def my_function(input_string, input_tuple):
    output = []

    for el in list(zip(input_string, input_tuple)):
        output.append(chr(ord(el[0]) + el[1]))

    return "".join(output)


my_string = "abcde"
my_tuple = (1, 1, 2, -1, 3)

res = my_function(my_string, my_tuple)

print(res)
