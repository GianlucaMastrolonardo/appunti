"""
Exercise!
11/10/22 Python basics 86
• Write a program that:
– gets two string from the user
– creates and prints two string in wich the first characters are swapped
– if one or both the strings are empty, the program prints an error
message
• e.g., if the inputs are foo and bar,
"""

first_string = input("Inserire una stringa: ")
second_string = input("Inserire una seconda stringa: ")

out1 = first_string[:1] + second_string[1:]
out2 = second_string[:1] + first_string[1:]

print("\tFirst String: " + first_string+"\n\tSecond String: " + second_string + "\n\tOutput 1: " + out1 + "\n\tOutput 2: " + out2)
