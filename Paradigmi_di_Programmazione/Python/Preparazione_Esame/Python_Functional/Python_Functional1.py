"""
Exercise!
11/10/22 Python functional 30
• Given a sequence of numbers from 1 to 20, write a program that
produces a list containing the word ‘even’ if the corresponding
number is even, ‘odd’ otherwise. Result would look like
– [‘odd’, ‘odd’, ‘even’, …]
• Try first with the imperative programming paradigm, then
translate the program in a more functional style by using list
comprehension
"""
numbers = list(range(1, 21))
print(numbers)

even_or_odd_imperative = []

for number in numbers:
    if number % 2 == 0:
        even_or_odd_imperative.append("even")
    else:
        even_or_odd_imperative.append("odd")

print(even_or_odd_imperative)

even_or_odd_functional = ["even" if number % 2 == 0 else "odd" for number in numbers if True]
print(even_or_odd_functional)
