"""
EXERCISE 2
Write a program that find all the numbers from 1-100 that are divisible by any single digit (from 2 to 9) besides 1. As an
example:
• 3 satisfies the condition, since it is divisible by 3;
• 11 does not satisfy the condition, because it is not divisible by any number from 2 to 9.
Try first with the imperative programming paradigm, then translate the program in a more functional style by using list
comprehension.
"""

numbers_list = range(1, 101)
divisor_list = range(2, 10)

divisible = set([num for num in numbers_list for divisor in divisor_list if num % divisor == 0])

print(divisible)
