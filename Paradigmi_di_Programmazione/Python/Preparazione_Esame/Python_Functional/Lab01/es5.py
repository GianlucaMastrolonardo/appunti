"""
EXERCISE 5
Write a program that exploits a custom “read_lines” generator to display a list of files line by line. To this end, create some
.txt files in the same folder of the program, and store their filenames inside a list, e.g., my_files = [‘file1.txt’, ‘file2.txt’,
‘file3.txt’]. Thanks to the read_lines generator, the program should start by displaying the first line of file1.txt. Then, the
program should wait for user input:
• if the user press Enter, the read_lines generator should return the next line of the current file;
• if the user press n + Enter, the read_lines generator should forget the rest of the current file, and it should return
the first slide of the next file.
TIPS: you can use the send() method to tell the generator that it’s time to change file.
"""


def read_file(list_files):
    for file in list_files:
        fr = open(file)
        for line in fr.readlines():
            val = yield line.strip()
            if val == 'n':
                break


if __name__ == '__main__':
    my_files = ["file1.txt", "file2.txt", "file3.txt"]
    it = read_file(my_files)
    print(next(it))
    user_input = input("Press 0 to exit\nPress n for go to the next file, press enter for go to the next line: ")
    while user_input != '0':
        try:
            if user_input == '':
                print(next(it))
            if user_input == 'n':
                print(it.send('n'))
            user_input = input(
                "Press 0 to exit\nPress n for go to the next file, press enter for go to the next line: ")
        except StopIteration:
            print("Non c'è più nessuna linea leggibile nel file")
            user_input = '0'
