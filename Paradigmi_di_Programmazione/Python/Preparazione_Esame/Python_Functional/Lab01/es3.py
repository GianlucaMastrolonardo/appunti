"""
EXERCISE 3
Given 2 list of strings, write a program that creates a list of tuples consisting of all the matches between the two lists in
input. In particular, given a string a in the first list, and a string b in the second list, there is a match if:
• a and b are equals, OR
• a is included in b, OR
• b is included in a.
As an example, if the two lists are the following
• list1 = ["pippo", "pluto", "paperino"]
• list2 = ["pip", "paperino&paperina", "topolino", "minnie", "pippo"]
The output should be: [("pippo", "pip"), ("pippo", "pippo"), ("paperino", "paperino&paperina")]
"""

list1 = ["pippo", "pluto", "paperino"]
list2 = ["pip", "paperino&paperina", "topolino", "minnie", "pippo"]

output = [(el1, el2) for el1 in list1 for el2 in list2 if (el1 is el2) or (el1 in el2) or (el2 in el1)]

print(output)
