"""
EXERCISE 1
Write a program that counts the number of spaces inside a string. Try first with the imperative programming paradigm,
then translate the program in a more functional style by using list comprehension.
"""

my_string = input("Inserisci una stringa: ")

cnt_list = [char for char in my_string if char == " "]

print(len(cnt_list))
