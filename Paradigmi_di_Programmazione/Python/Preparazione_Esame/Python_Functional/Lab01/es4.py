"""
EXERCISE 4
Write a generator which computes the running average of a sequence of arbitrary numbers: as long as a new element is
passed to the generator (through the send() method), the output is updated to reflect the average of all the numbers
considered up to that point.
As an example, with the following list of numbers [7, 13, 17, 231, 12, 8, 3], the output of multiple next operations on the
generator should be:

sent: 7, new avg: 7.00
sent: 13, new avg: 10.00
sent: 17, new avg: 12.33
sent: 231, new avg: 67
"""


def my_gen():
    num = []
    while True:
        try:
            avg = sum(num) / len(num)
        except ZeroDivisionError:
            avg = 0
        val = (yield avg)
        if val is not None:
            num.append(val)


it = my_gen()
next(it)

for num in [7, 13, 17, 231, 12, 8, 3]:
    it.send(num)
    print("sent: " + str(num) + ", new avg: " + str(next(it)))
