"""
Exercise!
11/10/22 Python functional 38
• Write an infinite generator for the Fibonacci series, where
– Fib[0] = 0
– Fib[1] = 1
– Fib[n+2] = Fib[n] + Fib[n+1]
- Fib[n] = Fib[n-1] + Fib[n-2]
    c =  a + b
"""


def fib():
    a = 0
    b = 0
    c = a + b
    yield c
    b = 1
    c = a + b
    yield c
    while True:
        a = b
        b = c
        c = a + b
        yield c


it = fib()

for i in range(1, 15):
    print("Fibonacci di " + str(i) + ": " + str(next(it)))
