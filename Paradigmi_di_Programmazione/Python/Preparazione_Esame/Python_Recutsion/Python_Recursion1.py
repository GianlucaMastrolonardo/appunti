def fib_calculator(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    return fib_calculator(n - 1) + fib_calculator(n - 2)


print(fib_calculator(7))
