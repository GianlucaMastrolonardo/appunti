#Given a string, create and print a string made of the first two and the last two chars of the original string.
#e.g., ‘spring’ yields ‘spng’
#If the string is shorter than two characters, return the empty string.

string = input("Inserisci la stringa: ")

if len(string) > 2:
    output_string = string[0:2] + string[len(string)-2:]

else:
    output_string = ''

print(output_string)