#Given a string, create and print a string without odd numbered characters
#e.g., ‘winter’ yields ‘wne’
#If the string is empty, print an error message.

string = input("Inserire stringa: ")

if string:
    output_string = string[::2]
    print(output_string)

else:
    print("ERRORE")