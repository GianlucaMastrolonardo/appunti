'''
Given a list of tuple composed of a number and a string, e.g.,:
• cities = [(19542209, "New York") ,(4887871, "Alabama"), (1420491, "Hawaii"), (626299, "Vermont"), (1805832,
"West Virginia"), (39865590, "California")]
write a program to sort the tuples in the list based on the last character of the second items in reverse order.
To this end, use the sorted function, its reverse parameter, and lambda expressions.
'''

#Era sbagliato perchè prendevo il primo carattere e non l'ultimo carattere 

def sort_tuple(tuples):
    return  sorted(tuples, key = lambda t: t[1][-1], reverse = True)

cities = [(19542209, "New York") ,(4887871, "Alabama"), (1420491, "Hawaii"), (626299, "Vermont"), (1805832, "West Virginia"), (39865590, "California")]

print(sort_tuple(cities))
