'''
Using the map function and lambda expressions, define a mapPreConcat function that takes a “pre”
string and a list of strings and returns the result of concatenating
“pre” in front of every string in the list.
As an example, the following call to the function:
• mapPreConcat('com', ['puter','pile','mute'])
should output:
• ['computer','compile','commute']
'''

def mapPreConcat(pre_word, words):
    return list(map(lambda w: pre_word+w, words))


my_words = ["linuz", "fornia", "mero"]
pre_word = "cali"

print(mapPreConcat(pre_word, my_words))