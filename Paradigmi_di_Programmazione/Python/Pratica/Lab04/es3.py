'''
Consider a list of tasks modeled as dictionaries, like this one (i.e., tasks):
task1 = {"todo": "call John for project organization", "deadline": (27,5,2022), "urgent": True}
task2 = {"todo": "buy a new mouse", "deadline": (27,12,2021), "urgent": True}
task3 = {"todo": "find a present for Angelina"s birthday", "deadline": (3,2,2022), "urgent": False}
task4 = {"todo": "organize mega party (last week of April)", "deadline": (27,11,2021), "urgent": False}
task5 = {"todo": "book summer holidays", "deadline": (13,6,2022), "urgent": False}
task6 = {"todo": "whatsapp Mary for a coffee", "deadline": (30,10,2021), "urgent": False}
tasks = [task1, task2, task3, task4, task5, task6]
Each task, in particular, contains:
• a "todo" value, i.e., a string to describe the task;
• a "deadline" value, i.e., a tuple containing three numbers representing day, month, and year, of the task deadline;
• a "urgent" value, i.e., a boolean that specify whether the task is urgent or not.

Write an interactive program that allow users to:
1. filter the urgent tasks;
2. filter the tasks by year;
3. sort the tasks by deadline in ascending order;
Write the program by exploiting the functional programming style, e.g., by taking advantage of list comprehension, built-in
functions like filter and sorted, and lambda expressions.
'''

def menu(tasks):
    print("1. filter the urgent tasks\n2. filter the tasks by year\n3. sort the tasks by deadline in ascending order\n100. Exit")
    while True:
        choose = int(input("Inserire valore: "))
        if choose == 1:
            print(filter_urgent_task(tasks))
        elif choose == 2:
            year = int(input("Inserire l'anno: "))
            print(filter_year(tasks, year))
        elif choose == 3:
            print(sort_ascending(tasks))
        else:
            break


def filter_urgent_task(tasks):
    return list(filter(lambda t: t["urgent"] is True ,tasks))

def filter_year(tasks, year):
    return list(filter(lambda t: t["deadline"][2] == year, tasks))

def sorting(tupla):
    return  tupla


def sort_ascending(tasks):
    return list(sorted(tasks, key = lambda t: t["deadline"][::-1]))


task1 = {"todo": "call John for project organization", "deadline": (27,5,2022), "urgent": True}
task2 = {"todo": "buy a new mouse", "deadline": (27,12,2021), "urgent": True}
task3 = {"todo": "find a present for Angelina's birthday", "deadline": (3,2,2022), "urgent": False}
task4 = {"todo": "organize mega party (last week of April)", "deadline": (27,11,2021), "urgent": False}
task5 = {"todo": "book summer holidays", "deadline": (13,6,2022), "urgent": False}
task6 = {"todo": "whatsapp Mary for a coffee", "deadline": (30,10,2021), "urgent": False}
tasks = [task1, task2, task3, task4, task5, task6]

menu(tasks)