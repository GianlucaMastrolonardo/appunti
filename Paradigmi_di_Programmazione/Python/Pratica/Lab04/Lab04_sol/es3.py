# prepare the dictionary
task1 = {'todo': 'call John for project organization','deadline':(27,5,2022),'urgent': True}
task2 = {'todo': 'buy a new mouse','deadline':(27,12,2021), 'urgent': True}
task3 = {'todo': 'find a present for Angelina\'s birthday', 'deadline':(3,2,2022), 'urgent': False}
task4 = {'todo': 'organize mega party (last week of April)', 'deadline':(27,11,2021),'urgent': False}
task5 = {'todo': 'book summer holidays','deadline':(13,6,2022), 'urgent': False}
task6 = {'todo': 'whatsapp Mary for a coffee','deadline':(30,10,2021), 'urgent': False}
tasks = [task1, task2, task3, task4, task5, task6]


while True:
  print("Insert the number corresponding to the action you want to perform")
  print("1: filter by urgency")
  print("2: filter by year")
  print("3: order by deadline")
  print("4: exit")
  choice = input("Your choice:\n>")

  # check the input
  while choice.isdigit() != True or int(choice) < 0 or int(choice) > 4:
    # if the string is not a number we will ask a new input
    choice = input("Wrong input! Your choice:\n>")

  # convert the string to int (integer number)
  choice = int(choice)

  # depending on the chosen input we perform the right action
  if (choice == 1):
    print(list(filter(lambda x: x['urgent'] == True, tasks)),"\n")

  elif (choice == 2):
    param = input("Type the year:\n>")
    while param.isdigit() != True:
      param = input("Wrong param, retry:\n>")
    param = int(param)
    print(list(filter(lambda x: x['deadline'][3] == param,tasks)), "\n")

  elif (choice == 3):
    print(sorted(tasks, key = lambda x: x['deadline'][::-1]))

  elif (choice == 4):  # exit
    break

print("\nGoodbye!")
