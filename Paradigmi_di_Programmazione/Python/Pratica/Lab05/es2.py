"""
Given an arbitrary list of integers, write a program to find if an integer is present or not in the list. To this end, implement a
recursive version of the binary search. Binary search can be applied to sorted arrays, only. The approach is the following:
• Divide the array in two halves
• Compare the middle element with the target element
• Reapply the problem over one of the two halves (left or right, depending on the comparison result)
• The other half may be ignored, since the vector is ordered
You can find more information about binary search at https://en.wikipedia.org/wiki/Binary_search_algorithm
"""


def binary_search(ordered_list, val):
    half = int(len(ordered_list) / 2)

    if ordered_list[half] == val:
        return True

    if len(ordered_list) == 1:
        return False

    if ordered_list[half] > val:
        ordered_list = ordered_list[:half]
    else:
        ordered_list = ordered_list[half:]

    return binary_search(ordered_list, val)


my_list = [1, 2, 4, 5, 6, 7, 8, 10, 11, 14, 16, 19]
print(binary_search(my_list, 8))

my_list = list(range(0, 10000))
print(binary_search(my_list, 8))
