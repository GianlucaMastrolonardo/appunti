"""
Write a recursive program that calculates the factorial of non-negative integer n:
• 0! = 1 for n = 0
• n! = n * (n-1)! for n > 0
"""


def fact(n):
    if n == 0:
        return 1

    return n * fact(n - 1)


n = int(input("Inserire il valore di n: "))
print("Fattoriale di", str(n) + ":")
print(fact(n))
