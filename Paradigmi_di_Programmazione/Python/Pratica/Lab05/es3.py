"""
When working with Boolean functions, we often use the symbol X, meaning that a given variable may have indifferently
the value 0 or 1. In the OR function, for example, the result is 1 when the inputs are 01, 10 or 11. More compactly, if the
inputs are X1 or 1X.
Write an “X-expansion” recursive program that, given a binary string that includes characters 0, 1 and X, computes all the
possible combinations implied by the given string.
Example: given the string 01X0X, algorithm must compute the following combinations
• 01000
• 01001
• 01100
• 01101
"""


def x_expansion(s, output):
    if "X" not in s:
        output.append(s)
        return
    x_index = s.index("X")
    x_expansion(s[:x_index] + "0" + s[x_index + 1 :], output)
    x_expansion(s[:x_index] + "1" + s[x_index + 1 :], output)


bits = "010X1"
output = []

x_expansion(bits, output)

print(output)
