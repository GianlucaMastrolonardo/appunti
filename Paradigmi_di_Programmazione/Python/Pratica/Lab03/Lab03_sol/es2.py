# imperative version
numbers = set()
for n in range(1, 100):
  for x in range(2,10):
    if n % x == 0:
      numbers.add(n)
print(numbers)
print()

# using list comprehension

numbers = [number for number in range(1,100) if True in [True for x in range(2,10) if number % x == 0]]
print(numbers)
