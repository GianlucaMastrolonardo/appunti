some_string = 'the slow solid squid swam sumptuously through the slimy swamp'

# imperative version
n_spaces = 0
for ch in some_string:
  if ch == ' ':
    n_spaces += 1

print(n_spaces)
print()

# using list comprehension
spaces = [s for s in some_string if s == ' ']
print(len(spaces))
