list_a = ["pippo", "pluto", "paperino"]
list_b = ["pip", "paperino&paperina","topolino", "minnie", "pippo"]

# imperative version
result = []
for a in list_a:
  for b in list_b:
    if a == b or a in b or b in a:
      result.append((a,b))

print(result)
print()

# using list comprehension
result = [(a, b) for a in list_a for b in list_b if a == b or a in b or b in a]
print(result)
