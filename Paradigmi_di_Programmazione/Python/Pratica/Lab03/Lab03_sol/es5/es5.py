def read_lines(file_list):
    for file in file_list:
        with open(file) as txt:
            for line in txt.read().splitlines():
                next_file = (yield line)
                if next_file:
                    txt.close()
                    break


def display_files(file_list):
    rl = read_lines(file_list)
    while True:
        inp = input()
        if inp == 'n':
            print(rl.send('next'))
        else:
            print(next(rl))


files = ['file1.txt', 'file2.txt', 'file3.txt']
display_files(files)

