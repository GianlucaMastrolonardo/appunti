'''
Write a program that find all the numbers from 1-100 that are divisible by any single digit (from 2 to 9) besides 1. As an
example:
• 3 satisfies the condition, since it is divisible by 3;
• 11 does not satisfy the condition, because it is not divisible by any number from 2 to 9.
Try first with the imperative programming paradigm, then translate the program in a more functional style by using list
comprehension.
'''

numbers = list(range(1,101))
divisors = list(range(2,10))

only_div = [num for num in numbers for div in divisors if num%div==0]

print(set(only_div))