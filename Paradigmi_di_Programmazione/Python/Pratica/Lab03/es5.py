"""
Write a program that exploits a custom “read_lines” generator to display a list of files line by line. To this end, create some
.txt files in the same folder of the program, and store their filenames inside a list, e.g., my_files = ['file1.txt', 'file2.txt',
'file3.txt']. Thanks to the read_lines generator, the program should start by displaying the first line of file1.txt. Then, the
program should wait for user input:
• if the user press Enter, the read_lines generator should return the next line of the current file;
• if the user press n + Enter, the read_lines generator should forget the rest of the current file, and it should return
the first slide of the next file.
TIPS: you can use the send() method to tell the generator that it's time to change file.
"""


def readfile(filename):
    try:
        file = open(filename)
        # print("File esiste")
        file_content = list(file)
        file.close()
        return file_content
    except FileNotFoundError:
        print("File non esiste")
        return None


def readline(filenames):
    for file in filenames:
        file_content = readfile(file)
        if file_content != None:
            # print(file_content)
            for string in file_content:
                var = yield string
                if var is not None and var == "n":
                    break


files_to_read = ["file1.txt", "file2.txt", "file3.txt"]

read = readline(files_to_read)

x = input("STOP to exit\nEnter: new line of file\nn + Enter: switch file\n")

while x != "STOP":
    try:
        print("\n---\n")
        if x == "n":
            try:
                print(read.send(x))
            except TypeError:
                next(read)
                print(read.send(x))
        elif x == "":
            print(next(read))
        else:
            print("Operazione Errata!")
        x = input("STOP to exit\nEnter: new line of file\nn + Enter: switch file\n")
    except StopIteration:
        print("Fine file, addio...")
        break
