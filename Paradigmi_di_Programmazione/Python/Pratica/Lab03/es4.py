"""
Write a generator which computes the running average of a sequence of arbitrary numbers: as long as a new element is
passed to the generator (through the send() method), the output is updated to reflect the average of all the numbers
considered up to that point.
As an example, with the following list of numbers [7, 13, 17, 231, 12, 8, 3], the output of multiple next operations on the
generator should be:
"""


def media():
    sum = 0
    count = 0
    average = 0
    while True:
        val = yield average
        if val is not None:
            count += 1
            sum += val
            average = sum / count


def media_optimized():
    numbers = []
    while True:
        try:
            val = yield sum(numbers) / len(numbers)
            if val is not None:
                numbers.append(val)
        except ZeroDivisionError:  # Caso quando inserisco il primo valore
            val = yield 0
            if val is not None:
                numbers.append(val)


myAvg = media()
myAvg_optimized = media_optimized()

next(myAvg)  # Inizializzo il Generatore
next(myAvg_optimized)

"""
x = int(input("-1 per terminare\nInserire valore: "))
while x != -1:
    myAvg.send(x)
    print("sent:", x, ", new average:", next(myAvg))
    x = int(input("-1 per terminare\nInserire valore: "))
"""

x = int(input("-1 per terminare\nInserire valore: "))
while x != -1:
    myAvg_optimized.send(x)
    print("sent:", x, ", new average:", next(myAvg_optimized))
    x = int(input("-1 per terminare\nInserire valore: "))
