'''
Write a program that counts the number of spaces inside a string. Try first with the imperative programming paradigm,
then translate the program in a more functional style by using list comprehension.
'''

my_string = input("Inserisci una frase: ")
char_of_string = list(my_string)

'''
cnt = 0
for char in char_of_string:
    if(char == " "):
        cnt+=1
'''

spaces = [space for space in char_of_string if space == " "]

print(len(spaces))
