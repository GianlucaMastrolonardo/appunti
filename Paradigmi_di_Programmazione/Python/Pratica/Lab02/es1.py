# Es4 fallo su un altro file

"""
EXERCISE 1 - TODO LIST
Given a list of tasks (i.e., actions that the user wants to do in the future) implement a todo_manager program to perform
4 actions:
1. insert a new task (a string of text);
2. remove a task (by typing its content, exactly);
3. show all existing tasks;
4. close the program.
At startup, the program shows a menu with the 4 options and, for each choice, performs the requested action. After the
action (except action 4), the program returns to the prompt for actions.

EXERCISE 2 - TODO LIST SAVED TO FILE
Extend the program developed in the previous exercise to save and retrieve the list of tasks to/from a text file.
Consequently, at startup, the program takes the list of tasks from the file and saves the changes to the file as soon as the
user decides to close the program.
For the exercise, you can create a task_list.txt file (saved in the same folder of the Python file) by saving each task as a
separate line.

EXERCISE 3 - TASKS DELETION EXTENSION
Modify the program developed in the previous exercise to remove all the tasks that contains a specified substring. For
example, when user types “shop
"""

# Aggiunto da me


def get_user_input():
    print(
        "\n1. insert a new task (a string of text);\n2. remove a task (by typing its content, exactly);\n3. show all existing tasks;\n4. delete multiple task by sub-string;\n5. close the program."
    )
    while True:
        try:
            user_input = int(input("\n>>> Select Operation: "))
            return user_input
        except:
            print("+++ Valore inserito errato +++")


# ES1 da qui in poi
def new_task(tasks):
    task_to_add = input("Inserisci task: ")
    tasks.append(task_to_add)


def print_tasks(tasks):
    print("Lista Tasks:")
    print(tasks)


def remove_task(tasks):
    task_to_delete = input("Inserisci task da rimuovere: ")
    if task_to_delete in tasks:
        tasks.remove(task_to_delete)
    else:
        print("\n+++ Task non trovato! +++")


def remove_sub_tasks(tasks):
    sub_task_to_delete = input("Inserisci la sotto stringa da rimuovere: ")
    # Duplico le tasks
    dup_tasks = list(tasks)
    # Suddivido le tasks duplicate in singole parole
    for elements in dup_tasks:
        if sub_task_to_delete in elements.split():
            tasks.remove(elements)

    if tasks == dup_tasks:
        print("\n+++ Task/s non trovato/i! +++")


def splash_page(tasks):
    user_input = get_user_input()
    while user_input != 5:
        if user_input == 1:
            new_task(tasks)
        elif user_input == 2:
            remove_task(tasks)
        elif user_input == 3:
            print_tasks(tasks)
        elif user_input == 4:
            remove_sub_tasks(tasks)
        user_input = get_user_input()


tasks = []
filename = "task_list.txt"
file = open(filename, "r")

# Carico la lista di task nella variabile tasks
tasks = file.read().splitlines()
file.close()

splash_page(tasks)

# Salvo le task nel file
file = open(filename, "w")
for strings in tasks:
    file.write(strings + "\n")
file.close()
