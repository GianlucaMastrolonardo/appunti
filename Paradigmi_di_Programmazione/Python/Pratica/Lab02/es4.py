# Es2 ed Es3 sono tutti assieme ad Es1
"""
Given a list of tasks modeled as dictionaries, like this one (i.e., tasks):
task1 = {`todo`: `call John for project organization`, `urgent`: True}
task2 = {`todo`: `buy a new mouse`, `urgent`: True}
task3 = {`todo`: `find a present for Angelina`s birthday`, `urgent`: False}
task4 = {`todo`: `organize mega party (last week of April)`, `urgent`: False}
task5 = {`todo`: `book summer holidays`, `urgent`: False}
task6 = {`todo`: `whatsapp Mary for a coffee`, `urgent`: False}
tasks = [task1, task2, task3, task4, task5, task6]

write a program that returns a new list that contains only the urgent tasks, i.e., take the dictionaries that have a True value
in the urgent field. BEWARE: it's a new program, and tasks can be hard-coded into the program.
"""

task1 = {"todo": "call John for project organization", "urgent": True}
task2 = {"todo": "buy a new mouse", "urgent": True}
task3 = {"todo": "find a present for Angelina's birthday", "urgent": False}
task4 = {"todo": "organize mega party (last week of April)", "urgent": False}
task5 = {"todo": "book summer holidays", "urgent": False}
task6 = {"todo": "whatsapp Mary for a coffee", "urgent": True}
tasks = [task1, task2, task3, task4, task5, task6]

urgent_tasks = []

for task in tasks:
    # Dato che true è un Truthy value. Se ti da fastidio è come scrivere:
    # if task["urgent"] == True:
    if task["urgent"]:
        urgent_tasks.append(task)

print(urgent_tasks)
