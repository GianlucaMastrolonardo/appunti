"""
Data una lista di numeri interi lista_partenza e un valore intero di riferimento target, scrivere una funzione ricorsiva che
trovi (se esiste) il sottoinsieme di numeri più piccolo (ovvero con il minor numero di elementi) in lista_partenza tale che la
somma dei suoi elementi sia esattamente uguale a target. Nel caso in cui ci sia più di un sottoinsieme “vincente”, il
programma ne fornirà in output uno solo, senza nessun vincolo in particolare. Ad esempio, dati questi valori in ingresso:
lista_partenza = [5,6,7,2,13,11,8,1,2,1]
target = 9
i risultati validi possono essere diversi, tra cui: [7,2] e [8,1]

SUGGERIMENTO: per ottenere la somma di una sequenza di numeri, si può usare la funzione sum()
(https://docs.python.org/3/library/functions.html#sum)
"""


#Soluzione Prof
def trova_insieme(parziale, numeri, target, soluzione):
    #caso terminazione

    #caso aggiungo un numero che sfora target
    if sum(parziale) > target:
        return  #smetto di fare backtracking

    #caso soluzione trovata
    if sum(parziale) == target:
        #non è detto che sia ottimale
        #controllo se ottimale, la prima volta soluzione è vuota
        if len(soluzione) == 0 or len(parziale) < len(soluzione):
            soluzione.clear()
            soluzione.extend(parziale)
        #se ho raggiunto il target esco
        return

    #caso intermedio
    for n in numeri:
        parziale.append(n)
        numeri.remove(n) #Rimuovo n dai numeri (solo un valore, non tutte le occorrenze)
        trova_insieme(parziale, numeri, target, soluzione)
        numeri.append(n) #Devo rimettere n tra i numeri
        parziale.pop() #Rimuovo ultimo valore



lista_partenza = [5, 6, 7, 2, 13, 11, 8, 1, 2, 1]
target = 9

parziale = []
soluzione = []

trova_insieme(parziale, lista_partenza, target, soluzione)
print(soluzione)



