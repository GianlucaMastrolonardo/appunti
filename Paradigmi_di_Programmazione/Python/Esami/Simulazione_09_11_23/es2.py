'''
Scrivere un generatore chiamato genera_primi che generi numeri primi in un range specificato. Il generatore, in
particolare, riceverà due parametri, start e stop, e restituirà (uno per volta) i numeri primi tra start (incluso) e stop
(incluso). Ad esempio, dato questo codice:
primi = genera_primi(50,60)
for n in primi:
 print(n)
Il risultato deve contenere 53 e 59.
'''

def genera_primi(start, stop):
    n = start
    while(n <= stop):

        isPrimo = True
        if n % 2 == 0 and n != 2:
            isPrimo = False

        if n % 5 == 0 and n != 5:
            isPrimo = False

        copy_n = list(str(n))
        sum = 0
        for num in copy_n:
            num = int(num)
            sum += num

        if sum % 3 == 0 and n != 3:
            isPrimo = False

        if isPrimo == True:
            yield n

        n = n+1


primi = genera_primi(50,60)
for n in primi:
    print(n)

#Soluzione Prof
def genera_primi_sol(start, stop):
    for n in range(start, stop+1):
        for divisore in range(2, n):
            if n % divisore == 0:
                break
        #Dall'else passi solo se non hai mai fatto un break
        else:
            yield n

primi_sol = genera_primi_sol(50, 60)
for n in primi_sol:
    print(n)