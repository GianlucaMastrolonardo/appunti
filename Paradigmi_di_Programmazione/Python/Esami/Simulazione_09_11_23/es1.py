'''
ESERCIZIO 1 (4 PT)
Scrivere un programma che permetta all’utente di inserire una stringa arbitraria, composta da caratteri alfanumerici,
punteggiatura, e spazi. Data la stringa in input, trovare e stampare il carattere numerico più piccolo incluso nella lista.
Data la seguente stringa, ad esempio:
my_string = “324rrg5b. htd ”
Il risultato deve essere 2.
SUGGERIMENTO: per controllare se un singolo carattere è un numero, si può usare il metodo .isdigit()
'''

def find_string_arb(string):
    list_string = list(string)
    try:
        return min(num for num in list_string if num.isdigit())
    except ValueError:
        return "Nessuna cifra all'interno della stringa"


my_string = input("inserisci una stringa: ")
print(find_string_arb(my_string))


#Soluzione Prof
my_string_sol = input("inserisci una stringa: ")
try:
    print(min(num for num in my_string2 if num.isdigit()))
except ValueError:
    print("Nessuna cifra all'interno della stringa")

