import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.security.interfaces.EdECKey;
import java.util.ArrayList;
import java.util.Arrays;

public class TestGraph {
    @Test
    void hasUndirectedCycleTest() {
        UndirectedGraph myGraph = new UndirectedGraph("4; 1 0; 0 2; 2 3; 3 0");
        BFSAndDFSApp testApp = new BFSAndDFSApp(myGraph);
        Assertions.assertTrue(testApp.hasUndirectedCycle());
        DirectedGraph mySecondGraph = new DirectedGraph("3; 0 1; 0 2");
        testApp = new BFSAndDFSApp(mySecondGraph);
        Assertions.assertFalse(testApp.hasUndirectedCycle());
        DirectedGraph myForest = new DirectedGraph("5; 0 1; 0 2; 3 4; 4 3");
        testApp = new BFSAndDFSApp(myForest);
        Assertions.assertTrue(testApp.hasUndirectedCycle());
    }

    @Test
    void orderPostVisitTest() {
        UndirectedGraph myGraph = new UndirectedGraph("4; 2 0; 1 0; 3 2; 3 1");
        BFSAndDFSApp testApp = new BFSAndDFSApp(myGraph);
        ArrayList<Integer> output = testApp.getNodesInOrderPostVisit(0);

        ArrayList<Integer> expected1 = new ArrayList<>();

        expected1.add(1);
        expected1.add(3);
        expected1.add(2);
        expected1.add(0);

        ArrayList<Integer> expected2 = new ArrayList<>();
        expected2.add(2);
        expected2.add(3);
        expected2.add(1);
        expected2.add(0);

        Assertions.assertTrue(output.equals(expected1) || output.equals(expected2));
    }

    @Test
    void hasDirCycleTest() {
        DirectedGraph g = new DirectedGraph("1");
        BFSAndDFSApp testApp = new BFSAndDFSApp(g);
        Assertions.assertFalse(testApp.hasDirCycle());

        g = new DirectedGraph("2; 0 1");
        testApp = new BFSAndDFSApp(g);
        Assertions.assertFalse(testApp.hasDirCycle());

        g = new DirectedGraph("3; 1 0; 1 2; 0 2");
        testApp = new BFSAndDFSApp(g);
        Assertions.assertFalse(testApp.hasDirCycle());

        g = new DirectedGraph("3; 0 2; 2 1; 1 0");
        testApp = new BFSAndDFSApp(g);
        Assertions.assertTrue(testApp.hasDirCycle());

        g = new DirectedGraph("5; 0 4; 4 1; 4 2; 3 4; 2 3");
        testApp = new BFSAndDFSApp(g);
        Assertions.assertTrue(testApp.hasDirCycle());
    }

    @Test
    void testgetShortestPath() {
        DirectedGraph g = new DirectedGraph("5; 3 4; 4 2; 2 3; 4 1; 1 2; 1 0");
        BFSAndDFSApp testApp = new BFSAndDFSApp(g);
        ArrayList<Edge> path = testApp.getShortestPath(4, 0);
        Assertions.assertTrue(path.contains(new Edge(4, 1)));
        Assertions.assertTrue(path.contains(new Edge(1, 0)));
        ArrayList<Edge> path2 = testApp.getShortestPath(0, 4);
        Assertions.assertTrue(path2.isEmpty());
    }
}
