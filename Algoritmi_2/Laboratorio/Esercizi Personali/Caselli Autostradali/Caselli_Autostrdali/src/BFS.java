import it.uniupo.graphLib.Graph;
import it.uniupo.graphLib.GraphInterface;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class BFS {
    private GraphInterface myGraph;

    BFS(GraphInterface g) {
        myGraph = g;
    }

    //Con questa variamente utilizzi founded che è un array di Booleani, in questo modo puoi fare accesso posizionale ed ottimizzi l'algoritmo
    public ArrayList<Integer> getNodesInOrderOfVisit(int sorgente) {
        if (this.myGraph.getOrder() <= sorgente) {
            throw new IllegalArgumentException("Sorgente errata");
        }
        ArrayList<Integer> returnList = new ArrayList<>();
        returnList.add(sorgente);
        boolean[] founded = new boolean[myGraph.getOrder()];
        Queue<Integer> queue = new LinkedList<>();
        founded[sorgente] = true;
        queue.add(sorgente);
        while (!queue.isEmpty()) {
            int v = queue.remove();
            Iterable<Integer> neighbors = this.myGraph.getNeighbors(v);
            for (int neighbor : neighbors) {
                if (!founded[neighbor]) {
                    founded[neighbor] = true;
                    queue.add(neighbor);
                    returnList.add(neighbor);
                }
            }
        }
        return returnList;
    }
}
