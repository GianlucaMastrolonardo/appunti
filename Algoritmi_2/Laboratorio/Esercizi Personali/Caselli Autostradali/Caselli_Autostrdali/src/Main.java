import it.uniupo.graphLib.*;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Graph myGraph = new UndirectedGraph("nodes: 5; edges: (3,4) weight 5; (1,2) weight -4; (0,4) weight 3; (1,4) weight 0");
        System.out.println(myGraph);
        System.out.println(myGraph.getOutEdges(4));

        BFS myBFS = new BFS(myGraph);
        System.out.println(myBFS.getNodesInOrderOfVisit(4));
    }
}