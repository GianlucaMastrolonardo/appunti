import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MSI {
    private final int[] myWeight;

    public MSI(int[] w) {
        myWeight = w;
    }

    private int foundMSI(int[] arr) {
        arr[0] = myWeight[0];
        arr[1] = Math.max(myWeight[0], myWeight[1]);

        for (int i = 2; i < arr.length; ++i) {
            arr[i] = Math.max(arr[i - 1], (arr[i - 2] + myWeight[i]));
        }
        return arr[arr.length - 1];
    }

    private ArrayList<Integer> foundOptSol(boolean node) {
        int[] arr = new int[myWeight.length];
        foundMSI(arr);

        ArrayList<Integer> solution = new ArrayList<>();

        int i = arr.length - 1;
        while (i > 0) {
            if (arr[i] > arr[i - 1]) {
                solution.add(i);
                i -= 2;
            } else {
                --i;
            }
        }
        if (i == 0) {
            solution.add(i);
        }
        Collections.reverse(solution);

        if (!node) {
            return solution;
        } else {
            ArrayList<Integer> nodesSolution = new ArrayList<>();

            for (int el : solution) {
                nodesSolution.add(myWeight[el]);
            }
            return nodesSolution;
        }

    }

    public int getMaxVal() {
        int[] arr = new int[myWeight.length];
        return foundMSI(arr);
    }

    public ArrayList<Integer> getOptSol() {
        boolean nodePrint = true;
        return foundOptSol(nodePrint);
    }
}
