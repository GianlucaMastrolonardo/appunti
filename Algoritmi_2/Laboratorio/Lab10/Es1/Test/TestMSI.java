import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestMSI {
    @Test
    void testCostructor() {
        int[] w = new int[5];
        MSI testMSI = new MSI(w);
        Assertions.assertNotNull(testMSI);
    }

    @Test
    void testGetMaxVal() {
        {
            int[] w = {3, 2, 5, 6, 1};
            MSI testMSI = new MSI(w);
            Assertions.assertEquals(testMSI.getMaxVal(), 9);
        }
        {
            int[] w = {2, 3, 4, 5, 1, 7};
            MSI testMSI = new MSI(w);
            Assertions.assertEquals(testMSI.getMaxVal(), 15);
        }
    }

    @Test
    void testGetOptSol() {
        {
            int[] w = {3, 2, 5, 6, 1};
            MSI testMSI = new MSI(w);
            System.out.println(testMSI.getOptSol());
        }
        {
            int[] w = {2, 3, 4, 5, 1, 7};
            MSI testMSI = new MSI(w);
            System.out.println(testMSI.getOptSol());
        }
        {
            int[] w = {1, 999, 1, 5};
            MSI testMSI = new MSI(w);
            System.out.println(testMSI.getOptSol());
        }
    }
}
