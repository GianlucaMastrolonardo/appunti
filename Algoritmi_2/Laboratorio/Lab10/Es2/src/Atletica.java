public class Atletica {
    private final int weigthLength;

    public Atletica(int numeroDiscipline) {
        weigthLength = numeroDiscipline;
    }

    private int foundMSI(int[] myWeight) {
        int[] arr = new int[myWeight.length];
        arr[0] = myWeight[0];
        arr[1] = Math.max(myWeight[0], myWeight[1]);

        for (int i = 2; i < arr.length; ++i) {
            arr[i] = Math.max(arr[i - 1], (arr[i - 2] + myWeight[i]));
        }
        return arr[arr.length - 1];
    }

    public int scelta(int[] rendAtleta1, int[] rendAtleta2) {
        if (rendAtleta1.length != weigthLength || rendAtleta2.length != weigthLength) {
            throw new IllegalArgumentException("Le lunghezze non sono uguali");
        }
        int MSIAtleta1 = foundMSI(rendAtleta1);
        int MSIAtleta2 = foundMSI(rendAtleta2);
        if (MSIAtleta1 == MSIAtleta2) {
            return 0;
        }

        if (MSIAtleta1 > MSIAtleta2) {
            return 1;
        } else {
            return 0;
        }
    }
}
