import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TestAllacciamentoIdrico {

    AllacciamentoIdrico allId;

    @Test
    public
    void testCreate() {
        UndirectedGraph mappa = new UndirectedGraph("4;1 2 2;1 0 5;2 3 3; 0 3 1");
        int[][] costoScavo = {{0, 6, -1, 8}, {6, 0, 7, -1}, {-1, 7, 0, 10}, {8, -1, 10, 0}};
        int costoTubo = 3;
        int puntoAll = 1;
        allId = new AllacciamentoIdrico(mappa, costoScavo, costoTubo, puntoAll);
        Assertions.assertNotNull(allId);
    }

    @Test
    public
    void test01() {
        UndirectedGraph mappa = new UndirectedGraph("4;1 2 2;1 0 5;2 3 3; 0 3 1");
        int[][] costoScavo = {{0, 6, -1, 8}, {6, 0, 7, -1}, {-1, 7, 0, 10}, {8, -1, 10, 0}};
        int costoTubo = 3;
        int puntoAll = 1;
        allId = new AllacciamentoIdrico(mappa, costoScavo, costoTubo, puntoAll);
        Assertions.assertTrue(allId.progettoComune().hasEdge(0, 3));
        Assertions.assertFalse(allId.progettoComune().hasEdge(2, 3));
        Assertions.assertTrue(allId.progettoProprietari().hasEdge(2, 3));
        Assertions.assertFalse(allId.progettoProprietari().hasEdge(0, 3));
        Assertions.assertEquals(2, allId.speseExtraComune());
        // Assertions.assertEquals(3, allId.speseExtraProprietario(3));
    }

}