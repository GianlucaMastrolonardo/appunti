import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.UndirectedGraph;

public
class AllacciamentoIdrico {
    private final UndirectedGraph mappa;
    private final int[][] costoScavo;
    private final int costoTubo;
    private final int puntoAllacciamento;

    private final UndirectedGraph mappaCostiScavi;


    public
    AllacciamentoIdrico(UndirectedGraph mappa, int[][] costoScavo, int costoTubo, int puntoAllacciamento) {
        this.mappa = mappa;
        this.costoScavo = costoScavo;
        this.costoTubo = costoTubo;
        this.puntoAllacciamento = puntoAllacciamento;
        mappaCostiScavi = (UndirectedGraph) mappa.create();
        for (int i = 0; i < costoScavo.length; ++i) {
            for (int j = 0; j < costoScavo[i].length; ++j) {
                if (i != j && costoScavo[i][j] != -1) {
                    if (!mappaCostiScavi.hasEdge(i, j)) {
                        mappaCostiScavi.addEdge(i, j, costoScavo[i][j]);
                    }
                }
            }
        }
    }

    //Ai proprietari conviene Dijkstra, perché hanno il cammino minimo
    UndirectedGraph progettoProprietari() {
        Dijkstra dijkstra = new Dijkstra(mappa);
        return dijkstra.getDijkstraTree(puntoAllacciamento);
    }


    public
    UndirectedGraph progettoComune() {
        //Uso l'MST ma non usi pesi normali ma uso il costo dello scavo
        Prim prim = new Prim(mappaCostiScavi);
        return prim.getMST();
    }

    public
    int speseExtraComune() {
        //Proprietari: percorso più veloce, dopo aggiungo i costi degli scavi
        Dijkstra dijkstra = new Dijkstra(mappa);
        //Comune: percorso più economico
        Prim prim = new Prim(mappaCostiScavi);
        return dijkstra.getDijkstraSize(puntoAllacciamento, costoScavo) - prim.getMSTSize();
    }
}
