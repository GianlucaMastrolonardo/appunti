import it.uniupo.algoTools.MinHeap;
import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public
class Dijkstra {

    private UndirectedGraph myGraph;
    private boolean[] founded;


    public
    Dijkstra(UndirectedGraph graph) {
        this.myGraph = graph;
        this.founded = new boolean[this.myGraph.getOrder()];
    }

    private
    int dijkstra(int sorg, UndirectedGraph dijkstraTree) {
        int size = 0;
        int[] distance = new int[myGraph.getOrder()];
        Arrays.fill(distance, -1);

        founded[sorg] = true;
        distance[sorg] = 0;
        MinHeap<Edge, Integer> heap = new MinHeap<Edge, Integer>();

        for (Edge e : myGraph.getOutEdges(sorg)) {
            heap.add(e, distance[sorg] + e.getWeight());
        }

        while (!heap.isEmpty()) {
            Edge heapReturnedEdge = heap.extractMin(); //arco (u,w), u alla prima iterazione è uguale a sorg
            int nodeU = heapReturnedEdge.getTail();
            int nodeW = heapReturnedEdge.getHead();
            if (!founded[nodeW]) {
                founded[nodeW] = true;
                distance[nodeW] = distance[nodeU] + heapReturnedEdge.getWeight();
                size += heapReturnedEdge.getWeight();
                dijkstraTree.addEdge(heapReturnedEdge);
                for (Edge e : myGraph.getOutEdges(nodeW)) {
                    heap.add(e, distance[nodeW] + e.getWeight());
                }
            }
        }
        return size;
    }

    public
    UndirectedGraph getDijkstraTree(int sorg) {
        Arrays.fill(founded, false);
        UndirectedGraph dijkstraTree = (UndirectedGraph) myGraph.create();
        dijkstra(sorg, dijkstraTree);
        return dijkstraTree;
    }

    public
    int getDijkstraSize(int sorg, int[][] costoScavi) {
        Arrays.fill(founded, false);
        UndirectedGraph dijkstraTree = (UndirectedGraph) myGraph.create();
        dijkstra(sorg, dijkstraTree);
        int size = 0;
        for (int i = 0; i < costoScavi.length; ++i) {
            for(int j = 0; j < costoScavi[i].length;++j){
                if(i < j) {
                    if (costoScavi[i][j] != -1 && dijkstraTree.hasEdge(i, j)) {
                        dijkstraTree.removeEdge(i, j);
                        dijkstraTree.addEdge(i, j, costoScavi[i][j]);
                        size += costoScavi[i][j];
                    }
                }
            }
        }
        return size;
    }


}