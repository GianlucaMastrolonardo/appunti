import it.uniupo.algoTools.MinHeap;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;

import java.util.Arrays;

public
class Prim {

    private UndirectedGraph myGraph;
    private boolean[] founded;

    public
    Prim(UndirectedGraph g) {
        this.myGraph = g;
        this.founded = new boolean[myGraph.getOrder()];
    }

    private
    int prim(int sorg, UndirectedGraph MST) {
        int cost = 0;
        founded[sorg] = true;
        MinHeap<Edge, Integer> heap = new MinHeap<>();

        for (Edge e : myGraph.getOutEdges(sorg)) {
            heap.add(e, e.getWeight());
        }

        while (!heap.isEmpty()) {
            Edge heapReturnedEdge = heap.extractMin(); //arco (u,w), u alla prima iterazione è uguale a sorg
            int nodeW = heapReturnedEdge.getHead();
            if (!founded[nodeW]) {
                founded[nodeW] = true;
                MST.addEdge(heapReturnedEdge);
                cost += heapReturnedEdge.getWeight();
                for (Edge e : myGraph.getOutEdges(nodeW)) {
                    heap.add(e, e.getWeight());
                }
            }
        }
        return cost;
    }

    public
    UndirectedGraph getMST() {
        Arrays.fill(founded, false);
        UndirectedGraph MST = (UndirectedGraph) myGraph.create();
        prim(0, MST);
        return MST;
    }

    public
    int getMSTSize() {
        Arrays.fill(founded, false);
        UndirectedGraph MST = (UndirectedGraph) myGraph.create();
        return prim(0, MST);
    }
}
