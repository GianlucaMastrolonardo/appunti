import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class testPrim {
    @Test
    void testMST() {
        UndirectedGraph g = new UndirectedGraph("5; 1 2 2; 2 0 4; 1 0 4; 0 3 5; 0 4 5; 3 4 1");
        Prim myPrim = new Prim(g);
        Assertions.assertNotEquals(myPrim.getMST(), myPrim.dijkstra(0));
    }
}
