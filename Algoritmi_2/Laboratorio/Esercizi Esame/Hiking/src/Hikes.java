import it.uniupo.algoTools.MinHeap;
import it.uniupo.algoTools.UnionByRank;
import it.uniupo.algoTools.UnionFind;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.UndirectedGraph;

public class Hikes {
    private UndirectedGraph myGraph;
    private final int orderMyGraph;
    private final boolean[][] percorribile;

    public Hikes(UndirectedGraph rifugi, boolean[][] percorribile) {
        myGraph = rifugi;
        orderMyGraph = myGraph.getOrder();
        this.percorribile = percorribile;
    }

    private int kruslkalSpacing(int k) {
        UnionByRank unionByRank = new UnionByRank(orderMyGraph);
        MinHeap<Edge, Integer> minHeap = new MinHeap<>();

        for (int i = 0; i < orderMyGraph; i++) {
            for (Edge e : myGraph.getOutEdges(i)) {
                minHeap.add(e, e.getWeight());
            }
        }

        while (!minHeap.isEmpty() && unionByRank.getNumberOfSets() > k) {
            Edge e = minHeap.extractMin();
            int headLeader = unionByRank.find(e.getHead());
            int tailLeader = unionByRank.find(e.getTail());

            if (headLeader != tailLeader) {
                unionByRank.union(headLeader, tailLeader);
            }
        }

        //Trovo Spaziamento
        int spaziamento = -1;
        boolean isValidEdge = false;
        while (!minHeap.isEmpty() && !isValidEdge) {
            Edge e = minHeap.extractMin();
            int headLeader = unionByRank.find(e.getHead());
            int tailLeader = unionByRank.find(e.getTail());

            if (headLeader != tailLeader) {
                isValidEdge = true;
                spaziamento = e.getWeight();
            }
        }


        return spaziamento;
    }

    public int minDistanza(int numGite) {
        if (numGite < 2 || numGite > orderMyGraph) throw new IllegalArgumentException("Num Gite errato");

        for (int i = 0; i < percorribile.length; i++) {
            for (int j = 0; j < percorribile[i].length; j++) {
                if (!percorribile[i][j]) {
                    myGraph.removeEdge(i, j);
                }
            }
        }

        return kruslkalSpacing(numGite);
    }
}