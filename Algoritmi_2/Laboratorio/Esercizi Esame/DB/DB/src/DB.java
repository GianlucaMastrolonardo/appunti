import it.uniupo.algoTools.MaxHeap;

public class DB {
    private final int[] dim;  //vol
    private final double[] time; //val

    public DB(int[] dim, double[] time) {
        if (dim.length != time.length)
            throw new IllegalArgumentException("Dimensione degli array devono essere uguali");
        this.dim = dim;
        this.time = time;
    }

    private double fraqKnapscak(int[] vol, double[] val, int capacity) {
        int objsCnt = vol.length;
        double[] dose = new double[objsCnt];
        int[] quant = new int[objsCnt];
        double valTot = 0;

        MaxHeap<Integer, Double> maxHeap = new MaxHeap<>();
        for (int i = 0; i < objsCnt; i++) {
            maxHeap.add(i, val[i] / vol[i]);
        }
        while (!maxHeap.isEmpty() && capacity > 0) {
            int extractedObj = maxHeap.extractMax();
            if (vol[extractedObj] < capacity) {
                dose[extractedObj] = 1;
                quant[extractedObj] = vol[extractedObj];
                valTot += val[extractedObj];
            } else {
                dose[extractedObj] = (double) capacity / vol[extractedObj];
                quant[extractedObj] = capacity;
                valTot += val[extractedObj] * dose[extractedObj];
            }
            capacity -= quant[extractedObj];
        }

        return valTot;


    }

    public double timeToRebuild(int memSpace) {
        if (memSpace < 0)
            throw new IllegalArgumentException("Il tempo per la ricostruzione di un DB non può essere negativo!");

        return fraqKnapscak(dim, time, memSpace);
    }
}