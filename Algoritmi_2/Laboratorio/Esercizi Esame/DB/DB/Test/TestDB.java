import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TestDB {
    @Test
    public void a() {
        var dim = new int[]{70, 12, 3};
        var time = new double[]{10, 2, .3};
        var db = new DB(dim, time);
        assertEquals(11.8, db.timeToRebuild(3));
    }

    @Test
    public void b() {
        var dim = new int[]{3, 2, 4};
        var time = new double[]{2, 5, 1};
        var db = new DB(dim, time);
        assertEquals(0, db.timeToRebuild(10));
    }

    @Test
    public void c() {
        var dim = new int[]{4, 3, 4};
        var time = new double[]{5, 3, 1};
        var db = new DB(dim, time);
        assertEquals(.25, db.timeToRebuild(10));
    }

    @Test
    public void d() {
        var dim = new int[]{5, 8, 3};
        var time = new double[]{3, 11, 9};
        var db = new DB(dim, time);
        assertThrows(IllegalArgumentException.class, () -> db.timeToRebuild(-1));
    }
}