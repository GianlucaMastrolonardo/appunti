import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.Graph;
import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public
class TestDFS {

    @Test
    void testCycle() {
        Graph g = new UndirectedGraph("4; 1 0; 0 2; 2 3; 3 0");
        Assertions.assertTrue((new DFS(g)).hasUndirectedCycle());
        g = new UndirectedGraph("5; 0 1; 1 2; 2 3; 3 4; 4 0");
        Assertions.assertTrue((new DFS(g)).hasUndirectedCycle());
    }

    @Test
    void testNoCycle() {
        Graph g = new UndirectedGraph("3; 0 1; 0 2");
        Assertions.assertFalse((new DFS(g)).hasUndirectedCycle());
    }

    @Test
    void testNodesOrderPostVisit() {
        Graph g = new UndirectedGraph("4; 2 0; 1 0; 3 2; 3 1");
        ArrayList<Integer> order = (new DFS(g)).getNodesInOrderPostVisit(0);
        ArrayList<Integer> a = new ArrayList<>(List.of(1, 3, 2, 0));
        ArrayList<Integer> b = new ArrayList<>(List.of(2, 1, 3, 0));
        Assertions.assertTrue(order.equals(a) || order.equals(b));
    }

    @Test
    void hasUndirectedCycle() {
        Graph g = new UndirectedGraph("4; 1 0; 0 2; 2 3; 3 0");
        Assertions.assertTrue((new DFS(g)).hasUndirectedCycle());
        g = new UndirectedGraph("4; 1 0; 2 0; 3 0");
        Assertions.assertFalse((new DFS(g)).hasUndirectedCycle());
    }

    @Test
    void hasDirectedCycle() {
        Graph g = new DirectedGraph("1;");
        Assertions.assertFalse((new DFS(g)).hasDirectedCycle());
        g = new DirectedGraph("2; 0 1");
        Assertions.assertFalse((new DFS(g)).hasDirectedCycle());
        g = new DirectedGraph("3; 1 0; 1 2; 0 2");
        Assertions.assertFalse((new DFS(g)).hasDirectedCycle());
        g = new DirectedGraph("3; 0 2; 2 1; 1 0");
        Assertions.assertTrue((new DFS(g)).hasDirectedCycle());
        g = new DirectedGraph("5; 0 4; 4 1; 4 2; 3 4; 2 3");
        Assertions.assertTrue((new DFS(g)).hasDirectedCycle());
    }

    @Test
    void getDirCycle() {
        Graph g = new DirectedGraph("6; 0 1; 2 0; 1 2; 2 3; 3 0; 3 4; 4 5");
        System.out.println((new DFS(g)).getDirCycle());
    }

}
