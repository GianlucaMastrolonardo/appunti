import it.uniupo.graphLib.*;

import java.util.ArrayList;
import java.util.Arrays;

public
class DFS {
    private final GraphInterface myGraph;
    private final int orderMyGraph;

    private boolean[] founded;
    private int[] fathers;
    private boolean[] terminated;

    private int startCycleNode = -1;
    private int endCycleNode = -1;

    public
    DFS(GraphInterface g) {
        myGraph = g;
        orderMyGraph = myGraph.getOrder();
        founded = new boolean[orderMyGraph];
        fathers = new int[orderMyGraph];
        terminated = new boolean[orderMyGraph];
    }

    private
    void visitDFSDirectedCycleNodes(int source, GraphInterface DFSTree) {
        founded[source] = true;
        for (Integer neighbor : myGraph.getNeighbors(source)) {
            if (!founded[neighbor]) {
                founded[neighbor] = true;
                fathers[neighbor] = source;
                DFSTree.addEdge(source, neighbor);
                visitDFSDirectedCycleNodes(neighbor, DFSTree);
            } else if (!terminated[neighbor]) {
                startCycleNode = neighbor;
                endCycleNode = source;
                return;
            }
        }
        terminated[source] = true;
    }


    private
    boolean visitDFSDirectedCycle(int source) {
        founded[source] = true;
        for (Integer neighbor : myGraph.getNeighbors(source)) {
            if (!founded[neighbor]) {
                founded[neighbor] = true;
                if (visitDFSDirectedCycle(neighbor)) {
                    return true;
                }
            } else if (!terminated[neighbor]) {
                return true;
            }
        }
        terminated[source] = true;
        return false;
    }


    private
    boolean visitDFSUndirectedCyles(int source) {
        founded[source] = true;
        for (Integer neighbor : myGraph.getNeighbors(source)) {
            if (!founded[neighbor]) {
                fathers[neighbor] = source;
                if (visitDFSUndirectedCyles(neighbor)) {
                    return true;
                }
            } else if (neighbor != fathers[source]) {
                return true;
            }
        }
        return false;
    }

    private
    void visitDFSPostOrder(int source, ArrayList<Integer> postOrder) {
        founded[source] = true;
        for (Integer neighbor : myGraph.getNeighbors(source)) {
            if (!founded[neighbor]) {
                founded[neighbor] = true;
                visitDFSPostOrder(neighbor, postOrder);
            }
        }
        postOrder.add(source);
    }

    private
    void inizializeIstanceVars() {
        Arrays.fill(founded, false);
        Arrays.fill(fathers, -1);
        Arrays.fill(terminated, false);
    }

    public
    boolean hasUndirectedCycle() {
        inizializeIstanceVars();
        if (myGraph instanceof DirectedGraph) throw new IllegalArgumentException("Invalid Graph Type");
        return visitDFSUndirectedCyles(0);
    }

    public
    ArrayList<Integer> getNodesInOrderPostVisit(int sorgente) {
        inizializeIstanceVars();

        ArrayList<Integer> postOrder = new ArrayList<>();
        visitDFSPostOrder(sorgente, postOrder);
        return postOrder;
    }

    public
    boolean hasDirectedCycle() {
        inizializeIstanceVars();
        if (myGraph instanceof UndirectedGraph) throw new IllegalArgumentException("Invalid Graph Type");
        return visitDFSDirectedCycle(0);
    }

    public
    ArrayList<Integer> getDirCycle() {
        inizializeIstanceVars();
        GraphInterface DFSTree = myGraph.create();
        visitDFSDirectedCycleNodes(0, DFSTree);
        if (startCycleNode == -1 && endCycleNode == -1) return null;
        int temp = endCycleNode;
        System.out.println(DFSTree);
        ArrayList<Integer> cycleNodes = new ArrayList<>();
        while (fathers[temp] == startCycleNode) {
            System.out.println(temp);
            cycleNodes.add(temp);
            temp = fathers[temp];
        }
        cycleNodes.add(temp);
        return cycleNodes;
    }
}
