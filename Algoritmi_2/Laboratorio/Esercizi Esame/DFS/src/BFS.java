import it.uniupo.graphLib.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public
class BFS {

    private final GraphInterface myGraph;
    private final int orderMyGraph;
    private boolean[] founded;
    private int[] fathers;

    public
    BFS(GraphInterface g) {
        myGraph = g;
        orderMyGraph = myGraph.getOrder();
        founded = new boolean[orderMyGraph];
        fathers = new int[orderMyGraph];
    }

    private
    boolean visitBFSCycles(int sorg) {
        founded[sorg] = true;
        Queue<Integer> queue = new LinkedList<>();
        queue.add(sorg);
        while (!queue.isEmpty()) {
            int node = queue.remove();
            for (Integer neighbor : myGraph.getNeighbors(node)) {
                if (!founded[neighbor]) {
                    founded[neighbor] = true;
                    queue.add(neighbor);
                    fathers[neighbor] = node;
                } else if (neighbor != fathers[node]) {
                    return true;
                }
            }
        }
        return false;
    }

    private
    void inizializeIstanceVars() {
        Arrays.fill(founded, false);
        Arrays.fill(fathers, -1);
    }


    /* Restituisce true se il grafo non
     * orientato ha un ciclo non banale (comportamento indefinito su grafo orientato):
     * potete implementarlo prima solo per un grafo non orientato connesso, poi per il
     * caso generale
     */
    public
    boolean hasUndirectedCycle() {
        if (myGraph instanceof DirectedGraph) throw new IllegalArgumentException("Invalid GraphType");
        inizializeIstanceVars();
        return visitBFSCycles(0);
    }

    public
    ArrayList<Integer> getNodesInOrderPostVisit(int i) {
        inizializeIstanceVars();
        return null;
    }
}
