import it.uniupo.graphLib.DirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public
class TestSoftwareSystem {
    @Test
    void testCostructor() {
        DirectedGraph g = new DirectedGraph("3");
        SoftwareSystem testSoftware = new SoftwareSystem(g);
        Assertions.assertNotNull(testSoftware);
    }

    @Test
    void testNoCycle() {
        {
            DirectedGraph g = new DirectedGraph("3; 0 1; 1 2");
            SoftwareSystem testSoftware = new SoftwareSystem(g);
            Assertions.assertFalse(testSoftware.hasCycle());
        }
        {
            DirectedGraph g = new DirectedGraph("10; 0 1; 1 2; 1 3; 3 4; 1 4; 8 9; 7 6");
            SoftwareSystem testSoftware = new SoftwareSystem(g);
            Assertions.assertFalse(testSoftware.hasCycle());
        }
    }

    @Test
    void testCycle() {
        {
            DirectedGraph g = new DirectedGraph("3; 0 1; 1 0");
            SoftwareSystem testSoftware = new SoftwareSystem(g);
            Assertions.assertTrue(testSoftware.hasCycle());
        }
        {
            DirectedGraph g = new DirectedGraph("10; 0 1; 1 2; 2 3; 3 4; 4 5; 5 6; 6 0");
            SoftwareSystem testSoftware = new SoftwareSystem(g);
            Assertions.assertTrue(testSoftware.hasCycle());
        }
    }
}
