/*
Un grafo orientato (non pesato) rappresenta un sistema software complesso in cui i nodi sono
componenti ed esiste un arco (s,t) se la componente t utilizza la componente s. Dunque: una
componente r dipende (eventualmente in modo indiretto) da una componente s se nel
grafo esiste un cammino orientato da s ad r. Un sistema di verifica della correttezza contiene
un metodo che controlla che non ci siano cicli di dipendenze nel sistema.
Scrivete una classe Java SoftwareSystem con il costruttore:
public SoftwareSystem (DirectedGraph system)
dove system è il grafo orientato, non pesato, descritto sopra;
e il metodo:
public boolean hasCycle()
che restituisce true se ci sono cicli di dipendenze nel sistema.
*/

import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.UndirectedGraph;

import java.util.ArrayList;
import java.util.Arrays;

public
class SoftwareSystem {
    private final DirectedGraph myGraph;
   private final int myOrder;

    public SoftwareSystem(DirectedGraph g){
        myGraph = g;
        myOrder= g.getOrder();
    }

    private boolean DFS(int source, boolean[] founded, boolean[] ended){
        founded[source] = true;
        for(int neighbor : myGraph.getNeighbors(source)){
           if(!founded[neighbor]){
              return DFS(neighbor, founded, ended);
           } else if (!ended[neighbor]) {
               return true; //the graph have a cycle
           }
        }
        ended[source] = true;
        return false;
    }

    public boolean hasCycle(){
        if (myOrder == 1) return false;

        boolean[] founded = new boolean[myOrder];
        Arrays.fill(founded, false);

        boolean[] ended = new boolean[myOrder];
        Arrays.fill(ended, false);

        return DFS(0, founded, ended);
    }
}
