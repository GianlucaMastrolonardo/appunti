import java.util.ArrayList;

public class TransportiPortuali {

    private final int[] myGuardagni;

    public TransportiPortuali(int[] guadagno) {
        myGuardagni = guadagno;
    }

    private int calcMSI() {
        int[] result = new int[myGuardagni.length];
        result[0] = 0;
        result[1] = myGuardagni[0];

        for (int i = 2; i < result.length; ++i) {
            result[i] = Math.max(result[i - 1], result[i - 2] + myGuardagni[i - 1]);
        }

        return result[result.length - 1];
    }

    private ArrayList<Integer> nodesOfOptimalSolution() {
        ArrayList<Integer> nodes = new ArrayList<>();
        int i = myGuardagni.length - 1;

        while (i > 1) {
            if (myGuardagni[i] > myGuardagni[i - 1]) {
                nodes.add(myGuardagni[i]);
                i -= 2;
            } else {
                i--;
            }
        }

        return nodes;
    }

    
}