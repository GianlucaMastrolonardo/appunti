public class Atletica {

    private final int numeroDiscipline;

    public Atletica(int numeroDiscipline) {
        this.numeroDiscipline = numeroDiscipline;
    }

    private int MSI(int[] rendAtleta) {
        int result[] = new int[numeroDiscipline];
        result[0] = 0;
        result[1] = rendAtleta[0];
        for (int i = 2; i < numeroDiscipline; ++i) {
            result[i] = Math.max(result[i - 1], result[i - 2] + rendAtleta[i - 1]);
        }
        return result[numeroDiscipline - 1];
    }

    public int scelta(int[] rendAtleta1, int[] rendAtleta2) {
        if (rendAtleta1.length > numeroDiscipline)
            throw new IllegalArgumentException("Lunghezza attività atleta1 errata");
        if (rendAtleta2.length > numeroDiscipline)
            throw new IllegalArgumentException("Lunghezza attività atleta2 errata");

        int ris1 = MSI(rendAtleta1);
        int ris2 = MSI(rendAtleta2);

        if (ris1 > ris2) return 1;
        if (ris2 > ris1) return 2;
        else return 0;
    }


}
