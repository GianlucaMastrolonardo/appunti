import it.uniupo.graphLib.UndirectedGraph;

import java.util.*;

public
class Metro {

    private final UndirectedGraph myMap;
    private final int orderOfMyMap;

    private boolean[] founded;
    private int[] fathers;

    Metro(UndirectedGraph mappa) {
        myMap = mappa;
        orderOfMyMap = myMap.getOrder();
        founded = new boolean[orderOfMyMap];
        fathers = new int[orderOfMyMap];
    }

    private
    void BFS(int source) {
        founded[source] = true;
        Queue<Integer> queue = new LinkedList<>();
        queue.add(source);

        while (!queue.isEmpty()) {
            int node = queue.remove();
            for (Integer neighbor : myMap.getNeighbors(node)) {
                if (!founded[neighbor]) {
                    queue.add(neighbor);
                    founded[neighbor] = true;
                    fathers[neighbor] = node;
                }
            }
        }

    }


    public
    ArrayList<Integer> fermate(int startStations, int endStation) {
        if (startStations < 0 || startStations > orderOfMyMap)
            throw new IllegalArgumentException("Stazione di partenza errata!");
        if (endStation < 0 || endStation > orderOfMyMap)
            throw new IllegalArgumentException("Stazione di destinazione errata!");

        Arrays.fill(founded, false);
        Arrays.fill(fathers, -1);

        BFS(startStations);
        if (fathers[endStation] == -1) return null;

        ArrayList<Integer> middleStations = new ArrayList<>();

        int temp = endStation;
        while (temp != -1) {
            middleStations.add(temp);
            temp = fathers[temp];
        }
        //middleStations.add(startStations);
        Collections.reverse(middleStations);
        return middleStations;
    }
}