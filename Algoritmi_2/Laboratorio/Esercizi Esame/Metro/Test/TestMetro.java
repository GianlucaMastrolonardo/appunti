import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public
class TestMetro {
    @Test
    void testConstructor() {
        UndirectedGraph g = new UndirectedGraph("3; 0 1; 1 2");
        Metro metroTest = new Metro(g);
        Assertions.assertNotNull(metroTest);
    }

    @Test
    void testNodes() {
        {
            UndirectedGraph g = new UndirectedGraph("6; 0 1; 1 2; 2 3; 3 4; 0 4");
            Metro metroTest = new Metro(g);
            ArrayList<Integer> expectedResult = new ArrayList<>();
            int startPos = 0;
            int endPos = 3;
            expectedResult.add(0);
            expectedResult.add(4);
            expectedResult.add(3);
            Assertions.assertEquals(expectedResult, metroTest.fermate(startPos, endPos));
        }
        {
            UndirectedGraph g = new UndirectedGraph("6; 0 1; 1 2; 2 3; 3 4; 0 4");
            Metro metroTest = new Metro(g);
            ArrayList<Integer> expectedResult = new ArrayList<>();
            int startPos = 0;
            int endPos = 4;
            expectedResult.add(0);
            expectedResult.add(4);
            Assertions.assertEquals(expectedResult, metroTest.fermate(startPos, endPos));
        }
        {
            UndirectedGraph g = new UndirectedGraph("6; 0 1; 1 2; 2 3; 3 4; 0 4");
            Metro metroTest = new Metro(g);
            int startPos = 0;
            int endPos = 5;
            Assertions.assertNull(metroTest.fermate(startPos, endPos));
        }
    }
}
