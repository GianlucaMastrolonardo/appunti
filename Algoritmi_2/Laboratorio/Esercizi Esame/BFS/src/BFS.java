import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.Graph;
import it.uniupo.graphLib.GraphInterface;

import java.util.*;

public
class BFS {
    private final
    GraphInterface myGraph;
    private final int orderMyGraph;

    private boolean[] founded;

    public
    BFS(GraphInterface g) {
        myGraph = g;
        orderMyGraph = myGraph.getOrder();

        founded = new boolean[orderMyGraph];
    }

    private
    boolean isValidNode(int node) {
        return (node >= 0 && node < orderMyGraph);
    }

    private
    ArrayList<Integer> implementationBFS(int sorg) {
        Arrays.fill(founded, false);
        Queue<Integer> queue = new LinkedList<>();
        ArrayList<Integer> visitOrder = new ArrayList<>();

        founded[sorg] = true;
        queue.add(sorg);
        visitOrder.add(sorg);

        while (!queue.isEmpty()) {
            int extractedNode = queue.remove();
            for (Integer neighbor : myGraph.getNeighbors(extractedNode)) {
                if (!founded[neighbor]) {
                    founded[neighbor] = true;
                    queue.add(neighbor);
                    visitOrder.add(neighbor);
                }
            }
        }
        return visitOrder;
    }

    private
    int[] implementationBFSDistance(int sorg) {
        int[] distance = new int[orderMyGraph];
        Arrays.fill(distance, -1);
        Arrays.fill(founded, false);
        Queue<Integer> queue = new LinkedList<>();
        founded[sorg] = true;
        distance[sorg] = 0;
        queue.add(sorg);
        while (!queue.isEmpty()) {
            int extractedNode = queue.remove();
            for (Integer neighbor : myGraph.getNeighbors(extractedNode)) {
                if (!founded[neighbor]) {
                    founded[neighbor] = true;
                    distance[neighbor] = distance[extractedNode] + 1;
                    queue.add(neighbor);
                }
            }
        }
        return distance;
    }

    private
    GraphInterface implementationBFSTree(int sorg) {
        GraphInterface treeBFS = myGraph.create();
        Arrays.fill(founded, false);
        Queue<Integer> queue = new LinkedList<>();
        founded[sorg] = true;
        queue.add(sorg);
        while (!queue.isEmpty()) {
            int extractedNode = queue.remove();
            for (Integer neighbor : myGraph.getNeighbors(extractedNode)) {
                if (!founded[neighbor]) {
                    founded[neighbor] = true;
                    treeBFS.addEdge(extractedNode, neighbor);
                    queue.add(neighbor);
                }
            }
        }
        return treeBFS;
    }

    private
    int[] implementationBFSFathers(int sorg) {
        int[] fathers = new int[orderMyGraph];
        Arrays.fill(founded, false);
        Arrays.fill(fathers, -1);
        Queue<Integer> queue = new LinkedList<>();
        founded[sorg] = true;
        fathers[sorg] = sorg;
        queue.add(sorg);
        while (!queue.isEmpty()) {
            int extractedNode = queue.remove();
            for (Integer neighbor : myGraph.getNeighbors(extractedNode)) {
                if (!founded[neighbor]) {
                    founded[neighbor] = true;
                    queue.add(neighbor);
                    fathers[neighbor] = extractedNode;
                }
            }
        }
        return fathers;
    }


    public
    ArrayList<Integer>
    getNodesInOrderOfVisit(int source) {
        if (!isValidNode(source)) throw new IllegalArgumentException("Errore");
        return implementationBFS(source);
    }

    public
    int[] getDistanza(int sorgente) {
        if (!isValidNode(sorgente)) throw new IllegalArgumentException("Errore");
        return implementationBFSDistance(sorgente);
    }


    public
    int getDistanza(int sorgente, int nodo) {
        if (!isValidNode(sorgente) && !isValidNode(nodo)) {
            throw new IllegalArgumentException("Sorgente o nodo non valide");
        }
        return implementationBFSDistance(sorgente)[nodo];
    }

    public
    GraphInterface bfsTree(int sorgente) {
        if (!isValidNode(sorgente)) throw new IllegalArgumentException("Errore");
        return implementationBFSTree(sorgente);
    }

    public
    ArrayList<Integer> camminoMinimo(int sorgente, int dest) {
        if (!isValidNode(sorgente) && !isValidNode(dest)) {
            throw new IllegalArgumentException("Sorgente o nodo non valide");
        }
        int[] fathers = implementationBFSFathers(sorgente);
        if (fathers[dest] == -1) return null;

        int node = dest;
        ArrayList<Integer> minPath = new ArrayList<>();
        while (node != sorgente) {
            minPath.add(node);
            node = fathers[node];
        }
        minPath.add(sorgente);
        Collections.reverse(minPath);

        return minPath;
    }
}
