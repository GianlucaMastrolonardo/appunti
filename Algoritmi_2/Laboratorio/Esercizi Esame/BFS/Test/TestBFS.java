import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

import java.util.ArrayList;

public
class TestBFS {

    @Test
    void testCostructor() {
        UndirectedGraph g = new UndirectedGraph("1");
        BFS bfsTest = new BFS(g);
        Assertions.assertNotNull(bfsTest);
    }

    @Test
    void testNodesInOrderVisit() {
        {
            UndirectedGraph g = new UndirectedGraph("4; 0 1; 1 2; 0 3");
            BFS bfsTest = new BFS(g);
            ArrayList<Integer> nodes = bfsTest.getNodesInOrderOfVisit(0);
            Assertions.assertTrue((nodes.indexOf(1) == 1 && nodes.indexOf(2) == 3) || (nodes.indexOf(1) == 3 && nodes.indexOf(2) == 1));
        }
        {
            UndirectedGraph g = new UndirectedGraph("4; 0 1; 1 2; 0 3");
            BFS bfsTest = new BFS(g);
            ArrayList<Integer> nodes = bfsTest.getNodesInOrderOfVisit(1);
            Assertions.assertTrue((nodes.indexOf(1) == 2 && nodes.indexOf(2) == 0) || (nodes.indexOf(1) == 0 && nodes.indexOf(2) == 2));
        }
    }

    @Test
    void testDistance() {
        {
            UndirectedGraph g = new UndirectedGraph("4; 0 1; 1 2; 0 3");
            BFS bfsTest = new BFS(g);
            int source = 0;
            int[] distance = bfsTest.getDistanza(source);
            Assertions.assertEquals(distance[source], 0);
            Assertions.assertEquals(distance[3], 1);
            Assertions.assertEquals(distance[2], 2);
        }
        {
            UndirectedGraph g = new UndirectedGraph("6; 0 1; 0 2; 0 3; 0 4; 0 5;");
            BFS bfsTest = new BFS(g);
            int source = 0;
            int[] distance = bfsTest.getDistanza(source);
            for (int i = 1; i < g.getOrder(); ++i) Assertions.assertEquals(distance[i], 1);
        }
        {
            UndirectedGraph g = new UndirectedGraph("6; 0 1; 1 2; 2 3; 3 4; 4 5; 0 5");
            BFS bfsTest = new BFS(g);
            int source = 0;
            int[] distance = bfsTest.getDistanza(source);
            Assertions.assertEquals(distance[5], 1);
            Assertions.assertEquals(distance[4], 2);
        }
    }

    @Test
    void testDistanceNode() {
        {
            UndirectedGraph g = new UndirectedGraph("6; 0 1; 1 2; 2 3; 3 4;");
            BFS bfsTest = new BFS(g);
            int source = 0;
            int destination = 4;
            int distance = bfsTest.getDistanza(source, destination);
            Assertions.assertEquals(distance, 4);
        }
        {
            UndirectedGraph g = new UndirectedGraph("6; 0 1; 1 2; 2 3; 3 4;");
            BFS bfsTest = new BFS(g);
            int source = 0;
            int destination = 5;
            int distance = bfsTest.getDistanza(source, destination);
            Assertions.assertEquals(distance, -1);
        }
    }

    @Test
    void testBFSTree() {
        {
            UndirectedGraph g = new UndirectedGraph("3; 0 1; 1 2; 2 1; 0 2;");
            BFS bfsTest = new BFS(g);
            int source = 0;
            GraphInterface bfSTree = bfsTest.bfsTree(source);
            Assertions.assertFalse(bfSTree.hasEdge(1, 2));
            Assertions.assertTrue(bfSTree.hasEdge(0, 1) && bfSTree.hasEdge(0, 2));
            Assertions.assertEquals(bfSTree.getEdgeNum(), g.getOrder() - 1);
        }
        {
            UndirectedGraph g = new UndirectedGraph("6; 0 1; 1 2; 2 3; 2 4; 0 5; 5 2;");
            BFS bfsTest = new BFS(g);
            int source = 0;
            GraphInterface bfSTree = bfsTest.bfsTree(source);
            Assertions.assertFalse(bfSTree.hasEdge(5, 2));
            Assertions.assertEquals(bfSTree.getEdgeNum(), g.getOrder() - 1);
        }
    }

    @Test
    void testMinPath() {
        {
            UndirectedGraph g = new UndirectedGraph("3; 0 1; 1 2; 0 2;");
            BFS bfsTest = new BFS(g);
            int source = 0;
            int dest = 2;
            ArrayList<Integer> minPath = bfsTest.camminoMinimo(source, dest);
            System.out.println(minPath);
            Assertions.assertTrue(minPath.size() == 2 && minPath.getFirst() == 0 && minPath.getLast() == 2);
        }
    }
}


