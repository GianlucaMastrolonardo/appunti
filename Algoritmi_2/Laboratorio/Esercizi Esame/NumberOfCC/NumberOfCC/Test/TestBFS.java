import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public
class TestBFS {

    @Test
    void TestCostructor() {
        UndirectedGraph g = new UndirectedGraph("3; 0 1; 1 2; 0 2");
        BFS bfsTest = new BFS(g);
        Assertions.assertNotNull(bfsTest);
    }

    @Test
    void TestCC() {
        UndirectedGraph g = new UndirectedGraph("3;");
        BFS bfsTest = new BFS(g);
        System.out.println(bfsTest.getCC());
    }
}
