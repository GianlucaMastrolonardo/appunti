import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.GraphUtils;

import javax.sound.sampled.Line;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public
class BFS {
    private final GraphInterface myGraph;
    private final int orderGraph;
    private boolean[] founded;


    public
    BFS(GraphInterface g) {
        myGraph = g;
        orderGraph = myGraph.getOrder();
        founded = new boolean[orderGraph];
    }

    private
    ArrayList<Integer> visitBFS(int source) {
        ArrayList<Integer> orderVisit = new ArrayList<>();
        founded[source] = true;

        Queue<Integer> queueBFS = new LinkedList<>();
        queueBFS.add(source);
        orderVisit.add(source);

        while (!queueBFS.isEmpty()) {
            int node = queueBFS.remove();
            for (Integer neighbor : myGraph.getNeighbors(node)) {
                if (!founded[neighbor]) {
                    queueBFS.add(neighbor);
                    founded[neighbor] = true;
                    orderVisit.add(neighbor);
                }
            }
        }
        return orderVisit;
    }

    public
    int getCC() {
        Arrays.fill(founded, false);
        int[] connectedComponentsArr = new int[orderGraph];
        Arrays.fill(connectedComponentsArr, -1);

        int cnt = 0;

        for (int node = 0; node < orderGraph; ++node) {
            if (!founded[node]) {
                ArrayList<Integer> nodes = visitBFS(node);
                for (int n : nodes) {
                    connectedComponentsArr[n] = cnt;
                }
                cnt++;
            }
        }
        return cnt;
    }

}
