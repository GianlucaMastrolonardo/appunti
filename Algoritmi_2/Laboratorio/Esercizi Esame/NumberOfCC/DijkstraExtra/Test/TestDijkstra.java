import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public
class TestDijkstra {
    @Test
    void testCostructor() {
        UndirectedGraph myGraph = new UndirectedGraph("4; 0 1 4; 0 2 2; 1 2 8; 2 3 2");
        Dijkstra dTest = new Dijkstra(myGraph);
        Assertions.assertNotNull(dTest);
    }

    @Test
    void testPath() {
        UndirectedGraph myGraph = new UndirectedGraph("4; 0 1 4; 0 2 2; 1 2 1; 2 3 2");
        Dijkstra dTest = new Dijkstra(myGraph);
        System.out.println(dTest.getAlberoCamminiMinimi(0));
        //Assertions.assertNotNull(dTest);
    }

    @Test
    void testPathSourceDest() {
        UndirectedGraph myGraph = new UndirectedGraph("4; 0 1 4; 0 2 2; 1 2 1; 2 3 2");
        Dijkstra dTest = new Dijkstra(myGraph);
        System.out.println(dTest.getCamminoMinimo(0, 1));
        //Assertions.assertNotNull(dTest);
    }
}
