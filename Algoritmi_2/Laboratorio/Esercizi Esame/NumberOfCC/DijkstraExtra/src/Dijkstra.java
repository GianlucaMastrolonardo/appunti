import it.uniupo.algoTools.MinHeap;
import it.uniupo.graphLib.*;

import javax.swing.undo.UndoableEdit;
import java.util.ArrayList;
import java.util.Arrays;

public
class Dijkstra {
    private final
    GraphInterface myGraph;

    private final int graphOrder;

    public
    Dijkstra(GraphInterface g) {
        myGraph = g;
        graphOrder = myGraph.getOrder();
    }

    private
    GraphInterface dijkstraAlgorithm(int source, int... destinationVar) {
        int destination = -1;
        if (destinationVar.length > 1) {
            throw new IllegalArgumentException("Invalid Destination");
        } else if (destinationVar.length == 1) {
            destination = destinationVar[0];
        }

        GraphInterface minPathThree = myGraph.create();

        boolean[] founded = new boolean[graphOrder];
        Arrays.fill(founded, false);
        int[] distance = new int[graphOrder];
        Arrays.fill(distance, -1);

        founded[source] = true;
        distance[source] = 0;
        MinHeap<Edge, Integer> minHeap = new MinHeap<>();
        System.out.println(destination);

        for (Edge e : myGraph.getOutEdges(source)) {
            minHeap.add(e, distance[source] + e.getWeight());
        }

        while (!minHeap.isEmpty()) {
            Edge e = minHeap.extractMin();
            int tail = e.getTail(); //Nodo u
            int head = e.getHead(); //Nodo v
            if (!founded[head]) {
                founded[head] = true;
                distance[head] = distance[tail] + e.getWeight();
                minPathThree.addEdge(e);
                if (destination != -1 && destination == head) {
                    return minPathThree;
                }
                for (Edge z : myGraph.getOutEdges(head)) {
                    if (!founded[z.getHead()]) minHeap.add(z, distance[head] + z.getWeight());
                }
            }
        }

        return minPathThree;

    }

    public
    GraphInterface getAlberoCamminiMinimi(int sorg) {
        return dijkstraAlgorithm(sorg);
    }

    public
    ArrayList<Edge> getCamminoMinimo(int sorg, int destinazione) {
        System.out.println(dijkstraAlgorithm(sorg, destinazione));
        return null;
    }

}
