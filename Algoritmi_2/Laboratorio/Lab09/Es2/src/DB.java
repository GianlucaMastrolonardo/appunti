import it.uniupo.algoTools.MaxHeap;

import java.util.Arrays;

public class DB {
    private final double capacity;
    private final int[] dim;
    private final double[] time;

    public DB(int[] dim, double[] time) {
        this.capacity = dim.length;
        this.dim = dim;
        this.time = time;
    }


    private double fracKnapsackImpl(double[] dose, double[] quant) {
        double valTot = 0;
        double remaningSpace = capacity;

        MaxHeap<Integer, Double> maxHeap = new MaxHeap<>();

        for (int i = 0; i < dose.length; ++i) {
            maxHeap.add(i, time[i] / dim[i]);
        }

        while (!maxHeap.isEmpty() && remaningSpace > 0) {
            // Estraggo il materiale con valore unitario massimo
            int extractedMaterial = maxHeap.extractMax();

            //Se posso prendererlo tutto senza riempite lo zaiono lo prendo tutto
            if (remaningSpace >= dim[extractedMaterial]) {
                dose[extractedMaterial] = 1;
                quant[extractedMaterial] = dim[extractedMaterial];
                valTot += dim[extractedMaterial];
            }
            //Altrimenti ne prendo una frazione
            else {
                dose[extractedMaterial] = remaningSpace / dim[extractedMaterial];
                quant[extractedMaterial] = remaningSpace;
                valTot += (time[extractedMaterial] * dose[extractedMaterial]);
            }
            remaningSpace -= quant[extractedMaterial];
        }
        return valTot;
    }

    public double timeToRebuild(int memSpace) {
        if (memSpace < 0) {
            throw new IllegalArgumentException("MemSpace negativo");
        }
        double[] dose = new double[memSpace];
        double[] quant = new double[memSpace];
    }

}


