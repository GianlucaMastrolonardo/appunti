import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class testFracKnapsack {
    @Test
    void testCreate() {
        double capacity = 10;
        double[] volume = new double[(int) capacity];
        double[] value = new double[(int) capacity];

        for (int i = 0; i < capacity; ++i) {
            volume[i] = i * 10;
            value[i] = i * 2.2;
        }

        FracKnapsack knapsack = new FracKnapsack(capacity, volume, value);
        Assertions.assertNotNull(knapsack);
    }

    @Test
    void testOneElement() {
        //capacità > volume elemento
        double capacity = 7;
        double[] volume = new double[(int) capacity];
        double[] value = new double[(int) capacity];

        value[0] = 10;
        volume[0] = 3;

        FracKnapsack knapsack = new FracKnapsack(capacity, volume, value);

        Assertions.assertEquals(10, knapsack.maxVal());
        Assertions.assertEquals(1, knapsack.dose(0));

        //capacità < volume elemento
        capacity = 2;
        value[0] = 10;
        volume[0] = 4;

        FracKnapsack knapsack2 = new FracKnapsack(capacity, volume, value);

        Assertions.assertEquals(5, knapsack2.maxVal());
        Assertions.assertEquals(0.5, knapsack2.dose(0));
    }


    @Test
    void testThreeElements() {
        //1 test in cui somma di tutti i volumi < capacità
        //es: valori = 2,5,1; volumi = 3,2,4; capacita = 10; quindi dosi= 1,1,1 valoreMassimo = 8

        double capacity = 10;
        double[] volume = new double[(int) capacity];
        double[] value = new double[(int) capacity];

        value[0] = 2;
        value[1] = 5;
        value[2] = 1;

        volume[0] = 3;
        volume[1] = 2;
        volume[2] = 4;

        FracKnapsack knapsack = new FracKnapsack(capacity, volume, value);

        Assertions.assertEquals(8, knapsack.maxVal());
        for (int i = 0; i < 3; ++i) {
            Assertions.assertEquals(1, knapsack.dose(i));
        }


        //1 test in cui si prende solo parte di un elemento
        //(es: valori = 10,2,0.5; volumi = 70,12,3; capacita = 3;
        //le soluzioni ottimali sono due: (0, 0.25, 0) oppure (0, 0, 1); valoreMassimo = 0.5)
        capacity = 3;

        value[0] = 10;
        value[1] = 2;
        value[2] = 0.5;

        volume[0] = 70;
        volume[1] = 12;
        volume[2] = 3;

        FracKnapsack knapsack2 = new FracKnapsack(capacity, volume, value);

        Assertions.assertEquals(0.5, knapsack2.maxVal());
        Assertions.assertTrue((knapsack2.dose(0) == 0 && knapsack2.dose(1) == 0.25 && knapsack2.dose(2) == 0) || ((knapsack2.dose(0) == 0) && knapsack2.dose(1) == 0) && knapsack2.dose(2) == 1);

        //es: valori = 5,3,1; volumi = 4,3,4; capacita = 10;
        //quindi dosi= 1.0 1.0 0.75 valoreMassimo = 8.75)

        capacity = 10;

        value[0] = 5;
        value[1] = 3;
        value[2] = 1;

        volume[0] = 4;
        volume[1] = 3;
        volume[2] = 4;

        FracKnapsack knapsack3 = new FracKnapsack(capacity, volume, value);

        Assertions.assertEquals(8.75, knapsack3.maxVal());
        Assertions.assertTrue(knapsack3.dose(0) == 1.0 && knapsack3.dose(1) == 1.0 && knapsack3.dose(2) == 0.75);
    }
}
