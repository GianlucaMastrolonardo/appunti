import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class GrafoOrientatoTest {
    GrafoOrientato testG;

    @Test
    public void testCreate() {
        testG = new GrafoOrientato(3);
        Assertions.assertNotNull(testG);
    }

    @Test
    public void testEsisteArco() {
        testG = new GrafoOrientato(3);
        testG.aggiungiArco(0, 1);
        Assertions.assertTrue(testG.esisteArco(0, 1));
    }

    @Test
    public void testEsisteArcoInput() {
        testG = new GrafoOrientato(3);
        testG.aggiungiArco(0, 1);
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            testG.aggiungiArco(0, 5);
        });
    }

    @Test
    public void testEliminaArcoInput() {
        testG = new GrafoOrientato(3);
        testG.aggiungiArco(0, 1);
        testG.aggiungiArco(1, 1);
        testG.eliminaArco(0, 1);
        Assertions.assertFalse(testG.esisteArco(0, 1));
        Assertions.assertTrue(testG.esisteArco(1, 1));
    }

    @Test
    public void testGradoUscente() {
        testG = new GrafoOrientato(4);
        testG.aggiungiArco(0, 1);
        testG.aggiungiArco(1, 2);
        testG.aggiungiArco(1, 3);
        testG.aggiungiArco(3, 3);
        Assertions.assertEquals(2, testG.gradoUscente(1));
        Assertions.assertEquals(0, testG.gradoUscente(2));
    }

    @Test
    public void testGradoEntrante() {
        testG = new GrafoOrientato(4);
        testG.aggiungiArco(0, 1);
        testG.aggiungiArco(1, 3);
        testG.aggiungiArco(3, 3);
        Assertions.assertEquals(1, testG.gradoEntrante(1));
        Assertions.assertEquals(0, testG.gradoEntrante(2));
    }

}