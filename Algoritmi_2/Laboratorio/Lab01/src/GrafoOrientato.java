public class GrafoOrientato {
    private int ordineGrafo;
    private int[][] matriceAdiacenza;

    public GrafoOrientato(int numNodi) {
        this.ordineGrafo = numNodi;
        this.matriceAdiacenza = new int[this.ordineGrafo][this.ordineGrafo];
    }

    public void aggiungiArco(int u, int v) {
        if (u < 0 || u >= this.ordineGrafo || v < 0 || v >= this.ordineGrafo) {
            throw new IllegalArgumentException("La matrice è troppo piccola");
        }
        this.matriceAdiacenza[u][v] = 1;
    }

    public boolean esisteArco(int u, int v) {
        if (u < 0 || u >= this.ordineGrafo || v < 0 || v >= this.ordineGrafo) {
            throw new IllegalArgumentException("La matrice è troppo piccola");
        }
        return (this.matriceAdiacenza[u][v] == 1);
    }

    public void eliminaArco(int u, int v) {
        if (u < 0 || u >= this.ordineGrafo || v < 0 || v >= this.ordineGrafo) {
            throw new IllegalArgumentException("La matrice è troppo piccola");
        }
        this.matriceAdiacenza[u][v] = 0;
    }

    public int gradoUscente(int u) {
        if (u < 0 || u >= this.ordineGrafo) {
            throw new IllegalArgumentException("La matrice è troppo piccola");
        }
        int cnt = 0;
        for (int i = 0; i < this.ordineGrafo; i++) {
            if (this.matriceAdiacenza[u][i] == 1) {
                cnt++;
            }
        }
        return cnt;
    }

    public int gradoEntrante(int u) {
        if (u < 0 || u >= this.ordineGrafo) {
            throw new IllegalArgumentException("La matrice è troppo piccola");
        }
        int cnt = 0;
        for (int i = 0; i < this.ordineGrafo; i++) {
            if (this.matriceAdiacenza[i][u] == 1) {
                cnt++;
            }
        }
        return cnt;
    }
}
