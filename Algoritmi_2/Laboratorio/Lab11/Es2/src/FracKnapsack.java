import it.uniupo.algoTools.MaxHeap;

import java.util.Arrays;

public class FracKnapsack {

    private final double capacity;
    private final double[] volume;
    private final double[] value;

    public FracKnapsack(double capacity, double[] volume, double[] value) {
        this.capacity = capacity;
        this.volume = volume;
        this.value = value;
    }

    private int fracKnapsackImpl(double[] dose, double[] quant) {
        int valTot = 0;
        int remaningSpace = capacity;

        MaxHeap<Integer, Double> maxHeap = new MaxHeap<>();

        for (int i = 0; i < dose.length; ++i) {
            maxHeap.add(i, (double) value[i] / volume[i]);
        }

        int k = 0;
        while (!maxHeap.isEmpty() && remaningSpace > 0) {
            // Estraggo il materiale con valore unitario massimo
            int extractedMaterial = maxHeap.extractMax();

            //Se posso prendererlo tutto senza riempite lo zaiono lo prendo tutto
            if (remaningSpace >= volume[extractedMaterial]) {
                dose[extractedMaterial] = 1;
                quant[extractedMaterial] = volume[extractedMaterial];
                valTot += value[extractedMaterial];
                ++k;
            }
            //Altrimenti mi fermo e confronto il successivo
            else {
                break;
            }
            remaningSpace -= quant[extractedMaterial];
        }
        if (!maxHeap.isEmpty()) {
            int lastMaterial = maxHeap.extractMax();
            int maxP = Math.max(valTot, lastMaterial);
            // return (double) val / vol;
        } else {
            return valTot;
        }
    }

    public double maxVal() {
        double[] dose = new double[volume.length];
        double[] quant = new double[volume.length];

        return fracKnapsackImpl(dose, quant);
    }

    public double dose(int i) {
        if (i < 0 || i > volume.length) {
            throw new IllegalArgumentException("Valore di dose inserito errato");
        }
        double[] dose = new double[volume.length];
        double[] quant = new double[volume.length];

        fracKnapsackImpl(dose, quant);
        return dose[i];
    }

    public boolean more(int i, int j) {
        if ((i < 0 || i > volume.length) || (j < 0 || j > volume.length)) {
            throw new IllegalArgumentException("Valore di dose inserito errato");
        }
        double[] dose = new double[volume.length];
        double[] quant = new double[volume.length];

        fracKnapsackImpl(dose, quant);
        return quant[i] > quant[j];
    }
}
