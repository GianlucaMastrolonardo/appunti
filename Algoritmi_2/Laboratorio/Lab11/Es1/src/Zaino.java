import java.util.Arrays;

public class Zaino {

    private final int[] myValues;
    private final int[] myVolumes;

    private final int myCapacity;


    public Zaino(int[] value, int[] volume, int capacity) {
        myValues = value;
        myVolumes = volume;
        myCapacity = capacity;
    }

    private int[][] knapsackAlgorithm() {
        int[][] matrixValues = new int[myValues.length + 1][myCapacity + 1];
        Arrays.fill(matrixValues[0], 0);

        for (int object = 1; object <= myValues.length; ++object) {
            int idObj = object - 1;
            for (int capacity = 0; capacity <= myCapacity; ++capacity) {
                if (myVolumes[idObj] <= capacity) {
                    matrixValues[object][capacity] = Math.max(matrixValues[object - 1][capacity - myVolumes[idObj]] + myValues[idObj], matrixValues[object - 1][capacity]);
                } else {
                    matrixValues[object][capacity] = matrixValues[object - 1][capacity];
                }
            }
        }
        return matrixValues;
    }

    public int getMaxVal() {
        return knapsackAlgorithm()[myValues.length][myCapacity];
    }
}
