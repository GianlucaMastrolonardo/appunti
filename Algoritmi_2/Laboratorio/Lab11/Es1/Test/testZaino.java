import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class testZaino {
    @Test
    void testCostructor() {
        int[] vol = {1, 3, 2};
        int[] val = {2, 3, 2};
        int cap = 3;
        Zaino myZaino = new Zaino(val, vol, cap);
        Assertions.assertNotNull(myZaino);
    }

    @Test
    void test3Obj() {
        int[] vol = {1, 3, 2};
        int[] val = {2, 3, 2};
        int cap = 3;
        Zaino myZaino = new Zaino(val, vol, cap);
        System.out.println(myZaino.getMaxVal());
    }
}
