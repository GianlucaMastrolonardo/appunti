import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class HikingTest {

    @Test
    void testMinDistanza() {
        UndirectedGraph graph = new UndirectedGraph("5; 0 1 13; 0 2 16; 0 4 9; 3 0 19; 3 1 14; 3 2 12; 3 4 26; 1 4 22; 1 2 7; 4 2 15");

        boolean[][] map = new boolean[graph.getOrder()][graph.getOrder()];
        for (int i = 0; i < map.length; ++i) {
            Arrays.fill(map[i], true);
        }
        // System.out.println(graph);
        Hikes hikesTest = new Hikes(graph, map);
        Assertions.assertEquals(12, hikesTest.minDistanza(3));
        Assertions.assertEquals(13, hikesTest.minDistanza(2));

        Assertions.assertTrue(hikesTest.minDistanza(2) > hikesTest.minDistanza(graph.getOrder()));
    }

    @Test
    void testMinDistanzaNonPercorribili() {
        UndirectedGraph graph = new UndirectedGraph("5; 0 1 13; 0 2 16; 0 4 9; 3 0 19; 3 1 14; 3 2 12; 3 4 26; 1 4 22; 1 2 7; 4 2 15");

        boolean[][] map = new boolean[graph.getOrder()][graph.getOrder()];
        for (int i = 0; i < map.length; ++i) {
            Arrays.fill(map[i], true);
        }
        map[0][1] = false;
        map[3][2] = false;


        Hikes hikesTest = new Hikes(graph, map);
        Assertions.assertEquals(14, hikesTest.minDistanza(3));
        Assertions.assertEquals(15, hikesTest.minDistanza(2));

        Assertions.assertTrue(hikesTest.minDistanza(2) > hikesTest.minDistanza(graph.getOrder()));
    }
}
