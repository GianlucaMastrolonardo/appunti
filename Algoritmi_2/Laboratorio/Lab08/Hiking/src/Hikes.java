import it.uniupo.algoTools.MinHeap;
import it.uniupo.algoTools.UnionByRank;
import it.uniupo.algoTools.UnionFind;
import it.uniupo.graphLib.*;

public class Hikes {

    UndirectedGraph myGraph;

    public Hikes(UndirectedGraph rifugi, boolean[][] percorribile) {
        for (int i = 0; i < percorribile.length; ++i) {
            for (int j = 0; j < percorribile[i].length; ++j) {
                if (!percorribile[i][j] && rifugi.hasEdge(i, j)) {
                    rifugi.removeEdge(i, j);
                }
            }
        }
        myGraph = rifugi;
    }

    private int kruskalClustering(int maxCluster) {
        UnionFind unionByRank = new UnionByRank(myGraph.getOrder());
        MinHeap<Edge, Integer> minHeap = new MinHeap<>();

        //UndirectedGraph copyGraph = myGraph;

        for (int i = 0; i < myGraph.getOrder(); ++i) {
            for (Edge e : myGraph.getOutEdges(i)) {
                minHeap.add(e, e.getWeight());
                // copyGraph.removeEdge(e.getTail(), e.getHead());
            }
        }

        while (!minHeap.isEmpty() && unionByRank.getNumberOfSets() > maxCluster) {
            Edge edge = minHeap.extractMin();
            int leaderTail = unionByRank.find(edge.getTail());
            int leaderHead = unionByRank.find(edge.getHead());
            if (leaderHead != leaderTail) {
                unionByRank.union(leaderHead, leaderTail);
            }
        }

        //Calcolo lo spaziamento
        while (!minHeap.isEmpty()) {
            Edge edge = minHeap.extractMin();
            int leaderTail = unionByRank.find(edge.getTail());
            int leaderHead = unionByRank.find(edge.getHead());
            if (leaderHead != leaderTail) {
                return edge.getWeight();
            }
        }

        //Se numGite è 0 o 1
        return -1;
    }

    public int minDistanza(int numGite) {
        if (numGite < 0) {
            throw new IllegalArgumentException("Il numero delle gite deve essere > 0");
        }
        return kruskalClustering(numGite);

    }

    public void printGraph() {
        System.out.println(myGraph);
    }
}
