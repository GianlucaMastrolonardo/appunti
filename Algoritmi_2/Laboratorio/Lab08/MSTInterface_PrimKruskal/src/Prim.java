import it.uniupo.algoTools.MST;
import it.uniupo.algoTools.MinHeap;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.UndirectedGraph;

public class Prim implements MST {
    private final UndirectedGraph myGraph;
    private boolean[] founded;

    public Prim(UndirectedGraph g) {
        myGraph = g;
        founded = new boolean[myGraph.getOrder()];
    }

    private int prim(int sorg, UndirectedGraph MST) {
        founded[sorg] = true;
        MinHeap<Edge, Integer> minHeap = new MinHeap<>();
        int cost = 0;

        for (Edge e : myGraph.getOutEdges(sorg)) {
            minHeap.add(e, e.getWeight());
        }

        while (!minHeap.isEmpty()) {
            Edge e = minHeap.extractMin();
            int v = e.getHead();
            if (!founded[v]) {
                founded[v] = true;
                MST.addEdge(e);
                cost += e.getWeight();
                for (Edge w : myGraph.getOutEdges(v)) {
                    minHeap.add(w, w.getWeight());
                }
            }
        }
        return cost;
    }


    @Override
    public MST create(UndirectedGraph undirectedGraph) {
        return new Prim(myGraph);
    }

    @Override
    public UndirectedGraph getMST() {
        UndirectedGraph MST = (UndirectedGraph) myGraph.create();
        prim(0, MST);
        return MST;
    }

    @Override
    public int getCost() {
        UndirectedGraph MST = (UndirectedGraph) myGraph.create();
        return prim(0, MST);
    }
}
