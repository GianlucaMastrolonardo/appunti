import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestPrim {
    @Test
    void testNullCostructor() {
        UndirectedGraph g = new UndirectedGraph("4;1 2 2;1 0 5;2 3 3; 0 3 1");
        Prim prim = new Prim(g);
        Assertions.assertNotNull(prim);
    }

    @Test
    void testGetMST() {
        UndirectedGraph g = new UndirectedGraph("4;1 2 2;1 0 5;2 3 3; 0 3 1");
        Prim prim = new Prim(g);
        UndirectedGraph MST = prim.getMST();
        Assertions.assertTrue(MST.hasEdge(0, 3));
        Assertions.assertTrue(MST.hasEdge(3, 2));
        Assertions.assertTrue(MST.hasEdge(2, 1));
        Assertions.assertFalse(MST.hasEdge(0, 1));
    }

    @Test
    void testGetCost() {
        UndirectedGraph g = new UndirectedGraph("4;1 2 2;1 0 5;2 3 3; 0 3 1");
        Prim prim = new Prim(g);
        Assertions.assertEquals(6, prim.getCost());
    }
}
