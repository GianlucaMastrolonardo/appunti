import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestKruskal {
    @Test
    void testNullCostructor() {
        UndirectedGraph g = new UndirectedGraph("4;1 2 2;1 0 5;2 3 3; 0 3 1");
        Kruskal kruskal = new Kruskal(g);
        Assertions.assertNotNull(kruskal);
    }

    @Test
    void testGetMST() {
        UndirectedGraph g = new UndirectedGraph("4;1 2 2;1 0 5;2 3 3; 0 3 1");
        Kruskal kruskal = new Kruskal(g);
        UndirectedGraph MST = kruskal.getMST();
        Assertions.assertTrue(MST.hasEdge(0, 3));
        Assertions.assertTrue(MST.hasEdge(3, 2));
        Assertions.assertTrue(MST.hasEdge(2, 1));
        Assertions.assertFalse(MST.hasEdge(0, 1));
    }

    @Test
    void testGetCost() {
        UndirectedGraph g = new UndirectedGraph("4;1 2 2;1 0 5;2 3 3; 0 3 1");
        Kruskal kruskal = new Kruskal(g);
        Assertions.assertEquals(6, kruskal.getCost());
    }
}
