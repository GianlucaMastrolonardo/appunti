import it.uniupo.algoTools.MinHeap;
import it.uniupo.algoTools.UnionByRank;
import it.uniupo.algoTools.UnionFind;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.UndirectedGraph;
import it.uniupo.graphLib.*;


public class Kruskal {
    private final UndirectedGraph myGraph;

    public Kruskal(UndirectedGraph g) {
        myGraph = g;
    }

    private int kruskal(UndirectedGraph MST) {
        UndirectedGraph myGraphCopy = myGraph;
        MinHeap<Edge, Integer> minHeap = new MinHeap<>();
        int cost = 0;

        for (int i = 0; i < myGraph.getOrder(); ++i) {
            for (Edge e : myGraphCopy.getOutEdges(i)) {
                minHeap.add(e, e.getWeight());
                myGraphCopy.removeEdge(e.getTail(), e.getHead());
            }
        }

        UnionFind unionFind = new UnionByRank(myGraph.getOrder());

        while (!minHeap.isEmpty()) {
            Edge e = minHeap.extractMin();
            int tail = e.getTail();
            int head = e.getHead();
            int tailLeader = unionFind.find(tail);
            int headLeader = unionFind.find(head);

            if (tailLeader != headLeader) {
                MST.addEdge(e);
                cost += e.getWeight();
                unionFind.union(tailLeader, headLeader);
            }
        }
        return cost;
    }

    public UndirectedGraph getMST() {
        UndirectedGraph MST = (UndirectedGraph) myGraph.create();
        kruskal(MST);
        return MST;
    }

    public int getCost() {
        UndirectedGraph MST = (UndirectedGraph) myGraph.create();
        return kruskal(MST);
    }

}
