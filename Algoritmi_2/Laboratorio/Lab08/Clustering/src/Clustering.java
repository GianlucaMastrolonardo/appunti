import it.uniupo.algoTools.MinHeap;
import it.uniupo.algoTools.UnionByRank;
import it.uniupo.algoTools.UnionFind;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.UndirectedGraph;

public class Clustering {
    private final UndirectedGraph myGraph;
    private final int clusterCnt;

    public Clustering(UndirectedGraph g, int numCluster) {
        myGraph = g;
        clusterCnt = numCluster;
    }

    private int kruskalClustering(UnionFind unionFind) {
        MinHeap<Edge, Integer> minHeap = new MinHeap<>();
        for (int u = 0; u < myGraph.getOrder(); ++u) {
            for (Edge uv : myGraph.getOutEdges(u)) {
                minHeap.add(uv, uv.getWeight());
            }
        }

        while (!minHeap.isEmpty() && unionFind.getNumberOfSets() > clusterCnt) {
            Edge e = minHeap.extractMin();
            int tail = e.getTail();
            int head = e.getHead();
            int tailLeader = unionFind.find(tail);
            int headLeader = unionFind.find(head);
            unionFind.union(tailLeader, headLeader);
        }
        if (minHeap.isEmpty()) return -99; //Se non ci sono più archi

        boolean isAValidEdge = false;
        int spaziamento = -99; //Se non ci sono più archi validi

        while (!minHeap.isEmpty() && !isAValidEdge) {
            Edge e = minHeap.extractMin();
            int tail = e.getTail();
            int head = e.getHead();
            int tailLeader = unionFind.find(tail);
            int headLeader = unionFind.find(head);
            if (tailLeader != headLeader) {
                isAValidEdge = true;
                spaziamento = e.getWeight();
            }
        }

        return spaziamento;
    }

    public UnionFind getClusters() {
        UnionFind unionFind = new UnionByRank(myGraph.getOrder());
        kruskalClustering(unionFind);
        return unionFind;
    }

    public int spaziamento() {
        UnionFind unionFind = new UnionByRank(myGraph.getOrder());
        return kruskalClustering(unionFind);
    }

    public boolean sameCluster(int a, int b) {
        UnionFind unionFind = new UnionByRank(myGraph.getOrder());
        kruskalClustering(unionFind);
        return unionFind.find(a) == unionFind.find(b);
    }

}
