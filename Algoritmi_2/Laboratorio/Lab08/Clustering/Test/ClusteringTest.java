import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ClusteringTest {
    @Test
    void dada() {
        UndirectedGraph g = new UndirectedGraph("8; 0 1 11; 0 7 9; 1 4 1; 1 7 1; 1 2 2; 2 3 3; 3 4 1; 4 5 2; 5 7 8");
        Clustering clu = new Clustering(g, 5);
    }

    @Test
    public void test4Clusters() {
        UndirectedGraph g = new UndirectedGraph("4; 0 1 2; 1 3 7; 3 2 4; 2 0 1");
        Clustering clustering = new Clustering(g, 4);

        Assertions.assertFalse(clustering.sameCluster(0, 1));
        Assertions.assertFalse(clustering.sameCluster(0, 2));
        Assertions.assertFalse(clustering.sameCluster(0, 3));
        Assertions.assertFalse(clustering.sameCluster(1, 2));
        Assertions.assertFalse(clustering.sameCluster(1, 3));
        Assertions.assertFalse(clustering.sameCluster(2, 3));

        Assertions.assertEquals(1, clustering.spaziamento());
    }

    @Test
    public void test3Clusters() {
        UndirectedGraph g = new UndirectedGraph("4; 0 1 2; 1 3 7; 3 2 4; 2 0 1");
        Clustering clustering = new Clustering(g, 3);

        Assertions.assertTrue(clustering.sameCluster(0, 2));
        Assertions.assertTrue(clustering.sameCluster(2, 0)); //Solo per vedere se la sameCluster è implementata bene

        Assertions.assertFalse(clustering.sameCluster(0, 1));
        Assertions.assertFalse(clustering.sameCluster(0, 3));
        Assertions.assertFalse(clustering.sameCluster(1, 2));
        Assertions.assertFalse(clustering.sameCluster(1, 3));
        Assertions.assertFalse(clustering.sameCluster(2, 3));

        Assertions.assertEquals(2, clustering.spaziamento());
    }

    @Test
    public void test2Clusters() {
        UndirectedGraph g = new UndirectedGraph("4; 0 1 2; 1 3 7; 3 2 4; 2 0 1");
        Clustering clustering = new Clustering(g, 2);

        Assertions.assertTrue(clustering.sameCluster(0, 2) && clustering.sameCluster(0, 1) && clustering.sameCluster(1, 2));

        for (int i = 0; i < 3; ++i) {
            Assertions.assertFalse(clustering.sameCluster(i, 3));
        }

        Assertions.assertEquals(4, clustering.spaziamento());
    }

    @Test
    public void test1Clusters() {
        UndirectedGraph g = new UndirectedGraph("4; 0 1 2; 1 3 7; 3 2 4; 2 0 1");
        Clustering clustering = new Clustering(g, 1);

        System.out.println(clustering.getClusters());
        System.out.println(clustering.spaziamento());

        for (int i = 0; i <= 3; ++i) {
            for (int j = 0; j <= 3; ++j) {
                if (i != j) {
                    Assertions.assertTrue(clustering.sameCluster(i, j));
                }
            }
        }

        Assertions.assertEquals(-99, clustering.spaziamento()); //Di fatto non esiste spaziamento
    }
}
