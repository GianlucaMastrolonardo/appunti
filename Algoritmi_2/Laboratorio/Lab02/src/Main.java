import it.uniupo.graphLib.Graph;
import it.uniupo.graphLib.UndirectedGraph;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Graph mioGrafo = new UndirectedGraph("10; 0 1; 1 2; 2 3; 3 4; 4 5; 5 6; 6 7; 7 8; 8 9; 9 0; 0 2; 2 4; 4 6; 6 8; 8 0;"); //Num Nodi + Archi
        BFS mioBFS = new BFS(mioGrafo);
        int source = 0;
        int destination = 4;

        System.out.println(">>> Sorgente delle Operazione: " + source);
        System.out.println(">>> Destinazione delle Operazione: " + destination);
        System.out.println(">>> Visita BFS sul Grafo: " + mioBFS.getNodesInOrderOfVisit(source));
        System.out.println(">>> Distanza dei nodi dalla sorgente: " + Arrays.toString(mioBFS.getDistance(source)));
        System.out.println(">>> Distanza del nodo da " + source + " a " + destination + ": " + mioBFS.getDistance(source, destination));
        System.out.println(">>> Albero di Visita con radice la sorgente: \n" + mioBFS.bfsTree(source));
        //Da qui in poi da finire
        System.out.println(">>> Cammino più corto per andare da sorgente a destinazione: " + mioBFS.camminoMinimo(source, destination));

    }
}