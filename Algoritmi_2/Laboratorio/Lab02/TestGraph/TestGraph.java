import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class TestGraph {

    //Inizio Test ES1
    @Test
    void testCreate() {
        GraphInterface grafo = new DirectedGraph(3);
        BFS bfsTest = new BFS(grafo);
        assertNotNull(bfsTest);
    }

    @Test
    void testInvalidSorgente() {
        GraphInterface grafo = new DirectedGraph("1");
        BFS bfsTest = new BFS(grafo);
        assertThrows(IllegalArgumentException.class, () -> bfsTest.getNodesInOrderOfVisit(3));
    }

    @Test
    @Timeout(value = 500)
    void testScoperti() {
        GraphInterface grafo = new DirectedGraph("3;0 1;1 2;2 0");
        BFS bfsTest = new BFS(grafo);
        assertTrue(bfsTest.getNodesInOrderOfVisit(0) != null);
    }

    @Test
    void testNumeroNodiVisitati() {
        //Un solo nodo
        GraphInterface grafo = new UndirectedGraph("1");
        BFS bfsTest = new BFS(grafo);
        assertEquals(1, bfsTest.getNodesInOrderOfVisit(0).size());

        //Due nodi ed un arco
        grafo = new UndirectedGraph("2; 0 1");
        bfsTest = new BFS(grafo);
        assertEquals(2, bfsTest.getNodesInOrderOfVisit(0).size());

        //Quattro nodi e quattro archi
        grafo = new UndirectedGraph("4;0 2;0 1;2 3;1 3");
        bfsTest = new BFS(grafo);
        assertEquals(4, bfsTest.getNodesInOrderOfVisit(2).size());

    }

    @Test
    void testBFSOrder() {
        GraphInterface grafo = new UndirectedGraph("4;0 2;0 1;2 3;1 3");
        BFS bfsTest = new BFS(grafo);
        assertTrue(bfsTest.getNodesInOrderOfVisit(2).get(2) == 0 || bfsTest.getNodesInOrderOfVisit(2).get(2) == 3);
    }

    @Test
    public void testInitNumeroNodiVisitati() {
        GraphInterface grafo = new UndirectedGraph("4;0 2;0 1;2 3;1 3");
        BFS bfsTest = new BFS(grafo); //<<- creato una volta sola
        int numeroNodi = bfsTest.getNodesInOrderOfVisit(0).size();//<<-prima chiamata del metodo
        assertEquals(4, numeroNodi);
        numeroNodi = bfsTest.getNodesInOrderOfVisit(2).size(); //<<-seconda chiamata, stesso oggetto, parametro diverso
        assertEquals(4, numeroNodi);
        numeroNodi = bfsTest.getNodesInOrderOfVisit(3).size(); //<<-terza chiamata, stesso oggetto, parametro diverso
        assertEquals(4, numeroNodi);
    }

    //Inizio Test Es2
    @Test
    public void testDistanzaUnNodo() {
        GraphInterface grafo = new UndirectedGraph("1");
        BFS bfsTest = new BFS(grafo);
        assertEquals(0, bfsTest.getDistance(0)[0]);
    }

    @Test
    public void testDistanzaDueNodiUnArco() {
        GraphInterface grafo = new UndirectedGraph("2; 0,1;");
        BFS bfsTest = new BFS(grafo);
        assertEquals(1, bfsTest.getDistance(0)[1]);
    }

    @Test
    public void testDistanzaGrafoComplesso() {
        GraphInterface grafo = new UndirectedGraph("4; 0,1; 0,2; 1,3; 2,3");
        BFS bfsTest = new BFS(grafo);
        assertEquals(0, bfsTest.getDistance(2)[2]);
        assertEquals(1, bfsTest.getDistance(2)[0]);
        assertEquals(bfsTest.getDistance(2)[0], bfsTest.getDistance(2)[3]);
        assertEquals(2, bfsTest.getDistance(2)[1]);
    }

    @Test
    public void testInitDistanza() {
        GraphInterface grafo = new UndirectedGraph("4; 0,1; 0,2; 1,3; 2,3");
        BFS bfsTest = new BFS(grafo);

        int[] expectedResultWithSource2 = {1, 2, 0, 1};
        int[] expectedResultWithSource3 = {2, 1, 1, 0};

        assertArrayEquals(expectedResultWithSource2, bfsTest.getDistance(2));
        assertArrayEquals(expectedResultWithSource3, bfsTest.getDistance(3));
    }
}
