import it.uniupo.graphLib.GraphInterface;

import java.util.ArrayList;
import java.util.Arrays;

public class DFS {

    private GraphInterface myGraph;
    private boolean[] scoperti;
    private GraphInterface treeDFS;

    public DFS(GraphInterface g) {
        this.myGraph = g;
        this.scoperti = new boolean[this.myGraph.getOrder()];
        this.treeDFS = this.myGraph.create();
    }

    private void visitaDFS(int sorg) {
        if (sorg >= this.myGraph.getOrder() || sorg < 0) {
            throw new IllegalArgumentException("Sorgente non presenten nel grafo");
        }
        scoperti[sorg] = true;
        for (int node : this.myGraph.getNeighbors(sorg)) {
            if (!scoperti[node]) {
                treeDFS.addEdge(sorg, node);
                visitaDFS(node);
            }
        }
    }

    public GraphInterface getTree(int sorg) {
        visitaDFS(sorg);
        return this.treeDFS;
    }

    public GraphInterface getTreeRicoprente(int sorg) throws NotAllNodesReachedException {
        visitaDFS(sorg);
        if (treeDFS.getOrder() != myGraph.getOrder() - 1) {
            throw new NotAllNodesReachedException("Esistono due alberi");
        }
        return this.treeDFS;
    }

    public GraphInterface getForest() {
        for (int nodo = 0; nodo < this.myGraph.getOrder(); nodo++) {
            if (!this.scoperti[nodo]) {
                getTree(nodo);
            }
        }
        return this.treeDFS;
    }
}
