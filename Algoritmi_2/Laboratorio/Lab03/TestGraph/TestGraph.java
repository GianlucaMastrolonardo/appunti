import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class TestGraph {
    //Slide 12 è sbagliata
    //Slide 18 in teoria è difficile

    @Test
    void testCreate() {
        GraphInterface grafo = new DirectedGraph(3);
        DFS dfsTest = new DFS(grafo);
        assertNotNull(dfsTest);
    }

    @Test
    @Timeout(value = 500)
    void testScoperti() {
        GraphInterface grafo = new DirectedGraph("3;0 1;1 2;2 0");
        DFS dfsTest = new DFS(grafo);
        assertNotNull(dfsTest.getTree(0));
    }

    @Test
    void testNumeroArchi() {
        GraphInterface grafo = new UndirectedGraph("4; 0 2; 0 1; 1 3; 2 3;");
        DFS dfsTest = new DFS(grafo);
        Assertions.assertEquals(grafo.getOrder() - 1, dfsTest.getTree(0).getEdgeNum());
    }

    @Test
    void testArchiDFS() {
        GraphInterface grafo = new UndirectedGraph("4; 0 2; 0 1; 1 3; 2 3;");
        DFS dfsTest = new DFS(grafo);
        GraphInterface dfsTree = dfsTest.getTree(2);
        Assertions.assertTrue((dfsTree.hasEdge(0, 2) && dfsTree.hasEdge(0, 1) && dfsTree.hasEdge(1, 3)) || (dfsTree.hasEdge(2, 3) && dfsTree.hasEdge(3, 1) && dfsTree.hasEdge(1, 0)));
        Assertions.assertFalse((dfsTree.hasEdge(0, 1) && dfsTree.hasEdge(0, 2) && dfsTree.hasEdge(2, 3)) || (dfsTree.hasEdge(0, 2)) && dfsTree.hasEdge(2, 3) && dfsTree.hasEdge(3, 1));
    }

    @Test
    void testInitAlbero() {
        GraphInterface grafo = new UndirectedGraph("4; 0 2; 0 1; 1 3; 2 3;");
        DFS dfsTest = new DFS(grafo);
        Assertions.assertEquals(3, dfsTest.getTree(2).getEdgeNum());
        Assertions.assertEquals(3, dfsTest.getTree(1).getEdgeNum());
    }

    @Test
    void testEccezione() {
        GraphInterface grafo = new UndirectedGraph("4; 0 2; 0 1; 1 3; 2 3;");
        DFS dfsTest = new DFS(grafo);
        Assertions.assertThrows(java.lang.IllegalArgumentException.class, () -> {
            dfsTest.getTree(200);
        });
        Assertions.assertThrows(java.lang.IllegalArgumentException.class, () -> {
            dfsTest.getTree(-1);
        });
    }

    @Test
    void testAlberiRicoprenti() {
        GraphInterface grafo = new UndirectedGraph("5; 0 3; 0 4; 3 4; 1 2;");
        DFS dfsTest = new DFS(grafo);
        Assertions.assertThrows(NotAllNodesReachedException.class, () -> {
            dfsTest.getTreeRicoprente(0);
        });
    }

    @Test
    void testForesta() {
        GraphInterface grafo = new UndirectedGraph("5; 0 3; 0 4; 3 4; 1 2;");
        DFS dfsTest = new DFS(grafo);
        GraphInterface forest = dfsTest.getForest();
        Assertions.assertFalse(forest.hasEdge(0,1) || forest.hasEdge(3,1) || forest.hasEdge(4, 1) || forest.hasEdge(0,2) || forest.hasEdge(3,2) || forest.hasEdge(4, 2));
    }
}
