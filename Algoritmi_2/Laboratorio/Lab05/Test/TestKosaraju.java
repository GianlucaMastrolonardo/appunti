import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

public
class TestKosaraju {

    @Test
    void testCostructor() {
        DirectedGraph g = new DirectedGraph("3; 0 1; 0 2");
        Kosaraju kosaraju = new Kosaraju(g);
        Assertions.assertNotNull(kosaraju);
    }

    @Test
    void testReversePostVisitOrder() {
        DirectedGraph g = new DirectedGraph("5; 1 4; 4 3; 3 1; 1 2; 4 0; 2 0; 0 2;");
        Kosaraju kosaraju = new Kosaraju(g);
        ArrayList<Integer> reversePostVisitOrder = kosaraju.reversePostVisitOrder();
        Assertions.assertTrue(reversePostVisitOrder.indexOf(1) < reversePostVisitOrder.indexOf(0) && reversePostVisitOrder.indexOf(1) < reversePostVisitOrder.indexOf(2));
        Assertions.assertTrue(reversePostVisitOrder.indexOf(4) < reversePostVisitOrder.indexOf(0) && reversePostVisitOrder.indexOf(4) < reversePostVisitOrder.indexOf(2));
        Assertions.assertTrue(reversePostVisitOrder.indexOf(3) < reversePostVisitOrder.indexOf(0) && reversePostVisitOrder.indexOf(3) < reversePostVisitOrder.indexOf(2));
    }

    @Test
    void testGetSCC() {
        DirectedGraph gOneNode = new DirectedGraph(1);
        Kosaraju kosarajuOneNode = new Kosaraju(gOneNode);
        Assertions.assertEquals(kosarajuOneNode.getSCC()[0], 0);

        DirectedGraph dag = new DirectedGraph("4; 0 2; 0 1; 1 2; 1 3");
        Kosaraju kosarajuDag = new Kosaraju(dag);
        int[] expectedSCCDag1 = {0, 1, 3, 2};
        int[] expectedSCCDag2 = {0, 1, 2, 3};
        Assertions.assertTrue(Arrays.equals(kosarajuDag.getSCC(), expectedSCCDag1) || Arrays.equals(kosarajuDag.getSCC(), expectedSCCDag2));

        DirectedGraph gOneCycle = new DirectedGraph("3; 0 1; 1 2; 2 0");
        Kosaraju kosarajuOneCycle = new Kosaraju(gOneCycle);
        for (int i = 0; i < kosarajuOneCycle.getSCC().length; i++) {
            Assertions.assertEquals(kosarajuOneCycle.getSCC()[i], 0);
        }


        DirectedGraph gBig = new DirectedGraph("5; 1 4; 4 3; 3 1; 1 2; 4 0; 2 0; 0 2;");
        Kosaraju kosarajuBig = new Kosaraju(gBig);
        Assertions.assertEquals(kosarajuBig.getSCC()[0], kosarajuBig.getSCC()[2]);
        Assertions.assertEquals(kosarajuBig.getSCC()[1], kosarajuBig.getSCC()[3]);
        Assertions.assertEquals(kosarajuBig.getSCC()[3], kosarajuBig.getSCC()[4]);
        Assertions.assertNotEquals(kosarajuBig.getSCC()[0], kosarajuBig.getSCC()[1]);
    }
}
