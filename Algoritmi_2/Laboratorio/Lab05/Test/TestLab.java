import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

public
class TestLab {
    @Test
    void testIsConnected() {
        UndirectedGraph gOneNode = new UndirectedGraph("1;");
        BFS bfsTest = new BFS(gOneNode);
        Assertions.assertTrue(bfsTest.isConnected());

        UndirectedGraph gConnected = new UndirectedGraph("3; 0 1; 1 2; 2 0");
        BFS bfsTest2 = new BFS(gConnected);
        Assertions.assertTrue(bfsTest2.isConnected());

        UndirectedGraph gUnonnected = new UndirectedGraph("5; 0 1; 0 2");
        BFS bfsTest3 = new BFS(gUnonnected);
        Assertions.assertFalse(bfsTest3.isConnected());

    }

    @Test
    void testConnectedComponents() {
        UndirectedGraph gOneNode = new UndirectedGraph("1");
        BFS bfsTest = new BFS(gOneNode);
        int[] cc1 = bfsTest.connectedComponents();
        Assertions.assertEquals(cc1[0], 0);

        UndirectedGraph gConnected = new UndirectedGraph("2; 0 1;");
        BFS bfsTest2 = new BFS(gConnected);
        int[] cc2 = bfsTest2.connectedComponents();
        Assertions.assertEquals(cc2[0], cc2[1]);

        UndirectedGraph gUnconnected = new UndirectedGraph("2;");
        BFS bfsTest3 = new BFS(gUnconnected);
        int[] cc3 = bfsTest3.connectedComponents();
        Assertions.assertNotEquals(cc3[0], cc3[1]);
        UndirectedGraph gBig = new UndirectedGraph("6; 0 4; 0 1; 2 5;");
        BFS bfsTestBig = new BFS(gBig);
        int[] ccBig = bfsTestBig.connectedComponents();
        Assertions.assertEquals(ccBig[0], ccBig[1]);
        Assertions.assertEquals(ccBig[0], ccBig[4]);
        Assertions.assertEquals(ccBig[2], ccBig[5]);
        Assertions.assertNotEquals(ccBig[3], ccBig[1]);
    }

    @Test
    void testTopologicalOrder() {
        DirectedGraph gOneNode = new DirectedGraph("1");
        DFS dfsTest = new DFS(gOneNode);
        Assertions.assertEquals(dfsTest.topologicalOrder().getFirst(), 0);


        DirectedGraph gTwoNodes = new DirectedGraph("2; 0 1;");
        DFS dfsTest2 = new DFS(gTwoNodes);
        Assertions.assertTrue(dfsTest2.topologicalOrder().indexOf(0) < dfsTest2.topologicalOrder().indexOf(1));

        DirectedGraph gThreeNodes = new DirectedGraph("3; 2 0;");
        DFS dfsTest3 = new DFS(gThreeNodes);
        Assertions.assertTrue(dfsTest3.topologicalOrder().indexOf(2) < dfsTest3.topologicalOrder().indexOf(0));
    }

    @Test
    void testTopologicalOrderDag() {
        DirectedGraph dag = new DirectedGraph("4; 0 1; 0 2; 1 2; 1 3");
        DFS dfsTest = new DFS(dag);
        Assertions.assertTrue(dfsTest.topologicalOrderDag().indexOf(0) < dfsTest.topologicalOrder().indexOf(3));

        System.out.println(dfsTest.hasDirCycle());

        DirectedGraph cyclicGraph = new DirectedGraph("3; 0 1; 1 2; 2 0");
        DFS dfsCyclicTest = new DFS(cyclicGraph);
        System.out.println(dfsCyclicTest.hasDirCycle());
        Assertions.assertThrows(IllegalArgumentException.class, () -> dfsCyclicTest.topologicalOrderDag());

        // Ora funziona anche con questo grafo
        DirectedGraph cyclicGraph2 = new DirectedGraph("2; 0 1; 1 0;");
        DFS dfsCyclicTest2 = new DFS(cyclicGraph2);
        Assertions.assertThrows(IllegalArgumentException.class, () -> dfsCyclicTest2.topologicalOrderDag());
    }
}
