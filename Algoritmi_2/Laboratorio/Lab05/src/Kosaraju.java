import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.GraphUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public
class Kosaraju {
    private DirectedGraph myGraph;
    private boolean[] founded;
    private ArrayList<Integer> postVisitOrder = new ArrayList<>();

    static public DirectedGraph reversedGraph;

    Kosaraju(DirectedGraph g) {
        this.myGraph = g;
        founded = new boolean[myGraph.getOrder()];
        reversedGraph = GraphUtils.reverseGraph(myGraph);
    }

    private
    void visitaDFS(int sorg) {
        founded[sorg] = true;
        for (Integer neighbor : myGraph.getNeighbors(sorg)) {
            if (!founded[neighbor]) {
                visitaDFS(neighbor);
            }
        }
        postVisitOrder.add(sorg);
    }

    public
    ArrayList<Integer> reversePostVisitOrder() {
        Arrays.fill(founded, false);
        postVisitOrder.clear();
        for (int node = 0; node < myGraph.getOrder(); node++) {
            if (!founded[node]) {
                visitaDFS(node);
            }
        }
        ArrayList<Integer> reversePostVisitOrderArray = postVisitOrder;
        Collections.reverse(reversePostVisitOrderArray);
        return reversePostVisitOrderArray;
    }

    private
    void visitaDFSOnReversedGraph(int sorg, int[] scc, int cnt) {
        founded[sorg] = true;
        for (Integer neighbor : reversedGraph.getNeighbors(sorg)) {
            if (!founded[neighbor]) {
                visitaDFSOnReversedGraph(neighbor, scc, cnt);
            }
        }
        scc[sorg] = cnt;
    }

    public
    int[] getSCC() {
        int[] SCC = new int[myGraph.getOrder()];
        int cnt = 0;
        ArrayList<Integer> reversePostVisitOrderArray = reversePostVisitOrder();
        Arrays.fill(founded, false);
        for (Integer node : reversePostVisitOrderArray) {
            if (!founded[node]) {
                visitaDFSOnReversedGraph(node, SCC, cnt);
                cnt++;
            }
        }
        return SCC;
    }
}
