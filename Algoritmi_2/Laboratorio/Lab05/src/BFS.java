import it.uniupo.graphLib.*;

import java.awt.List;
import java.util.*;

public class BFS {

    private GraphInterface myGraph;
    private boolean[] founded;


    public BFS(GraphInterface g) {
        //System.out.println(g);
        this.myGraph = g;
        this.founded = new boolean[this.myGraph.getOrder()];
    }

    public ArrayList<Integer> getNodesInOrderOfVisit(int sorgente) {
        if (this.myGraph.getOrder() <= sorgente) {
            throw new IllegalArgumentException("Sorgente errata");
        }
        ArrayList<Integer> returnList = new ArrayList<>();
        returnList.add(sorgente);
        Queue<Integer> q = new LinkedList<>();
        founded[sorgente] = true;
        q.add(sorgente);
        while (!q.isEmpty()) {
            int u = q.remove();
            Iterable<Integer> neighbors = this.myGraph.getNeighbors(u);
            for (int v : neighbors) {
                if (!founded[v]) {
                    founded[v] = true;
                    q.add(v);
                    returnList.add(v);
                }
            }
        }
        return returnList;
    }

    public boolean isConnected() {
        if (myGraph == null) {
            return true;
        }
        ArrayList<Integer> nodes = getNodesInOrderOfVisit(0);
        return nodes.size() == myGraph.getOrder();
    }

    public int[] connectedComponents() {
        int[] connectedComponentsArr = new int[myGraph.getOrder()];
        Arrays.fill(connectedComponentsArr, -1);
        Arrays.fill(founded, false);

        int cnt = 0;
        for (int node = 0; node < myGraph.getOrder(); node++) {
            if (!founded[node]) {
                ArrayList<Integer> nodes = getNodesInOrderOfVisit(node);
                for (int n : nodes) {
                    connectedComponentsArr[n] = cnt;
                }
                cnt++;
            }
        }
        return connectedComponentsArr;
    }
}
