import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public
class DFS {

    private GraphInterface myGraph;
    private boolean[] founded;
    private GraphInterface treeDFS;
    private ArrayList<Integer> terminated = new ArrayList<>();

    private ArrayList<Integer> postVisitOrder = new ArrayList<>();

    public
    DFS(GraphInterface g) {
        this.myGraph = g;
        this.founded = new boolean[this.myGraph.getOrder()];
        this.treeDFS = this.myGraph.create();
    }

    private
    void visitaDFS(int sorg) {
        if (sorg >= this.myGraph.getOrder() || sorg < 0) {
            throw new IllegalArgumentException("Sorgente non presenten nel grafo");
        }
        founded[sorg] = true;
        for (int node : this.myGraph.getNeighbors(sorg)) {
            if (!founded[node]) {
                treeDFS.addEdge(sorg, node);
                visitaDFS(node);
            }
        }
        postVisitOrder.add(sorg);
    }

    public
    GraphInterface getTree(int sorg) {
        visitaDFS(sorg);
        return this.treeDFS;
    }

    public
    GraphInterface getTreeRicoprente(int sorg) throws NotAllNodesReachedException {
        visitaDFS(sorg);
        if (treeDFS.getOrder() != myGraph.getOrder() - 1) {
            throw new NotAllNodesReachedException("Esistono due alberi");
        }
        return this.treeDFS;
    }

    public
    GraphInterface getForest() {
        for (int node = 0; node < this.myGraph.getOrder(); node++) {
            if (!this.founded[node]) {
                getTree(node);
            }
        }
        return this.treeDFS;
    }

    private
    boolean hasDirCycleImpl(int sorg) {
        founded[sorg] = true;
        for (int neighbour : myGraph.getNeighbors(sorg)) {
            if (!founded[neighbour]) {
                if (hasDirCycleImpl(neighbour)) { // Check the result of the recursive call
                    return true; // If a cycle is detected in the recursive call, propagate it up
                }
            } else if (!terminated.contains(neighbour)) {
                return true;
            }
        }
        terminated.add(sorg);
        return false;
    }

    public
    boolean hasDirCycle() {
        if (myGraph instanceof UndirectedGraph) {
            throw new IllegalArgumentException("Impossibile usare hasDirCycle su un grafo non orientato");
        }
        for (int i = 0; i < myGraph.getOrder(); i++) {
            terminated.clear();
            Arrays.fill(founded, false);
            if (hasDirCycleImpl(i)) {
                return true;
            }
        }
        return false;
    }


    public
    ArrayList<Integer> topologicalOrder() {
        postVisitOrder.clear();
        Arrays.fill(founded, false);
        if (myGraph instanceof UndirectedGraph) {
            throw new IllegalArgumentException("Graph need to be Directed!");
        }
        for (int node = 0; node < this.myGraph.getOrder(); node++) {
            if (!founded[node]) {
                visitaDFS(node);
            }
        }
        ArrayList<Integer> topologicOrder = postVisitOrder;
        Collections.reverse(topologicOrder);
        return topologicOrder;
    }

    public
    ArrayList<Integer> topologicalOrderDag() {
        if (hasDirCycle()) {
            throw new IllegalArgumentException("Il grafo non è un DAG");
        }

        postVisitOrder.clear();
        Arrays.fill(founded, false);
        if (myGraph instanceof UndirectedGraph) {
            throw new IllegalArgumentException("Graph need to be Directed!");
        }
        for (int node = 0; node < this.myGraph.getOrder(); node++) {
            if (!founded[node]) {
                visitaDFS(node);
            }
        }
        ArrayList<Integer> topologicOrder = postVisitOrder;
        Collections.reverse(topologicOrder);
        return topologicOrder;
    }


}
