#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int n, i;

    if (argc == 2)
    {
        n = atoi(argv[1]);
    }
    else
    {
        printf("Chiamare il programma con un valore");
        return 1;
    }

    char *my_string = "Squalo\n";
    for (i = 0; i < n; i++)
        write(1, my_string, strlen(my_string));

    return 0;
}
