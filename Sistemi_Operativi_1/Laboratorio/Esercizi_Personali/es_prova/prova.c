#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main()
{
    pid_t n = fork();
    if (n == (pid_t)-1)
    {
        printf("FORK FALLITA");
    }
    else if (n == (pid_t)0)
    {
        printf("SONO IL FIGLIO");
    }
    else
    {
        printf("SONO IL PADRE");
    }

    printf("Hello World\n");

    return 0;
}