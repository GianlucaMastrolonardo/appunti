#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main()
{
	int filedescriptor = open("output.txt", O_CREAT | O_RDWR);
	// Con open puoi anche creare i file con la differenza che se il file
	// esiste già lo apre e basta

	if (filedescriptor == -1)
	{
		perror("Errore creazione file");
	}
	else
	{
		printf("File Descriptor output: %d\n", filedescriptor);

		char buf[20];
		printf("[Lunghezza massima 20 caratteri]\nScrivi un carattere alla volta\n");
		int i = 0;
		while (i < 20)
		{
			printf("Carattere %d: ", i);
			scanf(" %c", &buf[i]);
			if (buf[i] == 'z')
			{
				break;
			}
			i++;
		}
		write(filedescriptor, buf, i);
		close(filedescriptor);
		printf("É stato scritto su file: ");
		for (int j = 0; j < i; j++)
		{
			printf("%c", buf[j]);
		}
	}

	return 0;
}
