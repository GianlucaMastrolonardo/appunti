#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	for (int i = 0; i < 10; i++)
	{
		pid_t pid = fork(); // crea un nuovo processo figlio

		if (pid == 0) // processo figlio
		{
			execl("hello", "hello", NULL); // esegue "hello"
			exit(EXIT_SUCCESS);			   // uscita dal processo figlio
		}
		else if (pid < 0)
		{
			perror("fork failed");
			exit(EXIT_FAILURE);
		}
		// processo padre aspetta il termine del processo figlio
		waitpid(pid, NULL, 0);
	}
	return 0;
}
