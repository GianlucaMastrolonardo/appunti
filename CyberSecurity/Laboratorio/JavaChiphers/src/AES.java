import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class AES {

    private final String inputString;
    private String outputString;

    AES(String inputString) {
        this.inputString = inputString;
    }

    public String getOutputString() {
        return outputString;
    }

    public void aes_CTR(String inputString, String outputString, boolean encrypt) throws NoSuchAlgorithmException {
        //Create Key
        KeyGenerator myKeyGenerator = KeyGenerator.getInstance("AES");
        SecretKeySpec myKey = (SecretKeySpec) myKeyGenerator.generateKey();

        System.out.println(myKey);
    }


}
