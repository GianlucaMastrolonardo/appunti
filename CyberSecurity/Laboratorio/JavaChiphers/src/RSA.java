import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import java.security.*;

import static javax.crypto.Cipher.getInstance;

public class RSA {

    byte[] encryptRSA(Key chiave, byte[] clearText) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        Cipher instance = getInstance("RSA");


        //Generate Keys
        KeyGenerator myKeyGenerator = KeyGenerator.getInstance("RSA");
        PrivateKey myKey = (PrivateKey) myKeyGenerator.generateKey();
        PublicKey myPublicKey = (PublicKey) myKeyGenerator.generateKey();

        System.out.println(myKey.toString());
        System.out.println(myPublicKey.toString());

        instance.init(Cipher.ENCRYPT_MODE, myKey);

    }

    byte[] decryptRSA(Key chiave, byte[] cipherText) {
    }
}
