/*
Esercizio 25:
Implementare il tipo lista concatenata, in cui l’informazione memorizzata in ogni elemento è un valore intero, e le
seguenti operazioni:
• inserimento di un elemento in testa alla lista (sono ammessi elementi duplicati),
• ricerca di un elemento nella lista (la funzione ritorna il puntatore al nodo della lista che contiene il valore cercato se
il valore è presente nella lista, o NULL altrimenti),
• cancellazione di un elemento dalla lista (in caso di elementi duplicati, si rimuove la prima occorrenza che s’incontra),
• stampa il contenuto della lista su un file (passato come un parametro di tipo FILE*), utilizzando il seguente formato
di output: [valore1, valore2, ...].
Nella funzione main() effettuare le seguenti operazioni:
1. Inserire nella lista i valori della sequenza [1,2,3,4,5,4,3,2,1].
2. Stampare il contenuto della lista.
3. Rimuovere dalla lista il primo, il secondo e l’ultimo valore della suddetta sequenza, e il valore -10 (che non è
memorizzato nella lista). Ogni volta che si cancella un elemento ricercare nella lista se è ancora presente.
4. Stampare il contenuto della lista.
5. Rimuovere tutti gli elementi.
6. Stampare il contenuto della lista.
L’output prodotto dovrebbe essere simile al seguente:
1 [1,2,3,4,5,4,3,2,1]
2 After removal -> Element 1 found
3 After removal -> Element 2 found
4 After removal -> Element 1 not found
5 After removal -> Element -10 not found
6 [3,4,5,4,3,2]
7 []
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct linked_list
{
    int x;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

void debug_printlist(LINK list)
{
    if (list == NULL)
    {
        printf("NULL\n");
    }
    else
    {
        printf("%d ", list->x);
        debug_printlist(list->next);
    }
}

// inserimento di un elemento in testa alla lista (sono ammessi elementi duplicati):
LINK headinsert(int x, LINK list)
{
    LINK head = newnode();
    head->x = x;
    head->next = list;
    return head;
}

// ricerca di un elemento nella lista (la funzione ritorna il puntatore al nodo della lista che contiene il valore cercato se
//  il valore è presente nella lista, o NULL altrimenti)
LINK findelement(int x, LINK list)
{
    if (list == NULL)
    {
        return NULL;
    }
    else
    {
        if (list->x == x)
        {
            return list;
        }

        return findelement(x, list->next);
    }
}

// cancellazione di un elemento dalla lista (in caso di elementi duplicati, si rimuove la prima occorrenza che s’incontra)
void delelement(int x, LINK list)
{
    if (list != NULL)
    {
    }
}

int main()
{
    LINK myList;
    for (int i = 0; i < 5; i++)
    {
        int x;
        scanf("%d", &x);
        myList = headinsert(x, myList);
    }

    debug_printlist(myList);
    int toFind;
    scanf("%d", &toFind);
    printf("%p", findelement(toFind, myList));

    return 0;
}