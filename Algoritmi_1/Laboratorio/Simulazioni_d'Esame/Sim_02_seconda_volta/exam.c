/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */


/******************************************************************************/
/*** NOME:                                                                  ***/
/*** COGNOME:                                                               ***/
/*** MATRICOLA:                                                             ***/
/******************************************************************************/


#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <upo/bst.h>
#include <upo/hashtable.h>


/**** BEGIN of EXERCISE #1 ****/

upo_bst_node_t* upo_bst_find_node(upo_bst_node_t* node, const void* key, size_t* height, upo_bst_comparator_t key_cmp){
    if(node == NULL){
        return NULL;
    }else{
        if(key_cmp(node->key, key) == 0){
            return node;
        }else if(key_cmp(node->key, key) < 0){
            *height += 1;
            return upo_bst_find_node(node->right, key, height, key_cmp);
        }else{
            *height += 1;
            return upo_bst_find_node(node->left, key, height, key_cmp);
        }
    }
}

void upo_bst_subtree_count_even_impl(upo_bst_node_t* node, size_t height, size_t* cnt){
    if(node == NULL){
        return;
    }else{
        if(height % 2 == 0){
            *cnt += 1; 
        }
        upo_bst_subtree_count_even_impl(node -> left, height+1, cnt);
        upo_bst_subtree_count_even_impl(node -> right, height+1, cnt);
    }
}

size_t upo_bst_subtree_count_even(const upo_bst_t bst, const void *key)
{
    if(bst == NULL) return 0;
    
    size_t height = 0;
    upo_bst_node_t* node = upo_bst_find_node(bst->root, key, &height, bst->key_cmp);
    
    //chiave non presente
    if(node == NULL) return 0;

    size_t cnt = 0;
    upo_bst_subtree_count_even_impl(node, height, &cnt);
    return cnt;
    
}

/**** END of EXERCISE #1 ****/


/**** BEGIN of EXERCISE #2 ****/

void upo_ht_sepchain_odelete(upo_ht_sepchain_t ht, const void *key, int destroy_data)
{
    if(ht == NULL) return;

    upo_ht_hasher_t key_hash = ht->key_hash;
    size_t h = key_hash(key, ht->capacity);

    if(ht->slots[h].head == NULL) return;

    int elDelete = 0;

    upo_ht_comparator_t key_cmp = ht->key_cmp;
    if(key_cmp(ht->slots[h].head->key, key) == 0){
        //Cancellazione in testa
        upo_ht_sepchain_list_node_t* tmp = ht->slots[h].head;
        ht->slots[h].head = ht->slots[h].head->next;
        
        if(destroy_data != 0){
            free(tmp->value);
        }
        free(tmp);
        elDelete = 1;
    }else{
        upo_ht_sepchain_list_node_t *curr_node = ht->slots[h].head;
        while(curr_node != NULL && curr_node -> next != NULL && key_cmp(curr_node->next->key, key) != 0){
            curr_node = curr_node->next;
        }
        if(curr_node != NULL && curr_node -> next != NULL){
            //Trovato l'elemento da cancellare
            upo_ht_sepchain_list_node_t* to_delete = curr_node->next;
            curr_node->next = curr_node->next->next;
            if(destroy_data != 0){
                free(to_delete -> value);
            }
            free(to_delete);
            elDelete = 1;
        }
    }
    if(elDelete != 0){
        ht->size -= 1;
    }
}

/**** END of EXERCISE #2 ****/
