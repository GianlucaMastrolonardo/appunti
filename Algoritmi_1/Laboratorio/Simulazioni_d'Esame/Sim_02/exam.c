/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/******************************************************************************/
/*** NOME: Gianluca                                                         ***/
/*** COGNOME: Mastrolonardo                                                 ***/
/*** MATRICOLA: 20050114                                                    ***/
/******************************************************************************/

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <upo/bst.h>
#include <upo/hashtable.h>

/**** BEGIN of EXERCISE #1 ****/

upo_bst_node_t *upo_bst_find_key_and_level(upo_bst_node_t *node, const void *key, upo_bst_comparator_t key_cmp, int *bst_lvl)
{
  if (node == NULL)
  {
    return NULL;
  }
  else
  {
    if (key_cmp(key, node->key) == 0)
    {
      return node;
    }
    if (key_cmp(key, node->key) < 0)
    {
      *bst_lvl = *bst_lvl + 1;
      return upo_bst_find_key_and_level(node->left, key, key_cmp, (bst_lvl));
    }
    else
    {
      *bst_lvl = *bst_lvl + 1;
      return upo_bst_find_key_and_level(node->right, key, key_cmp, (bst_lvl));
    }
  }
}

void upo_bst_subtree_count_even_impl(upo_bst_node_t *node, int tree_lvl, size_t *cnt)
{
  if (node == NULL)
  {
    return;
  }
  if (tree_lvl % 2 == 0)
  {
    *cnt = *cnt + 1;
  }
  upo_bst_subtree_count_even_impl(node->left, tree_lvl + 1, cnt);
  upo_bst_subtree_count_even_impl(node->right, tree_lvl + 1, cnt);
}

size_t upo_bst_subtree_count_even(const upo_bst_t bst, const void *key)
{
  size_t result = 0;

  if (bst != NULL)
  {
    int bst_lvl = 0;
    upo_bst_node_t *node = upo_bst_find_key_and_level(bst->root, key, bst->key_cmp, &bst_lvl);
    // printf("\n\t%d", *((int *)key));
    // printf("\n\t%d\n", *((int *)node->key));
    // printf("\n\t%d\n", bst_lvl);

    upo_bst_subtree_count_even_impl(node, bst_lvl, &result);
  }
  return result;
}

/**** END of EXERCISE #1 ****/

/**** BEGIN of EXERCISE #2 ****/

void upo_ht_sepchain_odelete(upo_ht_sepchain_t ht, const void *key,
                             int destroy_data)
{
    if(ht == NULL || ht->slots == NULL) return;

    upo_ht_hasher_t key_hash = ht->key_hash;
    size_t h = key_hash(key, ht->capacity);

    upo_ht_comparator_t key_cmp = ht->key_cmp;

    upo_ht_sepchain_list_node_t *prev_node = NULL;
    upo_ht_sepchain_list_node_t *node = ht->slots[h].head;

    while(node != NULL && key_cmp(key, node->key) != 0){
        prev_node = node;
        node = node->next;
    }

    if(node != NULL && key_cmp(key, node->key) == 0){
        if(prev_node == NULL){
            //Cancellazione in testa
            ht->slots[h].head = node->next;
        }else{
            //Cancellazione a metà o alla fine della lista;
            prev_node->next = node->next;
        }
        if(destroy_data != 0){
            free(node->key);
            free(node->value);
        }
        free(node);
        ht->size -= 1; 
    }

    return;
}

/**** END of EXERCISE #2 ****/
