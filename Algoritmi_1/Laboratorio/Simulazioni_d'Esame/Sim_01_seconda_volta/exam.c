/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */


/******************************************************************************/
/*** NOME:                                                                  ***/
/*** COGNOME:                                                               ***/
/*** MATRICOLA:                                                             ***/
/******************************************************************************/


#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <upo/bst.h>
#include <upo/sort.h>


/**** BEGIN of EXERCISE #1 ****/

void upo_bst_predecessor_impl(upo_bst_node_t *node, const void *key, void **pred, upo_bst_comparator_t key_cmp){
   if(node == NULL){
        return;
    }else{
        if(key_cmp(node->key, key) >= 0){
            upo_bst_predecessor_impl(node -> left, key, pred, key_cmp);
        }else{
            *pred = node -> key;
            if(node->right != NULL) upo_bst_predecessor_impl(node->right, key, pred, key_cmp);
            else return;
        }
    }
}

const void* upo_bst_predecessor(const upo_bst_t bst, const void *key)
{
    if(bst == NULL) return NULL;

    void* pred = NULL;
    upo_bst_predecessor_impl(bst->root, key, &pred, bst->key_cmp);

    return pred;
}

/**** END of EXERCISE #1 ****/


/**** BEGIN of EXERCISE #2 ****/

void upo_swap(void* el1, void* el2, size_t size){
    void *tmp = malloc(size);
    memcpy(tmp, el1, size);
    memcpy(el1, el2, size);
    memcpy(el2, tmp, size);
    free(tmp);
}

void upo_bidi_bubble_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
    unsigned char* arr = base;

    int swapNeeded = 1;

    while(swapNeeded == 1){
        swapNeeded = 0;

        for(size_t i = 0; i < n-1; ++i){
            if(cmp(arr+i*size, arr+(i+1)*size) > 0){
                //Devo effettuare swap
                upo_swap(arr+i*size, arr+(i+1)*size, size);
                swapNeeded = 1;
            }
        }
        
        for(size_t j = n-1; j > 1; --j){
            if(cmp(arr+j*size, arr+(j-1)*size) < 0){
                //swap
                upo_swap(arr+j*size, arr+(j-1)*size, size);
                swapNeeded = 1;
            }
        }

    }
}

/**** END of EXERCISE #2 ****/
