/**** BEGIN of EXERCISE #1 ****/

static const void* upo_bst_predecessor_impl(const upo_bst_node_t *node, const void *key, upo_bst_comparator_t key_cmp);


const void* upo_bst_predecessor(const upo_bst_t bst, const void *key) {
    if (key == NULL) {
        fprintf(stderr, "Invalid argument: pointer to key cannot be NULL\n");
        return NULL;
    }
    if (upo_bst_is_empty(bst)) {
        return NULL;
    }
    return upo_bst_predecessor_impl(bst->root, key, bst->key_cmp);
}


const void* upo_bst_predecessor_impl(const upo_bst_node_t *node, const void *key, upo_bst_comparator_t key_cmp) {
    if (node != NULL) {
        int cmp = key_cmp(key, node->key);
        if (cmp <= 0) {
            // Case: key <= node->key
            return upo_bst_predecessor_impl(node->left, key, key_cmp);
        }
        // Case: key > node->key
        const void *pred_key = upo_bst_predecessor_impl(node->right, key, key_cmp);
        return (pred_key != NULL) ? pred_key : node->key;
    }
    return NULL;
}


/**** END of EXERCISE #1 ****/


/**** BEGIN of EXERCISE #2 ****/

static void upo_swap(void *a, void *b, size_t sz);


void upo_bidi_bubble_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp) {
    assert( base != NULL );


    unsigned char *pc = base;
    int swapped = 1;
    for (size_t k = 0; k < n && swapped; ++k) {
        swapped = 0;
        for (size_t i = 0; i < (n-1); ++i) {
            if (cmp(pc + i*size, pc + (i+1)*size) > 0) {
                // base[i] and base[i+1] are out of order -> swap
                upo_swap(pc + i*size, pc + (i+1)*size, size);
                swapped = 1;
            }
        }
        if (swapped) {
            swapped = 0;
            for (size_t i = n-2; i > 0; --i) {
                if (cmp(pc + i*size, pc + (i-1)*size) < 0) {
                    // base[i] and base[i-1] are out of order -> swap
                    upo_swap(pc + i*size, pc + (i-1)*size, size);
                    swapped = 1;
                }
            }
        }
    }
}


void upo_swap(void *a, void *b, size_t sz) {
    unsigned char *aa = a;
    unsigned char *bb = b;
    while (sz-- > 0) {
        unsigned char tmp = *aa;
        *aa = *bb;
        *bb = tmp;
        ++aa;
        ++bb;
    }
}


/**** END of EXERCISE #2 ****/
