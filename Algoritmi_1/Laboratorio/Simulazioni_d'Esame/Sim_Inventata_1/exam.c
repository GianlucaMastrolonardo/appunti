/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/******************************************************************************/
/*** NOME:                                                                  ***/
/*** COGNOME:                                                               ***/
/*** MATRICOLA:                                                             ***/
/******************************************************************************/

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <upo/bst.h>
#include <upo/sort.h>

/**** BEGIN of EXERCISE #1 ****/

void *upo_bst_successor_impl(upo_bst_node_t *node, const void *key_to_find, upo_bst_comparator_t key_cmp)
{
  if (node != NULL)
  {
    void *successor = upo_bst_successor_impl(node->left, key_to_find, key_cmp);
    if (successor != NULL)
    {
      return successor;
    }

    if (key_cmp(node->key, key_to_find) > 0)
    {
      return node->key;
    }

    return upo_bst_successor_impl(node->right, key_to_find, key_cmp);
  }
  return NULL;
}

// In realtà cerca il successore però non ho cambiato il nome sennè si spaccavano tutti i test
const void *upo_bst_predecessor(const upo_bst_t bst, const void *key)
{
  void *succ_key = NULL;
  if (bst != NULL)
  {
    succ_key = upo_bst_successor_impl(bst->root, key, bst->key_cmp);
  }
  return succ_key;
}

/**** END of EXERCISE #1 ****/

/**** BEGIN of EXERCISE #2 ****/

void upo_swap(unsigned char *el1, unsigned char *el2, size_t size)
{
  unsigned char *tmp = malloc(size);
  memcpy(tmp, el1, size);
  memcpy(el1, el2, size);
  memcpy(el2, tmp, size);
  free(tmp);
}

void upo_selection_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
  unsigned char *arr = base;
  for (size_t i = 0; i < n; ++i)
  {
    unsigned char *min = arr + i * size;
    for (size_t j = i + 1; j < n; ++j)
    {
      if (cmp(min, arr + j * size) > 0)
      {
        min = arr + j * size;
      }
    }
    upo_swap(min, arr + i * size, size);
  }
}

// In realtà è il selection sort però non ho cambiato il nome sennè si spaccavano tutti i test
void upo_bidi_bubble_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
  upo_selection_sort(base, n, size, cmp);
}

/**** END of EXERCISE #2 ****/
