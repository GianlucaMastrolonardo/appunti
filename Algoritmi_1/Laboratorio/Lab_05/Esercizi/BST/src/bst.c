/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2015 University of Piemonte Orientale, Computer Science Institute
 *
 * This file is part of UPOalglib.
 *
 * UPOalglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * UPOalglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with UPOalglib.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bst_private.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**** EXERCISE #1 - BEGIN of FUNDAMENTAL OPERATIONS ****/


upo_bst_t upo_bst_create(upo_bst_comparator_t key_cmp)
{
    upo_bst_t tree = malloc(sizeof(struct upo_bst_s));
    if (tree == NULL)
    {
        perror("Unable to create a binary search tree");
        abort();
    }

    tree->root = NULL;
    tree->key_cmp = key_cmp;

    return tree;
}

void upo_bst_destroy(upo_bst_t tree, int destroy_data)
{
    if (tree != NULL)
    {
        upo_bst_clear(tree, destroy_data);
        free(tree);
    }
}

void upo_bst_clear_impl(upo_bst_node_t *node, int destroy_data)
{
    if (node != NULL)
    {
        upo_bst_clear_impl(node->left, destroy_data);
        upo_bst_clear_impl(node->right, destroy_data);

        if (destroy_data)
        {
            free(node->key);
            free(node->value);
        }

        free(node);
    }
}

void upo_bst_clear(upo_bst_t tree, int destroy_data)
{
    if (tree != NULL)
    {
        upo_bst_clear_impl(tree->root, destroy_data);
        tree->root = NULL;
    }
}

void* upo_bst_createnode(void *key, void *value){
	upo_bst_node_t *node = malloc(sizeof(struct upo_bst_node_s));
	node -> key = key;
	node -> value = value;
	node -> left = NULL;
	node -> right = NULL;

	return node;
}

void* upo_bst_put_impl(upo_bst_node_t *node, void *key, void *value, void *oldvalue, upo_bst_comparator_t key_cmp){
	if(node == NULL){
		//Se chiave non presente
		return upo_bst_createnode(key, value);
	}else if(key_cmp(key, node->key) <= -1){
		node->left = upo_bst_put_impl(node -> left, key, value, oldvalue, key_cmp);
	}else if(key_cmp(key, node->key) >= 1){
        node->right = upo_bst_put_impl(node -> right, key, value, oldvalue, key_cmp);
	}else{
		//oldvalue = value
        oldvalue = node->value;
        node->value = value;
	}
    return node;
}

void* upo_bst_put(upo_bst_t tree, void *key, void *value)
{
	upo_bst_node_t *oldvalue = NULL;
	if(tree != NULL){
		tree->root = upo_bst_put_impl(tree -> root, key, value, oldvalue, tree->key_cmp);
	}
	return oldvalue;
}

void* upo_bst_insert_impl(upo_bst_node_t *node, void *key, void *value, upo_bst_comparator_t key_cmp){
    if(node == NULL){
        return upo_bst_createnode(key, value);
    }else if(key_cmp(key, node->key) == -1){
        node->left = upo_bst_insert_impl(node -> left, key, value, key_cmp);
    }else if(key_cmp(key, node->key) == 1){
        node->right = upo_bst_insert_impl(node -> right, key, value, key_cmp);
    }else{
        //printf("\nChiave già presente\n");
    }

    return node;
}

void upo_bst_insert(upo_bst_t tree, void *key, void *value)
{
    if(tree != NULL){
        tree -> root = upo_bst_insert_impl(tree -> root, key, value, tree -> key_cmp);
    }
}

void* upo_bst_get_impl(upo_bst_node_t *node, const void *key, upo_bst_comparator_t key_cmp){
   if(node == NULL){
        return NULL;
   }
   if(key_cmp(key, node -> key) == -1){
        return upo_bst_get_impl(node -> left, key, key_cmp);
   }else if(key_cmp(key, node -> key) == 1){
        return upo_bst_get_impl(node -> right, key, key_cmp);
   }else{
        return node;
   }
}

void* upo_bst_get(const upo_bst_t tree, const void *key)
{
	upo_bst_node_t *node = NULL;
	node = (upo_bst_node_t*)upo_bst_get_impl(tree -> root, key, tree->key_cmp);
    return node != NULL ? node->value : NULL;
}

int upo_bst_contains(const upo_bst_t tree, const void *key)
{
    return upo_bst_get_impl(tree->root, key, tree->key_cmp) != NULL ? 1 : 0;
}

void* upo_bst_find_min(upo_bst_node_t *node){
    if(node == NULL){
        return NULL;
    }else if(node -> left != NULL){
        return upo_bst_find_min(node -> left);
    }else{
        return node;
    }
}

void* upo_bst_delete1_CI_impl(upo_bst_node_t *node, int destroy_data){
    upo_bst_node_t *x = node;
    if(node -> left != NULL){
        node = node -> left;
    }else{
        node = node -> right;
    }
    if(destroy_data != 0){
        free(x -> value);
    }
    free(x);
    return node;
}

//Ho inserito la firma della funzione qui così risolvo il problema che chiamo la funzione prima che
//venga definita.
void* upo_bst_delete_impl(upo_bst_node_t *node, const void *key, int destroy_data, upo_bst_comparator_t key_cmp);

void* upo_bst_delete2_CI_impl(upo_bst_node_t *node, int destroy_data, upo_bst_comparator_t key_cmp){
    upo_bst_node_t *min = upo_bst_find_min(node -> right);
    node -> key = min -> key;
    node -> value = min -> value;
    node -> right = upo_bst_delete_impl(node -> right, min -> key, destroy_data, key_cmp);
    return node;
}

void* upo_bst_delete_impl(upo_bst_node_t *node, const void *key, int destroy_data, upo_bst_comparator_t key_cmp){
    if(node == NULL){
        return NULL;
    }
    if(key_cmp(key, node->key) == -1){
        node -> left = upo_bst_delete_impl(node -> left, key, destroy_data, key_cmp);
    }else if(key_cmp(key, node->key) == 1){
        node -> right = upo_bst_delete_impl(node -> right, key, destroy_data, key_cmp);
    }else if(node -> left != NULL && node -> right != NULL){
        node = upo_bst_delete2_CI_impl(node, destroy_data, key_cmp);
    }else{
        node = upo_bst_delete1_CI_impl(node, destroy_data);
    }

    return node;
}

void upo_bst_delete(upo_bst_t tree, const void *key, int destroy_data)
{
    if(tree != NULL){
        tree->root = upo_bst_delete_impl(tree -> root, key, destroy_data, tree->key_cmp);
    }
}

size_t upo_bst_size_impl(upo_bst_node_t *node){
    if(node == NULL){
        return 0;
    }
    return 1 + upo_bst_size_impl(node -> left) + upo_bst_size_impl(node -> right);
}

size_t upo_bst_size(const upo_bst_t tree)
{
    if(tree != NULL){
        return upo_bst_size_impl(tree -> root);
    }else{
        return 0;
    }

}

int upo_bst_isLeaf(upo_bst_node_t *node){
    return (node -> left == NULL && node -> right == NULL) ? 1 : 0;
}

size_t upo_bst_height_max(size_t x, size_t y){
    return x > y ? x : y;
}

size_t upo_bst_height_impl(upo_bst_node_t *node){
    if(node == NULL || upo_bst_isLeaf(node)){
        return 0;
    }
    return 1 + upo_bst_height_max(upo_bst_height_impl(node -> left), upo_bst_height_impl(node -> right));
}

size_t upo_bst_height(const upo_bst_t tree)
{
    return upo_bst_height_impl(tree -> root);
}

void upo_bst_traverse_in_order_impl(upo_bst_node_t *node, upo_bst_visitor_t visit, void *visit_context){
    if(node != NULL){
        upo_bst_traverse_in_order_impl(node -> left, visit, visit_context);
        visit(node -> key, node -> value, visit_context);
        upo_bst_traverse_in_order_impl(node -> right, visit, visit_context);
    }
}

void upo_bst_traverse_in_order(const upo_bst_t tree, upo_bst_visitor_t visit, void *visit_context)
{
    upo_bst_traverse_in_order_impl(tree -> root, visit, visit_context);
}

int upo_bst_is_empty(const upo_bst_t tree)
{
    return (tree == NULL || tree -> root == NULL) ? 1 : 0;
}


/**** EXERCISE #1 - END of FUNDAMENTAL OPERATIONS ****/


/**** EXERCISE #2 - BEGIN of EXTRA OPERATIONS ****/

void* upo_bst_min_impl(upo_bst_node_t *node){
    if(node == NULL){
        return NULL;
    }else if(node -> left != NULL){
        return upo_bst_min_impl(node -> left);
    }else{
        return node -> key;
    }
}

void* upo_bst_min(const upo_bst_t tree)
{
    if(tree != NULL){
        return upo_bst_min_impl(tree -> root);
    }else{
        return NULL;
    }
}

void* upo_bst_max_impl(upo_bst_node_t *node){
    if(node == NULL){
        return NULL;
    }else if(node -> right != NULL){
        return upo_bst_max_impl(node -> right);
    }else{
        return node -> key;
    }
}

void* upo_bst_max(const upo_bst_t tree)
{
    if(tree != NULL){
        return upo_bst_max_impl(tree -> root);
    }else{
        return NULL;
    }
}


void upo_bst_delete_min(upo_bst_t tree, int destroy_data)
{
    if(tree != NULL){
        void* key = upo_bst_min(tree);
        tree -> root = upo_bst_delete_impl(tree -> root, key, destroy_data, tree -> key_cmp);
    }
}

void upo_bst_delete_max(upo_bst_t tree, int destroy_data)
{
    if(tree != NULL){
        void* key = upo_bst_max(tree);
        tree -> root = upo_bst_delete_impl(tree -> root, key, destroy_data, tree -> key_cmp);
    }
}

void upo_bst_floor_impl(upo_bst_node_t *node, const void *key, upo_bst_comparator_t key_cmp, void **floor){
    if(node != NULL){
        if(key_cmp(node -> key, key) == 0){
            *floor = node->key;
            return;
        }
        if(key_cmp(node -> key, key) > 0){
            upo_bst_floor_impl(node ->left, key, key_cmp, floor);
        }
        if(key_cmp(node -> key, key) < 0){
            *floor = node->key;
            upo_bst_floor_impl(node -> right, key, key_cmp, floor);
        }
    }else{
        return;
    }
}

void* upo_bst_floor(const upo_bst_t tree, const void *key)
{
    void *floor = NULL;
    if(tree != NULL){
        upo_bst_floor_impl(tree->root, key, tree->key_cmp, &floor);
    }
    return floor;
}

void* upo_bst_ceiling_impl(upo_bst_node_t *node, const void *key, upo_bst_comparator_t key_cmp){
    if(node == NULL){
        return NULL;
    }
    if(key_cmp(node -> key, key) == 0){
        return node -> key;
    }

    if(key_cmp(node -> key, key) > 0){
        if(node -> left != NULL){
            void* left = upo_bst_ceiling_impl(node -> left, key, key_cmp);
            if(left != NULL){
                return left;
            }
        }
        return node -> key;
    }else{
        if(node -> right != NULL){
            return upo_bst_ceiling_impl(node -> right, key, key_cmp);
        }
    }
    return NULL;
}

void* upo_bst_ceiling(const upo_bst_t tree, const void *key)
{
    return upo_bst_ceiling_impl(tree -> root, key, tree->key_cmp);
}

void upo_list_head_insert(upo_bst_key_list_t *list, void* node_key){
    upo_bst_key_list_t head = malloc(sizeof(upo_bst_key_list_node_t));
    head -> key = node_key;
    head -> next = *list;
    *list = head; 
}

void upo_bst_keys_range_impl(upo_bst_node_t *node, const void *low_key, const void *high_key, upo_bst_key_list_t *list, upo_bst_comparator_t key_cmp){
    if(node != NULL){
        upo_bst_keys_range_impl(node -> left, low_key, high_key, list, key_cmp);
        if((key_cmp(node -> key, low_key) >= 0) && (key_cmp(node->key, high_key) <= 0)){
            upo_list_head_insert(list, node -> key);
        }
        upo_bst_keys_range_impl(node -> right, low_key, high_key, list, key_cmp);
    }else{
        return;
    }
}

upo_bst_key_list_t upo_bst_keys_range(const upo_bst_t tree, const void *low_key, const void *high_key)
{
    upo_bst_key_list_t list = NULL;
    if(tree != NULL){
        upo_bst_keys_range_impl(tree -> root, low_key, high_key, &list, tree->key_cmp);
    }
    return list;
}

void upo_bst_keys_impl(upo_bst_node_t *node, upo_bst_key_list_t *list){
    if(node != NULL){
        upo_bst_keys_impl(node -> left, list);
        upo_list_head_insert(list, node->key);
        upo_bst_keys_impl(node -> right, list);
    }else{
        return;
    }
}

upo_bst_key_list_t upo_bst_keys(const upo_bst_t tree)
{
    upo_bst_key_list_t list = NULL;
    if (tree != NULL) {
        upo_bst_keys_impl(tree -> root, &list);
    }
    return list;
}

//Sigla Europa League
/*
void upo_bst_is_bst_impl(upo_bst_node_t *node, const void *min_key, const void *max_key, int *isBst, upo_bst_comparator_t key_cmp){
    if(node != NULL){
        upo_bst_is_bst_impl(node -> left, min_key, max_key, isBst, key_cmp);
        if(!((key_cmp(node->key,min_key) >= 0) && (key_cmp(node->key,max_key) <= 0))){
            *isBst = 0;
            return;
        }
        upo_bst_is_bst_impl(node -> right, min_key, max_key, isBst, key_cmp);
    }else{
        return;
    }
}
*/

//È di bino
int upo_bst_is_bst_impl(const upo_bst_node_t *n, const void *min_key, const void *max_key, upo_bst_comparator_t key_cmp)
{
    if(n == NULL)
    {
        return 1;
    }
    
    if(key_cmp(n -> key, min_key) >= 0 && key_cmp(n -> key, max_key) <= 0)
    {
        return upo_bst_is_bst_impl(n -> left, min_key, n -> key, key_cmp) && upo_bst_is_bst_impl(n -> right, n -> key, max_key, key_cmp);
    }
    return 0;
}

//Vecchio mio
/*
int upo_bst_is_bst(const upo_bst_t tree, const void *min_key, const void *max_key)
{
    if(tree != NULL){
        int isBst = 1;
        upo_bst_is_bst_impl(tree -> root, min_key, max_key, &isBst, tree->key_cmp);
        printf("\n\t\t\tVALORE RITORNO %d\n", isBst);
        return isBst;
    }else{
        return 1;
    }
}
*/

int upo_bst_is_bst(const upo_bst_t tree, const void *min_key, const void *max_key){
    return upo_bst_is_bst_impl(tree -> root, min_key, max_key, tree -> key_cmp);
}


/**** EXERCISE #2 - END of EXTRA OPERATIONS ****/


upo_bst_comparator_t upo_bst_get_comparator(const upo_bst_t tree)
{
    if (tree == NULL)
    {
        return NULL;
    }

    return tree->key_cmp;
}
