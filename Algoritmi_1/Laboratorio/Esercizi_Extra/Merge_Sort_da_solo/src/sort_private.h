/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file sort_private.h
 *
 * \brief Private header for sorting algorithms.
 *
 * \author Your Name
 *
 * \copyright 2015 University of Piemonte Orientale, Computer Science Institute
 *
 *  This file is part of UPOalglib.
 *
 *  UPOalglib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  UPOalglib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UPOalglib.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UPO_SORT_PRIVATE_H
#define UPO_SORT_PRIVATE_H

#include <upo/sort.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

void upo_merge(void *base, size_t lo, size_t mid, size_t hi, size_t size, upo_sort_comparator_t cmp){
    size_t i = 0;
    size_t j = mid + 1 - lo;
    size_t n = hi - lo +1;

    unsigned char* arr = base;
    unsigned char* supp_arr = NULL;
    supp_arr = malloc(size*n);

    memcpy(supp_arr, arr+lo*size, n*size);
    
    for(size_t k = lo; k <= hi; ++k){
        if(i > (mid - lo)){
            memcpy(arr+k*size, supp_arr+j*size, size);
            ++j;
        }else if(j > (hi - lo)){
            memcpy(arr+k*size, supp_arr+i*size, size);
            ++i;
        }else if(cmp(supp_arr+j*size, supp_arr+i*size) < 0){
            memcpy(arr+k*size, supp_arr+j*size, size);
            ++j;
        }else{
            memcpy(arr+k*size, supp_arr+i*size, size);
            ++i;
        }
    }

    free(supp_arr);

}

void upo_merge_sort_rec(void *base, size_t lo, size_t hi, size_t size, upo_sort_comparator_t cmp){
    if(lo >= hi) return;

    size_t m = lo+(hi-lo)/2;
    upo_merge_sort_rec(base, lo, m, size, cmp);
    upo_merge_sort_rec(base, m+1, hi, size, cmp);
    upo_merge(base, lo, m, hi, size, cmp);
}

size_t upo_quick_sort_partition(void* base, size_t lo, size_t hi, size_t size, upo_sort_comparator_t cmp){
    size_t p = lo;
    size_t i = lo;
    size_t j = hi + 1;

    unsigned char *arr = base;

    while(1){
        do{
            ++i;
        }while(i <= hi && cmp(arr+i*size, arr+p*size) <= 0);

        do{
            --j;
        }while(j >= lo && cmp(arr+j*size, arr+p*size) > 0);
        
        if(i >= j){
            break;
        }
        void* tmp = malloc(size);
        //puoi anche usare memcpy, sono equivalenti.
        memmove(tmp, arr+i*size, size);
        memmove(arr+i*size, arr+j*size, size);
        memmove(arr+j*size, tmp, size);
        free(tmp);
    }
    void* tmp = malloc(size);
    //puoi anche usare memcpy, sono equivalenti.
    memmove(tmp, arr+p*size, size);
    memmove(arr+p*size, arr+j*size, size);
    memmove(arr+j*size, tmp, size);
    free(tmp);

    return j;
}

void upo_quick_sort_rec(void* base, size_t lo, size_t hi, size_t size, upo_sort_comparator_t cmp){
    if(lo >= hi){
        return;
    }
    size_t j = upo_quick_sort_partition(base, lo, hi, size, cmp);
    if(j > 0){
        upo_quick_sort_rec(base, lo, j-1, size, cmp);
    }
    upo_quick_sort_rec(base, j+1, hi, size, cmp);
}


/* TO STUDENTS:
 *
 *  This file is currently "empty".
 *  Here you can put the prototypes of auxiliary "private" functions that are
 *  internally used by "public" functions.
 *  For instance, you can declare the prototype of the function that performs
 *  the "merge" operation in the merge-sort algorithm:
 *    static void upo_merge_sort_merge(void *base, size_t lo, size_t mid, size_t hi, size_t size, upo_sort_comparator_t cmp);
 *  Also, you can declare the prototype of the function that performs the
 *  the recursion in the merge-sort algorithm:
 *    static void upo_merge_sort_rec(void *base, size_t lo, size_t hi, size_t size, upo_sort_comparator_t cmp);
 *  Further, you can declare the prototype of the function that performs the
 *  "partition" operation in the quick-sort algorithm:
 *    static size_t upo_quick_sort_partition(void *base, size_t lo, size_t hi, size_t size, upo_sort_comparator_t cmp);
 *  And so on.
 *
 */


#endif /* UPO_SORT_PRIVATE_H */
