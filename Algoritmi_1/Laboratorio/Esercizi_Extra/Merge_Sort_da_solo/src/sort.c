/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2015 University of Piemonte Orientale, Computer Science Institute
 *
 * This file is part of UPOalglib.
 *
 * UPOalglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * UPOalglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with UPOalglib.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include "sort_private.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void upo_swap(unsigned char *el1, unsigned char *el2, size_t size)
{
    unsigned char *tmp = malloc(size);
    memcpy(tmp, el1, size);
    memcpy(el1, el2, size);
    memcpy(el2, tmp, size);
    free(tmp);
}


void upo_selection_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp){
    unsigned char *arr = base;

    for (size_t i = 0; i < n - 1; ++i) {
        size_t min = i;
        size_t j = i+1;
        for (j = i+1; j < n; ++j) {
            if(cmp(arr+min*size, arr+j*size) > 0){
                min = j;
            }
        }
        //Solo per un minimo di ottimizzazione
        if(min != i){
            //upo_swap(arr+min*size, arr+i*size, size);
            unsigned char *tmp = malloc(size);
            memcpy(tmp, arr+min*size, size);
            memcpy(arr+min*size, arr+i*size, size);
            memcpy(arr+i*size, tmp, size);
            free(tmp);
        }
    }
}















void upo_bubble_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
    unsigned char *arr = base;

    int hasSwapped = 1;

    while (hasSwapped != 0)
    {
        hasSwapped = 0;
        for (size_t i = 0; i < n - 1; ++i)
        {
            if (cmp(arr + i * size, arr + (i + 1) * size) > 0)
            {
                upo_swap(arr + i * size, arr + (i + 1) * size, size);
                hasSwapped = 1;
            }
        }
    }
}

void upo_insertion_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
    unsigned char *arr = base;

    for (size_t i = 1; i <= n - 1; i++)
    {
        size_t j = i;

        while (j > 0 && cmp(&(arr[(j - 1) * size]), &(arr[j * size])) >= 1)
        {
            unsigned char temp;
            for (size_t z = 0; z < size; ++z)
            {
                temp = arr[j * size + z];
                arr[j * size + z] = arr[(j - 1) * size + z];
                arr[(j - 1) * size + z] = temp;
            }
            --j;
        }
    }

    /*
    unsigned char *arr = base;

    for (size_t i = 1; i < n; i++)
    {
        printf("Versione rotta\n");
        fflush(stdout);
        size_t j = i;

        while (j > 0 && (cmp(&arr[(j - 1) * size], &arr[(j)*size]) >= 1))
        {
            unsigned char *temp = malloc(size);
            memcpy(&temp, &arr[j], size);
            memcpy(&arr[j], &arr[j - 1], size);
            memcpy(&arr[j - 1], &temp, size);
            j = j - 1;
        }
    }
    */

    // fprintf(stderr, "To be implemented!\n");
    // abort();
}

void upo_merge_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
    size_t start = 0;
    size_t end = n;
    upo_merge_sort_rec(base, start, end - 1, size, cmp);
}

void upo_quick_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
    size_t start = 0;
    size_t end = n;
    upo_quick_sort_rec(base, start, end - 1, size, cmp);
}
