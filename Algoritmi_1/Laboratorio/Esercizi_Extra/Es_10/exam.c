/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/******************************************************************************/
/*** NOME:                                                                  ***/
/*** COGNOME:                                                               ***/
/*** MATRICOLA:                                                             ***/
/******************************************************************************/

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <upo/bst.h>
#include <upo/sort.h>

/**** BEGIN of EXERCISE #1 ****/

upo_bst_node_t *upo_bst_heigth_and_val_impl(upo_bst_node_t *node, const void *key, int *height, upo_bst_comparator_t key_cmp)
{
    if (node == NULL)
    {
        return NULL;
    }
    if (key_cmp(node->key, key) == 0)
    {
        return node;
    }
    if (key_cmp(node->key, key) > 0)
    {
        *height = *height + 1;
        upo_bst_heigth_and_val_impl(node->left, key, height, key_cmp);
    }
    else
    {
        *height = *height + 1;
        upo_bst_heigth_and_val_impl(node->right, key, height, key_cmp);
    }
}

void upo_bst_heigth_and_val(const upo_bst_t bst, const void *key)
{
    upo_bst_node_t *result = NULL;
    int height = 0;
    if (bst != NULL)
    {
        result = upo_bst_heigth_and_val_impl(bst->root, key, &height, bst->key_cmp);
        if (result == NULL) {
            height = -1;
        }
    }
    printf("\t\t\nALTEZZA: %d\n", height);
    //Non faccio la stampa del nodo perché sennò si spacca tutto dato che non sempre sono interi
    // printf("\t\t\nVALORE: %d\n", (*(int *)result->value));

    // return result;
}

const void *upo_bst_predecessor(const upo_bst_t bst, const void *key)
{
    upo_bst_heigth_and_val(bst, key);

    return NULL;
}

/**** END of EXERCISE #1 ****/

/**** BEGIN of EXERCISE #2 ****/

void upo_swap(void *a, void *b, size_t sz)
{
    unsigned char *aa = a;
    unsigned char *bb = b;
    while (sz-- > 0)
    {
        unsigned char tmp = *aa;
        *aa = *bb;
        *bb = tmp;
        ++aa;
        ++bb;
    }
}

void upo_bidi_bubble_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
    unsigned char *arr = base;

    int toContinue = 1;

    while (toContinue != 0)
    {
        toContinue = 0;
        for (size_t i = 0; i < n - 1; ++i)
        {
            if (cmp((arr + i * size), (arr + (i + 1) * size)) > 0)
            {
                // bisogno di swap
                toContinue = 1;
                // upo_swap(arr + i*size, arr+(i+1)*size, size);

                unsigned char *tmp = malloc(size);
                memmove(tmp, arr + i * size, size);
                memmove(arr + i * size, arr + (i + 1) * size, size);
                memmove(arr + (i + 1) * size, tmp, size);
                free(tmp);
            }
        }

        for (size_t j = n - 1; j > 1; --j)
        {
            if (cmp((arr + (j - 1) * size), (arr + j * size)) > 0)
            {
                // bisogno di swap
                toContinue = 1;
                // upo_swap(arr+(j-1)*size, arr+j*size, size);

                void *tmp = malloc(size);
                memmove(tmp, arr + j * size, size);
                memmove(arr + j * size, arr + (j - 1) * size, size);
                memmove(arr + (j - 1) * size, tmp, size);
                free(tmp);
            }
        }
    }
}

/**** END of EXERCISE #2 ****/
