#include <stdio.h>
#include <string.h>
#include <assert.h>

/*
  Esercizio errato, probabilmente c'è qualcoda di sbagliato con l'if.
  Con i numeri tutto ok, quando uso le stringhe si rompe tutto
*/

int is_even(const void *v) {
    const int *num = (const int *)v;
    printf("%d\n", *num);
    return *num % 2 == 0 ? 1 : 0;
}

int is_palindrome(const void *v){
  assert( v );
  const char **ps = (const char**) v;
  size_t len = strlen(*ps);
  for (size_t i = 0; i < len/2; ++i){
    if ((*ps)[i] != (*ps)[len-i-1]) return 0;
    printf("Tra un po mi rompo\n");
    fflush(stdout);
  }
  printf("Col cazzo che mi rompo\n");
  return 1;
}


int upo_all_of(const void *base, size_t n, size_t size, int (*pred)(const void *)){
  int isEquals = 1;

  const unsigned char *arr = base;

  printf("Dimensione arr: %ld\n", n);

  for (size_t i = 0; i < n; ++i ) {
    if(pred(arr+i*size) == 0){
      isEquals = 0;
      return isEquals;
    }
  } 

  return isEquals;
} 

int main(){
  int arr[4] = {2, 4, 6, 666};
  char *arr2[4] = {"anna"};

  if(upo_all_of(arr, sizeof(arr)/sizeof(arr[0]), sizeof(arr[0]), is_even)) {
    printf("All even number in arr");
  } else{
    printf("Not all even number in arr");
  }
  /*
  if(upo_all_of(arr2, sizeof(arr2)/sizeof(arr2[0]), sizeof(arr2[0]), is_palindrome)) {
    printf("Is palindrome");
  } else{
    printf("Not is palindrome");
  }
  */
	return 0;
}
