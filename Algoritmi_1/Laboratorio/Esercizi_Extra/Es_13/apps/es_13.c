#include<stdio.h>
#include <string.h>
#include <upo/hashtable.h>
#include <upo/error.h>

int str_compare(const void *a, const void *b)
{
    const char **aa = (const char**) a;
    const char **bb = (const char**) b;

    return strcmp(*aa, *bb);
}

int main(){
  upo_ht_sepchain_t ht = upo_ht_sepchain_create(UPO_HT_SEPCHAIN_DEFAULT_CAPACITY, upo_ht_hash_str_kr2e, str_compare);
  char** strs = {"Tre", "tigri", "contro", "tre", "tigri"};


  for (size_t i = 0; i < sizeof(strs)/sizeof(strs[0]); ++i){
        upo_ht_sepchain_insert(ht, &strs[i], &strs[i]);
  }

  return 0;
}
