/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/******************************************************************************/
/*** NOME:                                                                  ***/
/*** COGNOME:                                                               ***/
/*** MATRICOLA:                                                             ***/
/******************************************************************************/

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <upo/bst.h>
#include <upo/sort.h>

/**** BEGIN of EXERCISE #1 ****/
void upo_bst_predecessor_impl(upo_bst_node_t* node, const void* key, void** pred, upo_bst_comparator_t key_cmp){
    if(node == NULL) return;

    if(key_cmp(node->key, key) >= 0){
        upo_bst_predecessor_impl(node->left, key, pred, key_cmp);
    }else{
        *pred = node->key;
        upo_bst_predecessor_impl(node->right, key, pred, key_cmp);
    }
}

const void *upo_bst_predecessor(const upo_bst_t bst, const void *key)
{
    if(bst == NULL) return NULL;

    void* pred = NULL;
    upo_bst_predecessor_impl(bst->root, key, &pred, bst->key_cmp);

    return pred;
}

/**** END of EXERCISE #1 ****/

/**** BEGIN of EXERCISE #2 ****/
void upo_bst_successor_impl(upo_bst_node_t* node, const void *key, void** succ, upo_bst_comparator_t key_cmp){
    if(node == NULL) return;

    if(key_cmp(node->key, key) > 0){
        *succ = node->key;
        upo_bst_successor_impl(node->left, key, succ, key_cmp);
    }else{
        upo_bst_successor_impl(node->right, key, succ, key_cmp);
    }
}

const void *upo_bst_successor(const upo_bst_t bst, const void *key)
{
    if(bst == NULL) return NULL;

    void* succ = NULL;
    upo_bst_successor_impl(bst->root, key, &succ, bst->key_cmp);

    return succ;
}
/**** END of EXERCISE #2 ****/
