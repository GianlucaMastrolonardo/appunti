/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2015 University of Piemonte Orientale, Computer Science Institute
 *
 * This file is part of UPOalglib.
 *
 * UPOalglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * UPOalglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with UPOalglib.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include "sort_private.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void upo_swap(void* el1, void* el2, size_t size){
    void* tmp = malloc(size);
    memcpy(tmp, el1, size);
    memcpy(el1, el2, size);
    memcpy(el2, tmp, size);
    free(tmp);
}

void upo_selection_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp){
    unsigned char* arr = base;

    for(size_t i = 0; i < n-1; ++i){
        size_t min = i;
        for (size_t j = i+1; j < n; ++j) {
            if(cmp(arr+min*size, arr+j*size) > 0){
                //Trovato un nuovo minimo
                min = j;
            }
        }
        //Minima ottimizzazione
        if(min != i){
            upo_swap(arr+min*size, arr+i*size, size);
        }
    }
}

void upo_bubble_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp){
    unsigned char* arr = base;

    for(size_t i = n - 1; i > 0; --i){
        for(size_t j = 0; j < i; ++j){
            if(cmp(arr+j*size, arr+(j+1)*size) > 0){
                //Swap 
                upo_swap(arr+j*size, arr+(j+1)*size, size);
            }
        }
    }
}

void upo_insertion_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
    unsigned char* arr = base;

    for(size_t i = 1; i < n; ++i){
        if(cmp(arr+(i-1)*size, arr+i*size) > 0){
            unsigned char* el = malloc(size);
            memcpy(el, arr+i*size, size);
            size_t j = i;
            do{
                memcpy(arr+j*size, arr+(j-1)*size, size);
                if(cmp(arr+j*size, el) < 0){
                    break;
                }
                else{
                    --j;
                }
            }while(j > 0);
            memcpy(arr+j*size, el, size);
            free(el);
        }
    }
}

void upo_merge_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
    upo_merge_sort_rec(base, 0, n-1, size, cmp);
}

void upo_quick_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
    upo_quick_sort_rec(base, 0, n-1, size, cmp);
}

void upo_quick_sort_optimized(void *base, size_t n, size_t size, upo_sort_comparator_t cmp){
    upo_quick_sort_rec_opt(base, 0, n-1, size, cmp);
}
