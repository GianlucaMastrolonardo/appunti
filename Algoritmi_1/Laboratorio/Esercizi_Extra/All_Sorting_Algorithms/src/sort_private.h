/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file sort_private.h
 *
 * \brief Private header for sorting algorithms.
 *
 * \author Your Name
 *
 * \copyright 2015 University of Piemonte Orientale, Computer Science Institute
 *
 *  This file is part of UPOalglib.
 *
 *  UPOalglib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  UPOalglib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UPOalglib.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UPO_SORT_PRIVATE_H
#define UPO_SORT_PRIVATE_H

#include <upo/sort.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

void upo_swap_intern(void* el1, void* el2, size_t size){
    void* tmp = malloc(size);
    memcpy(tmp, el1, size);
    memcpy(el1, el2, size);
    memcpy(el2, tmp, size);
    free(tmp);
}


void upo_merge(void *base, size_t lo, size_t mid, size_t hi, size_t size, upo_sort_comparator_t cmp){
    unsigned char *arr = base;
    unsigned char *aux = NULL;

    size_t n = hi-lo+1;
    size_t i = 0;            //Indice per prima metà
    size_t j = mid+1-lo;    //Indice per seconda metà

    size_t k;

    aux = malloc(n*size);

    if(aux == NULL){
        perror("Impossibile allocare spazio");
        abort();
    }

    memcpy(aux, arr+lo*size, n*size); //Copio in aux tutto l'array
    
    for(k = lo; k <= hi; ++k){
        //L'ordine dei due if è importante
        //Se la prima metà dell'array finita, copio gli elementi nella seconda metà
        if(i > (mid-lo)){
            memcpy(arr+k*size, aux+j*size, size);
            ++j;
        }
        //Se la seconda metà dell'array è finita
        else if(j > (hi-lo)){
            memcpy(arr+k*size, aux+i*size, size);
            ++i;
        }
        //Se elemento nella prima è minore di quello della seconda metà 
        else if(cmp(aux+i*size, aux+j*size) < 0){
            memcpy(arr+k*size, aux+i*size, size);
            ++i;
        }else{
            memcpy(arr+k*size, aux+j*size, size);
            ++j;
        }
    }

    free(aux);

}

void upo_merge_sort_rec(void *base, size_t lo, size_t hi, size_t size, upo_sort_comparator_t cmp){
    if(lo >= hi) return;
    size_t mid = lo+(hi-lo)/2;
    upo_merge_sort_rec(base, lo, mid, size, cmp);
    upo_merge_sort_rec(base, mid+1,hi,size, cmp);
    upo_merge(base, lo, mid, hi, size, cmp);
}

size_t upo_quick_sort_partition(void* base, size_t lo, size_t hi, size_t size, upo_sort_comparator_t cmp){
    unsigned char* arr = base;
    unsigned char* x = arr+lo*size; //Il pivot

    size_t inf = lo;
    size_t sup = hi + 1;

    while(1){
        do{
            ++inf;
        }while(inf <= hi && cmp(arr+inf*size, x) <= 0);

        do{
            --sup;
        }while(cmp(arr+sup*size, x) > 0);
        if(inf < sup){
            upo_swap_intern(arr+inf*size, arr+sup*size, size);
        }else{
            break;
        }
    }

    upo_swap_intern(arr+lo*size, arr+sup*size, size);
    return sup;
}

void upo_quick_sort_rec(void* base, size_t lo, size_t hi, size_t size, upo_sort_comparator_t cmp){
    if(lo >= hi) return;
    size_t m = upo_quick_sort_partition(base, lo, hi, size, cmp);
    upo_quick_sort_rec(base, lo, m, size, cmp);
    upo_quick_sort_rec(base, m+1, hi, size, cmp);
}

size_t upo_quick_sort_partition_opt(void* base, size_t lo, size_t hi, size_t size, upo_sort_comparator_t cmp){
    unsigned char* arr = base;
    
    size_t mid = (lo+hi)/2;

    
    if(cmp(arr+lo*size, arr+mid*size) >= 0){
        upo_swap_intern(arr+lo*size, arr+mid*size, size);
    }
    if(cmp(arr+mid*size, arr+hi*size) >= 0){
        upo_swap_intern(arr+mid*size, arr+hi*size, size);
    }
    if(cmp(arr+lo*size, arr+mid*size) >= 0){
        upo_swap_intern(arr+lo*size, arr+mid*size, size);
    }

    unsigned char* x = arr+mid*size; //Il pivot

    
    size_t inf = lo;
    size_t sup = hi + 1;

    while(1){
        do{
            ++inf;
        }while(inf <= hi && cmp(arr+inf*size, x) <= 0);

        do{
            --sup;
        }while(cmp(arr+sup*size, x) > 0);
        if(inf < sup){
            upo_swap_intern(arr+inf*size, arr+sup*size, size);
        }else{
            break;
        }
    }

    upo_swap_intern(arr+lo*size, arr+sup*size, size);
    return sup;
}

void upo_quick_sort_rec_opt(void* base, size_t lo, size_t hi, size_t size, upo_sort_comparator_t cmp){
    if(lo >= hi) return;
    size_t m = upo_quick_sort_partition_opt(base, lo, hi, size, cmp);
    upo_quick_sort_rec_opt(base, lo, m, size, cmp);
    upo_quick_sort_rec_opt(base, m+1, hi, size, cmp);
}

/* TO STUDENTS:
 *
 *  This file is currently "empty".
 *  Here you can put the prototypes of auxiliary "private" functions that are
 *  internally used by "public" functions.
 *  For instance, you can declare the prototype of the function that performs
 *  the "merge" operation in the merge-sort algorithm:
 *    static void upo_merge_sort_merge(void *base, size_t lo, size_t mid, size_t hi, size_t size, upo_sort_comparator_t cmp);
 *  Also, you can declare the prototype of the function that performs the
 *  the recursion in the merge-sort algorithm:
 *    static void upo_merge_sort_rec(void *base, size_t lo, size_t hi, size_t size, upo_sort_comparator_t cmp);
 *  Further, you can declare the prototype of the function that performs the
 *  "partition" operation in the quick-sort algorithm:
 *    static size_t upo_quick_sort_partition(void *base, size_t lo, size_t hi, size_t size, upo_sort_comparator_t cmp);
 *  And so on.
 *
 */


#endif /* UPO_SORT_PRIVATE_H */
