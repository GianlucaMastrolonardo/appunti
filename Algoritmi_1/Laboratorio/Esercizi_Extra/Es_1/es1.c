#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void upo_hex_fprintf(FILE *stream, const void *p, size_t n){
	const unsigned char *arr = p;


	for(size_t i = 0; i < n; ++i){
		fprintf(stream, "%02X", arr[i]);
		if(i < n-1) printf(" ");
	}
}

int main(){
	char *s = "Hello World!";
	upo_hex_fprintf(stdout, s, strlen(s));
	printf("\n");

	char cary[] = "GNU is Not Unix";
	upo_hex_fprintf(stdout, cary+(sizeof cary)/2, sizeof(cary) - sizeof(cary)/2);
	return 0;
}
