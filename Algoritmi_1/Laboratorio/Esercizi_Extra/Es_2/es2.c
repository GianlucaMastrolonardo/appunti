#include <stdio.h>
#include <string.h>
void upo_mem_set(void* p, unsigned char c, size_t size){
	unsigned char* arr = p;

	for(size_t i = 0; i < size; ++i){
		arr[i] = c;
	}
}

int main(){

	int i = 10;
	upo_mem_set(&i, 0, sizeof(i));

	printf("%d\n",i);
	
	char cary[] = "Hello, World!";
	upo_mem_set(cary, '?', strlen(cary));
	for(size_t i = 0; i < strlen(cary); i++){
		printf("%c", cary[i]);
	}
	printf("\n");

	unsigned char ucary[] = {255,128,64,32,16,8};
	upo_mem_set(ucary, 127, (sizeof ucary)/2);
	for(size_t i = 0; i < sizeof(ucary); i++){
		printf("%u ", ucary[i]);
	}

	return 0;
}
