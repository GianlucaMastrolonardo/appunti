#include <stdio.h>
#include <string.h>

int upo_mem_cmp(const void *p1, const void *p2, size_t n)
{
   int isEquals = 0;

   const unsigned char *pp1 = p1;
   const unsigned char *pp2 = p2;

   for (size_t i = 0; i < n && isEquals == 0; ++i)
   {
      isEquals = pp1[i] - pp2[i];
      //isEquals = *(pp1 + i * n) - *(pp2 + i * n); questa soluzione è sbagliata
   }

   return isEquals;
}

int main()
{
   char str1[15];
   char str2[15];
   int ret;

   memcpy(str1, "abcdef", 6);
   memcpy(str2, "abcdef", 6);

   ret = upo_mem_cmp(str1, str2, 5);
   printf("Ret: %d\n", ret);

   if (ret > 0)
   {
      printf("str2 is less than str1");
   }
   else if (ret < 0)
   {
      printf("str1 is less than str2");
   }
   else
   {
      printf("str1 is equal to str2");
   }

   int a = 8;
   int b = 8;

   ret = upo_mem_cmp(&a, &b, 1);

   printf("\n\nRet: %d\n", ret);

   if (ret > 0)
   {
      printf("b is less than a");
   }
   else if (ret < 0)
   {
      printf("a is less than b");
   }
   else
   {
      printf("a is equal to b");
   }

   return 0;
}
