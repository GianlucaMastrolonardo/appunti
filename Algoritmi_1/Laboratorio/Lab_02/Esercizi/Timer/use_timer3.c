/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

#include <math.h>
#include "timer3.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

static void do_something(long n)
{
    while (n > 0)
    {
        exp(sqrt(n));
        --n;
    }
}

int main(int argc, char *argv[])
{
    long n = 200000000L;
    timer3_t timer;

    if (argc > 1)
    {
        n = atol(argv[1]);
    }

    timer = timer3_create();

    timer3_start(timer);

    do_something(n);

    timer3_stop(timer);

    printf("Elapsed: %f seconds.\n", timer3_elapsed(timer));

    /* Won't compile since information hiding is enforced
    printf("Started at: %s", ctime(&timer->start));
    printf("Stopped at: %s", ctime(&timer->stop));
    */

    // Epoch to Human date here: https://www.epochconverter.com/
    printf("Started at: %jd\n", (intmax_t)timer3_get_start_time(timer));
    printf("Stopped at: %jd\n", (intmax_t)timer3_get_stop_time(timer));

    timer3_reset(timer);

    // Wait
    for (int i = 0; i < 5; i++)
    {
        do_something(n);
    }

    timer3_start(timer);

    do_something(n);

    timer3_stop(timer);

    printf("Elapsed: %f seconds.\n", timer3_elapsed(timer));

    /* Won't compile since information hiding is enforced
    printf("Started at: %s", ctime(&timer->start));
    printf("Stopped at: %s", ctime(&timer->stop));
    */

    // Epoch to Human date here: https://www.epochconverter.com/
    printf("Started at: %jd\n", (intmax_t)timer3_get_start_time(timer));
    printf("Stopped at: %jd\n", (intmax_t)timer3_get_stop_time(timer));

    timer3_destroy(timer);

    // timer3_is_started(timer);
    // timer3_start(timer);
    // printf("Started at: %jd\n", (intmax_t)timer3_get_start_time(timer));

    return 0;
}
