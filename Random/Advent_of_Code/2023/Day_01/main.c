/*
--- Day 1: Trebuchet?! ---
Something is wrong with global snow production, and you've been selected to take a look.
The Elves have even given you a map; on it, they've used stars to mark the top fifty locations that are likely to be having problems.
You've been doing this long enough to know that to restore snow operations, you need to check all fifty stars by December 25th.
Collect stars by solving puzzles. Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first.
Each puzzle grants one star. Good luck!

You try to ask why they can't just use a weather machine ("not powerful enough") and where they're even sending you ("the sky")
and why your map looks mostly blank ("you sure ask a lot of questions") and hang on did you just say the sky ("of course, where do you think snow comes from")
when you realize that the Elves are already loading you into a trebuchet ("please hold still, we need to strap you in").
As they're making the final adjustments, they discover that their calibration document (your puzzle input) has been amended by a
very young Elf who was apparently just excited to show off her art skills. Consequently, the Elves are having trouble reading the values on the document.

The newly-improved calibration document consists of lines of text; each line originally contained a specific
calibration value that the Elves now need to recover. On each line, the calibration value can be found by combining the first digit and the last digit
(in that order) to form a single two-digit number.

For example:

1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
In this example, the calibration values of these four lines are 12, 38, 15, and 77. Adding these together produces 142.

Consider your entire calibration document. What is the sum of all of the calibration values?

To play, please identify yourself via one of these services:
*/

// Program to implement atoi() in C
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

typedef struct linked_list
{
    int d;
    struct linked_list *next;
} ELEMENT;

typedef ELEMENT *LINK;

LINK newnode()
{
    return malloc(sizeof(ELEMENT));
}

LINK headInsert(LINK list, int val)
{
    LINK p;
    p = newnode();
    p->d = val;
    p->next = list;
    return p;
}

int sumAllValues(LINK list)
{
    int cnt = 0;
    while (list != NULL)
    {
        cnt = list->d + cnt;
        list = list->next;
    }
    return cnt;
}

int conc(int x, int y)
{
    int pow = 10;
    while (y >= pow)
        pow *= 10;
    return x * pow + y;
}

int concOfMinMax(char *str, size_t string_dimension)
{
    int min = 999;
    int max = -1;
    for (size_t i = 0; i < string_dimension; ++i)
    {
        if (isdigit(str[i]))
        {
            int val = str[i] - '0';
            if (val < min)
            {
                min = val;
            }
            if (val > max)
            {
                max = val;
            }
        }
        else
        {
        }
    }
    if (min == 999 || max == -1)
    {
        printf("Errore nella ricerca del Min/Max\n");
        exit(1);
    }
    return conc(min, max);
}

int readStringFromFile(char *filename)
{
    FILE *fPtr = fopen(filename, "r");
    if (fPtr == NULL)
    {
        printf("Addio");
        exit(1);
    }
    else
    {
        LINK head;

        char string[100]; // Adjust the size according to your requirements

        while (!feof(fPtr)) // Read a string into the buffer
        {
            fscanf(fPtr, "%s\n", string);
            int result = concOfMinMax(string, strlen(string));
            head = headInsert(head, result);
        }
        fclose(fPtr);
        return sumAllValues(head);
    }
}

int main()
{

    int result = readStringFromFile("input.txt");
    printf("\n%d\n", result);

    return 0;
}