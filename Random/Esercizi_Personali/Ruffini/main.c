// Ruffini

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DIM 4

int power(int base, int exponent)
{
    int result = 1;
    if (base == 0)
    {
        return 0;
    }
    else
    {
        while (exponent > 0)
        {
            result *= base;
            exponent--;
        }
    }

    return result;
}

int findSquare(int eq[], int dim)
{
    // Valore temporaneo usato per il confonto nel While finale

    int square = -99;

    int termineNoto = abs(eq[0]);
    int cnt = 0;

    int possibiliRadici[termineNoto * 2];

    for (int i = 0; i < termineNoto * 2; i++)
    {
        possibiliRadici[i] = -99;
    }

    for (int i = 1; i <= termineNoto; i++)
    {
        if (termineNoto % i == 0)
        {
            possibiliRadici[cnt] = i;
            cnt++;
        }
    }

    int cnt2 = 0;

    // Aggiungo i nelle possibiliRadici i valori negativi
    for (int i = 0; i < termineNoto * 2; i++)
    {
        if ((i > cnt - 1) && (cnt2 != cnt))
        {
            possibiliRadici[i] = -possibiliRadici[cnt2];
            cnt2++;
        }
    }

    cnt = 0;
    int expression;

    while (possibiliRadici[cnt] != -99 && square != 99)
    {
        expression = 0;
        for (int i = 1; i < dim; i++)
        {
            expression += eq[i] * power(possibiliRadici[cnt], i);
        }
        expression += eq[0];
        if (expression == 0)
        {
            square = possibiliRadici[cnt];
        }
        cnt++;
    }

    return square;
}

void solveEquation(int eq[], int dim)
{
    int cnt = 0;
    int square = findSquare(eq, dim);

    printf("Radice: %d\n", square);

    int solution[dim - 1];

    for (int i = dim - 1; i > 0; i--)
    {
        if (i == dim - 1)
        {
            // Porto giù il primo valore
            solution[cnt] = eq[i];
        }
        else
        {
            // Moltiplico il valore
            printf("EQ:%d\n", eq[i]);
            solution[cnt] = (solution[cnt - 1] * square) + eq[i];
        }
        cnt++;
    }
    cnt--;
    printf("( x %d ) (", -square);
    for (int i = 0; i <= dim - 2; i++)
    {
        printf(" %dx^%d", solution[i], cnt);
        cnt--;
    }
    printf(" )");
}

int main()
{

    // Esempio1
    //  x^3 + 2x -3 = 0
    // La radice dovrebbe venire 1
    // int equationValues[DIM] = {-3, 2, 0, 1};

    // Esempio2
    //  x^3 - x -24 = 0
    // int equationValues[DIM] = {-24, -1, 0, 1};

    // Esempio3 con radice negativa (nella stampa dovrebbe essere positiva);
    int equationValues[DIM] = {4, 2, 2, 1};

    // Esempio4 con dimensione equazione di grado più alto, funzionante! (BISOGNA CAMBIARE DIM)
    // int equationValues[DIM] = {+16, -8, 0, -2, 1};

    solveEquation(equationValues, DIM);

    return 0;
}
