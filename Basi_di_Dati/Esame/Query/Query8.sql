---Query 8
---Restituire nome, cognome e età (in anni) degli amministratori di canale che gestiscono almeno 10 canali con al massimo 100 followers.
---Ordinare il risultato in ordine decrescente per totale corrispettivo pagato al provider per il servizio di hosting.
---Nell'esempio, l'amministratore Luigi Gialli di 38 anni gestisce 30 canali per un totale di 700 followers. Luigi Gialli, per tutti e 30 i suoi canali,
---ha pagato il servizio di hosting per un totale di 2000 dollari.
SELECT canale.id_amministratore,
    count(canale.id_amministratore) as NumeroCanaliGesiti,
    max(canale.numero_seguiti) as MaxFollowers,
    (
        SELECT count(pagamento.id_amministratore) * 50
        FROM pagamento
        WHERE pagamento.id_amministratore = canale.id_amministratore
    ) as Pagamenti
FROM canale
GROUP BY canale.id_amministratore
HAVING count(canale.id_amministratore) >= 2
    AND max(canale.numero_seguiti) <= 1000;
--Ho cambiato un po' i valori dei controlli giusto da avere un risultato