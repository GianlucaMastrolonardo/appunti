---Query 10
---Per ogni administrator, restituire il numero dei canali sospesi i cui proprietari (gli streamers) NON sono affiliate.
---Scrivere la query utilizzando gli operatori insiemistici.
SELECT *
FROM canale
    JOIN streamer ON canale.nome_canale = streamer.nome_streamer
WHERE streamer.affiliato = TRUE;