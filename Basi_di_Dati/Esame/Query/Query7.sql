---Query 7
---Per ogni streamer di età compresa tra [25<=x<=40] anni CHE NON E' AFFILIATE, elencare il numero di video e il totale dei minuti che hanno trasmesso.
---Ordinare il risultato in ordine decrescente di TotaleMinutiTrasmessi.
---Nell'esempio, lo streamer di 28 anni Luigi Verdi (che non soddisfa i requisiti di affiliate) ha un totale di 900 video di una durata complessiva di 98740923 minuti.
SELECT streamer.nome_streamer,
    count(video.titolo) as NumeroVideo,
    streamer.minuti_trasmessi
FROM utente
    JOIN streamer ON utente.nome_utente = streamer.nome_streamer
    JOIN video ON video.nome_canale = streamer.nome_streamer
WHERE streamer.affiliato = FALSE
    AND (
        (
            EXTRACT (
                YEAR
                FROM CURRENT_DATE
            ) - EXTRACT (
                YEAR
                FROM utente.data_nascita
            )
        ) >= 25
        AND(
            EXTRACT (
                YEAR
                FROM CURRENT_DATE
            ) - EXTRACT (
                YEAR
                FROM utente.data_nascita
            )
        ) <= 40
    )
GROUP BY streamer.nome_streamer;