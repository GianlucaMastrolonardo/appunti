---Query 2
---Selezionare gli streamer di età compresa tra [30<=x<=40] anni (estremi inclusi) che stanno trasmettendo almeno 2 live i diretta.
---Ordinare le live in ordine decrescente per data di inizio.
SELECT utente.nome_utente,
    utente.data_nascita,
    (
        EXTRACT (
            YEAR
            FROM CURRENT_DATE
        ) - EXTRACT (
            YEAR
            FROM utente.data_nascita
        )
    ) AS Età,
    count(live.nome_canale) as LiveInDiretta,
    max(live.inizio) AS DataLive
FROM utente
    JOIN live ON utente.nome_utente = live.nome_canale
WHERE (
        EXTRACT(
            YEAR
            FROM CURRENT_DATE
        ) - EXTRACT(
            YEAR
            FROM utente.data_nascita
        )
    ) <= 40
    AND (
        EXTRACT(
            YEAR
            FROM CURRENT_DATE
        ) - EXTRACT(
            YEAR
            FROM utente.data_nascita
        )
    ) >= 30
AND live.fine IS NULL
GROUP BY utente.nome_utente
HAVING count(live.nome_canale) >= 2;