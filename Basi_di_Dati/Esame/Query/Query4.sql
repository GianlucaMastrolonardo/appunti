---Query 4
---Per ogni categoria, elencare il numero di video, la durata media (in minuti, AVG(TimeStampFineVideo-TimeStampInizioVideo)) in minuti) dei video
---e il numero di spettatori di quei video. Ordinare il risultato in ordine decrescente per numero video.
---Nell'esempio, nella categoria "Musica" sono presenti 908 video che hanno totalizzato un numero di 19043 spettatori. La durata media dei 908 video è stata di 7 minuti.
SELECT categoria.nome,
    count(*),
    sum(video.visualizzazioni),
    avg(contenuto_multimediale.durata)
FROM categoria
    JOIN contenuto_multimediale ON categoria.nome = contenuto_multimediale.categoria
    JOIN video on contenuto_multimediale.titolo = video.titolo
GROUP BY categoria.nome
ORDER BY count(*) DESC;