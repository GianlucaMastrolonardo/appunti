---Query 6
---Per ogni streamer diventato affiliate, si vuole conoscere il numero di video creati, il numero di spettatori (di quei video)
---e l'età media degli spettatori che hanno visualizzato i suoi video, ma che non li hanno votati.
---Restituire il risultato in ordine crescente per NumeroSpettatori.
---Nell'esempio, lo streamer Maria Neri (streamer che è diventato affiliate), ha creato 71 video visualizzati da 120 spettatori
---con età media di 24 anni che hanno visualizzato i suoi 71 video ma che non li hanno votati.
SELECT streamer.nome_streamer,
    count(video.nome_canale),
    sum(video.visualizzazioni) as SumVisualizzazioni,
    avg(
        EXTRACT (
            YEAR
            from CURRENT_DATE
        ) - EXTRACT (
            YEAR
            from utente.data_nascita
        )
    )
FROM utente,
    streamer
    JOIN video ON streamer.nome_streamer = video.nome_canale
WHERE streamer.affiliato = TRUE
    AND utente.nome_utente in (
        SELECT guardato.nome_spettatore
        FROM guardato
        WHERE streamer.nome_streamer = guardato.nome_canale
        EXCEPT
        SELECT voto.nome_spettatore
        FROM voto
        WHERE voto.nome_canale = streamer.nome_streamer
    )
GROUP BY streamer.nome_streamer
ORDER BY SumVisualizzazioni;