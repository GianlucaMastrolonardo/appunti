---Query 3
---Per ogni streamer, elencare i titolo del video con il relativo numero di visualizzazioni che ha ottenuto la media dei voti (secondo la scala likert [1-10]) massimo.
---Ordinare il risultato il ordine decrescente per media dei voti.
---Nell'esempio, il video "gita in montagna" dello streamer Andrea ha ricevuto 1884 visualizzazioni ottenendo il voto più alto (voto massimo tra tutti i suoi video) di 8.7
SELECT DISTINCT v1.nome_canale,
    v1.titolo,
    video.visualizzazioni,
    v2.media_punteggio
FROM voto v1
    JOIN (
        SELECT nome_canale,
            titolo,
            AVG(punteggio) AS media_punteggio
        FROM voto
        GROUP BY nome_canale,
            titolo
    ) v2 ON v1.nome_canale = v2.nome_canale
    AND v1.titolo = v2.titolo
    JOIN video ON v1.titolo = video.titolo
WHERE v1.punteggio > v2.media_punteggio
ORDER BY v2.media_punteggio DESC;