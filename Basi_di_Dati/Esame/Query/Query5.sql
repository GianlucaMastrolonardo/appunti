--- Query 5
---Per ogni streamer che ha creato almeno 2 video, si vuole conoscere la durata media dei suoi video
---(AVG(TimeStampFineVideo-TimeStampInizioVideo)) in minuti, numero di voti e numero totale di commenti.
---Ordinare il risultato in ordine decrescente per età dello streamer.
SELECT video.nome_canale,
    avg(contenuto_multimediale.durata) as DurataMedia,
    count(voto.punteggio) as NumeroVoti,
    count(commento.testo) as NumeroCommenti
FROM VIDEO
    JOIN contenuto_multimediale ON video.titolo = contenuto_multimediale.titolo
    AND video.nome_canale = contenuto_multimediale.nome_canale
    JOIN voto ON voto.nome_canale = video.nome_canale
    AND voto.titolo = video.titolo
    JOIN commento ON commento.nome_canale = video.nome_canale
    AND commento.titolo = video.titolo
GROUP BY video.nome_canale
HAVING count(video.nome_canale) >= 2;