---Query 1
---Per ciascun streamer, restituire il numero di live (passate e correnti) raggruppate per categoria.
---Riportare la durata media (AVG(TimeStampFineLivePassata-TimeStampInizioLivePassata)) in minuti delle live passate.
---Nell'esempio, lo streamer Mario Rossi ha 908 live passate, 3 live correnti. La durata media delle live passate è di 12 minuti.
SELECT streamer.nome_streamer,
    (
        streamer.numero_live_effettuate - (
            SELECT count(*)
            FROM live
            WHERE live.fine IS NULL
                AND streamer.nome_streamer = live.nome_canale
            GROUP BY live.nome_canale
        )
    ) as NumeroLivePassate,
    streamer.numero_live_effettuate - (
        streamer.numero_live_effettuate - (
            SELECT count(*)
            FROM live
            WHERE live.fine IS NULL
                AND streamer.nome_streamer = live.nome_canale
            GROUP BY live.nome_canale
        )
    ) as NumeroLiveInCorso,
    (
        SELECT EXTRACT (
                MINUTE
                FROM avg(live.fine - live.inizio)
            )
        FROM live
        WHERE live.fine IS NOT NULL
            AND live.nome_canale = streamer.nome_streamer
        GROUP BY live.nome_canale
    ) as DurataLivePassate
FROM streamer;