---Query 9
---Restituire nome, cognome e età (in anni) di followers appartenenti alle categorie dei fragili che NON hanno commentato video
--- di affiliate di età compresa tra [20<=x<=35] anni (estremi inclusi). Per ciascun followers fragile,
---calcolare l'età media dei membri affiliate (AVG(Affiliate.Età)). Ordinare il risultato per numero decrescente di commenti. 
---Svolgere la query con gli operatori in/not in.
---Nell'esempio, il follower Mario Neri di 33 anni - appartenente alla categoria dei fragili - ha scritto 98 commenti a contenuti multimediali
---accessibili di 120 membri affiliate di età compresa tra [20<=x<=35] di media 23 anni.
SELECT utente.nome_utente,
    (
        EXTRACT (
            YEAR
            FROM CURRENT_DATE
        ) - EXTRACT (
            YEAR
            FROM utente.data_nascita
        )
    ) as Età,
    (
        SELECT count(commento.nome_spettatore)
        FROM commento
        WHERE commento.nome_spettatore = utente.nome_utente
    ) as NumeroCommentiEffettuati
FROM (
        SELECT nome_utente
        FROM utente
        WHERE utente.fragile = TRUE
        EXCEPT(
                SELECT nome_utente
                FROM utente
                    JOIN commento ON utente.nome_utente = commento.nome_spettatore
                    JOIN seguito ON utente.nome_utente = seguito.nome_spettatore
                WHERE utente.fragile = TRUE
                    AND commento.nome_canale IN (
                        select streamer.nome_streamer
                        FROM streamer
                            JOIN utente on streamer.nome_streamer = utente.nome_utente
                        WHERE streamer.affiliato = TRUE
                            AND streamer.nome_streamer = seguito.nome_canale
                            AND (
                                (
                                    EXTRACT (
                                        YEAR
                                        FROM CURRENT_DATE
                                    ) - EXTRACT (
                                        YEAR
                                        FROM utente.data_nascita
                                    )
                                ) >= 20
                                AND (
                                    EXTRACT (
                                        YEAR
                                        FROM CURRENT_DATE
                                    ) - EXTRACT (
                                        YEAR
                                        FROM utente.data_nascita
                                    )
                                ) <= 35
                            )
                    )
            )
    ) UtentiFragiliNoCommentiAffiliate
    JOIN utente ON UtentiFragiliNoCommentiAffiliate.nome_utente = utente.nome_utente;