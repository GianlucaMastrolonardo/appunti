SELECT distinct s.sname
FROM s,
    sp,
    p
WHERE sp.qty >= 300
    AND p.pnum = sp.pnum
    AND s.snum = sp.snum
    AND (
        p.pname = 'Bolt'
        OR p.pname = 'Nut'
    )
ORDER BY s.sname;