SELECT distinct s.sname,
    s.status
FROM S
    JOIN SP ON s.snum = sp.snum
    JOIN P ON sp.pnum = p.pnum
WHERE p.weight < 14
    OR p.weight > 17
ORDER BY s.status DESC;