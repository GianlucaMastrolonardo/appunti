SELECT p.pname,
    p.color,
    p.weight,
    s.sname
FROM s,
    p
WHERE s.city = p.city;