SELECT pname
FROM p
WHERE color = 'Red'
    AND (
        weight < 17
        AND weight > 13
    );