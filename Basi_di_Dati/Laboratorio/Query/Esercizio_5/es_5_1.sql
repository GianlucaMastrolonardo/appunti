--Versione con IN
select distinct SNum
from SP
WHERE PNUM in (
        SELECT PNUM
        from P
        where City = 'London'
    );
--Versione con ANY
select distinct SNum
from SP
WHERE PNUM = any (
        SELECT PNUM
        from P
        where City = 'London'
    );