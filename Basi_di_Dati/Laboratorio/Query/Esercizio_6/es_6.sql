--Aumentare del 30% tutte le qty
update sp
set qty = qty * 1.3;
--Crea vista con quatità >= 600 da SP
CREATE view BigSP600 as
select *
from SP
where qty >= 600;
--Crea lista con quatità >= 750 dalla vista BigSP600
CREATE view BigSp750 as
select *
from BigSP600
where qty >= 750;