SELECT S.snum,
    S.status,
    sp.qty
FROM S
    left join SP ON S.snum = SP.snum
WHERE S.status > 20;