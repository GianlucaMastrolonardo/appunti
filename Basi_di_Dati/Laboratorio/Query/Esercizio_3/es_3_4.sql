SELECT sp1.snum,
    sp2.snum,
    sp2.pnum
FROM SP sp1
    JOIN SP sp2 ON sp1.pnum = sp2.pnum
WHERE sp1.snum < sp2.snum;