SELECT distinct p1.pnum,
    p2.pnum,
    p1.color
FROM P p1
    join p p2 on p1.color <> p2.color
    AND p1.city = p2.city
WHERE p1.pnum > p2.pnum;