SELECT distinct p.pnum,
    s.city
FROM p
    LEFT JOIN sp on p.pnum = sp.pnum
    JOIN s on sp.snum = s.snum
WHERE p.color = 'Green';