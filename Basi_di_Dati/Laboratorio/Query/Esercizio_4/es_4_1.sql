SELECT s.sname,
    sum(SP.qty)
FROM SP
    INNER JOIN P on SP.pnum = P.pnum
    INNER JOIN S on S.Snum = SP.Snum
WHERE p.color = 'Red'
GROUP BY s.sname;